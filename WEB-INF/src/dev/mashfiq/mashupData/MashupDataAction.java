package dev.mashfiq.mashupData;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.MasterAction;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Permissions;
import dev.mashfiq.util.RecordNavigation;
import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.SearchPanels;
import dev.mashfiq.util.SearchQueryBuilder;
import dev.mashfiq.util.Validations;

public class MashupDataAction extends MasterAction implements
		ModelDriven<MashupDataDTO>, Preparable {

	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(MashupDataAction.class.getName());
	MashupDataDTO dto = null;

	public String add() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			if (rimsUsersDTO
					.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_THREE) {
				if (dto != null) {
					dto.setCreatedBy(rimsUsersDTO.getId());
					ro = new MashupDataDAO().insert(dto);
					if (ro != null) {
						if (ro.getIsSuccessful()) {
							MashupDataRepository.getInstance(true);
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SUCCESSFULLY_ADDED);
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_DATA, dto);
							ro.setActionMessage(session);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.INVALID_REQUEST);
				}
			} else {
				result = PERMISSION_DENIED;
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For add: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String search() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		String condition = null;
		String[][] searchPanel = SearchPanels.MASHUP_DATA;
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			ro = new SearchQueryBuilder().getSearchQuery(request, session,
					searchPanel, MashupDataDAO.TABLE_NAME,
					rimsUsersDTO.getUsrID());
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					condition = ro.getData() + "";
					if (condition == null || condition.length() == 0) {
						condition = "";
					}
					count = new CommonDAO().getCount(MashupDataDAO.TABLE_NAME,
							MashupDataDAO.DEFAULT_KEY_COLUMN, condition, false);
					if (count > 0) {
						map = rn.getRecordNavigation(count);
						if (map != null && map.size() > 0) {
							condition += " ORDER BY id DESC LIMIT "
									+ map.get(RecordNavigation.START_INDEX)
									+ "," + map.get(RecordNavigation.LIMIT);
							ro = new MashupDataDAO().getMap(null, condition,
									null);
							if (ro != null) {
								if (ro.getIsSuccessful()) {
									session.setAttribute(
											ApplicationConstant.ACTION_DATA,
											ro.getData());
								} else {
									ro.setActionMessage(session);
								}
							} else {
								session.setAttribute(
										ApplicationConstant.ACTION_MESSAGE,
										ActionMessages.SYSTEM_ERROR);
							}
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SYSTEM_ERROR);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.NO_DATA_FOUND);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.SYSTEM_ERROR);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String get() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_TWO) {
			if (dto != null) {
				dto = MashupDataRepository.getInstance(false).getDTOById(
						dto.getId());
				if (dto != null) {
					new RimsUpdateLogHelper(rimsUsersDTO.getId(),
							MashupDataDAO.TABLE_NAME, dto.getId(),
							"Data Viewed", "get");
					result = INPUT;
					session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.INVALID_REQUEST);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For get: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String update() {
		long startTime = System.currentTimeMillis();
		result = INPUT;
		MashupDataDTO oldDTO = null;
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			if (rimsUsersDTO
					.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_FOUR) {
				if (dto != null) {
					oldDTO = MashupDataRepository.getInstance(false)
							.getDTOById(dto.getId());
					dto.setCreatedBy(rimsUsersDTO.getUsrID());
					ro = new MashupDataDAO().update(dto);
					if (ro != null) {
						if (ro.getIsSuccessful()) {
							result = SEARCH;
							MashupDataRepository.getInstance(true);
							new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
									MashupDataDAO.TABLE_NAME, dto.getId(),
									oldDTO, dto, "update");
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SUCCESSFULLY_UPDATED);
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_DATA, dto);
							ro.setActionMessage(session);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.INVALID_REQUEST);
				}
			} else {
				result = PERMISSION_DENIED;
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For update: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public MashupDataDTO getModel() {
		return dto;
	}

	public MashupDataDTO getDto() {
		return dto;
	}

	public void setDto(MashupDataDTO dto) {
		this.dto = dto;
	}

	public void prepare() throws Exception {
		dto = new MashupDataDTO();
	}

	public String searchRoles() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		String[][] searchPanel = SearchPanels.ROLES;
		String condition = null;
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			ro = new SearchQueryBuilder().getSearchQuery(request, session,
					searchPanel, MashupDataDAO.TABLE_NAME,
					rimsUsersDTO.getUsrID());
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					condition = ro.getData() + "";
					if (condition == null || condition.length() == 0) {
						condition = "";
					}
					condition += " AND field_type=4 ";
					count = new CommonDAO().getCount(MashupDataDAO.TABLE_NAME,
							MashupDataDAO.DEFAULT_KEY_COLUMN, condition, false);
					if (count > 0) {
						map = rn.getRecordNavigation(count);
						if (map != null && map.size() > 0) {
							condition += " ORDER BY id DESC LIMIT "
									+ map.get(RecordNavigation.START_INDEX)
									+ "," + map.get(RecordNavigation.LIMIT);
							ro = new MashupDataDAO().getMap(null, condition,
									null);
							if (ro != null) {
								if (ro.getIsSuccessful()) {
									session.setAttribute(
											ApplicationConstant.ACTION_DATA,
											ro.getData());
								} else {
									ro.setActionMessage(session);
								}
							} else {
								session.setAttribute(
										ApplicationConstant.ACTION_MESSAGE,
										ActionMessages.SYSTEM_ERROR);
							}
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SYSTEM_ERROR);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.NO_DATA_FOUND);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.SYSTEM_ERROR);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}
	
	public String goToSearch() {
		throw new UnsupportedOperationException();
	}

}
