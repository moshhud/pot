package dev.mashfiq.mashupData;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.ApplicationConstant;

public class MashupDataDTO extends CommonDTO {

	private int fieldType;
	private String keyValue;
	private String label;
	private String currentStatus;

	public String getCurrentStatus() {
		return v.checkDTO(currentStatus, ApplicationConstant.ACTIVE);
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public int getFieldType() {
		return fieldType;
	}

	public void setFieldType(int fieldType) {
		this.fieldType = fieldType;
	}

	public String getKeyValue() {
		return v.checkDTO(keyValue, "use id");
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public String getLabel() {
		return v.checkDTO(label);
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
