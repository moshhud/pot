package dev.mashfiq.mashupData;

import java.sql.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManager;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.CollectionHelper;
import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.Validations;

public class MashupDataDAO implements DAOInterface {

	private Logger logger = Logger.getLogger(MashupDataDAO.class.getName());
	public static final String TABLE_NAME = "mashup_data";
	public static final String DEFAULT_KEY_COLUMN = "id";
	private QueryHelper queryHelper = new QueryHelper();
	private MashupDataDTO dto;
	private CommonDAO commonDAO;
	private CollectionHelper ch;
	private LinkedHashMap<String, MashupDataDTO> data;
	private ReturnObject ro;

	public ReturnObject insert(Object obj) {
		ro = new ReturnObject();
		MashupDataDTO dto = null;
		String sql = null;
		if (validate(obj)) {
			dto = (MashupDataDTO) obj;
			if (dto != null && isExisting()) {
				sql = "INSERT INTO mashup_data (created_by,field_type,key_value,label,creation_time) VALUES ("
						+ dto.getCreatedBy()
						+ ","
						+ dto.getFieldType()
						+ ",'"
						+ queryHelper
								.getMysqlRealScapeString(dto.getKeyValue())
						+ "','"
						+ queryHelper.getMysqlRealScapeString(dto.getLabel())
						+ "'," + System.currentTimeMillis() + ")";
				if (commonDAO == null) {
					commonDAO = new CommonDAO();
				}
				ro = commonDAO.executeUpdate(sql, false);
			} else {
				ro.setActionMessage(ActionMessages.DUPLICATE_ENTRY);
			}
		} else {
			ro.setActionMessage(ActionMessages.VALIDATION_ERROR);
		}
		return ro;
	}

	@SuppressWarnings("unchecked")
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,String condition, String keyColumn) {
		ro = getMap(vals, condition, keyColumn);
		ArrayList<MashupDataDTO> data = null;
		LinkedHashMap<String, MashupDataDTO> result = null;
		if (ro != null && ro.getIsSuccessful()) {
			try {
				result = (LinkedHashMap<String, MashupDataDTO>) ro.getData();
				if (ch == null) {
					ch = new CollectionHelper();
				}
				if (ch.checkMap(result)) {
					data = new ArrayList<MashupDataDTO>(result.size());
					for (MashupDataDTO dto : result.values()) {
						data.add(dto);
					}
					if (ch.checkCollection(data)) {
						ro.setData(data);
					}
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		return ro;
	}

	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals, String condition,String keyColumn) {
		ro = new ReturnObject();
		MashupDataDTO dto = null;
		Connection connection = null;
		ResultSet rs = null;
		Statement stmt = null;
		String sql = null;
		DateAndTimeHelper dth;
		try {
			sql = "SELECT * FROM mashup_data WHERE id!=0 ";
			sql += queryHelper.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += condition;
			} else {
				sql += " ORDER BY label";
			}
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			data = new LinkedHashMap<String, MashupDataDTO>();
			keyColumn =queryHelper.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			dth = new DateAndTimeHelper();
			while (rs.next()) {
				dto = new MashupDataDTO();
				dto.setId(rs.getInt("id"));
				dto.setCreatedBy(rs.getInt("created_by"));
				dto.setCreationTime(dth.getDateFromLong(rs.getLong("creation_time")));
				dto.setCurrentStatus(rs.getString("current_status"));
				dto.setFieldType(rs.getInt("field_type"));
				dto.setKeyValue(rs.getString("key_value"));
				dto.setLabel(rs.getString("label"));
				data.put(rs.getString(keyColumn), dto);
			}
			if (ch == null) {
				ch = new CollectionHelper();
			}
			if (ch.checkMap(data)) {
				ro.setIsSuccessful(true);
				ro.setData(data);
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);			
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

	public ReturnObject update(Object obj) {
		ro = new ReturnObject();
		String sql = null;
		MashupDataDTO dto = null;
		if (validate(obj)) {
			dto = (MashupDataDTO) obj;
			// update the uniqe condition later
			// if (condition) {}
			if (dto != null) {
				sql = "UPDATE mashup_data SET field_type=" + dto.getFieldType()
						+ ",label='"
						+ queryHelper.getMysqlRealScapeString(dto.getLabel())
						+ "',key_value='" + queryHelper.getMysqlRealScapeString(dto.getKeyValue()) + "',current_status='" + queryHelper.getMysqlRealScapeString(dto.getCurrentStatus()) + "' WHERE id=" + dto.getId();
				if (commonDAO == null) {
					commonDAO = new CommonDAO();
				}
				ro = commonDAO.executeUpdate(sql, false);
			} else {
				ro.setActionMessage(ActionMessages.INVALID_DATA_FORMAT);
			}
		} else {
			ro.setActionMessage(ActionMessages.VALIDATION_ERROR);
		}
		return ro;
	}

	public boolean validate(Object obj) {
		boolean isValid = false;
		Validations v;
		if (obj != null && obj instanceof MashupDataDTO) {
			dto = (MashupDataDTO) obj;
			v = new Validations();
			if (dto != null && dto.getFieldType() > 0 && dto.getCreatedBy() > 0
					&& v.checkInput(dto.getKeyValue())
					&& v.checkInput(dto.getLabel())) {
				isValid = true;
			}
		}
		return isValid;
	}

	public boolean isExisting() {
		boolean status = false;
		String condition = null;
		if (dto != null) {
			commonDAO = new CommonDAO();
			condition = " AND label='"+ queryHelper.getMysqlRealScapeString(dto.getLabel())+ "' ";
			condition += " AND field_type=" + dto.getFieldType();
			if (commonDAO.getCount(TABLE_NAME, DEFAULT_KEY_COLUMN, condition, false) == 0) {
				status = true;
			}
		}
		return status;
	}

	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,String condition) {
		ArrayList<MashupDataDTO> list;
		ro = getMap(vals, condition, null);
		if (ro != null && ro.getIsSuccessful() && data != null && data.size() > 0) {
			list = new ArrayList<MashupDataDTO>(data.values());
			ro.setData(list);
		}
		return ro;
	}

}
