package dev.mashfiq.mashupData;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.CollectionHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.Validations;

public class MashupDataRepository {

	private static MashupDataRepository mashupDataRepository;
	private Logger logger = Logger.getLogger(MashupDataRepository.class.getName());
	private CollectionHelper ch = new CollectionHelper();
	private ArrayList<MashupDataDTO> list = null;
	public int DEPARTMENT = 2;
	public int DESIGNATION = 3;
	public int ROLE = 4;
	public int OFFICE_LOCATION = 5;
	public int EMPLOYMENT_STATUS = 6;
	public static final int SERVER_INFO_CURRENT_STATUS = 9;
	public static final int SERVER_CATEGORY = 43;
	public static final int DEPARTMENT_SALES = 52;
	public static final int ROLE_WEBADMIN = 57;
	public int GENDER = 71;
	public int MARITAL_STATUS = 67;
	public int PERMISSION_LEVEL = 79;
	public int CONTACT_CATEGORY = 89;
	public int ACTION_TAKEN = 93;
	public int FOLLOW_UP_ACTION = 104;
	public int FOLLOW_UP_ACTION_STATUS = 140;
	public int LEAD_TYPE = 113;
	public int LEAD_QUALITY = 114;
	public int LEAD_SOURCE = 115;
	public int DROP_DOWN_YES_NO = 145;
	public int HOSTED_CATEGORY = 148;
	public int CURRENT_STATUS = 151;
	public int PAYMENT_METHOD = 154;
	public int TYPE_OF_BUSINESS = 158;
	public int LANGUAGE_PREFERENCE = 166;
	public int RELATIONSHIP = 172;
	public int OFFICE_COUNTRY = 180;
	public int SERVER_GROUP = 186;
	public int YES_1_NO_0 = 189;
	public int INVOICE_DELETE_OPTIONS = 192;
	public int CURRENCY = 500;
	public int CURRENCY_DEFAULT = 501;
	public int PAYMENT_METHOD_BANK = 155;
	public int PAYMENT_METHOD_PAYPAL = 156;
	public int PAYMENT_METHOD_CASH = 157;
	public int DEPARTMENT_ACCOUNTS_AND_FINANCE = 187;
	public int UPLOADED_FILES_MODULE_TYPE = 201;
	public int UPLOADED_FILES_MODULE_TYPE_SUCCESS_STORIES = 202;
	public int UPLOADED_FILES_MODULE_TYPE_WOW_STORIES = 203;
	public int ALLOW_BALANCE_DISPLAY_DROPDOWN = 206;
	public int ROLE_ACCOUNT = 207;
	public int POST_TYPE = 239;
	public int POST_TYPE_COVER_STORY = 240;
	public int UPLOADED_FILES_MODULE_TYPE_COVER_STORY_ALBUM = 241;
	public int IS_FOR = 242;
	public int PC_DIALER_STATUS = 250;
	public int BYTE_SAVER_TYPE = 256;
	public int BYTE_SAVER_CATEGORY = 260;
	public int DATA_BACKUP_OPTIONS = 264;
	public int CURRENCY_TK = 200;
	public int CONFIRMATION_STATUS = 267;
	public int AREA_OF_INTEREST = 270;
	public int DROPDOWN_BLOCK_UNBLOCK = 277;
	public int SIGNALING_PROTOCOL = 280;
	public int COLO_NAMES = 284;
	public static int ROLE_ACCOUNTS_MANAGER = 296;
	public static int ROLE_OPERATION_MANAGER = 305;
	public int COMMITTED_ACTIONS = 308;
	public int EXISTING_SOLUTIONS = 318;
	public int EXISTING_CC = 322;
	public int INTERESTED_IN = 328;
	public int MEETING_TYPE = 340;
	public int WHATSAPP_MSG_TYPE = 344;
	public int INVOICE_SHIFTED_OPTIONS = 347;
	public int SIP_SWITCH_NAMES = 359;
	public int SUPPORTED_PLATFORMS = 366;
	public int INVOICE_TYPE = 372;
	public int REPORT_CATEGORY = 375;
	public int OSC_PRODUCT_TYPE = 379;
	public int RIMS_HOLIDAY_TYPE  = 381;
	public int RIMS_HOLIDAY_DURATION = 385;
	public int ALTERNATE_INTERFACE_STATUS = 405;
	public static final int MDPRODUCTLIST_PRODUCT_SUB_TYPE = 431;
	public static final int SIGNING_STATUS = 444;
	public static final int PROJECT_STATUS = 450;
	public static final int SECURITY_QUESTIONS = 459;
	public final int INVOICE_INITIAL_STATUS = 504;
	public final int INVOICE_INITIAL = 503;
	public final int PO_INITIAL_STATUS = 509;
	public final int PO_INITIAL = 508;

	@SuppressWarnings("unchecked")
	private MashupDataRepository() {
		ReturnObject ro = new MashupDataDAO().getArrayList(null, null, null);
		if (ro != null && ro.getIsSuccessful()) {
			try {
				list = (ArrayList<MashupDataDTO>) ro.getData();
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
	}

	public static synchronized MashupDataRepository getInstance(
			boolean forceReload) {
		if (forceReload || mashupDataRepository == null) {
			mashupDataRepository = new MashupDataRepository();
		}
		return mashupDataRepository;
	}

	public ArrayList<MashupDataDTO> getByFieldType(int fieldType, boolean onlyActive) {
		ArrayList<MashupDataDTO> data = null;
		Validations v = new Validations();
		if (ch.checkCollection(list) && fieldType > 0) {
			data = new ArrayList<MashupDataDTO>();
			for (MashupDataDTO dto : list) {
				if (dto.getFieldType() == fieldType) {
					if (v.checkEqualsIgnoreCase(dto.getCurrentStatus(), ApplicationConstant.ACTIVE) || onlyActive == false) {
						data.add(dto);
					}
				}
			}
		}
		return data;
	}

	public String getLabelById(int id) {
		String label = "N/A";
		if (ch.checkCollection(list) && id > 0) {
			for (MashupDataDTO dto : list) {
				if (dto.getId() == id) {
					label = dto.getLabel();
					break;
				}
			}
		}
		return new Validations().checkString(label, "N/A");
	}
	
	public String getKeyValueById(int id) {
		String keyValue = "N/A";
		if (ch.checkCollection(list) && id > 0) {
			for (MashupDataDTO dto : list) {
				if (dto.getId() == id) {
					keyValue = dto.getKeyValue();
					break;
				}
			}
		}
		return new Validations().checkString(keyValue, "N/A");
	}
	
	public String getLabelByKeyValue(String keyValue, int fieldType) {
		String label = "N/A";
		Validations v = new Validations();
		if (ch.checkCollection(list) && v.checkInput(keyValue)) {
			for (MashupDataDTO dto : list) {
				if (dto.getFieldType() == fieldType &&  v.checkEqualsIgnoreCase(dto.getKeyValue(), keyValue)) {
					label = dto.getLabel();
					break;
				}
			}
		}
		return v.checkString(label, "N/A");
	}
	
	public int getIdByKeyValue(String keyValue, int fieldType) {
		int id = 0;
		Validations v = new Validations();
		if (ch.checkCollection(list) && v.checkInput(keyValue)) {
			for (MashupDataDTO dto : list) {
				if (dto.getFieldType() == fieldType &&  v.checkEqualsIgnoreCase(dto.getKeyValue(), keyValue)) {
					id = dto.getId();
					break;
				}
			}
		}
		return id;
	}
	
	public MashupDataDTO getDTOById(int id) {
		MashupDataDTO dto = null;
		if (id > 0 && list != null && list.size() > 0) {
			for (MashupDataDTO v : list) {
				if (v != null && v.getId() == id) {
					dto = v;
					break;
				}
			}
		}
		return dto;
	}
	
	public int getFieldTypeById(int id) {
		int fieldType = 0;
		if (ch.checkCollection(list) && id > 0) {
			for (MashupDataDTO dto : list) {
				if (dto.getId() == id) {
					fieldType = dto.getFieldType();
					break;
				}
			}
		}
		return fieldType;
	}

}
