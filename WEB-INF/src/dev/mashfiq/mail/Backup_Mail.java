package dev.mashfiq.mail;

import java.io.ByteArrayOutputStream;

import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

import dev.mashfiq.util.Validations;

public class Backup_Mail extends Thread {

	Logger logger = Logger.getLogger(Mail.class.getName());
	String from;
	String toList;
	String ccList;
	String host = "mail.revesoft.com";
	String port = "2525";
	String subject;
	String message;
	String bcc;
	ByteArrayOutputStream baos;
	String fileName;
	String filetype;
	
	public Backup_Mail(String from, String toList, String ccList, String subject,
			String message, String bcc,String fileName,String fileType, ByteArrayOutputStream baos) {
		Validations v = new Validations();
		if (v.checkInput(toList) && v.checkInput(subject)
				&& v.checkInput(message)) {
			if (ccList == null || ccList.length() == 0) {
				ccList = "";
			}
			this.from = from;
			this.toList = toList;
			this.ccList = ccList;
			this.bcc = bcc;
			this.fileName = fileName;
			this.filetype = fileType;
			this.subject = subject;
			this.message = message;
			this.baos = baos;
			start();
		} else {
			logger.debug("Invalid Request Format");
		}
	}

	public Backup_Mail(String from, String toList, String ccList, String subject,
			String message, String bcc, ByteArrayOutputStream baos) {
		Validations v = new Validations();
		if (v.checkInput(toList) && v.checkInput(subject)
				&& v.checkInput(message)) {
			if (ccList == null || ccList.length() == 0) {
				ccList = "";
			}
			this.from = from;
			this.toList = toList;
			this.ccList = ccList;
			this.bcc = bcc;
			this.subject = subject;
			this.message = message;
			this.baos = baos;
			start();
		} else {
			logger.debug("Invalid Request Format");
		}
	}

	public void run() {
		try {
		StringTokenizer stk = new StringTokenizer(toList, ",;");
		String to[] = new String[stk.countTokens()];
		String bccList[] = null;
		String cc[] = null;
		Properties props = null;
		Session session = null;
		Message msg = null;
		InternetAddress[] address = null;
		MimeBodyPart mimeBodyPart = null;
		DataSource source = null;
		Authenticator authenticator = null;
		Multipart multipart = null;
		for (int i = 0; i < to.length; i++) {
			to[i] = stk.nextToken().trim();
		}
		stk = new StringTokenizer(ccList, ",;");
		cc = new String[stk.countTokens()];
		for (int i = 0; i < cc.length; i++) {
			cc[i] = stk.nextToken().trim();

		}
		stk = new StringTokenizer(bcc, ",;");
		bccList = new String[stk.countTokens()];
		for (int i = 0; i < bccList.length; i++) {
			bccList[i] = stk.nextToken().trim();
		}

		props = new Properties();
		authenticator = new Authenticator();
		props.setProperty("mail.smtp.submitter", authenticator
				.getPasswordAuthentication().getUserName());
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.host", host);
		props.setProperty("mail.smtp.port", port);

		session = Session.getInstance(props, authenticator);

		session.setDebug(false);

			msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			address = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++) {
				address[i] = new InternetAddress(to[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, address);

			address = new InternetAddress[cc.length];
			for (int i = 0; i < cc.length; i++) {
				address[i] = new InternetAddress(cc[i]);
			}
			msg.setRecipients(Message.RecipientType.CC, address);

			address = new InternetAddress[bccList.length];
			for (int i = 0; i < bccList.length; i++) {
				address[i] = new InternetAddress(bccList[i]);
			}
			msg.setRecipients(Message.RecipientType.BCC, address);

			msg.setSubject(subject);
			msg.setSentDate(new Date());
			String content = new String(message);
			msg.setContent(content, "text/html");
			if (baos != null) {
				mimeBodyPart = new MimeBodyPart();
				source = new ByteArrayDataSource(baos.toByteArray(),
						"application/pdf");
				mimeBodyPart.setDataHandler(new DataHandler(source));
				mimeBodyPart.setFileName("test.pdf");
				if(filetype != null && filetype.length() > 0){
					source = new ByteArrayDataSource(baos.toByteArray(),filetype);
				}
				if(fileName != null && fileName.length() > 0){
					mimeBodyPart.setFileName(fileName);
				}
				multipart = new MimeMultipart();
				multipart.addBodyPart(mimeBodyPart);
				msg.setContent(multipart);
			}
			Transport.send(msg);
			Thread.sleep(500);
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (AddressException e) {
			logger.fatal("AddressException", e);
		} catch (MessagingException e) {
			logger.fatal("MessagingException", e);
		} catch (InterruptedException e) {
			logger.fatal("InterruptedException", e);
		}
	}

}
