package dev.mashfiq.mail;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.revesoft.po.revesoft.users.RimsUsersDTO;

import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.Validations;

public class MailService {

	Logger logger = Logger.getLogger(MailService.class.getName());

	public void sendErrorMail(String message) {
		String from = "moshhud@revesoft.com";
		String subject = "RIMS V4: Error";
		String to = "moshhud@revesoft.com";
		new Mail(from, to, null, subject, message, null, null);
	}

	public void sendMail(String subject, String message) {
		String from = "moshhud@revesoft.com";
		String to = "moshhud@revesoft.com";
		new Mail(from, to, null, subject, message, null, null);
	}


	public void sendEmployeeProfileMail(RimsUsersDTO dto) {
		String from = "moshhud@revesoft.com";
		String to = dto.getUsrEmail(); 
		String ccList = ""; 
		String subject = "PO Tracker: Profile Details";
		String message = "Dear " + dto.getUsrName() + ",<br/>Welcome to PO Tracker. Find your login credentials below.";
		message += "<table>" + "<tr><td>Name</td><td>" + dto.getUsrName()
				+ "</td></tr>" + "<tr><td>Email</td><td>" + dto.getUsrEmail()
				+ "</td></tr>" + "<tr><td>Password</td><td>" + dto.getUsrPassword()
				/*+ "</td></tr>" + "<tr><td>Security Token</td><td>"*/
				+ "" + "</td></tr>" + "</table>";
		message += "<br/><br/>Regards,<br/>Team REVE Systems";
		new Mail(from, to, ccList, subject, message, "moshhud@revesoft.com",null);
	}

	public boolean sendMail(MailDTO dto, int empProId, String tableName,
			int tableId) {
		boolean status = false;
		String bcc = "moshhud@revesoft.com";
		Validations v = new Validations();
		if (dto != null && v.checkEmail(dto.getTo()) && v.checkEmail(dto.getFrom())
		&& v.checkInput(dto.getSubject()) && v.checkInput(dto.getContent())) {
			
			if (dto.getBcc() != null && dto.getBcc().length() > 0) {
				bcc += "," + dto.getBcc();
			}
			new Mail(dto.getFrom(), dto.getTo(), dto.getCc(), dto.getSubject(),dto.getContent(), bcc, null);
			new RimsUpdateLogHelper(empProId, tableName, tableId, "From: "
					+ dto.getFrom() + ", To: " + dto.getTo() + ", Cc: "
					+ dto.getCc() + ", Bcc " + dto.getBcc() + ", Subject "
					+ dto.getSubject() + ", Content: " + dto.getContent(),
					"send mail");
			status = true;
		}
		return status;
	}




}
