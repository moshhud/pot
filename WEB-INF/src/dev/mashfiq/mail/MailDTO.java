package dev.mashfiq.mail;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.Validations;

public class MailDTO extends CommonDTO {
	private String to;
	private String from;
	private String cc;
	private String bcc;
	private String subject;
	private String content;
	private Validations v = new Validations();

	public String getTo() {
		return v.checkDTO(to, "");
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getFrom() {
		return v.checkDTO(from, "");
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getCc() {
		return v.checkDTO(cc, "");
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getSubject() {
		return v.checkDTO(subject, "");
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		content = v.checkDTO(content, "");
		content = content.replaceAll("\\r?\\n", "<br/>");
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getBcc() {
		return v.checkDTO(bcc, "");
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

}