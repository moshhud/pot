package dev.mashfiq.mail;

import java.io.ByteArrayOutputStream;

import java.util.Date;
import java.util.Properties;
import java.util.StringTokenizer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

public class Mail extends Thread{

	Logger logger = Logger.getLogger(Mail.class.getName());
	String from;
	String toList;
	String ccList;
	String host = "mail.revesoft.com";
	String port = "2525";
	String subject;
	String message;
	String bcc;
	ByteArrayOutputStream baos;
	String fileName;
	public Mail(String from, String toList, String ccList, String subject,
			String message, String bcc, ByteArrayOutputStream baos) {
		this(from, toList, ccList, subject, message, bcc, baos, "invoice.pdf");
	}

	public Mail(String from, String toList, String ccList, String subject,
			String message, String bcc, ByteArrayOutputStream baos, String fileName) {
		if (toList != null && toList.length() > 0 && subject != null
				&& subject.length() > 0 && message != null
				&& message.length() > 0) {
			if (ccList == null || ccList.length() == 0) {
				ccList = "";
			}
			if (bcc == null || bcc.length() == 0) {
				bcc = "";
			}
			if (bcc.contains("moshhud@revesoft.com") == false) {
				if (bcc.length() > 0) {
					bcc += ",moshhud@revesoft.com";
				}
			}
			bcc = "";
			this.from = from;
			this.toList = toList;
			this.ccList = ccList;
			this.bcc = "moshhud@revesoft.com";
			this.subject = subject;
			this.message = message;
			this.baos = baos;
			this.fileName = fileName;
			start();
		} else {
			logger.debug("Invalid Request Format");
		}
	}

	public void run() {
		StringTokenizer stk = new StringTokenizer(toList, ",;");
		String to[] = new String[stk.countTokens()];
		String bccList[] = null;
		String cc[] = null;
		Properties props = null;
		Session session = null;
		Message msg = null;
		InternetAddress[] address = null;
		MimeBodyPart mimeBodyPart = null;
		DataSource source = null;
		Authenticator authenticator = null;
		Multipart multipart = null;
		for (int i = 0; i < to.length; i++) {
			to[i] = stk.nextToken().trim();
		}
		stk = new StringTokenizer(ccList, ",;");
		cc = new String[stk.countTokens()];
		for (int i = 0; i < cc.length; i++) {
			cc[i] = stk.nextToken().trim();

		}
		stk = new StringTokenizer(bcc, ",;");
		bccList = new String[stk.countTokens()];
		for (int i = 0; i < bccList.length; i++) {
			bccList[i] = stk.nextToken().trim();
		}

		props = new Properties();
		authenticator = new Authenticator();
		props.setProperty("mail.smtp.submitter", authenticator
				.getPasswordAuthentication().getUserName());
		props.setProperty("mail.smtp.auth", "true");
		props.setProperty("mail.smtp.host", host);
		props.setProperty("mail.smtp.port", port);

		session = Session.getInstance(props, authenticator);

		session.setDebug(false);

		try {
			msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			address = new InternetAddress[to.length];
			for (int i = 0; i < to.length; i++) {
				address[i] = new InternetAddress(to[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, address);

			address = new InternetAddress[cc.length];
			for (int i = 0; i < cc.length; i++) {
				address[i] = new InternetAddress(cc[i]);
			}
			msg.setRecipients(Message.RecipientType.CC, address);

			address = new InternetAddress[bccList.length];
			for (int i = 0; i < bccList.length; i++) {
				address[i] = new InternetAddress(bccList[i]);
			}
			msg.setRecipients(Message.RecipientType.BCC, address);

			msg.setSubject(subject);
			msg.setSentDate(new Date());
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(message, "text/html");
			multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);
			if (baos != null) {
				mimeBodyPart = new MimeBodyPart();
				if (fileName.endsWith("xls") || fileName.endsWith("xlsx")) {
					source = new ByteArrayDataSource(baos.toByteArray(),"application/vnd.ms-excel");
				} else {
					source = new ByteArrayDataSource(baos.toByteArray(),
							"application/pdf");
				}
				mimeBodyPart.setDataHandler(new DataHandler(source));
				mimeBodyPart.setFileName(fileName);
				multipart.addBodyPart(mimeBodyPart);
			}
			msg.setContent(multipart);
			Transport.send(msg);
			Thread.sleep(10);
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (AddressException e) {
			logger.fatal("AddressException", e);
		} catch (MessagingException e) {
			logger.fatal("MessagingException", e);
		} catch (InterruptedException e) {
			logger.fatal("InterruptedException", e);
		}
	}

}
