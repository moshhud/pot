package dev.mashfiq.mail;

import java.util.HashMap;

import dev.mashfiq.util.Validations;

public class MailCCMap {

	private static HashMap<String, String> ccMap, fromMap;
	private static Validations v = new Validations();

	public HashMap<String, String> getCcMap() {
		return ccMap;
	}

	private static void populate() {
		ccMap = new HashMap<String, String>(8);
		ccMap.put("ME-BD",
				"shahin.rahman@revesoft.com;kader@revesoft.com,azad@revesoft.com");
		ccMap.put("ME-India",
				"siddharth@revesoft.com;barun@revesoft.com;sangeeta@revesoft.com");
		ccMap.put("Africa", "ismail@revesoft.com;khair@revesoft.com");
		ccMap.put("MEA (Africa)", "charbel@revesoft.com");
		ccMap.put("Asia Pacific", "sajed@revesoft.com");
		ccMap.put("Europe", "quazi.limon@revesoft.com");
		ccMap.put("America", "mehmud@revesoft.com");
		ccMap.put("ME-Pakistan", "sajed@revesoft.com;mahwish@revesoft.com;arsalan@revesoft.com");

		fromMap = new HashMap<String, String>(7);
		fromMap.put("ME-BD", "invoice.mebd@revesoft.com");
		fromMap.put("ME-India", "invoice.meindia@revesoft.com");
		fromMap.put("Africa", "invoice@revesoft.com");
		fromMap.put("MEA (Africa)", "invoice@revesoft.com");
		fromMap.put("Asia Pacific", "invoice@revesoft.com");
		fromMap.put("Europe", "invoice@revesoft.com");
		fromMap.put("America", "invoice@revesoft.com");
		fromMap.put("ME-Pakistan", "invoice@revesoft.com");
	}

	public static synchronized String getCCListFrom(String region) {
		String cc = "";
		if (ccMap == null || ccMap.size() == 0) {
			populate();
		}
		if (ccMap.containsKey(region)) {
			cc = ccMap.get(region);
		}
		return v.checkDTO(cc, getFrom(region));
	}

	public static synchronized String getFrom(String region) {
		String from = "";
		if (fromMap == null || fromMap.size() == 0) {
			populate();
		}
		if (fromMap.containsKey(region)) {
			from = fromMap.get(region);
		}
		return v.checkDTO(from, "sales@revesoft.com");
	}

}
