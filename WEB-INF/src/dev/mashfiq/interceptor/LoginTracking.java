package dev.mashfiq.interceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.QueryHelper;

public class LoginTracking extends Thread {

	private Logger logger = Logger.getLogger(LoginTracking.class.getName());
	private String sessionID;
	private String actionName;
	private String userIp;
	private int createdBy;
	private String browserInfo;
	private String tableName = "poLoginTracking";

	public LoginTracking(String sessionID, String actionName, String userIp,
			int createdBy, String browserInfo) {
		this.sessionID = sessionID;
		this.actionName = actionName;
		this.userIp = userIp;
		this.createdBy = createdBy;
		this.browserInfo = browserInfo;
		start();
	}

	@SuppressWarnings("resource")
	public void run() {
		Connection connection = null;
		Statement stmt = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			if (sessionID != null && sessionID.length() > 0
					&& actionName != null && actionName.length() > 0
					&& userIp != null && userIp.length() > 0 && createdBy > 0) {
				sql = "SELECT id,activity_track FROM " + tableName
						+ " WHERE session_id='"
						+ new QueryHelper().getMysqlRealScapeString(sessionID)
						+ "'";
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				if (rs.next()) {
					sql = "UPDATE " + tableName
							+ " SET activity_track =?,logout_time=? WHERE id=?";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, rs.getString("activity_track") + ">"
							+ actionName);
					pstmt.setLong(2, System.currentTimeMillis());
					pstmt.setInt(3, rs.getInt("id"));
				} else {
					sql = "INSERT INTO " + tableName + " (session_id,user_ip,"
							+ "browser_info,login_time,logout_time,"
							+ "activity_track,created_by) "
							+ "VALUES (?,?,?,?,?,?,?)";
					pstmt = connection.prepareStatement(sql);
					pstmt.setString(1, sessionID);
					pstmt.setString(2, userIp);
					pstmt.setString(3, browserInfo);
					pstmt.setLong(4, System.currentTimeMillis());
					pstmt.setLong(5, System.currentTimeMillis());
					pstmt.setString(6, actionName);
					pstmt.setInt(7, createdBy);
				}
				pstmt.executeUpdate();
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
	}

}
