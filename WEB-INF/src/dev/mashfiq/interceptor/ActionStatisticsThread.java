package dev.mashfiq.interceptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.util.FinalCleanUp;

public class ActionStatisticsThread extends Thread {

	private Logger logger = Logger.getLogger(ActionStatisticsThread.class
			.getName());
	private String actionName;
	
	public static final String TABLE_NAME = "poActionStatistices";

	public static ArrayList<String> actionList;

	public ActionStatisticsThread(String actionName) {
		this.actionName = actionName;
		start();
	}

	@SuppressWarnings("resource")
	public void run() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			if (actionList == null || actionList.size() == 0) {
				sql = "SELECT action_name FROM " + TABLE_NAME;
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				actionList = new ArrayList<String>();
				while (rs.next()) {
					actionList.add(rs.getString("action_name"));
				}
				if (rs != null) {
					rs.close();
				}
			}
			if (actionList != null && actionList.contains(actionName)) {
				sql = "UPDATE " + TABLE_NAME + " SET "
						+ "no_of_request=no_of_request+1 "
						+ "WHERE action_name=?";
			} else {
				sql = "INSERT INTO " + TABLE_NAME + " (action_name) VALUES (?)";
				if (actionList == null) {
					actionList = new ArrayList<String>();
				}
				actionList.add(actionName);
			}
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, actionName);
			pstmt.executeUpdate();
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (Exception e) {
			logger.fatal("Exception", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
	}

}
