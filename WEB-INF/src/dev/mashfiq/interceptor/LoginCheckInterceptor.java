package dev.mashfiq.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.dispatcher.mapper.ActionMapping;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.revesoft.po.revesoft.users.RimsUsersDTO;

import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Validations;

public class LoginCheckInterceptor implements Interceptor {

	private static final long serialVersionUID = 1L;
	private static final String ERROR = "error";
	private static final String LOGIN = "login";

	public void destroy() {
	}

	public void init() {
	}

	public String intercept(ActionInvocation actionInvocation) throws Exception {
		String resultName = ERROR;
		HttpServletRequest request = (HttpServletRequest) ActionContext
				.getContext().get(ServletActionContext.HTTP_REQUEST);
		HttpSession session = request.getSession();
		ActionMapping actionMapping = ServletActionContext.getActionMapping();
		RimsUsersDTO dto = null;
		String actionName = null;
		String ipAddress = null;
		Validations v;
		if (actionMapping != null && session != null) {
			actionName = actionMapping.getName();
			v = new Validations();
			if (v.checkInput(actionName)
					&& v.checkEqualsIgnoreCase(actionName, "login") == false
					&& v.checkEqualsIgnoreCase(actionName, "otp-validation") == false
							&& v.checkEqualsIgnoreCase(actionName, "sq-validation") == false
							&& v.checkEqualsIgnoreCase(actionName, "sq") == false
					&& v.checkEqualsIgnoreCase(actionName, "logout") == false) {
				dto = (RimsUsersDTO) session
						.getAttribute(ApplicationConstant.LOGIN_INFO);
				if (dto == null) {
					if (v.checkEqualsIgnoreCase(actionName, "otp")) {
						dto = (RimsUsersDTO) session
								.getAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
					}
					if (dto == null) {
						resultName = LOGIN;
					}
				}
				ipAddress = request.getRemoteAddr();
				if (ipAddress == null) {
					ipAddress = request.getRemoteHost();
				}
				/**
				 * the else block can be used to track user ip and if the ip
				 * changes then will be logout else { try { ipAddress =
				 * request.getRemoteAddr(); if (ipAddress == null) {
				 * ipAddress = request.getRemoteHost(); } if
				 * (dto.getLoginIp().equals(ipAddress) == false) {
				 * resultName = LOGIN; } } catch (RuntimeException e) {
				 * resultName = LOGIN; } } //
				 */
			}
			if (v.checkEqualsIgnoreCase(resultName, "login") == false) {
				new ActionStatisticsThread(actionName);
				if (dto != null && request != null) {
					new LoginTracking(session.getId(), actionName,
							ipAddress, dto.getId(),
							request.getHeader("User-Agent"));
				}
				resultName = actionInvocation.invoke();
			}
		}
		return resultName;
	}

}
