package dev.mashfiq.util;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.json.JSONObject;


import dev.mashfiq.common.CommonDAO;

public class SearchQueryBuilder {

	private static Logger logger = Logger.getLogger(SearchQueryBuilder.class.getName());

	public static String NO_DATA_FOUND = "NO_DATA_FOUND";
	private String tempQuery = "";
	private String prevTableName = null;
	private String prevReturnColumn = null;
	private boolean isDialerRegistration = false;
	private String relationColumnName = null;
	private QueryHelper qh = new QueryHelper();
	private Validations v = new Validations();

	@SuppressWarnings("unchecked")
	public ReturnObject getSearchQuery(HttpServletRequest request, HttpSession session, String[][] searchPanel, String tableName, int usrID) {
		ReturnObject ro = new ReturnObject();
		String query = null;
		String fieldName = null;
		String fieldValue = null;
		ArrayList<String> list = null;
		boolean isValid = true;
		String temp = null;
		boolean hasDue = true;
		String relationColumnName = null;
		String val[] = null;
		boolean isLast = false;
		
		if (searchPanel != null && searchPanel.length > 0) {
			query = "";
			for (int i = 0; i < searchPanel.length; i++) {
				val = searchPanel[i];
				fieldName = val[1];
				relationColumnName = val[7];
				
				if (v.checkInput(fieldName)) {
					fieldValue = request.getParameter(fieldName);
					if (fieldValue != null) {
						fieldValue = fieldValue.trim();
					}
					if (v.checkEqualsIgnoreCase(fieldName, "version")) {
						String tmp=fieldValue;
						if (v.checkInput(fieldValue)) {
							if(!fieldValue.equals("1")) {
								fieldValue=getDialerVersion(fieldValue);
							}							
							query += " AND "+ relationColumnName + " IN (" + fieldValue+ ") ";
						}
						fieldValue=tmp;
					}
					else if (v.checkEqualsIgnoreCase(fieldName, "version2")) {						 
						if (v.checkInput(fieldValue)) {							 
							query += " AND version IN ('" + fieldValue+ "') ";
						}
						 
					}
					
					
					else {
						if (i == searchPanel.length - 1) {
							isLast = true;
						}
						temp = getQuery(request, session, val, isLast);
						if (temp != null && temp.equalsIgnoreCase(NO_DATA_FOUND) == false) {
							query += temp;
							temp = null;
						} else {
							ro.setActionMessage(ActionMessages.NO_DATA_FOUND);
							isValid = false;
							break;
						}
					}
					if (v.checkInput(fieldValue)) {
						session.setAttribute(fieldName, fieldValue);
						new RimsUpdateLogHelper(usrID,tableName,0,"FieldName: "+ qh.getMysqlRealScapeString(fieldName)
							+ ", FieldVale: "+ qh.getMysqlRealScapeString(fieldValue) + ", Record Per Page: " + request.getParameter(ApplicationConstant.RECORD_PER_PAGE),"search");
					}
				}
			}
			if (isValid) {
				ro.setData(query);
				ro.setIsSuccessful(true);
			}
		}
		return ro;
	}
	
	public String getDialerVersion(String dv) {
		String version="0.0.0";
		
		Version vr = new Version();
		vr.parse(dv.getBytes(), 0, dv.getBytes().length);
		version=Integer.toString(vr.toInt());
		
		return version;
		
	}
	
	private String getQuery(HttpServletRequest request, HttpSession session, String[] val, boolean isLast) {
		String query = "";
		String fieldName = null;
		String fieldValue = null;
		String colName = null;
		String matchType = null;
		String tableType = null;
		String dbType = null;
		String additionalCondition = "";
		String additionalMasterCondition = "";
		ArrayList<String> list = null;
		JSONObject json = null;
		CommonDAO commonDAO = null;
		boolean databaseDialerRegistration = false;
		String condition = null;
		
		if (val != null && val.length >= 10) {
			fieldName = val[1];
			colName = val[2];
			matchType = val[4];
			tableType = val[5];
			dbType = val[8];
			additionalCondition = val[9];
			
			if (v.checkInput(fieldName) && v.checkInput(colName) && v.checkInput(matchType) && v.checkInput(tableType) && v.checkInput(dbType)) {
				fieldValue = request.getParameter(fieldName);
				if (fieldValue != null) {
					fieldValue = fieldValue.trim();
				}
				if (commonDAO == null) {
					commonDAO = new CommonDAO();
				}
				
				if (fieldValue != null && fieldValue.length() > 0) {
					session.setAttribute(fieldName, fieldValue);
					
					if (v.checkEqualsIgnoreCase(tableType,SearchPanels.IS_FOREIGN_TABLE)) {
						if (prevTableName != null && v.checkEqualsIgnoreCase(prevTableName, val[6]) == false) {
							
							list = getList(commonDAO, prevTableName, prevReturnColumn, tempQuery, isDialerRegistration);
							
							if (list != null && list.size() > 0) {
								query += " AND " + relationColumnName + " IN ("+ qh.getQueryStringFromArrayList(list)+ ") " + additionalMasterCondition;
								
								if (additionalCondition != null && additionalCondition.length() > 0 && additionalCondition.equalsIgnoreCase(SearchPanels.NA) == false) {
									query += additionalCondition;
								}
								
								tempQuery = qh.getQueryString(colName, fieldValue, matchType, additionalCondition);
								
								if (additionalCondition != null && additionalCondition.length() > 0 && additionalCondition.equalsIgnoreCase(SearchPanels.NA) == false) {
									tempQuery += additionalCondition;
								}
							}
							else {
								query = NO_DATA_FOUND;
							}
						} 
						else {
							tempQuery += qh.getQueryString(colName, fieldValue,matchType, additionalCondition);
							
							if (additionalCondition != null && additionalCondition.length() > 0 && additionalCondition.equalsIgnoreCase(SearchPanels.NA) == false && matchType.equalsIgnoreCase(SearchPanels.REPLACE) == false) {
								tempQuery += additionalCondition;
							}
						}
						if (v.checkEqualsIgnoreCase(dbType,SearchPanels.DIALERREGISTRATION)) {
							isDialerRegistration = true;
						} else {
							isDialerRegistration = false;
						}
						prevTableName = val[6];
						prevReturnColumn = val[3];
						relationColumnName = val[7];
					}
					else if (v.checkEqualsIgnoreCase(tableType,SearchPanels.NESTED_TABLE)) {
						try {
							if(val.length > 11) {
								json = new JSONObject(val[11]);
								if(json != null) {
									if (v.checkEqualsIgnoreCase(json.getString("dbType"),SearchPanels.DIALERREGISTRATION)) {
										databaseDialerRegistration = true;
									} else {
										databaseDialerRegistration = false;
									}
									condition = qh.getQueryString(json.getString("colName"), fieldValue, json.getString("matchType"), json.getString("additionalCondition"));
									list = commonDAO.getArrayList(json.getString("tableName"), json.getString("returnCol"), null, condition, databaseDialerRegistration);
									if(list != null && list.size() > 0) {
										condition = " AND " + val[SearchPanels.FOREIGN_TABLE_COLUMN_NAME] + " IN (" + qh.getQueryStringFromArrayList(list) + ") " ;
										if (v.checkEqualsIgnoreCase(val[SearchPanels.DATABASE_NAME],SearchPanels.DIALERREGISTRATION)) {
											databaseDialerRegistration = true;
										} 
										else {
											databaseDialerRegistration = false;
										}
										list = getList(commonDAO, val[SearchPanels.TABLE_NAME], val[SearchPanels.FOREIGN_TABLE_RETURN_COLUMN_NAME],
												condition, databaseDialerRegistration);
										if (list != null && list.size() > 0) {
											if(val.length > 10) {
												additionalMasterCondition = val[10];
												if(SearchPanels.NA.equalsIgnoreCase(additionalMasterCondition)) {
													additionalMasterCondition = "";
												}
											} else {
												additionalMasterCondition = "";
											}
											query += " AND " + val[SearchPanels.MASTER_TABLE_COLUMN_NAME] + " IN ("+ qh.getQueryStringFromArrayList(list) + ") " + additionalMasterCondition;
										} else {
											query = NO_DATA_FOUND;
										}
									}
								}
							}
						} catch (Exception e) {
							logger.fatal("Exception", e);
						}
					} 
					else {
						query += qh.getQueryString(colName, fieldValue, matchType, additionalCondition);
						if (additionalCondition != null && additionalCondition.length() > 0
						&& additionalCondition.equalsIgnoreCase(SearchPanels.NA) == false 
						&& matchType.equalsIgnoreCase(SearchPanels.REPLACE) == false) {
							query += additionalCondition;
						}
					}
				} 
				else {
					session.removeAttribute(fieldName);
				}
				if(val.length > 10) {
					additionalMasterCondition = val[10];
				} else {
					additionalMasterCondition = "";
				}
				if (isLast && v.checkInput(tempQuery)) {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
					list = getList(commonDAO, prevTableName, prevReturnColumn,tempQuery, isDialerRegistration);
					if (list != null && list.size() > 0) {
						query += " AND " + relationColumnName + " IN ("+ qh.getQueryStringFromArrayList(list) + ") " + additionalMasterCondition;
						tempQuery = "";
					} else {
						query = NO_DATA_FOUND;
					}
				}
				
			}
		}
		return query;
	}

	private ArrayList<String> getList(CommonDAO commonDAO, String tableName, String returnCol, String tempQuery, boolean isDialerRegistration) {
		return commonDAO.getArrayList(tableName, returnCol, null, tempQuery,isDialerRegistration);
	}

}
