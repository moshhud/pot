package dev.mashfiq.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManager;
import databasemanager.DatabaseManagerPinProtector;
import databasemanager.DatabaseManagerReseller;
import databasemanager.DatabaseManagerSuccessful;

public class FinalCleanUp {

	private static Logger logger = Logger.getLogger(FinalCleanUp.class.getName());

	public static void closeResource(Object obj) {
		try {
			if (obj != null) {
				if (obj instanceof ResultSet) {
					((ResultSet) obj).close();
				} 
				else if (obj instanceof Statement) {
					((Statement) obj).close();
				} 
				else if (obj instanceof PreparedStatement) {
					((PreparedStatement) obj).close();
				}
			}
		} 
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} 
		catch (SQLException e) {
			logger.fatal("NullPointerException", e);
		}
		catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} 
		catch (UnsupportedOperationException e) {
			logger.fatal("UnsupportedOperationException", e);
		}
	}

	public static void closeDatabaseConnection(Connection connection) {
		try {
			if (connection != null) {
				
				connection.setAutoCommit(true);
				if (connection instanceof DatabaseManager) {
					((DatabaseManager) connection).freeConnection(connection);
				} 
				else if (connection instanceof DatabaseManagerReseller) {
					((DatabaseManagerReseller) connection).freeConnection(connection);
				} 
				else if (connection instanceof DatabaseManagerSuccessful) {
					((DatabaseManagerSuccessful) connection).freeConnection(connection);
				}
				else if (connection instanceof DatabaseManagerPinProtector) {
					((DatabaseManagerPinProtector) connection).freeConnection(connection);
				} 
				else if (connection instanceof Connection) {
					connection.close();
				}
				
				//connection.close();
			}
		} 
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} 
		catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} 
		catch (UnsupportedOperationException e) {
			logger.fatal("UnsupportedOperationException", e);
		} 
		catch (SQLException e) {
			logger.fatal("UnsupportedOperationException", e);
		}
		catch (Exception e) {
			logger.fatal(e.toString());
		}
	}

}
