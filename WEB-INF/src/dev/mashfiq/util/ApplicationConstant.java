package dev.mashfiq.util;

import javax.servlet.http.HttpSession;

public class ApplicationConstant {
	
	
	public static final String RIMS_ADMIN_EMAIL = "moshhud@revesoft.com";
	public static final String SALES_EMAIL = "sales@revesoft.com";
	public static final String NOTIFICATIONS_URL = "http://180.234.212.118:2224/broadcast?cid=2";
	public static final String BASE_URL = null;
	public static final String BASEURL = "baseURL";
	public static final String TEMPORARY_LOGIN_INFO = "temporaryLoginInfo";
	public static final String LOGIN_INFO = "loginInfo";
	public static final String SESSION_ID = "sessionID";

	public static final float USD_TO_TAKA = 85.00F;
	public static final byte THREAD_JOIN_WAITING_PERIOD = 10;
	
	public static final String WARNING_MESSAGE = "alert alert-warning";
	public static final String ERROR_MESSAGE = "alert alert-danger";
	public static final String SUCCESS_MESSAGE = "alert alert-success";

	public static final String ERROR = "error";
	public static final String SUCCESS = "success";
	public static final String SEARCH = "search";
	public static final String LOGIN_ERROR = "loginError";
	public static final String PERMISSION_DENIED = "permissionDenied";
	public static final String ACTION_NAME = "actionName";
	public static final String ACTION_DATA = "actionData";
	public static final String ACTION_MESSAGE = "actionMessage";

	public static final String METHOD = "method";
	public static final String CAPTCHA = "captcha";
	public static final String ID = "id";
	public static final String TYPE = "type";
	public static final String NEXT = "next";
	public static final String PREVIOUS = "previous";
	public static final String CATEGORY = "category";
	public static final String RECORD_NAVIGATOR = "recordNavigator";
	public static final String RECORD_PER_PAGE = "rpp";
	public static final int PAGE_SIZE = 30;
	
	public static final String[] PERMITTED_FILE_EXTENSIONS = { ".pdf", ".jpg", ".jpeg", ".png",".txt", ".doc", ".docx", ".xls", ".xlsx", ".zip", ".msg", ".svg" };
	public static final String PO_FILES = "resources/upload-folder/google-docs/";
	public static final String PROJECT_PATH = "/usr/local/apache-tomcat-8.5.11/webapps/pot/";
	public static final String PROJECT_PATH_WIN = "D:/Software/Apache Software Foundation/Tomcat 8.5/webapps/pot/";
	public static final boolean KEEP_DEBUGGING = false;

	public static final String ACTIVE = "active";
	public static final String DEACTIVE = "deactive";
	
	public static final String apiKey = "CrPLoHU9xkYhit6y";
	public static final String secretKey = "34a7e5e9697cd58a760449d763d761812de4858c8fefb43627e6910da45f0b3a";
	public static final String hmacSign = "9e83a16b6dfe3cbb2c5fc97f4054b56988b4c24650c221811c1d2ca309ad4a45";
	
	public static synchronized String getBaseURL(HttpSession session) {
		String baseURL = "http://localhost:8080/tp-oss/";
		if (session!= null) {
			baseURL = session.getAttribute(BASEURL) + "";
			if (baseURL == null || baseURL.length() == 0) {
				baseURL = "http://localhost:8080/tp-oss/";
			}
		}
		return baseURL;
	}

}