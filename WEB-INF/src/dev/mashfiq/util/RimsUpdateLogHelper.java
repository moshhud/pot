package dev.mashfiq.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.mail.MailService;

public class RimsUpdateLogHelper extends Thread {

	private int usrID;
	private String tableName = null;
	private int tableID;
	private String changeDetails = null;
	private String module = null;
	public static final String TABLE_NAME = "poUpdateLog";

	private static Logger logger = Logger.getLogger(RimsUpdateLogHelper.class.getName());

	public RimsUpdateLogHelper(int usrID, String tableName, int tableID, Object oldData, Object newData, String module) {
		this.usrID = usrID;
		this.tableName = tableName;
		this.tableID = tableID;
		this.changeDetails = getChangedDetails(oldData, newData);
		this.module = module;
		oldData = null;
		newData = null;
		start();
	}
	
	public RimsUpdateLogHelper(int usrID, String tableName, int tableID, String changeDetails, String module) {
		this.usrID = usrID;
		this.tableName = tableName;
		this.tableID = tableID;
		this.changeDetails = changeDetails;
		this.module = module;
		start();
	}

	public void run() {
		Connection connection = null;
		PreparedStatement pstmt = null;
		String sql = null;
		try {
			if (changeDetails != null && changeDetails.length() > 0) {
				sql = "INSERT INTO "+TABLE_NAME+" (user_id,table_name,table_id,changed_values,module) VALUES (?,?,?,?,?)";
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
				pstmt = connection.prepareStatement(sql);
				pstmt.setInt(1, usrID);
				pstmt.setString(2, tableName);
				pstmt.setInt(3, tableID);
				pstmt.setString(4, changeDetails);
				pstmt.setString(5, module);
				if (pstmt.executeUpdate() <= 0) {
					logger.debug("Update failed. Please check");
					new MailService().sendErrorMail("Update Query: " + sql);
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} catch (UnsupportedOperationException e) {
			logger.fatal("UnsupportedOperationException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			//FinalCleanUp.closeDatabaseConnection(connection);	
			try{
				DatabaseManagerSuccessful.getInstance().freeConnection(connection);
				
			}
			catch(Exception eggg){}
		}
	}

	public static synchronized String getChangedDetails(Object oldData,Object newData) {
		String details = "";
		Method[] oldMethods = null;
		Method[] newMethods = null;
		String fieldName = null;
		Object oldVal = null;
		Object newVal = null;
		String methodName = null;
		if (oldData != null && newData != null) {
			oldMethods = oldData.getClass().getDeclaredMethods();
			newMethods = newData.getClass().getDeclaredMethods();
			if (oldMethods != null && oldMethods.length > 0 && newMethods != null && newMethods.length > 0) {
				for (int i = 0; i < oldMethods.length; i++) {
					methodName = oldMethods[i].getName();
					if (methodName.startsWith("get")) {
						try {
							fieldName = methodName.substring(3);
							for (int j = 0; j < newMethods.length; j++) {
								if (methodName.equalsIgnoreCase(newMethods[j].getName())) {
									oldVal = oldMethods[i].invoke(oldData);
									newVal = newMethods[j].invoke(newData);
									details += getDetails(fieldName, oldVal,newVal);
									break;
								}
							}
						} catch (IllegalArgumentException e) {
							logger.fatal("Exception", e);
						} catch (IllegalAccessException e) {
							logger.fatal("Exception", e);
						} catch (InvocationTargetException e) {
							logger.fatal("Exception", e);
						} catch (NullPointerException e) {
							logger.fatal("Exception", e);
						} catch (IndexOutOfBoundsException e) {
							logger.fatal("Exception", e);
						}
					}
				}
				if (details != null && details.length() > 1) {
					details = details.trim();
					if (details.endsWith(",")) {
						details = details.substring(0, details.length() - 1);
					}
				}
			}
		}
		return details;
	}

	public static synchronized boolean isArray(Object obj) {
		boolean isArray = false;
		try {
			if (obj != null && obj.getClass().isArray()) {
				isArray = true;
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (UnsupportedOperationException e) {
			logger.fatal("Exception", e);
		}
		return isArray;
	}

	private static synchronized String getDetails(String fieldName,Object oldVal, Object newVal) {
		String details = "";
		String[] oldValArray, newValArray;
		String oldValString = "";
		String newValString = "";
		try {
			if (fieldName != null && fieldName.length() > 0 && oldVal != null
					&& newVal != null) {
				if (isArray(oldVal) && isArray(newVal)) {
					oldValArray = (String[]) oldVal;
					newValArray = (String[]) newVal;
					for (String val : oldValArray) {
						oldValString += val + ",";
					}
					if (oldValString.endsWith(",")) {
						oldValString = oldValString.substring(0, oldValString.length() - 1);
					}
					for (String val : newValArray) {
						newValString += val + ",";
					}
					if (newValString.endsWith(",")) {
						newValString = newValString.substring(0, newValString.length() - 1);
					}
					details = checkVal(fieldName, oldValString, newValString);
				} else {
					details = checkVal(fieldName, oldVal + "", newVal + "");
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (IndexOutOfBoundsException e) {
			logger.fatal("Exception", e);
		} catch (UnsupportedOperationException e) {
			logger.fatal("Exception", e);
		} catch (IllegalStateException e) {
			logger.fatal("Exception", e);
		}
		return details;
	}

	private static String checkVal(String fieldName, String oldVal,String newVal) {
		String details = "";
		if (oldVal == null || oldVal.equals("null")) {
			if (newVal != null && newVal.length() > 0 && newVal.equals("null") == false) {
				details = fieldName + ": '" + oldVal + "' > '" + newVal + "', ";
			}
		} else {
			if(oldVal.contains("com.revesoft.rims.")) {
				return details;
			}
			if (newVal == null || newVal.equals("null")) {
				details = fieldName + ": '" + oldVal + "' > '" + newVal + "', ";
			} else if (oldVal.equalsIgnoreCase(newVal) == false) {
				details = fieldName + ": '" + oldVal + "' > '" + newVal + "', ";
			}
		}
		return details;
	}

}
