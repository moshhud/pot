package dev.mashfiq.util;

import java.text.DecimalFormat;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

public class NumericHelper {

	static Logger logger = Logger.getLogger(NumericHelper.class.getName());

	public static synchronized int parseInt(String val) {
		int data = 0;
		try {
			if (isNumber(val)) {
				data = Integer.parseInt(val);
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}

	public static synchronized long parseLong(String val) {
		long data = 0;
		try {
			if (isNumber(val)) {
				data = Long.parseLong(val);
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}

	public static synchronized float parseFloat(String val) {
		float data = 0;
		if (new Validations().isNumber(val)) {
			try {
				data = Float.parseFloat(val);
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return data;
	}

	public static synchronized String formatNumber(double number, String format) {
		if (new Validations().checkInput(format) == false) {
			format = "#.##";
		}
		return new DecimalFormat(format).format(number);
	}
	
	public static synchronized String formatNumber(double number) {
		return formatNumber(number, null);
	}
	
	public static synchronized boolean isNumber(String val) {
		boolean isNumber = false;
		if (val != null && val.length() > 0) {
			isNumber = NumberUtils.isNumber(val);
		}
		return isNumber;
	}
	
	public static long getLongFromIp(String ipAddress) {
		long result = 0;
		String[] atoms = null;
		if (new Validations().checkIp(ipAddress)) {
			atoms = ipAddress.split("\\.");
			for (int i = 3; i >= 0; i--) {
				result |= (Long.parseLong(atoms[3 - i]) << (i * 8));
			}
			result = result & 0xFFFFFFFF;
		}

		return result;
	}

	public static String getIpFromLong(long ip) {
		StringBuilder sb = new StringBuilder(15);
		if (ip > 0) {
			for (int i = 0; i < 4; i++) {
				sb.insert(0, Long.toString(ip & 0xff));
				if (i < 3) {
					sb.insert(0, '.');
				}
				ip >>= 8;
			}
		}
		return sb.toString();
	}
	
}
