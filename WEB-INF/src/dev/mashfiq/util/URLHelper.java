package dev.mashfiq.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class URLHelper {

	private Logger logger = Logger.getLogger(URLHelper.class.getName());

	public ReturnObject readWriteToURl(String urlString,
			HashMap<String, String> params, boolean doOutput) {
		ReturnObject ro = new ReturnObject();
		URLConnection connection = null;
		OutputStreamWriter osw = null;
		BufferedReader br = null;
		String line;
		try {
			if (urlString != null && urlString.length() > 0) {
				if (params != null && params.size() > 0) {
					if (urlString.contains("?") == false) {
						urlString += "?";
					}
					for (Entry<String, String> entry : params.entrySet()) {
						if (entry != null) {
							urlString +=  "&" + entry.getKey()
									+ "="
									+ URLEncoder.encode(entry.getValue(),
											"UTF-8");
						}
					}
					if (urlString.endsWith("&")) {
						urlString = urlString.substring(0,
								urlString.length() - 1);
					}
				}
				new RimsUpdateLogHelper(1, "URLHelper", 1, urlString, "readWriteToURl");
				connection = new URL(urlString).openConnection();
				connection.setDoOutput(doOutput);
				if (doOutput) {
					osw = new OutputStreamWriter(connection.getOutputStream());
					osw.write("");
					osw.close();
				}
				br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				urlString = "";
				while ((line = br.readLine()) != null) {
					urlString += line;
				}
				br.close();
				ro.setIsSuccessful(true);
				ro.setData(urlString);
			} else {
				ro.setMessage(new Messages().setErrorMessage("Invalid URL: "+ urlString));
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (MalformedURLException e) {
			logger.fatal("MalformedURLException", e);
		} catch (IOException e) {
			logger.fatal("IOException", e);
		} finally {
			try {
				if (osw != null) {
					osw.close();
				}
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				logger.fatal("Exception", e);
			}
		}
		return ro;
	}

	public static void main(String[] args) {
		HashMap<String, String> params = new HashMap<String, String>(3);
		params.put("title", "Whatsapp:");
		params.put("msg", "Test message from mashfiq");
		params.put("users", "1026552");
		params.put("url", "/quick-contact/search-quick-contact.html?phone=8801724205909");
		params.put("tag", "whatsApp#8801724205909");
		System.out.println(new URLHelper().readWriteToURl(ApplicationConstant.NOTIFICATIONS_URL, params, false).getData());
	}

}
