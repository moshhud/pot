package dev.mashfiq.util;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

public class DtoListParser<T1> {

	private Logger logger = Logger.getLogger(DtoListParser.class.getName());
	
	@SuppressWarnings("unchecked")
	public ArrayList<String> getFieldValueList(String className, LinkedHashMap<String,T1> dtoMap,
			String methodName, boolean isDistinct) {
		ReturnObject ro = new ReturnObject();
		Class<T1> classReference = null;
		ArrayList<String> valueList = null;
		Method[] methods = null;
		int methodIndex = 0;
		String value;
		if (className != null && className.length() > 0 && dtoMap != null
				&& dtoMap.size() > 0 && methodName != null
				&& methodName.length() > 0) {
			try {
				classReference = (Class<T1>) Class.forName(className);
				if (classReference != null) {
					methods = classReference.getDeclaredMethods();
					if (methods != null && methods.length > 0) {
						for (Method method : methods) {
							if (method != null && methods[methodIndex].getName() != null && methodName.equalsIgnoreCase(methods[methodIndex]
									.getName())) {
								break;
							}
							methodIndex++;
						}
						valueList = new ArrayList<String>();
						for (T1 dto : dtoMap.values()) {
							if (dto != null) {
								value = methods[methodIndex].invoke(dto)
										+ "";
								if (isDistinct) {
									if (valueList != null
											&& valueList.contains(value) == false) {
										valueList.add(value);
									}
								} else {
									valueList.add(value);
								}
							}
						}
						if (valueList != null && valueList.size() > 0) {
							ro.setIsSuccessful(true);
							ro.setData(valueList);
						}
					}
				}
			} catch (ClassNotFoundException e) {
				logger.fatal("ClassNotFoundException", e);
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			} catch (IllegalAccessException e) {
				logger.fatal("IllegalAccessException", e);
			} catch (InvocationTargetException e) {
				logger.fatal("InvocationTargetException", e);
			}
		}
		return valueList;
	}

	@SuppressWarnings("unchecked")
	public ReturnObject getFieldValues(String className, ArrayList<T1> dtoList,
			String methodName, boolean isDistinct) {
		ReturnObject ro = new ReturnObject();
		Class<T1> classReference = null;
		ArrayList<String> valueList = null;
		Method[] methods = null;
		int methodIndex = 0;
		String value;
		if (className != null && className.length() > 0 && dtoList != null
				&& dtoList.size() > 0 && methodName != null
				&& methodName.length() > 0) {
			try {
				classReference = (Class<T1>) Class.forName(className);
				if (classReference != null) {
					methods = classReference.getDeclaredMethods();
					if (methods != null && methods.length > 0) {
						for (Method method : methods) {
							if (method != null && methods[methodIndex].getName() != null && methodName.equalsIgnoreCase(methods[methodIndex]
									.getName())) {
								break;
							}
							methodIndex++;
						}
						valueList = new ArrayList<String>();
						for (T1 dto : dtoList) {
							if (dto != null) {
								value = methods[methodIndex].invoke(dto)
										+ "";
								if (isDistinct) {
									if (valueList != null
											&& valueList.contains(value) == false) {
										valueList.add(value);
									}
								} else {
									valueList.add(value);
								}
							}
						}
						if (valueList != null && valueList.size() > 0) {
							ro.setIsSuccessful(true);
							ro.setData(valueList);
						}
					}
				}
			} catch (ClassNotFoundException e) {
				logger.fatal("ClassNotFoundException", e);
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			} catch (IllegalAccessException e) {
				logger.fatal("IllegalAccessException", e);
			} catch (InvocationTargetException e) {
				logger.fatal("InvocationTargetException", e);
			}
		}
		return ro;
	}
	
}
