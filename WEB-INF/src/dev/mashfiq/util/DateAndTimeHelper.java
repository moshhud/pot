package dev.mashfiq.util;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

public class DateAndTimeHelper {

	private static Logger logger = Logger.getLogger(DateAndTimeHelper.class.getName());

	public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");
	public static final SimpleDateFormat SIMPLE_DATE_TIME_FORMAT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	public static final SimpleDateFormat SIMPLE_DATE_TIME_AMPM_FORMAT = new SimpleDateFormat("dd/MM/yyyy hh:mm a");
	public static final SimpleDateFormat SIMPLE_SQL_DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static final long ONE_HOUR_IN_MS = 60 * 60 * 1000;
	public static final long ONE_DAY_IN_MS = ONE_HOUR_IN_MS * 24;
	public static final long ONE_MONTH_IN_MS = ONE_DAY_IN_MS * 30;
	public static final long ONE_YEAR_IN_MS = ONE_DAY_IN_MS * 365;
	private static final ArrayList<String> MONTH_NAME_LIST;

	static {
		String[] months = new DateFormatSymbols().getMonths();
		MONTH_NAME_LIST = new ArrayList<String>();
		if (months != null && months.length > 0) {
			for (String month : months) {
				if (month != null && month.length() > 0) {
					MONTH_NAME_LIST.add(month);
				}
			}
		}
	}

	public String getDateFromMysqlTimestam(Date date) {
		String data = "N/A";
		if (date != null) {
			try {
				data = SIMPLE_DATE_FORMAT.format(date);
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return data;
	}
	
	public String getDateTimeFromMysqlTimestam(Date date) {
		String data = "N/A";
		if (date != null) {
			try {
				data = SIMPLE_DATE_TIME_FORMAT.format(date);
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return data;
	}
	
	public String getLocalDateTimeAMPMFromMysqlTimestam(Date date) {
		String data = "N/A";
		if (date != null) {
			try {
				Calendar cal = Calendar.getInstance();
				cal.setTime(date);
				cal.add(Calendar.HOUR_OF_DAY, 6);
				
				data = SIMPLE_DATE_TIME_AMPM_FORMAT.format(cal.getTime());
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return data;
	}


	public String getDateFromLong(long data) {
		String date = "N/A";
		try {
			if (data > 0) {
				date = SIMPLE_DATE_FORMAT.format(new Date(data));
				if (date == null || date.length() == 0 || date.equals("1970-01-01")) {
					date = "N/A";
				}
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return date;
	}

	public String getDateFromString(String data) {
		String date = "N/A";
		try {
			if (data != null && data.length() > 0) {
				date = getDateFromLong(Long.parseLong(data));
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return date;
	}

	public long getLongFromDate(String date) {
		long data = 0;
		Date d = null;
		try {
			if (date != null && date.length() > 0 && date.equalsIgnoreCase("N/A") == false) {
				d = SIMPLE_DATE_FORMAT.parse(date);
				data = d.getTime();
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (ParseException e) {
			logger.fatal("ParseException", e);
		}
		return data;
	}

	public String getPreviousMonthFromDate(String currentDate) {
		String previousMonthDate = null;
		if (currentDate != null && currentDate.length() > 0) {
			try {
				Calendar c = Calendar.getInstance();
				Date d = SIMPLE_DATE_FORMAT.parse(currentDate);
				c.setTime(d);
				c.add(Calendar.MONTH, -1);
				d = c.getTime();
				previousMonthDate = SIMPLE_DATE_FORMAT.format(d);
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			} catch (ParseException e) {
				logger.fatal("ParseException", e);
			}
		}
		return previousMonthDate;
	}

	public int getNumberOfDaysOfMonth(int year, int month) {
		int numberOfdays = 0;
		if (year > 1970 && month > 0 && month <= 12) {
			Calendar c = new GregorianCalendar(year, month, 1);
			numberOfdays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
		}
		return numberOfdays;
	}

	public String getStartOfMonthFromDate(long date) {
		return getStartOfMonthFromDate(getDateFromLong(date));
	}

	public long getStartOfMonthFromDateAsLong(long date) {
		String currentDate = getDateFromLong(date);
		String startDateOfMonth = getStartOfMonthFromDate(currentDate);
		return getLongFromDate(startDateOfMonth);
	}

	public long getLastOfMonthFromDateAsLong(long date) {
		String temp[] = null;
		int lastDay = 0;
		long result = 0;
		if (date > 0) {
			String currentDate = getDateFromLong(date);
			if (currentDate != null && currentDate.length() > 0) {
				temp = currentDate.split("/");
				lastDay = getNumberOfDaysOfMonth(Integer.parseInt(temp[2]),Integer.parseInt(temp[1]));
			}
		}
		result = getLongFromDate(lastDay + "/" + temp[1] + "/" + temp[2]);
		return result;
	}

	public String getStartOfMonthFromDate(String date) {
		String startOfMonth = null;
		String temp[] = null;
		try {
			if (date != null && date.length() > 0) {
				temp = date.split("/");
				if (temp != null && temp.length >= 3) {
					startOfMonth = "01/" + temp[1] + "/" + temp[2];
				}
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return startOfMonth;
	}

	public String getAlteredDate(long longDate, int noOfDays, boolean isPrevious) {
		String date = "N/A";
		try {
			if (longDate > 0 && noOfDays > 0) {
				longDate = getAlteredDateAsLong(longDate, noOfDays, isPrevious);
				date = SIMPLE_DATE_FORMAT.format(longDate);
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return date;
	}

	public String getAlteredDate(String currentDate, int noOfDays,boolean isPrevious) {
		return getAlteredDate(getLongFromDate(currentDate), noOfDays,isPrevious);
	}

	public long getAlteredDateAsLong(long date, int noOfDays, boolean isPrevious) {
		try {
			if (date > 0 && noOfDays > 0) {
				if (isPrevious) {
					date -= noOfDays * ONE_DAY_IN_MS;
				} else {
					date += noOfDays * ONE_DAY_IN_MS;
				}
			} else {
				date = 0;
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return date;
	}

	public long getAlteredDateAsLong(String date, int noOfDays,boolean isPrevious) {
		return getAlteredDateAsLong(getLongFromDate(date), noOfDays, isPrevious);
	}

	public ArrayList<String> getMonthNames() {
		return MONTH_NAME_LIST;
	}

	public String getMonthNameFromStringDate(String date) {
		String monthName = "N/A";
		String temp[] = null;
		int index = 0;
		if (date != null && date.length() > 0) {
			temp = date.split("/");
			if (temp != null && temp.length == 3) {
				index = NumericHelper.parseInt(temp[1]);
				if (index > 0 && index < 13) {
					monthName = MONTH_NAME_LIST.get(index - 1);
				}
			}
		}
		return monthName;
	}

	public String getMonthNameFromIndex(int i) {
		String monthName = "N/A";
		if (i > 12) {
			i -= 12;
		}
		if (i > 0 && i < 13) {
			monthName = MONTH_NAME_LIST.get(i - 1);
		}
		return monthName;
	}

	public String getMonthNameFromLongDate(long date) {
		String monthName = "N/A";
		if (date > 0) {
			monthName = getMonthNameFromStringDate(getDateFromLong(date));
		}
		return monthName;
	}

	public String getHostedServiceMonthNameFromStringDate(String date) {
		String monthName = "N/A";
		String temp[] = null;
		int index = 0;
		int checkDate = 0;
		if (date != null && date.length() > 0) {
			temp = date.split("/");
			if (temp != null && temp.length == 3) {
				checkDate = NumericHelper.parseInt(temp[0]);
				index = NumericHelper.parseInt(temp[1]);
				if (index > 0 && index < 13) {
					if (checkDate >= 20) {
						index++;
					}
					monthName = MONTH_NAME_LIST.get(index - 1);
				}
			}
		}
		return monthName;
	}

	public String getHostedServiceMonthNameFromLongDate(long date) {
		String monthName = "N/A";
		if (date > 0) {
			monthName = getHostedServiceMonthNameFromStringDate(getDateFromLong(date));
		}
		return monthName;
	}

	public long getLongStartOfDateFromLong(long date) {
		String str = null;
		if (date > 0) {
			str = getDateFromLong(date);
			if (str != null && str.length() > 0
					&& str.equalsIgnoreCase("N/A") == false) {
				date = getLongFromDate(str);
			}
		}
		return date;
	}

	public String getMysqlTimeStampFromDate(long date) {
		return getMysqlTimeStampFromDate(getDateFromLong(date));
	}

	public String getMysqlTimeStampFromDate(String date) {
		String data = null;
		String[] temp = null;
		if (date != null && date.length() > 0
				&& date.equalsIgnoreCase("N/A") == false) {
			temp = date.split("/");
			if (temp != null && temp.length == 3) {
				data = temp[2] + "-" + temp[1] + "-" + temp[0];
			}
		}
		return data;
	}

	public String getDateTimeFromLong(long date) {
		String dateTime = "";
		if (date > 0) {
			try {
				
				dateTime = SIMPLE_DATE_TIME_FORMAT.format(new Date(date));
				if (dateTime == null || dateTime.length() == 0 || dateTime.equals("01/01/1970")) {
					dateTime = "";
				}
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return dateTime;
	}
	
	public String getDateTimeAMPMFromLong(long date) {
		String dateTime = "";
		if (date > 0) {
			try {
				Calendar cal = Calendar.getInstance();
				cal.setTime(new Date(date));
				cal.add(Calendar.HOUR_OF_DAY, 6);
				
				dateTime = SIMPLE_DATE_TIME_AMPM_FORMAT.format(cal.getTime());
				if (dateTime == null || dateTime.length() == 0 || dateTime.equals("01/01/1970")) {
					dateTime = "";
				}
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return dateTime;
	}

	public String getMonthFormatDateFromString(long data) {
		String date = "N/A";
		try {
			if (data > 0) {
				date = SIMPLE_DATE_TIME_FORMAT.format(new Date(data));
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return date;
	}
	
	public String getMonthYearFromDate(long data) {
		String date = "N/A";
		String[] args = null;
		try {
			if (data > 0) {
				date = getDateFromLong(data);
				args = date.split("/");
				date = getMonthNameFromIndex(NumericHelper.parseInt(args[1])) + "-" + args[2];
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return date;
	}

	

	public long getDifferenceInDays(String date1, String date2,boolean doCeiling, boolean isAbsolute) {
		return getDifferenceInDays(getLongFromDate(date1),getLongFromDate(date2), doCeiling, isAbsolute);
	}

	public long getDifferenceInDays(long date1, long date2, boolean doCeiling,boolean isAbsolute) {
		long differenceInDays = -1;
		if (date1 > 0 && date2 > 0) {
			differenceInDays = (date1 - date2) / ONE_DAY_IN_MS;
			if (isAbsolute) {
				differenceInDays = Math.abs(differenceInDays);
			}
			if (doCeiling && ((date1 - date2) % ONE_DAY_IN_MS) != 0) {
				if (differenceInDays >= 0) {
					differenceInDays++;
				} else {
					differenceInDays--;
				}
			}
		}
		return differenceInDays;
	}
	
	
	public String getFullDateFromDate(String date, String regex){
		String arr[] = null;
		if(date != null && date.length() > 0) {
			if(regex == null || regex.length() == 0) {
				regex = "/";
			}
			arr = date.split(regex);
			if(arr != null && arr.length >=3) {
				date = arr[0] + " " + getMonthNameFromIndex(NumericHelper.parseInt(arr[1])) + ", " + arr[2];
			} else {
				date = "N/A";
			}
		} else {
			date = "N/A";
		}
		return date;
	 }
	
	public String getSqlDateFromLong(long time) {
		String sqlDate = null;
		Date date = null;
		if(time > 0) {
			date = new Date(time);
			sqlDate = SIMPLE_SQL_DATE_TIME_FORMAT.format(date);
		}
		return sqlDate;
	}
	
	public String getSpecificDayDate(int year, int month, int day, int week) {
		Calendar calendar;
		String date = "N/A";
		if(year >= 1970 && month >=1 && month <=12 && day >=1 && day <=7 && week >=1 && week <=5) {
			calendar = Calendar.getInstance();
			calendar.set(year, month - 1, 1);
			calendar.set(GregorianCalendar.DAY_OF_WEEK, day);
	    	calendar.set(GregorianCalendar.DAY_OF_WEEK_IN_MONTH, week);
	    	date = getDateFromLong(calendar.getTimeInMillis());
		}
		return date;
	}
	
	public String getSpecificDayDate(String date, int day, int week) {
		String arr[];
		if(date != null) {
			arr = date.split("/");
			if(arr != null && arr.length == 3) {
		    	date = getSpecificDayDate(NumericHelper.parseInt(arr[2]),  NumericHelper.parseInt(arr[1]), day, week);
			}  else {
				date = "N/A";
			}
		}  else {
			date = "N/A";
		}
		return date;
}
	
	public long getStartOfDate(long data) {
		return getLongFromDate(getDateFromLong(data));
	}
	
	public long getEndOfDate(long data) {
		return getLongFromDate(getAlteredDate(data, 1, false));
	}
	
	

}
