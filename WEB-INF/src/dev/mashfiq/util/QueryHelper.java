package dev.mashfiq.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import org.apache.log4j.Logger;

public class QueryHelper {

	private static Logger logger = Logger.getLogger(QueryHelper.class.getName());

	public String getMysqlRealScapeString(String str) {
		String data = null;
		if (str != null && str.length() > 0) {
			str = str.trim();
			str = str.replace("\\", "\\\\");
			str = str.replace("'", "\\'");
			str = str.replace("\0", "\\0");
			str = str.replace("\n", "\\n");
			str = str.replace("\r", "\\r");
			str = str.replace("\"", "\\\"");
			str = str.replace("\\x1a", "\\Z");
			data = str;
		}
		return data;
	}

	public String getQueryFromHashMap(HashMap<String, ArrayList<String>> vals) {
		String data = null;
		StringBuffer sql = new StringBuffer("");
		Set<String> keySet = null;
		ArrayList<String> list = null;
		
		if (vals != null && vals.size() > 0) {
			keySet = vals.keySet();
			
			if (keySet != null && keySet.size() > 0) {
				for (String key : keySet) {
					if (key != null && key.length() > 0) {
						list = vals.get(key);
						
						if (new CollectionHelper().checkCollection(list)) {
							sql.append(" AND "+ getMysqlRealScapeString(key)+ " IN ("+ getQueryStringFromArrayList(list, true) + ")");
						}
					}
				}
			}
		}
		if (sql != null) {
			data = sql.toString();
		}
		return data;
	}
	
	public String getQueryFromHashMapCondition(HashMap<String, ArrayList<String>> vals, String condition) {
		String data = getQueryFromHashMap(vals);
		if (condition != null && condition.length() > 0) {
			data += condition;
		}
		return data;
	}

	public String getUpdateQueryFromHashMap(HashMap<String, String> data) {
		String str = null;
		StringBuffer sql = new StringBuffer("");
		if (data != null && data.size() > 0) {
			for (String key : data.keySet()) {
				sql.append(getMysqlRealScapeString(key) + "='"
						+ getMysqlRealScapeString(data.get(key)) + "',");
			}
		}
		if (sql != null) {
			str = sql.toString();
		}
		if (str != null && str.endsWith(",")) {
			str = str.substring(0, str.length() - 1);
		}
		return str;
	}
	
	public String getQueryString(String colName,String value, String matchType) {
		return getQueryString(colName, value, matchType, null);
	}

	public String getQueryString(String colName,String value, String matchType, String additionalCondition) {
		String result = "";
		if (colName != null && colName.length() > 0 && value != null && value.length() > 0 && matchType != null && matchType.length() > 0) {
			if (matchType.equals(SearchPanels.LIKE)) {
				result += " AND " + getMysqlRealScapeString(colName)+ " LIKE '%" + getMysqlRealScapeString(value) + "%' ";
			} 
			else if (matchType.equals(SearchPanels.IN)) {
				result += " AND "+ getMysqlRealScapeString(colName)+ " IN ("+ getMysqlRealScapeStringFromArray(new StringHelper().getArrayFromString(value, ","),true) + ") ";
			} 
			else if (matchType.equals(SearchPanels.GREATER_THAN_DATE)) {
				result += " AND " + getMysqlRealScapeString(colName) + "> " + (new DateAndTimeHelper().getLongFromDate(value) - 100) + " ";
			} 
			else if (matchType.equals(SearchPanels.GREATER_THAN)) {
				result += " AND " + getMysqlRealScapeString(colName) + ">'" + getMysqlRealScapeString(value) + "' ";
			} 
			else if (matchType.equals(SearchPanels.GREATER_THAN_EQUAL)) {
				result += " AND " + getMysqlRealScapeString(colName) + ">='" + getMysqlRealScapeString(value) + "'";
			} 
			else if (matchType.equals(SearchPanels.LESS_THAN_DATE)) {
				result += " AND "+ getMysqlRealScapeString(colName)+ "<"+ new DateAndTimeHelper().getAlteredDateAsLong(value, 1,false) + " ";
			} 
			else if (matchType.equals(SearchPanels.LESS_THAN)) {
				result += " AND " + getMysqlRealScapeString(colName) + "<'"+ getMysqlRealScapeString(value) + "' ";
			} 
			else if (matchType.equals(SearchPanels.LESS_THAN_EQUAL)) {
				result += " AND " + getMysqlRealScapeString(colName) + "<='"+ getMysqlRealScapeString(value) + "' ";
			} 
			else if (matchType.equals(SearchPanels.GREATER_THAN_IP)) {
				result += " AND " + getMysqlRealScapeString(colName) + ">="+ NumericHelper.getLongFromIp(value);
			} 
			else if (matchType.equals(SearchPanels.LESS_THAN_IP)) {
				result += " AND " + getMysqlRealScapeString(colName) + "<="+ NumericHelper.getLongFromIp(value);
			} 
			else if (matchType.equals(SearchPanels.REPLACE)) {
				if (additionalCondition != null && additionalCondition.length() > 0) {
					result += additionalCondition.replace(SearchPanels.REPLACE, value);
					
				}
			} 
			else {
				result += " AND " + getMysqlRealScapeString(colName) + "='"+ getMysqlRealScapeString(value) + "' ";
			}
		}
		return result;
	}

	public String getMysqlRealScapeStringFromArray(String[] vals, boolean useInvertedComma) {
		String data = null;
		try {
			if (vals != null && vals.length > 0) {
				data = "";
				for (String val : vals) {
					if (val != null && val.length() > 0) {
						val = getMysqlRealScapeString(val.trim());
						if (useInvertedComma) {
							data += "'" + val + "',";
						} else {
							data += val + ",";
						}
					}
				}
				if (data != null && data.endsWith(",")) {
					data = data.substring(0, data.length() - 1);
				}
			}
		} 
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} 
		catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} 
		catch (UnsupportedOperationException e) {
			logger.fatal("UnsupportedOperationException", e);
		} 
		catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}
	
	public String getKeyColumn(String keyColumn, String DEFAULT_KEY_COLUMN) {
		if (keyColumn != null && keyColumn.length() > 0) {
			keyColumn = getMysqlRealScapeString(keyColumn);
		} else {
			keyColumn = DEFAULT_KEY_COLUMN;
		}
		return keyColumn;
	}
	
	public String getQueryStringFromArrayList(
			@SuppressWarnings("rawtypes") ArrayList vals) {
		return getQueryStringFromArrayList(vals, true);
	}

	public String getQueryStringFromArrayList(
			@SuppressWarnings("rawtypes") ArrayList vals,
			boolean useInvertedComma) {
		String data = null;
		try {
			if (vals != null && vals.size() > 0) {
				data = "";
				for (Object val : vals) {
					if (useInvertedComma) {
						data += "'" + val + "',";
					} else {
						data += val + ",";
					}
				}
				if (data != null && data.endsWith(",")) {
					data = data.substring(0, data.length() - 1);
				}
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}

}
