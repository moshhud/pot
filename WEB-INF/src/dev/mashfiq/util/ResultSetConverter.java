package dev.mashfiq.util;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class ResultSetConverter {
	public static JSONArray convert(ResultSet rs){
		
		
		JSONArray json = null;
		ResultSetMetaData rsmd;
		String column_name;
		JSONObject obj;
		int numColumns;
		try {
			if(rs != null) {
				rsmd = rs.getMetaData();
				numColumns = rsmd.getColumnCount();
				json = new JSONArray();
				while (rs.next()) {
					obj = new JSONObject();
					for (int i = 1; i < numColumns + 1; i++) {
						column_name = rsmd.getColumnName(i);

						if (rsmd.getColumnType(i) == java.sql.Types.ARRAY) {
							obj.put(column_name, rs.getArray(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BOOLEAN) {
							obj.put(column_name, rs.getBoolean(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BLOB) {
							obj.put(column_name, rs.getBlob(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.DOUBLE) {
							obj.put(column_name, rs.getDouble(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.FLOAT) {
							obj.put(column_name, rs.getFloat(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.NVARCHAR) {
							obj.put(column_name, rs.getNString(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.VARCHAR) {
							obj.put(column_name, rs.getString(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.BIGINT
							|| rsmd.getColumnType(i) == java.sql.Types.INTEGER
							|| rsmd.getColumnType(i) == java.sql.Types.TINYINT
							|| rsmd.getColumnType(i) == java.sql.Types.SMALLINT) {
							obj.put(column_name, rs.getInt(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.DATE) {
							obj.put(column_name, rs.getDate(column_name));
						} else if (rsmd.getColumnType(i) == java.sql.Types.TIMESTAMP) {
							obj.put(column_name, rs.getTimestamp(column_name));
						} else {
							obj.put(column_name, rs.getObject(column_name));
						}
					}
					json.put(obj);
				}
			}
		} catch (Exception e) {
			Logger.getLogger(ResultSetConverter.class.getName()).fatal("Exception", e);
		}
		return json;
	}

}