package dev.mashfiq.util;


import com.revesoft.po.revesoft.orderlist.OrderListDAO;
import com.revesoft.po.revesoft.users.RimsUsersDAO;
import com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDAO;
import com.revesoft.rims.revesoft.rimsModules.RimsModulesDAO;

import dev.mashfiq.mashupData.MashupDataDAO;

public class SearchPanels {

	/**
	 * Label or page to include, field name, column name, search type other
	 * parameters are added to check foreign table relation, table name, present
	 * table relation column name and database check;
	 */

	public static final String LIKE = "like";
	public static final String EQUAL = "equal";
	public static final String IN = "in";
	public static final String GREATER_THAN = "gt";
	public static final String GREATER_THAN_DATE = "gtd";
	public static final String GREATER_THAN_IP = "gtip";
	public static final String GREATER_THAN_EQUAL = "gte";
	public static final String LESS_THAN = "lt";
	public static final String LESS_THAN_DATE = "ltd";
	public static final String EQUAL_IP = "eip";
	public static final String LESS_THAN_IP = "ltip";
	public static final String LESS_THAN_EQUAL = "lte";
	public static final String REPLACE = "###";
	public static final String IS_FOREIGN_TABLE = "true";
	public static final String NOT_FOREIGN_TABLE = "false";
	public static final String NESTED_TABLE = "nested";
	public static final String DIALERREGISTRATION = "dr";
	public static final String REVESOFT = "rs";
	public static final String NA = "NA";
	public static final int LABEL = 0;
	public static final int  FIELD_NAME = 1;
	public static final int FOREIGN_TABLE_COLUMN_NAME = 2;
	public static final int FOREIGN_TABLE_RETURN_COLUMN_NAME = 3;
	public static final int MATCH_TYPE = 4;
	public static final int FOREIGN_TABLE_OR_NOT = 5;
	public static final int TABLE_NAME = 6;
	public static final int MASTER_TABLE_COLUMN_NAME = 7;
	public static final int DATABASE_NAME = 8;
	public static final int ADDITIONAL_CONDITION = 9;
	

	/**
	 * 1 = Label / include file name, 2 = Field Name, 3 = Foreign Table Column Name,
	 * 4 = Foreign Table return column name, 5 = Match Type, 6 = foreign table or not,
	 * 7 = table name, 8 = master table match column name, 9 = database name, 
	 * 10 = additional condition
	 */


	public final static String[][] ROLES = { 
		{ "Role Name", "label", "label","label", LIKE, NOT_FOREIGN_TABLE, MashupDataDAO.TABLE_NAME,"label", REVESOFT, NA }
	};

	public final static String[][] MASHUP_DATA = {
		{ "Label", "label", "label", "label", LIKE, NOT_FOREIGN_TABLE,MashupDataDAO.TABLE_NAME, "label", REVESOFT, NA },
		{ "field-type.jsp", "fieldType", "field_type", "field_type", EQUAL,NOT_FOREIGN_TABLE, MashupDataDAO.TABLE_NAME, "field_type",REVESOFT, NA },
		{ "Key Value", "keyValue", "key_value", "key_value", LIKE,NOT_FOREIGN_TABLE, MashupDataDAO.TABLE_NAME, "key_value",REVESOFT, NA } };
	
		
	public final static String[][] RIMS_USERS = {
		{ "user-name.jsp", "name", "usrName", "usrName", LIKE, NOT_FOREIGN_TABLE, RimsUsersDAO.TABLE_NAME, "usrName", REVESOFT, NA  },
		{ "Email", "email", "usrEmail", "usrEmail", LIKE, NOT_FOREIGN_TABLE, RimsUsersDAO.TABLE_NAME, "usrEmail", REVESOFT, NA  },
		{ "roles.jsp", "role", "usrRoleId", "usrRoleId", EQUAL, NOT_FOREIGN_TABLE, RimsUsersDAO.TABLE_NAME, "usrRoleId", REVESOFT, NA  },
		{ "Mobile No", "mobileNo", "office_phone", "office_phone", LIKE, NOT_FOREIGN_TABLE, RimsUsersDAO.TABLE_NAME, "office_phone", REVESOFT, NA  }
		};
	
	public static String[][] ORDERLIST = {
			{ "PO Number", "poNumber", "invcPONumber", "invcPONumber", IN, NOT_FOREIGN_TABLE, OrderListDAO.INVOICE_TABLE_NAME, "invcPONumber", REVESOFT, NA },
			{ "Customer Company", "clCompanyName", "clCompanyName", "clCompanyName", LIKE, NOT_FOREIGN_TABLE, OrderListDAO.CLIENT_TABLE_NAME, "clCompanyName", REVESOFT, NA },
			{ "Customer Email", "cEmail", "clEmail", "clEmail", LIKE, NOT_FOREIGN_TABLE, OrderListDAO.CLIENT_TABLE_NAME, "clEmail", REVESOFT, NA },
			{ "Customer Name", "clName", "clName", "clName", LIKE, NOT_FOREIGN_TABLE, OrderListDAO.CLIENT_TABLE_NAME, "clName", REVESOFT, NA },
			{ "Customer Phone", "clPhone", "clPhone", "clPhone", LIKE, NOT_FOREIGN_TABLE, OrderListDAO.CLIENT_TABLE_NAME, "clPhone", REVESOFT, NA },
			{ "start-date.jsp", "startDate", "invcDeliveryDate", "invcDeliveryDate", GREATER_THAN_DATE, NOT_FOREIGN_TABLE, OrderListDAO.INVOICE_TABLE_NAME, "invcDeliveryDate", REVESOFT, NA },
			{ "end-date.jsp", "endDate", "invcDeliveryDate", "invcDeliveryDate", LESS_THAN_DATE, NOT_FOREIGN_TABLE, OrderListDAO.INVOICE_TABLE_NAME, "invcDeliveryDate", REVESOFT, NA },

			
	};
	
	public static String[][] ORDERLIST_DASHBOARD = {
			
	};
	
	public final static String[][] MDPRODUCTLIST = {
			{ "Product ID", "productID", "plProductID", "plProductID", IN, NOT_FOREIGN_TABLE, MdProductListDAO.TABLE_NAME, "plProductID", DIALERREGISTRATION, NA  },
			/*{ "Product Name", "plProductName", "plProductName", "plProductName", LIKE, NOT_FOREIGN_TABLE, MdProductListDAO.TABLE_NAME, "plProductName", DIALERREGISTRATION, NA  },*/
			{ "product-name.jsp", "plProductName", "plProductName", "plProductName", LIKE, NOT_FOREIGN_TABLE, MdProductListDAO.TABLE_NAME, "plProductName", DIALERREGISTRATION, NA  },
			{ "Price", "price", "plPriceUSD","plPriceUSD", EQUAL, NOT_FOREIGN_TABLE, MdProductListDAO.TABLE_NAME, "plPriceUSD", DIALERREGISTRATION, NA },
			{ "product-status.jsp", "plCurrentStatus", "plCurrentStatus", "plCurrentStatus", EQUAL, NOT_FOREIGN_TABLE, MdProductListDAO.TABLE_NAME, "plCurrentStatus", DIALERREGISTRATION, NA  }};
		
			
    public static String[][] MODULES_SEARCH = {
    		{ "Module Name", "poModuleName", "module_name", "module_name", LIKE, NOT_FOREIGN_TABLE, RimsModulesDAO.TABLE_NAME, "module_name", REVESOFT, NA  },
			
	};
	
}
