package dev.mashfiq.util;

public class RecordNavigator {
	private int currentPageNo;
	private int totalRecords;
	private int pageSize = ApplicationConstant.PAGE_SIZE;
	private int totalPages;
	private String searchPanel[][];
	private String actionName;

	public RecordNavigator() {
		currentPageNo = 1;
		totalRecords = 0;
		totalPages = 1;
		searchPanel = null;
	}

	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getPageSize() {
		return pageSize;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	public String[][] getSearchPanel() {
		return searchPanel;
	}

	public void setSearchPanel(String[][] searchPanel) {
		this.searchPanel = searchPanel;
	}

	public String getActionName() {
		if (actionName == null || actionName.length() == 0) {
			actionName = "";
		}
		return actionName;
	}

	public void setActionName(String actionName) {
		this.actionName = actionName;
	}

}