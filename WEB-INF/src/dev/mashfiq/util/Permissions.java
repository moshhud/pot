package dev.mashfiq.util;

public class Permissions {

	public static final int LEVEL_ONE = 1;
	public static final int LEVEL_TWO = 2;
	public static final int LEVEL_THREE = 3;
	public static final int LEVEL_FOUR = 4;
	public static final int LEVEL_FIVE = 5;
	public static final int LEVEL_SIX = 6;

	public static final int REVECLIENTS = 1;
	public static final int MANAGERIAL_PERMISSION = 2;
	public static final int DEMO_ASSIGN_PERMISSION = 3;
	public static final int PORTFOLIO_PERMISSION = 4;
	public static final int RIMS_ADMIN = 5;
	public static final int MASHUP_DATA = 6;
	public static final int RIMS_MODULES = 7;
	public static final int PERMISSIONS = 8;
	public static final int ZONE_RESTRICTION = 9;
	public static final int ZONE_SHIFT = 10;
	public static final int EMAIL_CHANGE = 11;
	public static final int MASTER_CREATION = 12;
	public static final int ACCOUNT_MANAGER = 13;
	public static final int MASTER_SEARCH = 14;
	public static final int PROCESS_LEADS = 15;
	public static final int INVOICE_AND_PAYMENT = 16;
	public static final int HOSTED_SERVICE = 17;
	public static final int REPORTS = 18;
	public static final int VBREGISTRATION = 19;
	public static final int MDSWITCHINFO = 20;
	public static final int AVAILABILITY_SEARCH = 21;
	public static final int BLOCK_UNBLOCK = 22;
	public static final int ADVANCE_PRODUCT_DETAILS = 23;
	public static final int QUICK_CONTACT = 24;
	public static final int CALLBACKREGISTRATION = 25;
	public static final int RIMS_EVENT_CONTACTS = 26;
	public static final int SEND_MAIL = 27;
	public static final int MDINVOICE = 28;
	public static final int RIMS_HOSTED_INVOICE = 29;
	public static final int INVOICE_SHIFT = 30;
	public static final int RIMS_CUSTOMER_ACCOUNT_TRANSACTIONS = 31;
	public static final int INVOICE_DELETE = 32;
	public static final int RIMS_HOSTED_SERVICE = 33;
	public static final int MDPAYMENTINFO = 34;
	public static final int MDINVOICE_PDF = 35;
	public static final int RIMS_HOSTED_INVOICE_PDF = 36;
	public static final int RIMS_CUSTOMER_ACCOUNT = 37;
	public static final int RIMS_CUSTOMER_ACCOUNT_COMMENTS = 38;
	public static final int REPORT_PRODUCTWISE_SALES = 39;
	public static final int REPORT_RIMS_CUSTOMER_ACCOUNT_TRANSACTIONS = 40;
	public static final int REPORT_SALES_AND_COLLECTION = 41;
	public static final int LEAD_STATUS_UPDATE = 42;
	public static final int REPORT_ORDER_SALE_AND_COLLECTION = 43;
	public static final int MDSWITCHINFO_COMMENTS = 44;
	public static final int VBREGISTRATION_COMMENTS = 45;
	public static final int RIMS_COLO_SERVERS = 46;
	public static final int SERVER_INFO = 47;
	public static final int RECORD_PER_PAGE = 48;
	public static final int ISP_CONFIGURATION = 49;
	public static final int RIMS_HOSTED_PRODUCTS = 50;
	public static final int MDPRODUCTLIST = 51;
	public static final int VBREGISTRATION_SHIFT = 52;
	public static final int RIMS_USERS = 53;
	public static final int REPORT_HOSTED_ORDER_SALE_AND_COLLECTION = 54;
	public static final int SEND_WHATSAPP_MESSAGE = 55;
	public static final int RIMS_HOSTED_PAYMENT = 56;
	public static final int REPORT_DUE = 57;
	public static final int INSTALLMENT_MODIFICATION = 58;
	public static final int REPORT_MAIL_INVOICE = 59;
	public static final int REPORT_QUICK_CONTACT = 60;
	public static final int REPORT_LEAD_STATUS = 61;
	public static final int RIMS_CHAT = 62;
	public static final int REPORT_BANK_COLLECTION = 63;
	public static final int REPORT_LEAD_DETAILS = 64;
	public static final int WHATSAPP_MSG = 65;
	public static final int RIMS_HOLIDAY_LIST = 66;
	public static final int RIMS_ACTION_TAKEN_HISTORY= 67;
	public static final int RIMS_COLO_SERVER_IP_BLOCKS = 68;
	public static final int VBREGISTRATION_SALES_COMMENTS = 69;
	public static final int MDSWITCHINFO_SALES_COMMENTS = 70;
	public static final int MDSWITCHINFO_SHIFT = 71;
	public static final int RIMS_CAMPAIGN = 72;
	public static final int RIMS_CAMPAIGN_REPORTS = 73;
	public static final int REPORT_SERVER_REVENUE = 74;
	public static final int REPORT_DASHBOARD = 75;
	public static final int VBREPLICATIONINFO = 76;
	public static final int RIMS_SIGNING_DETAILS = 77;
	public static final int VBMOBILEMONEYAGENT = 78;
	public static final int RIMS_PROJECTS = 79;
	public static final int RIMS_NOTIFICATION = 80;
	public static final int COMMUNICATE = 81;
	public static final int COUNTRY = 82;
	public static final int SILVER_DIALER_UPDATE = 83;
	public static final int SWITCH_ADDITIONAL_MODULE = 84;//rims_modules_2: id
	public static final int PURCHASE_ORDER = 85;
	public static final int PURCHASE_ORDER_MODIFICATION = 86;
	public static final int DIALER_VALIDATION_GOLD = 87;
	

	

}
