package dev.mashfiq.util;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class FileUploadHelper {

	Logger logger = Logger.getLogger(FileUploadHelper.class.getName());

	public static final String STATUS = "status";
	public static final String ORIGINAL_FILE_NAME = "originalFileName";
	public static final String FILE_NAME = "fileName";
	public static final String FILE_UPLOADED = "uploaded";
	public static final String FILE_UPLOADED_FAILED = "uploadFailed";
	public static final String FILE_EXTENSION_EXCEPION = "fileExtenstionException";
	public static final String FILE_SIZE_EXCEPION = "fileSizeException";
	public static final String[] PERMITTED_FILE_EXTENTIONS = { ".png", ".gif",
			".jpg", ".jpeg", ".ppt", ".pptx", ".doc", ".docx", ".pdf", ".zip",
			".rar", ".txt", ".xls", ".xlsx", "image/jpeg" };
	public static final String RIMS_USER_FILES = "resources/upload-folder/rims-users-files/";
	public static final String RIMS_USER_SIGNATURE = "resources/upload-folder/signatures/";
	public static final String RIMS_TASKS_FILES = "resources/upload-folder/rims-tasks-files/";
	public static final String ISP_CONFIGURATION_FILES = "resources/upload-folder/isp-configuration/";
	public static final String RIMS_COLO_SERVER_IP_BLOCKS_FILES = "resources/upload-folder/rims-colo-server-ip-blocks/";
	public static final String RIMS_COLO_SERVER_FILES = "resources/upload-folder/rims-colo-server/";
	public static final long MAX_FILE_SIZE = 1 * 1024 * 1024 * 2;

	public HashMap<String, String> upload(File file, String fileName,
			String path) {
		return upload(file, fileName, path, ApplicationConstant.PERMITTED_FILE_EXTENSIONS,
				MAX_FILE_SIZE);
	}

	public HashMap<String, String> upload(File file, String fileName,
			String path, String[] permittedExtension, long maxFileSize) {
		HashMap<String, String> response = new HashMap<String, String>();
		ArrayList<String> extensions = null;
		String fileType = null;
		File destDir = null;
		String dynamicName = null;
		try {
			if (file != null && new Validations().checkInput(path)
					&& permittedExtension != null
					&& permittedExtension.length > 0 && maxFileSize > 0) {
				extensions = new ArrayList<String>(
						Arrays.asList(permittedExtension));
				response.put(STATUS, FILE_UPLOADED_FAILED);
				if (file.length() <= maxFileSize) {
					fileType = fileName.substring(
							fileName.lastIndexOf("."));
					if (new CollectionHelper().checkContains(extensions, fileType)) {
						dynamicName = System.currentTimeMillis() + "_"
								+ new StringHelper().getRandomString() + ""
								+ fileType;
						destDir = new File(path, dynamicName);
						FileUtils.copyFile(file, destDir);
						response.put(STATUS, FILE_UPLOADED);
						response.put(ORIGINAL_FILE_NAME, fileName);
						response.put(FILE_NAME, dynamicName);
					} else {
						response.put(STATUS, FILE_EXTENSION_EXCEPION);
					}
				} else {
					response.put(STATUS, FILE_SIZE_EXCEPION);
				}
			} else {
				response.put(STATUS, FILE_UPLOADED_FAILED);
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (FileNotFoundException e) {
			logger.fatal("FileNotFoundException", e);
		} catch (IOException e) {
			logger.fatal("IOException", e);
		} catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} catch (ArrayIndexOutOfBoundsException e) {
			logger.fatal("ArrayIndexOutOfBoundsException", e);
		} catch (StringIndexOutOfBoundsException e) {
			logger.fatal("StringIndexOutOfBoundsException", e);
		}
		return response;
	}

	public boolean resizeImage(String name, String path, int newWidth,
			String prefix) {
		boolean status = false;
		BufferedImage originalImage = null;
		BufferedImage resizeImage = null;
		Graphics2D graphics2D = null;
		String fileType = null;
		int type = 0;
		int newHeight = 0;
		Validations v= new Validations();
		try {
			if (v.checkInput(name) && v.checkInput(path)
					&& newWidth > 0 && v.checkInput(prefix)) {
				fileType = name.substring(name.lastIndexOf("."));
				originalImage = ImageIO.read(new File(path + name));
				type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB
						: originalImage.getType();
				newHeight = Math.round((originalImage.getHeight() * newWidth)
						/ originalImage.getWidth());
				resizeImage = new BufferedImage(newWidth, newHeight, type);
				graphics2D = resizeImage.createGraphics();
				graphics2D.drawImage(originalImage, 0, 0, newWidth, newHeight,
						null);
				graphics2D.dispose();
				name = prefix + name;
				ImageIO.write(resizeImage, fileType.substring(1), new File(path
						+ name));
				status = true;
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (FileNotFoundException e) {
			logger.fatal("FileNotFoundException", e);
		} catch (IOException e) {
			logger.fatal("IOException", e);
		} finally {
			if (originalImage != null) {
				originalImage.flush();
			}
			if (resizeImage != null) {
				resizeImage.flush();
			}
		}
		return status;
	}
	
	public File getFileFromStream(byte[] bytes, String path, String name) {
		File file = new File(path+name);
		try {
			FileUtils.writeByteArrayToFile(file, bytes);
		} catch (IOException e) {
			logger.fatal("IOException", e);
		}
		return file;
	}

}
