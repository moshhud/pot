package dev.mashfiq.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.commons.collections.list.TreeList;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.OfficeXmlFileException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import dev.mashfiq.common.CommonDTO;

public class ExcelParseHelper {

	Logger logger = Logger.getLogger(ExcelParseHelper.class.getName());

	public ArrayList<ArrayList<String>> parseFile(CommonDTO dto) {
		POIFSFileSystem poifsFileSystem = null;
		HSSFWorkbook hssfWorkbook = null;
		HSSFSheet hssfSheet = null;
		Iterator<Row> rowIterator = null;
		Row hssfRow = null;
		Iterator<Cell> cellIterator = null;
		boolean isFirstRow = true;
		ArrayList<ArrayList<String>> rows = null;
		ArrayList<String> columns = null;
		HSSFCell hssfCell = null;
		try {
			poifsFileSystem = new POIFSFileSystem(new FileInputStream(
					dto.getFile()));
			hssfWorkbook = new HSSFWorkbook(poifsFileSystem);
			hssfSheet = hssfWorkbook.getSheetAt(0);
			rowIterator = hssfSheet.iterator();
			rows = new ArrayList<ArrayList<String>>();
			while (rowIterator.hasNext()) {
				hssfRow = rowIterator.next();
				cellIterator = hssfRow.cellIterator();
				if (isFirstRow) {
					isFirstRow = false;
					continue;
				}
				columns = new ArrayList<String>();
				while (cellIterator.hasNext()) {
					hssfCell = (HSSFCell) cellIterator.next();
					columns.add(hssfCell.toString());
				}
				rows.add(columns);
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (FileNotFoundException e) {
			logger.fatal("Exception", e);
		} catch (IOException e) {
			logger.fatal("Exception", e);
		} catch (OfficeXmlFileException e) {
			logger.fatal("Exception", e);
		}
		return rows;
	}

	/**
	 * permitted file type: excel, maximum size: 1MB
	 * 
	 * @param form
	 * @return
	 */
	public boolean validate(CommonDTO dto) {
		boolean status = false;
		TreeList allowedExtensions = new TreeList(Arrays.asList(".xls"));
		String fileName = null;
		String extension = null;
		try {
			if (dto.getFile() != null && dto.getFile().length() > 0
					&& dto.getFile().length() < 1024 * 1024) {
				fileName = dto.getFileFileName();
				extension = fileName.substring(fileName.lastIndexOf("."));
				if (extension != null && allowedExtensions.contains(extension)) {
					status = true;
				}
			}
		} catch (StringIndexOutOfBoundsException e) {
			logger.fatal("StringIndexOutOfBoundsException", e);
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		}
		return status;
	}

	public void writeToExcelFile(String fileName, String sheetName,
			ArrayList<ArrayList<String>> data, boolean append)
			throws IOException {
		HSSFWorkbook workbook = new HSSFWorkbook();
		FileInputStream fis = null;
		POIFSFileSystem poifs = null;
		HSSFSheet sheet = null;
		HSSFRow row = null;
		HSSFCell cell = null;
		int firstRowNo = 0;
		ArrayList<String> cellValues = null;
		FileOutputStream fos = null;
		try {
			if (append && new File(fileName).exists()) {
				fis = new FileInputStream(fileName);
				poifs = new POIFSFileSystem(fis);
				workbook = new HSSFWorkbook(poifs);
				sheet = workbook.getSheetAt(0);
			} else {
				sheet = workbook.createSheet(sheetName);
			}
			sheet.autoSizeColumn((short) 1);
			firstRowNo = sheet.getLastRowNum();
			if (firstRowNo > 0) {
				firstRowNo++;
			}
			for (int i = 0; i < data.size(); i++) {
				row = sheet.createRow(i + firstRowNo);
				cellValues = data.get(i);
				for (int j = 0; j < cellValues.size(); j++) {
					cell = row.createCell(j);
					sheet.autoSizeColumn((short) j);
					cell.setCellValue(new HSSFRichTextString(cellValues.get(j)));
				}
			}
			fos = new FileOutputStream(fileName);
			workbook.write(fos);
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
				if (fos != null) {
					fos.close();
				}
			} catch (NullPointerException e) {
				logger.fatal("Exception", e);
			} catch (IllegalStateException e) {
				logger.fatal("Exception", e);
			}
		}
	}

	public static void main(String args[]) {
		ArrayList<ArrayList<String>> data = new ArrayList<ArrayList<String>>();
		ArrayList<String> cell = null;
		for (int i = 0; i < 5; i++) {
			cell = new ArrayList<String>();
			for (int j = 0; j < 5; j++) {
				cell.add(j + "");
			}
			data.add(cell);
		}
		try {
			new ExcelParseHelper().writeToExcelFile("E://test.xls", "Report",
					data, true);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
