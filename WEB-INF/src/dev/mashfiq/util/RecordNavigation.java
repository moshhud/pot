package dev.mashfiq.util;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;

public class RecordNavigation {

	public static final String FIRST_PAGE = "first";
	public static final String LAST_PAGE = "last";
	public static final String PREVIOUS_PAGE = "previous";
	public static final String NEXT_PAGE = "next";
	public static final String GO_TO_PAGE = "goToPage";
	public static final String LIMIT = "limit";
	public static final String START_INDEX = "startIndex";
	public static final String ID = "navId";
	public static final String CURRENT_PAGE_NO = "pageno";
	private String[][] searchPanel;
	private HttpServletRequest request;
	private RecordNavigator rn;

	public RecordNavigation(String[][] searchPanel, HttpServletRequest request) {
		this(searchPanel, request, null);
	}
	
	public RecordNavigation(String[][] searchPanel, HttpServletRequest request, String actionName) {
		this.searchPanel = searchPanel;
		this.request = request;
		rn = new RecordNavigator();
		rn.setSearchPanel(searchPanel);
		rn.setActionName(actionName);
		request.getSession().setAttribute(ApplicationConstant.RECORD_NAVIGATOR, rn);
	}
	
	public RecordNavigation(HttpServletRequest request) {
		this.request = request;
		rn = new RecordNavigator();
		request.getSession().setAttribute(ApplicationConstant.RECORD_NAVIGATOR, rn);
	}

	public HashMap<String, Integer> getRecordNavigation(int totalRecords) {
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		int pageSize = ApplicationConstant.PAGE_SIZE;
		int limit = pageSize;
		int startIndex = 0;
		int totalPages = 1;
		int currentPageNo = 1;
		String goCheck = request.getParameter(GO_TO_PAGE);
		String id = request.getParameter(ID);
		String rpp = request.getParameter(ApplicationConstant.RECORD_PER_PAGE) == null ? "30"
				: request.getParameter(ApplicationConstant.RECORD_PER_PAGE);
		try {
			pageSize = Integer.parseInt(rpp);
			request.getSession().setAttribute(ApplicationConstant.RECORD_PER_PAGE, pageSize);
		} catch (NumberFormatException e) {
			pageSize = ApplicationConstant.PAGE_SIZE;
		}
		currentPageNo = NumericHelper.parseInt(request.getParameter(CURRENT_PAGE_NO));
		totalPages = totalRecords / pageSize;
		if (totalRecords % pageSize != 0) {
			totalPages++;
		}
		if (id != null) {
			if (id.equals(FIRST_PAGE)) {
				currentPageNo = 1;
			} else if (id.equals(NEXT_PAGE)) {
				if (currentPageNo < totalPages) {
					currentPageNo++;
				}
			} else if (id.equals(PREVIOUS_PAGE)) {
				if (currentPageNo > 1) {
					currentPageNo--;
				}
			} else if (id.equals(LAST_PAGE)) {
				currentPageNo = totalPages;
			}
		} 
		else if (goCheck != null) {
			int tempNumber = NumericHelper.parseInt(request.getParameter(CURRENT_PAGE_NO));
			if (tempNumber <= totalPages && tempNumber > 0) {
				currentPageNo = tempNumber;
			} else {
				currentPageNo = 1;
			}
		}
		if (currentPageNo < 1) {
			currentPageNo = 1;
		} else if (currentPageNo > totalPages) {
			currentPageNo = totalPages;
		}
		if (totalRecords == 0) {
			limit = 0;
		} 
		else if ((totalRecords > 0) && (currentPageNo == totalPages) && (totalRecords % pageSize != 0)) {
			limit = totalRecords % pageSize;
		} 
		else {
			limit = pageSize;
		}
		startIndex = (limit == 0) ? 0 : (currentPageNo - 1) * pageSize;
		rn = new RecordNavigator();
		rn.setCurrentPageNo(currentPageNo);
		rn.setTotalPages(totalPages);
		rn.setTotalRecords(totalRecords);
		rn.setSearchPanel(searchPanel);
		request.getSession().setAttribute(ApplicationConstant.RECORD_NAVIGATOR, rn);
		map.put(LIMIT, limit);
		map.put(START_INDEX, startIndex);
		return map;
	}

}
