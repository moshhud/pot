package dev.mashfiq.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

public class CollectionHelper {

	private static Logger logger = Logger.getLogger(CollectionHelper.class
			.getName());

	public ArrayList<String> getSubArrayList(
			ArrayList<String> vals, int fromIndex, int length) {
		ArrayList<String> data = null;
		int toIndex = 0;
		int size = 0;
		try {
			if (vals != null && fromIndex >= 0 && length > 0) {
				size = vals.size();
				if (size < fromIndex + length) {
					toIndex = size;
				} else {
					toIndex = fromIndex + length;
				}
				data = new ArrayList<String>(length);

				for (int i = fromIndex; i < toIndex; i++) {
					data.add(vals.get(i));
				}
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ArrayList<String> getDistinctValues(
			ArrayList data) {
		ArrayList list = null;
		try {
			if (data != null && data.size() > 0) {
				list = new ArrayList();
				for (Object val : data) {
					if (list != null && list.contains(val) == false) {
						list.add(val);
					}
				}
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return list;
	}

	public HashMap<String, ArrayList<String>> clearMapOfStringArrayList(
			HashMap<String, ArrayList<String>> vals) {
		if (vals != null) {
			vals.clear();
		}
		if (vals == null) {
			vals = new HashMap<String, ArrayList<String>>();
		}
		return vals;
	}

	public HashMap<String, String> clearMapOfString(
			HashMap<String, String> vals) {
		if (vals != null) {
			vals.clear();
		}
		if (vals == null) {
			vals = new HashMap<String, String>();
		}
		return vals;
	}

	public ArrayList<String> clearArrayListOfString(
			ArrayList<String> list) {
		if (list != null) {
			list.clear();
		}
		if (list == null) {
			list = new ArrayList<String>();
		}
		return list;
	}

	public ArrayList<String> getSubList(
			ArrayList<String> vals, int startIndex, int limit) {
		ArrayList<String> data = null;
		if (vals != null) {
			int listSize = vals.size();
			if (listSize > 0 && startIndex >= 0 && startIndex < listSize
					&& limit > 0) {
				if (listSize < (startIndex + limit)) {
					limit = listSize - startIndex;
				}
				limit = startIndex + limit;
				data = new ArrayList<String>(limit);
				try {
					for (int i = startIndex; i < limit; i++) {
						data.add(vals.get(i));
					}
				} catch (RuntimeException e) {
					logger.fatal("RuntimeException", e);
				}
			}
		}
		return data;
	}

	public boolean checkCollection(
			@SuppressWarnings("rawtypes") Collection data) {
		boolean status = false;
		if (data != null && data.size() > 0) {
			status = true;
		}
		return status;
	}

	public boolean checkMap(
			@SuppressWarnings("rawtypes") Map data) {
		boolean status = false;
		if (data != null && data.size() > 0) {
			status = true;
		}
		return status;
	}

	public boolean checkContainsKey(
			@SuppressWarnings("rawtypes") Map data, String key) {
		boolean status = false;
		if (data != null && data.containsKey(key)) {
			status = true;
		}
		return status;
	}

	public boolean checkContains(
			@SuppressWarnings("rawtypes") ArrayList data, Object key) {
		boolean status = false;
		if (data != null && data.contains(key)) {
			status = true;
		}
		return status;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ArrayList<String> getArrayListFromMap(Map map,
			boolean isKeyList) {
		ArrayList<String> data = null;
		Set<Entry> set = null;
		String key = null;
		if (checkMap(map)) {
			data = new ArrayList<String>(map.size());
			set = map.entrySet();
			try {
				for (Entry entry : set) {
					if (isKeyList) {
						key = entry.getKey() + "";
					} else {
						key = entry.getValue() + "";
					}
					data.add(key);
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		return data;
	}

	public ArrayList<String> getArrayListFromString(
			String str, String delimiter) {
		ArrayList<String> list = null;
		if (str != null && str.length() > 0 && delimiter != null) {
			list = new ArrayList<String>(Arrays.asList(new StringHelper()
					.getArrayFromString(str, delimiter)));
		}
		return list;
	}

	public ArrayList<String> addAll(
			ArrayList<String> list1, ArrayList<String> list2) {
		ArrayList<String> list = null;
		if (checkCollection(list1)) {
			if (checkCollection(list2)) {
				list1.addAll(list2);
			}
			list = list1;
		} else if (checkCollection(list2)) {
			list = list2;
		}
		return list;
	}

}