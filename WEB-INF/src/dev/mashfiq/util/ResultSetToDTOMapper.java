package dev.mashfiq.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedHashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerReseller;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.Column;

public class ResultSetToDTOMapper<T> {

	private Logger logger = Logger.getLogger(ResultSetToDTOMapper.class.getName());

	public ReturnObject getResultSetToDTOMap(String sql, boolean isDialerRegistration, String keyColumn, Class<T> dtoClass) {
		ReturnObject ro = null;
		Connection connection = null;		
		try {			
			if (isDialerRegistration) {
				connection = DatabaseManagerReseller.getInstance().getConnection();
				ro = getResultSetToDTOMap(sql, connection , keyColumn, dtoClass);
			} 
			else {
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
				ro = getResultSetToDTOMap(sql, connection , keyColumn, dtoClass);
			}
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		}
		finally{
			try{
				if (isDialerRegistration) {
					DatabaseManagerReseller.getInstance().freeConnection(connection);
					/*if(connection!=null){
						connection.close();
						//logger.debug("DatabaseManagerReseller: Closed");
					}*/
				} 
				else {
					DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					/*if(connection!=null){
						connection.close();
						//logger.debug("DatabaseManagerSuccessful: Closed");
					}*/
				}
				
			}catch(Exception exxx){
				logger.fatal("Closing error: "+exxx);
			}

		}
		return ro;
	}

	public ReturnObject getResultSetToDTOMap(String sql, Connection connection, String keyColumn, Class<T> dtoClass) {
		ReturnObject ro = new ReturnObject();
		LinkedHashMap<String, T> data = null;
		ResultSet rs = null;
		T dto = null;
		ResultSetMetaData rsmd = null;
		Field[] fields = null;
		String columnName;
		Object columnValue;
		Column column = null;
		Statement stmt = null;
		try {
			if (connection != null && keyColumn != null && keyColumn.length() > 0 && dtoClass != null) {
				stmt = connection.createStatement();				
				rs = stmt.executeQuery(sql);
				
				if (rs != null) {
					rsmd = rs.getMetaData();
					fields = dtoClass.getDeclaredFields();
					data = new LinkedHashMap<String, T>();
					
					while (rs.next()) {
						dto = (T) dtoClass.newInstance();
						
						for (int i = 1; i <= rsmd.getColumnCount(); i++) {
							columnName = rsmd.getColumnName(i);
							columnValue = rs.getObject(i);
							
							if (columnName != null && columnValue != null) {
								for (Field field : fields) {
									if (field.isAnnotationPresent(Column.class)) {
										column = field.getAnnotation(Column.class);
										
										if (column != null && columnName.equals(column.name())) {
											BeanUtils.setProperty(dto,field.getName(),columnValue);
											data.put(rs.getString(keyColumn),dto);
										}
									}
								}
							}
						}
					}
					
					if (data != null && data.size() > 0) {
						ro.setIsSuccessful(true);
						ro.setData(data);
					}
				}
			}
		} catch (SQLException e) {
			ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
			logger.fatal("SQLException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InvocationTargetException e) {
			logger.fatal("InvocationTargetException", e);
		} finally {
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

}