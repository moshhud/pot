package dev.mashfiq.util;

import java.math.BigInteger;

import java.security.SecureRandom;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.Logger;

public class StringHelper {

	static Logger logger = Logger.getLogger(StringHelper.class.getName());
	private static SecureRandom secureRandom = new SecureRandom();
	private static final String DELIMITER = ",";

	public String getStringFromArray(String[] arr, String delimiter,
			boolean useInvertedComma) {
		String data = null;
		try {
			if (arr != null && arr.length > 0) {
				if (delimiter == null || delimiter.length() == 0) {
					delimiter = DELIMITER;
				}
				data = "";
				for (String val : arr) {
					if (useInvertedComma) {
						data += "'" + val + "'" + delimiter;
					} else {
						data += val + delimiter;
					}
				}
				if (data != null && data.endsWith(delimiter)) {
					data = data.substring(0, data.length() - 1);
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (IndexOutOfBoundsException e) {
			logger.fatal("IndexOutOfBoundsException", e);
		} catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} catch (UnsupportedOperationException e) {
			logger.fatal("UnsupportedOperationException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}

	public String[] getArrayFromString(String str, String delimiter) {
		String[] data = null;
		try {
			if (delimiter == null || delimiter.length() == 0) {
				delimiter = DELIMITER;
			}
			if (str != null && str.length() > 0) {
				str = str.replace("'", "");
				data = str.split(delimiter);
				for (int i = 0; i < data.length; i++) {
					str = data[i];
					if (str != null && str.length() > 0) {
						data[i] = str.trim();
					}
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (IndexOutOfBoundsException e) {
			logger.fatal("IndexOutOfBoundsException", e);
		} catch (IllegalStateException e) {
			logger.fatal("IllegalStateException", e);
		} catch (UnsupportedOperationException e) {
			logger.fatal("UnsupportedOperationException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return data;
	}

	public String getSubString(String str, int length, String append) {
		if (str != null && str.length() > length) {
			str = str.substring(0, length);
			if (append != null && append.length() > 0) {
				str += append;
			}
		}
		return str;
	}

	public String[] split(String val, String regex) {
		String[] data = null;
		String str = null;
		if (val != null && val.length() > 0 && regex != null
				&& regex.length() > 0) {
			try {
				data = val.split(regex);
				if (data != null && data.length > 0) {
					for (int i = 0; i < data.length; i++) {
						str = data[i];
						if (str != null && str.length() > 0) {
							data[i] = str.trim();
						}
					}
				}
			} catch (RuntimeException e) {
			}
		}
		return data;
	}

	public String getTrimedString(String str, int length) {
		if (str != null && str.length() > 0 && length > 0
				&& length < str.length()) {
			str = str.substring(0, length) + "...";
		}
		return str;
	}

	public String getRandomString() {
		String randomString = null;
		int i = 0;
		try {
			while (true) {
				i++;
				randomString = new BigInteger(130, secureRandom).toString(32);
				if (randomString != null && randomString.length() > 0) {
					break;
				}
				if (i == 10) {
					randomString = new BigInteger(130, new SecureRandom())
							.toString(32) + "ReVeSyStEmS";
					break;
				}
			}
		} catch (RuntimeException e) {
		}
		return randomString;
	}

	public String getPassword() {
		String password = null;
		password = getRandomString();
		int i = 0;
		Validations v = new Validations();
		while (true) {
			if (v.checkInput(password)) {
				password = getSubString(password, 6, null);
				if (v.validatePassword(password) || i > 49) {
					break;
				}
				i++;
			}
		}
		return password;
	}

	public String capitalize(String str) {
		if (str != null && str.length() > 0) {
			str = WordUtils.capitalize(str);
		}
		return str;
	}
	
	public String generatePassword(){
		String password = "";
		Validations v = new Validations();
		while(true){
			password = new BigInteger(130, new SecureRandom()).toString(32).substring(0,6);
			if(v.validatePassword(password)){
				break;
			}
		}
		return password;
	}

}
