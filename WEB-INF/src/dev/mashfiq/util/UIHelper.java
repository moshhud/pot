package dev.mashfiq.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.revesoft.po.revesoft.users.RimsUsersDTO;
import com.revesoft.po.revesoft.users.RimsUsersRepository;
import com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDTO;
import com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository;

import dev.mashfiq.mashupData.MashupDataDTO;
import dev.mashfiq.mashupData.MashupDataRepository;

public class UIHelper {

	Logger logger = Logger.getLogger(UIHelper.class.getName());
	private Validations v = new Validations();
	
	public String getSelectFromMashup(int mashupDataId, boolean useIdAsKey, int id, String keyValue) {
		return getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(mashupDataId, true), useIdAsKey, id, keyValue, null, "Select");
	}

	public String getSelectFromMashup(ArrayList<MashupDataDTO> list, boolean useIdAsKey, int id, String keyValue) {
		
		return getSelectFromMashup(list, useIdAsKey, id, keyValue, null, "Select");
	}

	public String getSelectFromMashup(ArrayList<MashupDataDTO> list, boolean useIdAsKey, int id, String keyValue, String fieldLabel) {
		return getSelectFromMashup(list, useIdAsKey, id, keyValue, null, fieldLabel);
	}

	public String getSelectFromMashup(ArrayList<MashupDataDTO> list, boolean useIdAsKey, int id, String keyValue, String[] ignoreList, String fieldLabel) {
		String select = "<option value=''>" + fieldLabel + "</option>";
		ArrayList<String> ignrList = null;
		CollectionHelper ch = new CollectionHelper();
		String key = null;
		if (ch.checkCollection(list)) {
			if (ignoreList != null && ignoreList.length > 0) {
				ignrList = new ArrayList<String>(Arrays.asList(ignoreList));
			}
			for (MashupDataDTO dto : list) {
				if (useIdAsKey) {
					key = dto.getId() + "";
					keyValue = id + "";
				} else {
					key = dto.getKeyValue();
				}
				if (ignrList == null || ch.checkContains(ignrList, dto.getId() + "") == false) {
					select += "<option " + v.checkSelected(key, keyValue)
							+ " value=\"" + key + "\">" + dto.getLabel()
							+ "</option>";
				}
			}
		}
		return select;
	}

	public String getSelectFromMashup(ArrayList<MashupDataDTO> list, String keyValue[], String[] ignoreList, String fieldLabel) {
		String select = "<option value=''>" + fieldLabel + "</option>";
		ArrayList<String> ignrList = null;
		CollectionHelper ch = new CollectionHelper();
		String key = null;
		if (ch.checkCollection(list)) {
			if (ignoreList != null && ignoreList.length > 0) {
				ignrList = new ArrayList<String>(Arrays.asList(ignoreList));
			}
			for (MashupDataDTO dto : list) {
				key = dto.getKeyValue();
				if (ignrList == null
						|| ch.checkContains(ignrList, dto.getId() + "") == false) {
					select += "<option " + v.checkSelected(keyValue, key)
							+ " value=\"" + key + "\">" + dto.getLabel()
							+ "</option>";
				}
			}
		}
		return select;
	}

	public String getSpecificOptionsFromMashup(ArrayList<MashupDataDTO> list, boolean useIdAsKey, int id, String keyValue, String[] allowed) {
		String select = "<option value=''>Select</option>";
		ArrayList<String> allowList = null;
		CollectionHelper ch = new CollectionHelper();
		String key = null;
		if (ch.checkCollection(list)) {
			if (allowed != null && allowed.length > 0) {
				allowList = new ArrayList<String>(Arrays.asList(allowed));
			}
			for (MashupDataDTO dto : list) {
				if (useIdAsKey) {
					key = dto.getId() + "";
					keyValue = id + "";
				} else {
					key = dto.getKeyValue();
				}
				if (allowList == null
						|| ch.checkContains(allowList, dto.getId() + "")) {
					select += "<option " + v.checkSelected(key, keyValue)
							+ " value=\"" + key + "\">" + dto.getLabel()
							+ "</option>";
				}
			}
		}
		return select;
	}


	public String getAccountManagerOptions(int selectedValue, int usrID,
			int permissionLevel, String label){
		return getAccountManagerOptions(selectedValue, usrID, permissionLevel, label, true);
	}
	
	public String getAccountManagerOptions(int selectedValue, int usrID, int permissionLevel, String label, boolean onlyActive) {
		String options = "<option value=\"\">" + label + "</option>";
		RimsUsersRepository rimsUsersRepository = RimsUsersRepository.getInstance(false);
		RimsUsersDTO rimsUsersDTO = rimsUsersRepository.getDTOByUsrID(usrID);
		ArrayList<RimsUsersDTO> data = rimsUsersRepository.getAll(ApplicationConstant.ACTIVE);
		if (rimsUsersDTO != null && permissionLevel > 0 && data != null && data.size() > 0) {
			LOOP: for (RimsUsersDTO dto : data) {
				if (dto != null 
						&& (onlyActive == false || "active".equalsIgnoreCase(dto.getCurrentStatus()))
						) {
					switch (permissionLevel) {
					case 1:
						if (dto.getUsrID() == usrID) {
							options += "<option "
									+ v.checkSelected(selectedValue, dto.getUsrID()) + " value=\""
									+ dto.getUsrID() + "\">"
									+ dto.getUsrEmail() + "</option>";
						}
						break LOOP;
					case 2:
						
						break;
					default:
						options += "<option "
								+ v.checkSelected(selectedValue, dto.getUsrID())
								+ " value=\"" + dto.getUsrID() + "\">"
								+ dto.getUsrEmail() + "</option>";
						break;
					}
				}
			}
		}
		return options;
	}

	

	public String getInputFromMashup(ArrayList<MashupDataDTO> list,
			String fieldName, String keyValue[], int[] ignoreArr, int noOfColumn, boolean useIdAsKey, String type) {
		String output = "";
		ArrayList<Integer> ignrList = null;
		String key = null;
		int index = 0;
		Validations v = null;
		String id = null;
		if (list != null && list.size() > 0) {
			if (noOfColumn == 0) {
				noOfColumn = 1;
			}
			if (ignoreArr != null && ignoreArr.length > 0) {
				ignrList = new ArrayList<Integer>(ignoreArr.length);
				for (Integer i : ignoreArr) {
					ignrList.add(i);
				}
			}
			v = new Validations();
			for (MashupDataDTO dto : list) {
				key = dto.getKeyValue();
				if (ignrList == null || ignrList.contains(dto.getId()) == false) {
					index++;
					if (useIdAsKey) {
						key = dto.getId() + "";
					} else {
						key = dto.getKeyValue();
					}
					id = fieldName + "#" + dto.getId(); 
					output += "<input type=\"" + type + "\" id=\"" + id + "\" name=\"" + fieldName + "\" value=\"" + key + "\" " + v.checkChecked(keyValue, key)+ " />&nbsp;<label for=\"" + id + "\">" + dto.getLabel() + "</label>&nbsp;";
					if (index%noOfColumn == 0) {
						output += "<br/>";
					}
				}
			}
		} else {
			output = "N/A";
		}
		return output;
	}
	
	

}
