package dev.mashfiq.util.password;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class EncryptionHelper {
	
	public static String HMAC_MD5_encode(String key, String message){

        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(),"HmacMD5");
        Mac mac = null;
        byte[] rawHmac = null;
        try {
        	mac = Mac.getInstance("HmacMD5");
            mac.init(keySpec);
            rawHmac = mac.doFinal(message.getBytes());
		} catch (Exception e) {
		}
        return new String(Hex.encodeHex(rawHmac));
    }
	
	public static void main(String[] args) {
		String key = "moshhud@revesoft.com";
		String value = "moshhud@revesoft.com";
		System.out.println(HMAC_MD5_encode(key, value));
	}

}
