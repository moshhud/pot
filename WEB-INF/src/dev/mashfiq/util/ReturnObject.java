package dev.mashfiq.util;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;

public class ReturnObject {

	private boolean isSuccessful;
	private Messages messages;
	private ActionMessages actionMessage;
	private Object data;
	private Exception exception;
	private ArrayList<Messages> msgList;

	public ArrayList<Messages> getMsgList() {
		return msgList;
	}

	public void setMsgList(ArrayList<Messages> msgList) {
		this.msgList = msgList;
	}

	public ReturnObject() {
		isSuccessful = false;
		messages = new Messages();
		messages.setErrorMessage("Default Error Set In Constructor");
	}

	public ReturnObject(boolean isSuccessful, Messages messages) {
		this.isSuccessful = isSuccessful;
		this.messages = messages;
	}

	public boolean getIsSuccessful() {
		return isSuccessful;
	}

	public void setIsSuccessful(boolean isSuccessful) {
		this.isSuccessful = isSuccessful;
	}

	public Messages getMessage() {
		return messages;
	}

	public void setMessage(Messages messages) {
		this.messages = messages;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception e) {
		this.exception = e;
	}

	public ActionMessages getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(ActionMessages actionMessage) {
		this.actionMessage = actionMessage;
	}

	public void clear() {
		isSuccessful = false;
		messages = null;
		actionMessage = null;
		data = null;
		exception = null;
	}

	public static synchronized ReturnObject clearInstance(ReturnObject ro) {
		if (ro == null) {
			ro = new ReturnObject();
		} else {
			ro.clear();
		}
		return ro;
	}

	public void setActionMessage(HttpSession session) {
		if (session != null) {
			if (msgList != null) {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,msgList);
			}  else if (messages != null && actionMessage == null) {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,messages);
			} else if (actionMessage != null) {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,actionMessage);
			}
		}
	}

}
