package dev.mashfiq.util;

import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.apache.log4j.Logger;

public class Validations {

	static Logger logger = Logger.getLogger(Validations.class.getName());
	//public static final String HOST_VALIDATION_REGEX = "^(([a-zA-Z][a-zA-Z][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\\-]*[A-Za-z0-9])$";
	public static final String HOST_VALIDATION_REGEX = "^([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9]))*$";

	public boolean checkEquals(String val1, String val2, boolean ignoreCase) {
		boolean status = false;
		if (val1 != null && val2 != null) {
			if (ignoreCase) {
				status = val1.equalsIgnoreCase(val2);
			} else {
				status = val1.equals(val2);
			}
		}
		return status;
	}

	public boolean checkEqualsIgnoreCase(String val1, String val2) {
		return checkEquals(val1, val2, true);
	}

	public boolean containsKey(@SuppressWarnings("rawtypes") Map map, Object key) {
		boolean status = false;
		if (new CollectionHelper().checkMap(map) && key != null
				&& map.containsKey(key)) {
			status = true;
		}
		return status;
	}

	public boolean contains(String str, String val) {
		boolean status = false;
		if (checkInput(str) && checkInput(val)) {
			status = str.contains(val);
		}
		return status;
	}

	public String checkString(String str, String replaceWith) {
		if (str == null || str.length() == 0 || str.equals("null")
				|| str.equalsIgnoreCase("select")) {
			str = replaceWith;
		} else {
			str = str.trim();
		}
		return str;
	}

	public boolean checkInput(String value) {
		boolean isValid = false;
		if (value != null && value.length() > 0 && value.equalsIgnoreCase("null") == false) {
			isValid = true;
		}
		return isValid;
	}

	public boolean checkSelect(String value) {
		boolean isValid = false;
		if (value != null && value.length() > 0
				&& value.equalsIgnoreCase("select") == false) {
			isValid = true;
		}
		return isValid;
	}

	public boolean checkIp(String ip) {
		boolean isValid = false;
		try {
			if (ip != null && ip.length() > 0) {
				isValid = new InetAddressValidator().isValidInet4Address(ip);
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return isValid;
	}

	public boolean checkNumber(String number) {
		boolean status = true;
		try {
			Float.parseFloat(number);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return status;
	}

	public boolean validatePassword(String password) {
		return password.matches("((?=.*\\d)(?=.*[a-zA-Z]).{6,})");
	}

	public boolean checkEmail(String email) {
		boolean status = false;
		try {
			status = EmailValidator.getInstance().isValid(email);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return status;
	}

	public boolean checkPhoneNumber(String phoneNumber, int lenth) {
		boolean isValid = false;
		if (phoneNumber != null && phoneNumber.length() >= lenth) {
			try {
				Long.parseLong(phoneNumber);
				isValid = true;
			} catch (RuntimeException e) {
				logger.fatal("RuntimeException", e);
			}
		}
		return isValid;
	}

	public boolean checkPhoneNumber(String phoneNumber) {
		return checkPhoneNumber(phoneNumber, 8);
	}
	
	public boolean isValidPassword(String password) {
		boolean isValid = false;
		if (password != null && password.length() > 0) {
			isValid = password.matches("((?=.*\\d)(?=.*[a-zA-Z]).{6,})");
		}
		return isValid;
	}

	public static synchronized boolean checkCaptcha(String captcha, HttpSession session) {
		boolean status = false;
		String sessionCaptcha = null;
		try {
			sessionCaptcha = session.getAttribute(ApplicationConstant.CAPTCHA)
					+ "";
			session.removeAttribute(ApplicationConstant.CAPTCHA);
			if (captcha != null && sessionCaptcha != null
					&& captcha.equals(sessionCaptcha)) {
				status = true;
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE, ActionMessages.MULTIPLE_SUBMIT);
			}
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return status;
	}

	public String checkDTO(String val) {
		return checkDTO(val, "");
	}

	public String checkDTO(String val, String replaceWith) {
		if (val == null || val.length() == 0 || val.equals("null") || val.equalsIgnoreCase("select")) {
			val = replaceWith;
		} else {
			val = val.trim();
		}
		return val;
	}

	public boolean checkInput(String str, boolean ignoreMinLength,int minLenth, boolean ignoreMaxLength, int maxLenth, String except) {
		boolean isValid = false;
		if (str != null && (ignoreMinLength || str.length() >= minLenth)
				&& (ignoreMaxLength || str.length() <= maxLenth)) {
			if (except == null || str.equalsIgnoreCase(except) == false) {
				isValid = true;
			}
		}
		return isValid;
	}

	public boolean checkInput(String str, int minLenth, int maxLenth,
			String except) {
		return checkInput(str, false, minLenth, false, maxLenth, except);
	}

	public boolean checkInput(String str, int minLenth, int maxLenth) {
		return checkInput(str, false, minLenth, false, maxLenth, null);
	}

	public boolean checkInput(String str, int maxLenth) {
		return checkInput(str, true, 0, false, maxLenth, null);
	}

	public String checkSelected(String val1, String val2) {
		String selected = "";
		if (checkEquals(val1, val2, true)) {
			selected = "selected=\"selected\"";
		}
		return selected;
	}
	
	public String checkSelected(String[] val1, String val2) {
		String selected = "";
		if (val1 != null  && val1.length > 0 && val2 != null && val2.length() > 0) {
			for(String key: val1) {
				if (val2.equalsIgnoreCase(key)) {
					selected = "selected=\"selected\"";
					break;
				}
			}
		}
		return selected;
	}

	public String checkSelected(int val1, int val2) {
		String selected = "";
		if (val1 == val2) {
			selected = "selected=\"selected\"";
		}
		return selected;
	}

	public boolean checkNumericVal(String val) {
		boolean isNumericVal = false;
		try {
			if (val != null && val.length() > 0) {
				Double.parseDouble(val);
				isNumericVal = true;
			}
		} catch (RuntimeException e) {
		}
		return isNumericVal;
	}

	public String checkChecked(String val1, String val2) {
		String checked = "";
		if (checkEquals(val1, val2, true)) {
			checked = "checked=\"checked\"";
		}
		return checked;
	}
	
	public String checkChecked(String val1[], String val2) {
		String checked = "";
		if (val1 != null && val1.length > 0 && val2 != null && val2.length() > 0) {
			for(String v: val1) {
				if (v != null && val2.equalsIgnoreCase(v)) {
					checked = "checked=\"checked\"";
					break;
				}
			}
		}
		return checked;
	}

	public String checkChecked(int val1, int val2) {
		String checked = "";
		if (val1 == val2) {
			checked = "checked=\"checked\"";
		}
		return checked;
	}
	
	public boolean isNumber(String val) {
		boolean isNumber = false;
		if (val != null && val.length() > 0) {
			isNumber = NumberUtils.isNumber(val);
		}
		return isNumber;
	}
	
	public boolean isIPv6Address(final String input) {
	 	
	 	Pattern IPV6_STD_PATTERN = Pattern.compile("^(?:[0-9a-fA-F]{1,4}:){7}[0-9a-fA-F]{1,4}$");
		Pattern IPV6_HEX_COMPRESSED_PATTERN = Pattern.compile("^((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)::((?:[0-9A-Fa-f]{1,4}(?::[0-9A-Fa-f]{1,4})*)?)$");
		return IPV6_STD_PATTERN.matcher(input).matches() || IPV6_HEX_COMPRESSED_PATTERN.matcher(input).matches(); 
	 }
	
	public static void main(String[] args) {
		System.out.println(new Validations().isIPv6Address("2001:db8:85a3::8a2e:370:7334"));
	}

}
