package dev.mashfiq.util;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManager;
import databasemanager.DatabaseManagerReseller;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.Column;
import dev.mashfiq.common.CommonDTO;

public class DAOHelper<T> {

	private Logger logger = Logger.getLogger(DAOHelper.class.getName());

	public ReturnObject getResultSetToDTOMap(String sql, String keyColumn,
			Class<T> dtoClass) {
		ReturnObject ro = null;
		Connection connection = null;
		try {
			connection =  DatabaseManager.getInstance().getConnection();
			ro = getResultSetToDTOMap(sql,connection, keyColumn, dtoClass);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		}
		finally
		{
			try
			{
				//DatabaseManager.getInstance().freeConnection(connection);						
				FinalCleanUp.closeDatabaseConnection(connection);
			}catch(Exception esss){}
		}
		return ro;
	}

	public ReturnObject getResultSetToDTOMap(String sql, Connection connection,
			String keyColumn, Class<T> dtoClass) {
		ReturnObject ro = new ReturnObject();
		LinkedHashMap<String, T> data = null;
		ResultSet rs = null;
		T dto = null;
		ResultSetMetaData rsmd = null;
		Field[] fields = null;
		String columnName;
		Object columnValue;
		Column column = null;
		Statement stmt = null;
		try {
			if (connection != null && keyColumn != null
					&& keyColumn.length() > 0 && dtoClass != null) {
				stmt = connection.createStatement();
				rs = stmt.executeQuery(sql);
				if (rs != null) {
					rsmd = rs.getMetaData();
					fields = dtoClass.getDeclaredFields();
					data = new LinkedHashMap<String, T>();
					while (rs.next()) {
						dto = (T) dtoClass.newInstance();
						for (int i = 1; i <= rsmd.getColumnCount(); i++) {
							columnName = rsmd.getColumnName(i);
							columnValue = rs.getObject(i);
							if (columnName != null && columnValue != null) {
								for (Field field : fields) {
									if (field.isAnnotationPresent(Column.class)) {
										column = field
												.getAnnotation(Column.class);
										if (column != null
												&& columnName.equals(column
														.name())) {
											BeanUtils.setProperty(dto,
													field.getName(),
													columnValue);
											data.put(rs.getString(keyColumn),
													dto);
										}
									}
								}
							}
						}
					}
					if (data != null && data.size() > 0) {
						ro.setIsSuccessful(true);
						ro.setData(data);
					}
				}
			}
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InvocationTargetException e) {
			logger.fatal("InvocationTargetException", e);
		} finally {
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

	public ReturnObject insert(String tableName, String[] columnArray,
			CommonDTO dto, boolean isDialerRegistration) {
		return insert(tableName,
				new ArrayList<String>(Arrays.asList(columnArray)), dto,
				isDialerRegistration);
	}

	public ReturnObject insert(String tableName, ArrayList<String> columnList,
			CommonDTO dto, boolean isDialerRegistration) {
		ReturnObject ro = new ReturnObject();
		ResultSet rs = null;
		Field[] fields = null;
		Column column = null;
		PreparedStatement pstmt = null;
		Connection connection = null;
		String sql, columns;
		HashMap<String, Integer> map;
		QueryHelper qh = null;
		try {
			if (tableName != null && tableName.length() > 0
					&& columnList != null && columnList.size() > 0
					&& dto != null) {
				map = new HashMap<String, Integer>(columnList.size());
				columns = "";
				qh = new QueryHelper();
				sql = "INSERT INTO  " + qh.getMysqlRealScapeString(tableName)
						+ " (";
				for (int i = 0; i < columnList.size(); i++) {
					sql += qh.getMysqlRealScapeString(columnList.get(i)) + ",";
					columns += "?,";
					map.put(columnList.get(i), i + 1);
				}
				if (columns.endsWith(",")) {
					columns = columns.substring(0, columns.length() - 1);
					sql = sql.substring(0, sql.length() - 1);
				}
				sql += ") VALUES (" + columns + ")";
				if (isDialerRegistration) {
					connection = DatabaseManager.getInstance().getConnection();
				} else {
					connection = DatabaseManagerSuccessful.getInstance()
							.getConnection();
				}
				pstmt = connection.prepareStatement(sql,
						PreparedStatement.RETURN_GENERATED_KEYS);
				fields = dto.getClass().getDeclaredFields();
				for (Field field : fields) {
					if (field.isAnnotationPresent(Column.class)) {
						column = field.getAnnotation(Column.class);
						if (column != null
								&& columnList.contains(column.name())) {
							pstmt.setString(map.get(column.name().toString()),
									BeanUtils.getProperty(dto, field.getName())
											+ "");
						}
					}
				}
				if (pstmt.executeUpdate() > 0) {
					ro.setIsSuccessful(true);
					rs = pstmt.getGeneratedKeys();
					if (rs.next()) {
						dto.setId(rs.getInt(1));
						ro.setData(rs.getInt(1));
					}
				}
			}
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InvocationTargetException e) {
			logger.fatal("InvocationTargetException", e);
		} catch (NoSuchMethodException e) {
			logger.fatal("NoSuchMethodException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeDatabaseConnection(connection);			
		}
		return ro;
	}

	public ReturnObject update(String tableName, String[] columnArray,
			CommonDTO dto, String additionalUpdate, String colName,
			String colValue, boolean isDialerRegistration) {
		return update(tableName,
				new ArrayList<String>(Arrays.asList(columnArray)), dto,
				additionalUpdate, colName, colValue, isDialerRegistration);
	}

	public ReturnObject update(String tableName, ArrayList<String> columnList,
			CommonDTO dto, String additionalUpdate, String colName,
			String colValue, boolean isDialerRegistration) {
		ReturnObject ro = new ReturnObject();
		Field[] fields = null;
		Column column = null;
		PreparedStatement pstmt = null;
		Connection connection = null;
		String sql;
		HashMap<String, Integer> map;
		QueryHelper qh = null;
		try {
			if (tableName != null && tableName.length() > 0
					&& columnList != null && columnList.size() > 0
					&& dto != null && colName != null && colName.length() > 0
					&& colValue != null && colValue.length() > 0) {
				map = new HashMap<String, Integer>(columnList.size());
				qh = new QueryHelper();
				sql = "UPDATE " + qh.getMysqlRealScapeString(tableName)
						+ " SET ";
				for (int i = 0; i < columnList.size(); i++) {
					sql += qh.getMysqlRealScapeString(columnList.get(i))
							+ "=?,";
					map.put(columnList.get(i), i + 1);
				}
				if (sql.endsWith(",")) {
					sql = sql.substring(0, sql.length() - 1);
				}
				if (additionalUpdate != null && additionalUpdate.length() > 0) {
					sql += additionalUpdate;
				}
				sql += " WHERE " + qh.getMysqlRealScapeString(colName) + "='"
						+ qh.getMysqlRealScapeString(colValue) + "' ";
				if (isDialerRegistration) {
					connection = DatabaseManager.getInstance().getConnection();
				} else {
					connection = DatabaseManagerSuccessful.getInstance()
							.getConnection();
				}
				pstmt = connection.prepareStatement(sql);
				fields = dto.getClass().getDeclaredFields();
				for (Field field : fields) {
					if (field.isAnnotationPresent(Column.class)) {
						column = field.getAnnotation(Column.class);
						if (column != null
								&& columnList.contains(column.name())) {
							pstmt.setString(map.get(column.name().toString()),
									BeanUtils.getProperty(dto, field.getName())
											+ "");
						}
					}
				}
				if (pstmt.executeUpdate() > 0) {
					ro.setIsSuccessful(true);
				}
			}
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InvocationTargetException e) {
			logger.fatal("InvocationTargetException", e);
		} catch (NoSuchMethodException e) {
			logger.fatal("NoSuchMethodException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		
			
		}
		return ro;
	}

}