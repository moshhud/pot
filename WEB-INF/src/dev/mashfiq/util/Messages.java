package dev.mashfiq.util;

public class Messages{

	public static final String WARNING_MESSAGE = "alert alert-warning";
	public static final String ERROR_MESSAGE = "alert alert-danger";
	public static final String SUCCESS_MESSAGE = "alert alert-success";
	public static final String VALIDATION_ERROR = "Please Check Mandatory Fields!";

	private String msg;
	private String type;

	public String getMsg() {
		return msg;
	}

	public String getType() {
		return type;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Messages setSuccessMessage(String msg) {
		this.msg = msg;
		type = SUCCESS_MESSAGE;
		return this;
	}

	public Messages setErrorMessage(String msg) {
		this.msg = msg;
		type = ERROR_MESSAGE;
		return this;
	}

	public Messages setWarningMessage(String msg) {
		this.msg = msg;
		type = WARNING_MESSAGE;
		return this;
	}

}
