package dev.mashfiq.relationMapper;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.Validations;

public class RelationMapperDTO extends CommonDTO {
	private String tableA;
	private String tableB;
	private String columnA;
	private String columnB;
	private String valueA;
	private String valueB;
	Validations v = new Validations();

	public String getTableA() {
		return v.checkDTO(tableA);
	}

	public void setTableA(String tableA) {
		this.tableA = tableA;
	}

	public String getTableB() {
		return v.checkDTO(tableB);
	}

	public void setTableB(String tableB) {
		this.tableB = tableB;
	}

	public String getColumnA() {
		return v.checkDTO(columnA, "id");
	}

	public void setColumnA(String columnA) {
		this.columnA = columnA;
	}

	public String getColumnB() {
		return v.checkDTO(columnB, "id");
	}

	public void setColumnB(String columnB) {
		this.columnB = columnB;
	}

	public String getValueA() {
		return v.checkDTO(valueA);
	}

	public void setValueA(String valueA) {
		this.valueA = valueA;
	}

	public String getValueB() {
		return v.checkDTO(valueB);
	}

	public void setValueB(String valueB) {
		this.valueB = valueB;
	}
}
