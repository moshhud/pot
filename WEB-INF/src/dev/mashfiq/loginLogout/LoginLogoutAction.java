package dev.mashfiq.loginLogout;

import java.util.Enumeration;
import java.util.LinkedHashMap;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;
import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.revesoft.po.revesoft.users.RimsUsersDTO;
import com.revesoft.rims.dialerRegistration.vbOTPInfo.VbOTPInfoService;
import com.revesoft.rims.revesoft.rimsPermissions.RimsPermissionsDAO;

import dev.mashfiq.common.MasterAction;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Messages;
import dev.mashfiq.util.NumericHelper;
import dev.mashfiq.util.Validations;
import dev.mashfiq.util.password.MD5Crypt;

public class LoginLogoutAction extends MasterAction implements
		ModelDriven<RimsUsersDTO>, Preparable {

	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(LoginLogoutAction.class.getName());
	private RimsUsersDTO dto;
	

	public void prepare() throws Exception {
		dto = new RimsUsersDTO();
	}

	public RimsUsersDTO getModel() {
		return dto;
	}

	public String login() {
		startTime = System.currentTimeMillis();
		result = INPUT;
		String password = null;
		
		Validations v = new Validations();
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			if (dto != null && v.checkEmail(dto.getUsrEmail())
					&& (v.validatePassword(dto.getUsrPassword()) || true)) {
				password = dto.getUsrPassword();
				dto = new LoginLogoutService().getDTO("usrEmail", dto.getUsrEmail(), null);
				
				if (dto != null) {
					
					if (v.checkEqualsIgnoreCase(dto.getCurrentStatus(),
							ApplicationConstant.ACTIVE)) {
						if (MD5Crypt.check(password, dto.getUsrPassword())) {
							session.setAttribute(
									ApplicationConstant.TEMPORARY_LOGIN_INFO,
									dto);
							/*-------without otp login-------*/
							if (dto != null) {
								dto.setPermissionMap(new RimsPermissionsDAO()
										.getPermissionLevel(dto.getUsrRoleId(),
												dto.getUsrID()));
								session.setAttribute(ApplicationConstant.LOGIN_INFO,
										dto);
								session.removeAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
								session.setAttribute(ApplicationConstant.LOGIN_INFO,
										dto);
								result = SUCCESS;
							}
							/*-------End--------*/
							
							//result = SUCCESS;
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.LOGIN_ERROR);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								new Messages()
										.setErrorMessage("This profile has been deactivated"));
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.LOGIN_ERROR);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.INVALID_REQUEST);
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time for login " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		
		try {
			
		 	String hmackey = encode(ApplicationConstant.secretKey,ApplicationConstant.apiKey);
		 	logger.debug("HmacSign: "+hmackey);
		 	
		}
		catch(Exception e) {
			System.out.println("HmacSign error: "+e);
		}
		
		logger.debug("Login verification: "+result);
		
		
		return result;
	}
	
		

	@SuppressWarnings("rawtypes")
	public String logout() {
		try {
			Enumeration e = session.getAttributeNames();
			while (e.hasMoreElements()) {
				String sessionName = (String) e.nextElement();
				if (sessionName != null) {
					session.removeAttribute(sessionName);
				}
			}
			session.invalidate();
		} catch (RuntimeException e) {
			logger.fatal("RuntimeException", e);
		}
		return SUCCESS;
	}
	
	
	

	public String otpValidation() {
		startTime = System.currentTimeMillis();
		result = INPUT;		
		
		
		/*
		 //For seamoon OTP verification 
		 String password="";
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			if ((dto.getUsrPassword() != null
					&& dto.getUsrPassword().length() == 6)) {
				password = dto.getUsrPassword();
				dto = (RimsUsersDTO) session
						.getAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
				if (dto != null) {
					dto.setUsrPassword(password);
					if (new VbOTPInfoService().checkOtp(dto) && password != null) {
						dto.setPermissionMap(new RimsPermissionsDAO()
								.getPermissionLevel(dto.getUsrRoleId(),
										dto.getUsrID()));
						session.setAttribute(ApplicationConstant.LOGIN_INFO,
								dto);
						session.removeAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
						session.setAttribute(ApplicationConstant.LOGIN_INFO,
								dto);
												
						result = SUCCESS;
						
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.OTP_ERROR);
					}
				} else {
					result = "loginError";
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.LOGIN_ERROR);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.INVALID_REQUEST);
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time for otpValidation " + startTime
					+ " : " + (System.currentTimeMillis() - startTime));
		}*/
		
		
		
		
		//Reve secure 2FA: added by moshhud
		
		String otpTokenUniqueID = (String)session.getAttribute("otpTokenUniqueID");
		String otpAuth = request.getParameter("auth");
		logger.debug("otpTokenUniqueID: "+otpTokenUniqueID);
		logger.debug("Auth: "+otpAuth);
		String SUCCESSSTAT="";
		String FAILURESTAT="";
		try {
			SUCCESSSTAT = encode(ApplicationConstant.secretKey, "success|" + otpTokenUniqueID);
			FAILURESTAT = encode(ApplicationConstant.secretKey, "failure|" + otpTokenUniqueID);
			logger.debug("SUCCESS : "+SUCCESSSTAT);
			logger.debug("FAILURE : "+FAILURESTAT);
			if(otpAuth.equals(SUCCESSSTAT)){
				logger.debug("verification successful ");				
				dto = (RimsUsersDTO) session
						.getAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
				if (dto != null) {
					dto.setPermissionMap(new RimsPermissionsDAO()
							.getPermissionLevel(dto.getUsrRoleId(),
									dto.getUsrID()));
					session.setAttribute(ApplicationConstant.LOGIN_INFO,
							dto);
					session.removeAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
					session.setAttribute(ApplicationConstant.LOGIN_INFO,
							dto);
					result = SUCCESS;
					
				}
			}
			else if(otpAuth.equals(FAILURESTAT)){
				result = INPUT;
				logger.debug("verification failed ");
			}
			else {
				logger.debug("invalid request ");
				result = PERMISSION_DENIED;
			}
			
		}
		catch(Exception e) {
			logger.fatal(e.toString());
		}
		
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time for otpValidation " + startTime
					+ " : " + (System.currentTimeMillis() - startTime));
		}
		
		
		
		
		return result;
	}
	
	public static String encode(String key, String data) throws Exception {
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        SecretKeySpec secret_key = new SecretKeySpec(hexStringToByteArray(key), "HmacSHA256");
        sha256_HMAC.init(secret_key);
        return Hex.encodeHexString(sha256_HMAC.doFinal(data.getBytes("UTF-8")));
    }
	
	public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
	


	public String add() {
		throw new UnsupportedOperationException();
	}

	public String search() {
		throw new UnsupportedOperationException();
	}

	public String get() {
		throw new UnsupportedOperationException();
	}

	public String update() {
		throw new UnsupportedOperationException();
	}

	public String goToSearch() {
		throw new UnsupportedOperationException();
	}

}
