package dev.mashfiq.loginLogout;

import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import com.revesoft.po.revesoft.users.RimsUsersDAO;
import com.revesoft.po.revesoft.users.RimsUsersDTO;

import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public class LoginLogoutService {
	
	private Logger logger = Logger.getLogger(LoginLogoutService.class.getName());
	
	@SuppressWarnings("unchecked")
	public RimsUsersDTO getDTO(String colName, String colValue, String condition) {
		RimsUsersDTO dto = null;
		QueryHelper qh = null;
		ReturnObject ro = null;
		LinkedHashMap<String, RimsUsersDTO> data = null;
		if (colName != null && colName.length() > 0 && colValue != null && colValue.length() > 0) {
			if (condition == null || condition.length() == 0) {
				condition = "";
			}
			qh = new QueryHelper();
			condition += " AND " + qh.getMysqlRealScapeString(colName) + "='" + qh.getMysqlRealScapeString(colValue) + "' ";
			ro = new RimsUsersDAO().getMap(null, condition, colName);
			if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
				try {
					data = (LinkedHashMap<String, RimsUsersDTO>) ro.getData();
					if (data != null && data.containsKey(colValue)) {
						dto = data.get(colValue);
					}
				} catch (ClassCastException e) {
					logger.fatal("ClassCastException", e);
				}
			}
		}
		return dto;
	}

}
