package dev.mashfiq.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FieldDefination {

	String type() default "text";

	String id() default "";

	String label() default "";

	String defaultValue() default "";

	boolean required() default false;

	boolean readonly() default false;

	String placeHolder() default "";

	String cssClass() default "";

	String useFor() default "all";
	
	int mashupFieldType() default 0;

	boolean useMashupDataIdAsValue() default true;

	boolean isCountry() default false;
	
	String matchType() default "like";
	
	String field() default "";
	
	String notification() default "";
	
	boolean isProduct() default false;
	
	boolean isAccountManager() default false;
	
	String dataHide() default "phone,tablet";
	
}