package dev.mashfiq.loginTracking;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.Validations;

public class LoginTrackingDTO extends CommonDTO {
	private String sessionId;
	private String userIp;
	private String browserInfo;
	private String logoutTime;
	private String track;
	Validations v = new Validations();

	public String getSessionId() {
		return v.checkDTO(sessionId);
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getUserIp() {
		return v.checkDTO(userIp);
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	public String getBrowserInfo() {
		return v.checkDTO(browserInfo);
	}

	public void setBrowserInfo(String browserInfo) {
		this.browserInfo = browserInfo;
	}

	public String getLogoutTime() {
		return v.checkDTO(logoutTime);
	}

	public void setLogoutTime(String logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getTrack() {
		return v.checkDTO(track);
	}

	public void setTrack(String track) {
		this.track = track;
	}

}