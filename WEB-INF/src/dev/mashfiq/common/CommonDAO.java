package dev.mashfiq.common;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManager;
import databasemanager.DatabaseManagerReseller;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.Messages;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.Validations;

public class CommonDAO {

	private Logger logger = Logger.getLogger(CommonDAO.class.getName());
	private QueryHelper qh = new QueryHelper();

	public int getCount(String tblName, String columnName, String condition,boolean isDialerRegistration) {
		int count = 0;
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		
		try {
			if (isDialerRegistration)
				connection = DatabaseManagerReseller.getInstance().getConnection();
			else
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
			
			if (tblName != null && tblName.length() > 0 && columnName != null && columnName.length() > 0 && connection != null) {
				
				sql = "SELECT COUNT(" + qh.getMysqlRealScapeString(columnName) + ") AS total_count FROM " + qh.getMysqlRealScapeString(tblName);
				
				if (condition != null && condition.length() > 0) 
					sql += " WHERE 1=1 " + condition;
				logger.debug(sql);
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				
				if (rs.next()) 
					count = rs.getInt("total_count");
			} 
			else 
				count = -1;
			
		} catch (SQLException e) {
			logger.debug("Exception Query: " + sql);
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		}
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} 
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			try{
				if(isDialerRegistration) {
					//DatabaseManagerReseller.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
						//logger.debug("DatabaseManagerReseller: Closed");
					}
				}					
				else {
					//DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
						//logger.debug("DatabaseManagerSuccessful: Closed");
					}
				}
					
				
			}
			catch(Exception exx){
				logger.fatal(exx.toString());
			}
		}
		return count;
	}
	
	public int getCount(String tblName, String columnName, String condition, Connection connection) {
		int count = 0;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		
		try {
			if (tblName != null && tblName.length() > 0 && columnName != null && columnName.length() > 0 && connection != null) {
				
				sql = "SELECT COUNT(" + qh.getMysqlRealScapeString(columnName) + ") AS total_count FROM " + qh.getMysqlRealScapeString(tblName);
				
				if (condition != null && condition.length() > 0) {
					sql += " WHERE 1=1 " + condition;
				}
				
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				
				if (rs.next()) {
					count = rs.getInt("total_count");
				}
			} else {
				count = -1;
			}
		}
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} 
		catch (SQLException e) {
			logger.debug("Exception Query: " + sql);
			logger.fatal("SQLException", e);
		} 
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			//FinalCleanUp.closeDatabaseConnection(connection);
			try {
				if(connection!=null){
					connection.close();
				} 
			}
			catch(Exception e) {
				logger.fatal(e.toString());
			}
		}
		return count;
	}



	public ArrayList<String> getArrayList(String tableName, String returnCol,HashMap<String, ArrayList<String>> vals, String condition, boolean isDialerRegistration) {
		ArrayList<String> data = null;
		Connection connection = null;
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		String sql = null;
		
		try {
			
			if (isDialerRegistration) 
				connection = DatabaseManagerReseller.getInstance().getConnection();			 
			else 
				connection = DatabaseManagerSuccessful.getInstance().getConnection();			
			
			if (tableName != null && tableName.length() > 0 && returnCol != null && returnCol.length() > 0 && connection != null) {
				returnCol = qh.getMysqlRealScapeString(returnCol);
				sql = "SELECT " + returnCol + " FROM "+ qh.getMysqlRealScapeString(tableName) + " WHERE 1=1 ";
				sql += qh.getQueryFromHashMap(vals);
				if (condition != null && condition.length() > 0) {
					sql += " " + condition;
				}
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				data = new ArrayList<String>();
				
				while (rs.next()) {
					if (data != null && data.contains(rs.getString(returnCol)) == false) {
						data.add(rs.getString(returnCol));
					}
				}
			}
			
			//data = getArrayList(tableName, returnCol, vals, condition, connection);
		} 
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		}
		catch (SQLException e) {
			logger.debug("Exception Query: " + sql);
			logger.fatal("SQLException", e);
		} 
		catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} 
		catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} 
		catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} 
		catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		}
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			try{
				if(isDialerRegistration) {
					//DatabaseManagerReseller.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}  
				else {
					//DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
			}
			catch(Exception exx){}
		}
		
		return data;
	}

	public ReturnObject getMultipleValues(String keyCol, String colName,HashMap<String, ArrayList<String>> vals, String condition,String tableName, boolean getDistinct, boolean isDialerRegistration) {
		ReturnObject ro = new ReturnObject();
		HashMap<String, ArrayList<String>> data = null;
		ArrayList<String> list = null;
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		QueryHelper qh;
		dev.mashfiq.util.CollectionHelper ch = null;
		Validations v = new Validations();
		try {
			if (keyCol != null && keyCol.length() > 0 && colName != null && colName.length() > 0) {
				qh = new QueryHelper();
				ch = new dev.mashfiq.util.CollectionHelper();
				keyCol = qh.getMysqlRealScapeString(keyCol);
				colName = qh.getMysqlRealScapeString(colName);
				
				sql = "SELECT " + qh.getMysqlRealScapeString(keyCol) + "," + qh.getMysqlRealScapeString(colName) + " FROM " + qh.getMysqlRealScapeString(tableName) + " WHERE 1=1 ";
				sql += qh.getQueryFromHashMap(vals);
				
				if (v.checkInput(condition)) {
					sql += condition;
				}
				
				if (isDialerRegistration) 
					connection = DatabaseManagerReseller.getInstance().getConnection();
				else 
					connection = DatabaseManagerSuccessful.getInstance().getConnection();
							
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				data = new HashMap<String, ArrayList<String>>();
				
				while (rs.next()) {
					if (data != null) {
						if (ch.checkContainsKey(data, rs.getString(keyCol))) {
							list = data.get(rs.getString(keyCol));
						}
						if (list == null) {
							list = new ArrayList<String>();
						}
						if (getDistinct == false || ch.checkContains(list, rs.getString(colName)) == false) {
							list.add(rs.getString(colName));
						}
						data.put(rs.getString(keyCol), list);
						list = null;
					}
				}
				if (ch.checkMap(data)) {
					ro.setData(data);
					ro.setIsSuccessful(true);
				}
			}
		} 
		catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} 
		catch (SQLException e) {
			logger.debug("Exception Query: " + sql);
			logger.fatal("SQLException", e);
		} 
		catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} 
		catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} 
		catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} 
		catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} 
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			try{
				if(isDialerRegistration) {
					//DatabaseManagerReseller.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
				else {
					//DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
			}
			catch(Exception exx){}
		}
		return ro;
	}

	public ReturnObject executeUpdate(String sql, Connection connection) {
		ReturnObject ro = new ReturnObject();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		if (sql != null && sql.length() > 0 && connection != null) {
			try {
				pstmt = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
				if (pstmt.executeUpdate() > 0) {
					ro.setIsSuccessful(true);
					rs = pstmt.getGeneratedKeys();
					if (rs.next()) {
						ro.setData(rs.getInt(1));
					}
				}
			} catch (SQLException e) {
				logger.fatal("SQLException", e);
			} 
			finally {
				FinalCleanUp.closeResource(rs);
				FinalCleanUp.closeResource(pstmt);
				//FinalCleanUp.closeDatabaseConnection(connection);
				//try{DatabaseManagerReseller.getInstance().freeConnection(connection);}catch(Exception exx){}
				try {
					if(connection!=null){
						connection.close();
					} 
				}
				catch(Exception e) {
					logger.fatal(e.toString());
				}
			}
		}
		return ro;
	}

	public ResultSet executeQuery(String sql, Connection connection)throws SQLException {
		long startTime= System.currentTimeMillis();
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		if (sql != null && sql.length() > 0 && connection != null) {
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time for executeQuery " + startTime + " : " + (System.currentTimeMillis() - startTime));
		}
		return rs;
	}

//	public ResultSet executeQuery(String sql, boolean isDialerRegistration) {
//		ResultSet rs = null;
//		Connection connection = null;
//		try {
//			if (isDialerRegistration)
//				connection = DatabaseManager.getInstance().getConnection();
//			else 
//				connection = DatabaseManagerSuccessful.getInstance().getConnection();
	
//			rs = executeQuery(sql, connection);
//		} catch (SQLException e) {
//			logger.fatal("SQLException", e);
//		} catch (JDOMException e) {
//			logger.fatal("JDOMException", e);
//		} catch (ClassNotFoundException e) {
//			logger.fatal("ClassNotFoundException", e);
//		} catch (IllegalAccessException e) {
//			logger.fatal("IllegalAccessException", e);
//		} catch (InstantiationException e) {
//			logger.fatal("InstantiationException", e);
//		}
//		return rs;
//	}

	public ReturnObject executeUpdate(String sql, boolean isDialerRegistration) {
		ReturnObject ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		
		try {
			if (isDialerRegistration) 
				connection = DatabaseManager.getInstance().getConnection();
			 else 
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
			
			pstmt = connection.prepareStatement(sql,PreparedStatement.RETURN_GENERATED_KEYS);
			if (pstmt.executeUpdate() > 0) {
				ro.setIsSuccessful(true);
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) 
					ro.setData(rs.getInt(1));
			}
			
		} 
		catch (SQLException e) {
			logger.debug("Exception Query: " + sql);
			logger.fatal("SQLException", e);
		} 
		catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} 
		catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} 
		catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} 
		catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		}
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			
			try{
				if (isDialerRegistration) {
					//DatabaseManager.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
				 else {
					 //DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					 if(connection!=null){
							connection.close();
						} 
				 }
					
			}
			catch(Exception exx){}
		}
		
		return ro;
	}

	public LinkedHashMap<String, String> getMap(String tblName,String keyColName, String valColName,HashMap<String, ArrayList<String>> vals, String condition, boolean isDialerRegistration) {
		LinkedHashMap<String, String> data = null;
		Connection connection = null;
		long startTime = System.currentTimeMillis();
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		
		try {
			if (isDialerRegistration) {
				connection = DatabaseManagerReseller.getInstance().getConnection();
			}
			else {
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
			}
			
			if (tblName != null && tblName.length() > 0 && keyColName != null && keyColName.length() > 0 && valColName != null
			&& valColName.length() > 0 && ((vals != null && vals.size() > 0) || (condition != null && condition.length() > 0)) && connection != null) {
				
				keyColName = qh.getMysqlRealScapeString(keyColName);
				valColName = qh.getMysqlRealScapeString(valColName);
				
				sql = "SELECT " + keyColName + "," + valColName + " FROM " + qh.getMysqlRealScapeString(tblName) + " WHERE 1=1 ";
				
				sql += qh.getQueryFromHashMap(vals);
				
				if (condition != null && condition.length() > 0) {
					sql += " " + condition;
				}
				logger.debug("SQL: " + sql);
				
				pstmt = connection.prepareStatement(sql);
				rs = pstmt.executeQuery();
				data = new LinkedHashMap<String, String>();
				
				while (rs.next()) {
					data.put(rs.getString(keyColName), rs.getString(valColName));
				}
			}
			
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		}
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			try{
				if(isDialerRegistration) {
					//DatabaseManagerReseller.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}					
				else {
					//DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
			}
			catch(Exception exx){}
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time for getMap: " + (System.currentTimeMillis() - startTime));
		}
		return data;
	}

	public ReturnObject update(String tableName, HashMap<String, String> vals, String additionalUpdateFields, String colName, String value,String matchType, String condition, boolean isDialerRegistration) {
		ReturnObject ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		String sql = null;
		
		try {
			if (isDialerRegistration) {
				connection = DatabaseManager.getInstance().getConnection();
			} else {
				connection = DatabaseManagerSuccessful.getInstance().getConnection();
			}
			
			if (tableName != null && tableName.length() > 0 && colName != null && colName.length() > 0
					&& value != null && value.length() > 0 && matchType != null && matchType.length() > 0
					&& connection != null && ((vals != null && vals.size() > 0) || (additionalUpdateFields != null && additionalUpdateFields.length() > 0))) {
				
				sql = "UPDATE " + qh.getMysqlRealScapeString(tableName)+ " SET ";
				sql += qh.getUpdateQueryFromHashMap(vals);
				
				if (additionalUpdateFields != null && additionalUpdateFields.length() > 0) {
					if (vals != null && vals.size() > 0) {
						sql += ",";
					}
					sql += additionalUpdateFields;
				}
				sql += " WHERE " + qh.getMysqlRealScapeString(colName) + qh.getMysqlRealScapeString(matchType) + " '" + qh.getMysqlRealScapeString(value) + "' ";
				
				if (condition != null && condition.length() > 0) {
					sql += condition;
				}
				logger.debug(sql);
				pstmt = connection.prepareStatement(sql);
				if (pstmt.executeUpdate() > 0) {
					ro.setIsSuccessful(true);
				}
			} else {
				ro.setActionMessage(ActionMessages.INVALID_REQUEST);
				ro.setMessage(new Messages().setErrorMessage(ActionMessages.INVALID_REQUEST.getMsg()));
				throw new SQLException("Invalid query format");
			}
		
		} 
		catch (SQLException e) {
			logger.fatal("SQLException", e);
		} 
		catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} 
		catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		}
		catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		}
		catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		}
		finally {
			FinalCleanUp.closeResource(pstmt);
			try{
				if(isDialerRegistration) {
					//DatabaseManager.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
				else {
					//DatabaseManagerSuccessful.getInstance().freeConnection(connection);
					if(connection!=null){
						connection.close();
					} 
				}
					
			}
			catch(Exception exx){}
		}
		return ro;
	}

}
