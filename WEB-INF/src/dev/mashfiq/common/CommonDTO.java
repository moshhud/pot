package dev.mashfiq.common;

import java.io.File;
import java.util.LinkedHashMap;

import dev.mashfiq.util.Validations;

public class CommonDTO {

	@Column(name="id")
	private int id;
	
	@Column(name="created_by", type="int")
	private int createdBy;
	
	@Column(name="creation_time")
	private String creationTime;
	
	@Column(name="table_name")
	private String tableName;
	
	@Column(name="table_id", type="int")
	private int tableId;
	
	@Column(name="file_name")
	private String fileFileName;
	
	private String captcha;
	private File file;

	
	protected Validations v = new Validations();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	
	public String getCreationTime() {
		return v.checkDTO(creationTime);
	}

	public void setCreationTime(String creationTime) {
		this.creationTime = creationTime;
	}

	public String getTableName() {
		return v.checkDTO(tableName);
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public int getTableId() {
		return tableId;
	}

	public void setTableId(int tableId) {
		this.tableId = tableId;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileFileName() {
		return v.checkDTO(fileFileName);
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}
	
	
}