package dev.mashfiq.common;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.revesoft.po.revesoft.users.RimsUsersDTO;

import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.ReturnObject;

public abstract class MasterAction extends ActionSupport {

	private static final long serialVersionUID = 1L;
	protected ActionContext actionContext;
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected HttpSession session;
	protected String resultName = ERROR;
	protected RimsUsersDTO rimsUsersDTO;
	protected final String PERMISSION_DENIED = "permissionDenied";
	protected final String INPUT = "input";
	protected final String SEARCH = "search";
	protected String result = SEARCH;
	protected ReturnObject ro = null;
	protected long startTime = 0;

	public MasterAction() {
		actionContext = ActionContext.getContext();
		request = (HttpServletRequest) actionContext.get(ServletActionContext.HTTP_REQUEST);
		response = (HttpServletResponse) actionContext.get(ServletActionContext.HTTP_RESPONSE);
		session = request.getSession();
		rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
	}

	public abstract String add();
	
	public abstract String goToSearch();

	public abstract String search();

	public abstract String get();

	public abstract String update();

}
