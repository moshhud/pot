package dev.mashfiq.common;

import java.util.ArrayList;
import java.util.HashMap;

import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public abstract class MasterDAO {
	
	protected ReturnObject ro;
	
	abstract protected ReturnObject insert(Object obj);

	abstract protected ReturnObject getMap(HashMap<String, ArrayList<String>> vals,String condition, String keyColumn);
	
	abstract protected ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,String condition);

	abstract protected ReturnObject update(Object obj);
	
	abstract protected boolean validate(Object obj);

	protected abstract boolean isExisting();
	
	protected QueryHelper qh = new QueryHelper();
	protected DateAndTimeHelper dth;

}
