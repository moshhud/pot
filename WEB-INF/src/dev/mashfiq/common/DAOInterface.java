package dev.mashfiq.common;

import java.util.ArrayList;
import java.util.HashMap;

import dev.mashfiq.util.ReturnObject;

public interface DAOInterface {

	ReturnObject insert(Object obj);

	ReturnObject getMap(HashMap<String, ArrayList<String>> vals,String condition, String keyColumn);
	
	ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,String condition);

	ReturnObject update(Object obj);
	
	boolean validate(Object obj);

	boolean isExisting();

}
