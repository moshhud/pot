package com.revesoft.po.revesoft.users;

import java.util.HashMap;

import javax.servlet.http.HttpSession;

import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.FileUploadHelper;
import dev.mashfiq.util.ReturnObject;

public class RimsUsersService {
	
	public RimsUsersDTO getDTO(int usrID, boolean getFullDetails) {
		RimsUsersDTO dto = RimsUsersRepository.getInstance(false).getDTOByUsrID(usrID);
		if (dto != null && getFullDetails) {
			
		}
		return dto;
	}
	
	public ReturnObject update(RimsUsersDTO dto, HttpSession session) {
		FileUploadHelper fuh = new FileUploadHelper();
		boolean isUploaded = true;
		HashMap<String, String> uploadedResult;
		if (dto.getUsrSignature() != null && dto.getUsrSignature().length() > 0) {
			uploadedResult = fuh.upload(dto.getUsrSignature(),
					dto.getUsrSignatureFileName(),session.getServletContext().getRealPath("/")+
					FileUploadHelper.RIMS_USER_FILES);
			if(uploadedResult != null && uploadedResult.size() > 0) {
				if(FileUploadHelper.FILE_UPLOADED.equalsIgnoreCase(uploadedResult.get(FileUploadHelper.STATUS))){
					dto.setUsrSignatureFileName(uploadedResult.get(FileUploadHelper.FILE_NAME));
				}else{
					isUploaded = false;
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE, uploadedResult.get(FileUploadHelper.STATUS));
				}
			}
		}
	
		return new RimsUsersDAO().update(dto);
	}

}
