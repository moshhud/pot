package com.revesoft.po.revesoft.users;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.MasterAction;
import dev.mashfiq.mail.MailService;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Messages;
import dev.mashfiq.util.Permissions;
import dev.mashfiq.util.RecordNavigation;
import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.SearchPanels;
import dev.mashfiq.util.SearchQueryBuilder;
import dev.mashfiq.util.StringHelper;
import dev.mashfiq.util.Validations;

public class RimsUsersAction extends MasterAction implements
		ModelDriven<RimsUsersDTO>, Preparable {

	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(RimsUsersAction.class.getName());
	private RimsUsersDTO dto;

	@Override
	public void prepare() throws Exception {
		dto = new RimsUsersDTO();
	}

	@Override
	public RimsUsersDTO getModel() {
		return dto;
	}

	public String newRimsUsers() {
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_THREE) {
			result = INPUT;
		} else {
			result = PERMISSION_DENIED;
		}
		return result;
	}

	@Override
	public String add() {
		result = SEARCH;
		String password = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_THREE) {
			if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
				password = new StringHelper().generatePassword();
				dto.setUsrPassword(password);
				ro = new RimsUsersDAO().insert(dto);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						dto.setUsrPassword(password);
						new MailService().sendEmployeeProfileMail(dto);
						RimsUsersRepository.getInstance(true);
						new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(), RimsUsersDAO.TABLE_NAME, dto.getId(), "added", "add");
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_ADDED);
					} else {
						ro.setActionMessage(session);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.SYSTEM_ERROR);
				}
			}
		} else {
			result = PERMISSION_DENIED;
		}
		return result;
	}

	public String goToSearch() {
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_TWO) {
			new RecordNavigation(SearchPanels.RIMS_USERS, request,
					"search-users.html");
			result = SEARCH;
		} else {
			result = PERMISSION_DENIED;
		}
		return result;
	}

	@Override
	public String search() {
		return searchUser("",true);
	}
	
	public String searchUser(String additionalCondition,boolean ignoreSearch) {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		String[][] searchPanel = SearchPanels.RIMS_USERS;
		String condition = "";
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			ro = new SearchQueryBuilder().getSearchQuery(request, session,
					searchPanel, RimsUsersDAO.TABLE_NAME,
					rimsUsersDTO.getUsrID());
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					
					condition = ro.getData() + "";
					logger.debug("condition: "+condition);
					if (ignoreSearch) {
						count = new CommonDAO().getCount(
								RimsUsersDAO.TABLE_NAME,
								RimsUsersDAO.DEFAULT_KEY_COLUMN, condition,
								false);
						logger.debug("Count: "+count);
						if (count > 0) {
							map = rn.getRecordNavigation(count);
							if (map != null && map.size() > 0) {
								condition += " ORDER BY usrName LIMIT "
										+ map.get(RecordNavigation.START_INDEX)
										+ "," + map.get(RecordNavigation.LIMIT);
								ro = new RimsUsersDAO().getMap(null, condition,
										null);
								if (ro != null) {
									if (ro.getIsSuccessful()) {
										session.setAttribute(
												ApplicationConstant.ACTION_DATA,
												ro.getData());
									} else {
										session.setAttribute(
												ApplicationConstant.ACTION_MESSAGE,
												ActionMessages.NO_DATA_FOUND);
									}
								} else {
									session.setAttribute(
											ApplicationConstant.ACTION_MESSAGE,
											ActionMessages.SYSTEM_ERROR);
								}
							} else {
								session.setAttribute(
										ApplicationConstant.ACTION_MESSAGE,
										ActionMessages.SYSTEM_ERROR);
							}
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.NO_DATA_FOUND);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.USE_SEARCH);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.SYSTEM_ERROR);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	@Override
	public String get() {
		startTime = System.currentTimeMillis();
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_TWO) {
			dto = RimsUsersRepository.getInstance(false).getDTOByUsrID(
					dto.getUsrID());
			if (dto != null) {
				result = INPUT;
				new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
						RimsUsersDAO.TABLE_NAME, dto.getUsrID(), "viewed",
						"get");
				session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.NO_DATA_FOUND);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For get " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	@Override
	public String update() {
		startTime = System.currentTimeMillis();
		RimsUsersDTO oldDTO = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_FOUR) {
			if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
				oldDTO = RimsUsersRepository.getInstance(false).getDTOByUsrID(
						dto.getUsrID());
				if (oldDTO != null) {
					ro = new RimsUsersDAO().update(dto);
					if (ro != null) {
						if (ro.getIsSuccessful()) {
							RimsUsersRepository.getInstance(true);
							new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
									RimsUsersDAO.TABLE_NAME, dto.getUsrID(),
									oldDTO, dto, "update");
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SUCCESSFULLY_UPDATED);
						} else {
							ro.setActionMessage(session);
						}
					} else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For update " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}
	
	public String getProfile() {
		startTime = System.currentTimeMillis();
		dto = RimsUsersRepository.getInstance(false).getDTOByUsrID(rimsUsersDTO.getUsrID());
		if (dto != null) {
			result = INPUT;
			new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
					RimsUsersDAO.TABLE_NAME, dto.getUsrID(), "viewed",
					"getProfile");
			session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.NO_DATA_FOUND);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getProfile " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}
	
	public String updateProfile() {
		startTime = System.currentTimeMillis();
		RimsUsersDTO oldDTO = null;
		if (dto.getUsrID() == rimsUsersDTO.getUsrID()) {
			if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
				oldDTO = RimsUsersRepository.getInstance(false).getDTOByUsrID(
						dto.getUsrID());
				if (oldDTO != null) {
					dto.setCurrentStatus(oldDTO.getCurrentStatus());					
					dto.setUsrRoleId(oldDTO.getUsrRoleId());					
					ro = new RimsUsersService().update(dto, session);
					if (ro != null) {
						if (ro.getIsSuccessful()) {
							RimsUsersRepository.getInstance(true);
							new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
									RimsUsersDAO.TABLE_NAME, dto.getUsrID(),
									oldDTO, dto, "updateProfile");
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SUCCESSFULLY_UPDATED);
						} else {
							ro.setActionMessage(session);
						}
					} else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					new Messages().setErrorMessage("Are you trying to update someone else's profile!"));
			new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(), RimsUsersDAO.TABLE_NAME, dto.getUsrID(), "Illegal Access", "updateProfile");
			new MailService().sendErrorMail("Illegal Access updateProfile: " + rimsUsersDTO.getUsrEmail() + " tried to update " + dto.getUsrID());
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For updateProfile " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}
	
	

}
