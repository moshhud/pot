package com.revesoft.po.revesoft.users;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.Messages;
import dev.mashfiq.util.NumericHelper;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ResultSetToDTOMapper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.Validations;
import dev.mashfiq.util.password.MD5Crypt;

public class RimsUsersDAO implements DAOInterface {

	public static final String TABLE_NAME = "poUsers";
	public static final String DEFAULT_KEY_COLUMN = "usrID";

	private Logger logger = Logger.getLogger(RimsUsersDAO.class.getName());
	private QueryHelper qh = new QueryHelper();
	private ReturnObject ro;
	private RimsUsersDTO dto;
	private CommonDAO commonDAO;

	@SuppressWarnings("unchecked")
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		long startTime = System.currentTimeMillis();
		ArrayList<RimsUsersDTO> list = null;
		ro = getMap(vals, condition, null);
		LinkedHashMap<String, RimsUsersDTO> data = null;
		if (ro != null && ro.getIsSuccessful()) {
			try {
				data = (LinkedHashMap<String, RimsUsersDTO>) ro.getData();
				if (data != null && data.size() > 0) {
					list = new ArrayList<RimsUsersDTO>();
					for (Entry<String, RimsUsersDTO> entry : data.entrySet()) {
						list.add(entry.getValue());
					}
					if (list != null && list.size() > 0) {
						ro.setData(list);
					}
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getArrayList: "
					+ (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		long startTime = System.currentTimeMillis();
		String sql = "SELECT * FROM " + TABLE_NAME + " WHERE usrID!=0 "
				+ qh.getQueryFromHashMapCondition(vals, condition);
		keyColumn = qh.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
		ro = new ResultSetToDTOMapper<RimsUsersDTO>().getResultSetToDTOMap(sql,
				false, keyColumn, RimsUsersDTO.class);
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getMap " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	public ReturnObject insert(Object obj) {
		ro = new ReturnObject();
		Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			if (validate(obj)) {
				if (isExisting() == false) {
					sql = "INSERT INTO "
							+ TABLE_NAME
							+ " (usrName,usrUserID,usrPassword,usrEmail,office_phone,"
							+ "usrRoleId,current_status) "
							+ "VALUES (?,?,?,?,?,?,?)";
					connection = DatabaseManagerSuccessful.getInstance()
							.getConnection();
					pstmt = connection.prepareStatement(sql,
							PreparedStatement.RETURN_GENERATED_KEYS);
					
					int i=1;					
					pstmt.setString(i++, dto.getUsrName());
					pstmt.setString(i++, dto.getUsrName());//usrUserID
					pstmt.setString(i++, MD5Crypt.crypt(dto.getUsrPassword()));
					pstmt.setString(i++, dto.getUsrEmail());					
					pstmt.setString(i++, dto.getOfficePhone());						
					pstmt.setInt(i++, dto.getUsrRoleId());
					//pstmt.setString(i++, dto.getUsrSecurityToken());
					pstmt.setString(i++, dto.getCurrentStatus());
					if (pstmt.executeUpdate() > 0) {
						rs = pstmt.getGeneratedKeys();
						if (rs.next()) {
							ro.setIsSuccessful(true);
							dto.setId(rs.getInt(1));
							dto.setUsrID(rs.getInt(1));
						}
					}
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeResource(rs);
			//FinalCleanUp.closeDatabaseConnection(connection);
			try {
				if(connection!=null){
					connection.close();
				}
			}catch(Exception e) {
				logger.fatal(e.toString());
			}
		}
		return ro;
	}

	public boolean validate(Object obj) {
		boolean isValid = false;
		Validations v = new Validations();
		ArrayList<Messages> msgList = new ArrayList<Messages>();
		if (obj != null && obj instanceof RimsUsersDTO) {
			dto = (RimsUsersDTO) obj;
			if (dto != null) {
				isValid = true;
				if (v.checkEmail(dto.getUsrEmail()) == false) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Invalid Email"));
				}
				if (v.checkInput(dto.getUsrName()) == false) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Invalid Name"));
				}
				if (dto.getUsrRoleId() <= 0) {
					isValid = false;
					msgList.add(new Messages().setErrorMessage("Select Role"));
				}
				/*if (NumericHelper.parseInt(dto.getUsrSecurityToken()) <= 0) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Invalid Security Token"));
				}*/
				if (v.isValidPassword(dto.getUsrPassword())== false) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Invalid Password"));
				}
				/*if (v.checkPhoneNumber(dto.getOfficePhone(), 8) == false) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Enter Phone"));
				}*/
				
				if (v.checkInput(dto.getCurrentStatus()) == false) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Select current status"));
				}
				
			}
		}
		if (msgList != null && msgList.size() > 0) {
			ro.setMsgList(msgList);
		}
		return isValid;
	}

	@Override
	public ReturnObject update(Object obj) {
		ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		String sql = null;
		try {
			if (validate(obj)) {
				if(dto.getUsrEmail().equalsIgnoreCase(dto.getCurrentEmail()) || isExisting() == false) {
					if (dto.getUsrPassword().equals(dto.getCurrentPassword()) == false) {
						dto.setUsrPassword(MD5Crypt.crypt(dto.getUsrPassword()));
					}					
					
					sql = "UPDATE "
							+ TABLE_NAME
							+ " SET usrName=?,usrUserID=?,"
							+ "usrEmail=?,office_phone=?,"
							+ "usrPassword=?,usrRoleId=?,usrDepartmentId=?,current_status=? "
							+ "WHERE usrID=?";
					connection = DatabaseManagerSuccessful.getInstance()
							.getConnection();
					pstmt = connection.prepareStatement(sql);
					int i=1;
					
					pstmt.setString(i++, dto.getUsrName());
					pstmt.setString(i++, dto.getUsrName());//usrUserID
					pstmt.setString(i++, dto.getUsrEmail());					
					pstmt.setString(i++, dto.getOfficePhone());	
					pstmt.setString(i++, dto.getUsrPassword());					
					pstmt.setInt(i++, dto.getUsrRoleId());	
					pstmt.setInt(i++, dto.getDepartment());	
					pstmt.setString(i++, dto.getCurrentStatus());					
					pstmt.setInt(i++, dto.getUsrID());
					
					if (pstmt.executeUpdate() > 0) {
						ro.setIsSuccessful(true);
					}
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			//FinalCleanUp.closeDatabaseConnection(connection);
			try {
				if(connection!=null){
					connection.close();
				}
			}catch(Exception e) {
				logger.fatal(e.toString());
			}
		}
		return ro;
	}

	@Override
	public boolean isExisting() {
		boolean isExisting = true;
		if (dto != null) {
			if (commonDAO == null) {
				commonDAO = new CommonDAO();
			}
			if (qh == null) {
				qh = new QueryHelper();
			}
			if (commonDAO.getCount(TABLE_NAME, DEFAULT_KEY_COLUMN, " AND usrEmail='" + qh.getMysqlRealScapeString(dto.getUsrEmail()) + "' ", false) == 0) {
				isExisting = false;
			}
		}
		return isExisting;
	}

	@SuppressWarnings("unused")
	public static void main(String args[]) {
		ReturnObject ro = new RimsUsersDAO().getMap(null, " LIMIT 1000", null);
		System.out.println("Done");
	}

}
