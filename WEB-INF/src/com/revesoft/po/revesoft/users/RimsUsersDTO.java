package com.revesoft.po.revesoft.users;

import java.io.File;
import java.util.LinkedHashMap;



import dev.mashfiq.common.Column;
import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.ApplicationConstant;

public class RimsUsersDTO extends CommonDTO {

	private LinkedHashMap<Integer, Integer> permissionMap;

	@Column(name = "usrID")
	private int usrID;

	@Column(name = "usrName")
	private String usrName;

	@Column(name = "usrUserID")
	private String usrUserID;

	@Column(name = "usrPassword")
	private String usrPassword;
	
	@Column(name = "usrEmail")
	private String usrEmail; 
	
	private String currentEmail;

	@Column(name = "usrSignatureFileName")
	private String usrSignatureFileName;

	@Column(name = "usrRoleId")
	private int usrRoleId;

	@Column(name = "current_status")
	private String currentStatus;


	@Column(name = "usrDepartmentId")
	private int department;	
	
	@Column(name = "office_phone")
	private String officePhone;

	

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getCurrentStatus() {
		return v.checkDTO(currentStatus, ApplicationConstant.ACTIVE);
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	private String currentPassword;
	private File usrSignature;
	private File profileImage;

	
	@Override
	public int getId() {
		return usrID;
	}

	public int getUsrID() {
		return usrID;
	}

	public void setUsrID(int usrID) {
		this.usrID = usrID;
	}

	public String getUsrName() {
		return v.checkDTO(usrName);
	}

	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}

	public String getUsrUserID() {
		return v.checkDTO(usrUserID);
	}

	public void setUsrUserID(String usrUserID) {
		this.usrUserID = usrUserID;
	}

	public String getUsrPassword() {
		return v.checkDTO(usrPassword);
	}

	public void setUsrPassword(String usrPassword) {
		this.usrPassword = usrPassword;
	}

	public String getUsrEmail() {
		return v.checkDTO(usrEmail);
	}

	public void setUsrEmail(String usrEmail) {
		this.usrEmail = usrEmail;
	}

	public String getUsrSignatureFileName() {
		return v.checkDTO(usrSignatureFileName);
	}

	public void setUsrSignatureFileName(String usrSignatureFileName) {
		this.usrSignatureFileName = usrSignatureFileName;
	}

	public int getUsrRoleId() {
		return usrRoleId;
	}

	public void setUsrRoleId(int usrRoleId) {
		this.usrRoleId = usrRoleId;
	}

	

	public int getDepartment() {
		return department;
	}

	public void setDepartment(int department) {
		this.department = department;
	}

	
	

	public String getCurrentPassword() {
		return v.checkDTO(currentPassword);
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}

	public LinkedHashMap<Integer, Integer> getPermissionMap() {
		return permissionMap;
	}

	public void setPermissionMap(LinkedHashMap<Integer, Integer> permissionMap) {
		this.permissionMap = permissionMap;
	}

	public int getPermissionLevelByModuleId(int moduleId) {
		int permissionLevel = 0;
		if (permissionMap != null && moduleId > 0
				&& permissionMap.containsKey(moduleId)) {
			permissionLevel = permissionMap.get(moduleId);
		}
		return permissionLevel;
	}



	public String getCurrentEmail() {
		return v.checkDTO(currentEmail);
	}

	public void setCurrentEmail(String currentEmail) {
		this.currentEmail = currentEmail;
	}

	public File getUsrSignature() {
		return usrSignature;
	}

	public void setUsrSignature(File usrSignature) {
		this.usrSignature = usrSignature;
	}

	public File getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(File profileImage) {
		this.profileImage = profileImage;
	}
	
}
