package com.revesoft.po.revesoft.users;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;



import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.Validations;

public class RimsUsersRepository {
	
	public static final int USRID_RAIHAN_HOSSAIN = 1000004;

	static {
		mdUserRepository = new RimsUsersRepository();
	}

	private Logger logger = Logger.getLogger(RimsUsersRepository.class
			.getName());
	private static RimsUsersRepository mdUserRepository;
	private ArrayList<RimsUsersDTO> list;

	@SuppressWarnings("unchecked")
	private RimsUsersRepository() {
		ReturnObject ro = new RimsUsersDAO().getArrayList(null, " ORDER By usrEmail");
		if (ro != null && ro.getIsSuccessful()
				&& ro.getData() instanceof ArrayList) {
			try {
				list = (ArrayList<RimsUsersDTO>) ro.getData();
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
	}

	public static RimsUsersRepository getInstance(boolean forceReload) {
		if (forceReload || mdUserRepository == null) {
			mdUserRepository = new RimsUsersRepository();
		}
		return mdUserRepository;
	}

	
	
	
	public ArrayList<RimsUsersDTO> getAll(String currentStatus) {
		ArrayList<RimsUsersDTO> data = null;
		if (list != null && list.size() > 0) {
			data = new ArrayList<RimsUsersDTO>(list.size());
			for (RimsUsersDTO dto : list) {
				if (dto != null
						&& (currentStatus == null || currentStatus
								.equalsIgnoreCase(dto.getCurrentStatus()))) {
					data.add(dto);
				}
			}
		}
		return data;
	}

	public RimsUsersDTO getDTOByUsrID(int usrID) {
		RimsUsersDTO data = null;
		if (usrID > 0 && list != null && list.size() > 0) {
			for (RimsUsersDTO dto : list) {
				if (dto != null && dto.getUsrID() == usrID) {
					data = dto;
					break;
				}
			}
		}
		return data;
	}
	
	public ArrayList<RimsUsersDTO> getListByRole(int role, String currentStatus) {
		ArrayList<RimsUsersDTO> data = getAll(currentStatus);
		ArrayList<RimsUsersDTO> list = null; 
		if (data != null && data.size() > 0) {
			list = new ArrayList<RimsUsersDTO>();
			for (RimsUsersDTO dto : data) {
				if (dto != null
						&& dto.getUsrRoleId() == role) {
					list.add(dto);
				}
			}
		}
		return list;
	}
	
	public ArrayList<RimsUsersDTO> getListByDepartment(int department, String currentStatus) {
		ArrayList<RimsUsersDTO> data = getAll(currentStatus);
		ArrayList<RimsUsersDTO> list = null; 
		if (data != null && data.size() > 0) {
			list = new ArrayList<RimsUsersDTO>();
			for (RimsUsersDTO dto : data) {
				if (dto != null
						&& dto.getDepartment() == department) {
					list.add(dto);
				}
			}
		}
		return list;
	}
	
	

	public RimsUsersDTO getDTOByUsrEmail(String usrEmail) {
		RimsUsersDTO data = null;
		Validations v = new Validations();
		if (v.checkEmail(usrEmail) && list != null && list.size() > 0) {
			for (RimsUsersDTO dto : list) {
				if (dto != null
						&& v.checkEqualsIgnoreCase(dto.getUsrEmail(), usrEmail)) {
					data = dto;
					break;
				}
			}
		}
		return data;
	}

	public String getUsrUserIDByUsrID(int usrID) {
		String usrUserID = "N/A";
		RimsUsersDTO dto = getDTOByUsrID(usrID);
		if (dto != null) {
			usrUserID = dto.getUsrEmail().replace("@revesoft.com", "");
			
		}
		return usrUserID;
	}
	
	

}
