package com.revesoft.po.revesoft.orderlist;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jdom.JDOMException;
import com.revesoft.po.revesoft.users.RimsUsersDTO;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.MasterDAO;
import dev.mashfiq.mashupData.MashupDataRepository;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.RimsUpdateLogHelper;
import java.io.File;


public class OrderListDAO extends MasterDAO{
	private Logger logger = Logger.getLogger(OrderListDAO.class.getName());
	public static final String INVOICE_TABLE_NAME = "poInvoice";
	public static final String DEFAULT_KEY_COLUMN = "invcID";
	public static final String CLIENT_TABLE_NAME = "poClients";
	public static final String PO_TABLE_NAME = "poProductDtails";
	public static final String DEFAULT_PO_KEY_COLUMN = "pdID";
	private OrderListDTO dto;

	@Override
	protected ReturnObject insert(Object obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ReturnObject getMap(HashMap<String, ArrayList<String>> vals, String condition, String keyColumn) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis(), t1;
		ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		LinkedHashMap<String, OrderListDTO> data = null;
		OrderListDTO dto= null;
		DateAndTimeHelper dth = null;
		
		try {
			sql = "select *from "+INVOICE_TABLE_NAME+","+CLIENT_TABLE_NAME+" where invcIsDeleted=0 and clID=invc_clID ";
			sql += qh.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += " " + condition;
			} else {
				sql += " ORDER BY  invcID DESC";
			}
			if (keyColumn == null || keyColumn.length() == 0) {
				keyColumn = DEFAULT_KEY_COLUMN;
			} else {
				keyColumn = qh.getMysqlRealScapeString(keyColumn);
			}
			
			logger.debug("getMap: "+sql);
			t1 = System.currentTimeMillis();
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
			logger.debug("Database Execution time " + startTime + " : " + (System.currentTimeMillis() - t1));
			data = new LinkedHashMap<String, OrderListDTO>();
			dth = new DateAndTimeHelper();
			while (rs.next()) {
				dto = new OrderListDTO();
				dto.setInvcID(rs.getInt("invcID"));
				dto.setClID(rs.getInt("invc_clID"));
				dto.setPurchaseOrderNumber(rs.getString("invcPONumber"));
				dto.setCurrencyID(rs.getString("invc_cID"));
				dto.setOrderDate(dth.getDateFromLong(rs.getLong("invcOrderDate")));
				dto.setDeliveryDate(dth.getDateFromLong(rs.getLong("invcDeliveryDate")));
				dto.setInvcAmount(rs.getDouble("invcAmount"));
				dto.setInvcDue(rs.getDouble("invcDue"));
				dto.setInvcReceived(rs.getDouble("invcReceived"));
				dto.setInvcInvoiceStatus(rs.getInt("invcInvoiceStatus"));
				dto.setInvcPOStatus(rs.getInt("invcPOStatus"));
				dto.setUploadDocFileName(rs.getString("invcUploadedFileName"));
				
				dto.setCustomerName(rs.getString("clName"));
				dto.setCustomerEmail(rs.getString("clEmail"));
				dto.setCompanyName(rs.getString("clCompanyName"));
				dto.setCustomerPhone(rs.getString("clPhone"));
				dto.setBillingAddress(rs.getString("clBillingAddress"));
				dto.setShippingAddress(rs.getString("clShippingAddress"));
				data.put(rs.getString(keyColumn), dto);	
				
			}
			
			rs.close();
			pstmt.close();
			
			if (data != null && data.size() > 0) {
				ro.setData(data);
				ro.setIsSuccessful(true);
			}	
			
			
		}
		catch(Exception e) {
			logger.fatal(e.toString());
		}
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
			
		}
		
		return ro;
	}

	@Override
	protected ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals, String condition) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ReturnObject update(Object obj) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected boolean validate(Object obj) {
		// TODO Auto-generated method stub
		boolean isValid = false;
		if(obj != null && obj instanceof OrderListDTO) {
			dto = (OrderListDTO)obj;
			if(dto != null) {
				isValid = true;
			}
		}
		return isValid;
	}

	@Override
	protected boolean isExisting() {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	protected ReturnObject getMapProductDetais(HashMap<String, ArrayList<String>> vals, String condition, String keyColumn) {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis(), t1;
		ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		LinkedHashMap<String, PoDetailsDTO> data = null;
		PoDetailsDTO dto= null;
		DateAndTimeHelper dth = null;
		
		try {
			sql = "select *from "+PO_TABLE_NAME+" where 1=1 ";
			sql += qh.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += " " + condition;
			} else {
				sql += " ORDER BY  pdID DESC";
			}
			if (keyColumn == null || keyColumn.length() == 0) {
				keyColumn = DEFAULT_PO_KEY_COLUMN;
			} else {
				keyColumn = qh.getMysqlRealScapeString(keyColumn);
			}
			
			logger.debug("getMapProductDetais: "+sql);
			t1 = System.currentTimeMillis();
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
			logger.debug("Database Execution time " + startTime + " : " + (System.currentTimeMillis() - t1));
			data = new LinkedHashMap<String, PoDetailsDTO>();
			dth = new DateAndTimeHelper();
			while (rs.next()) {
				dto = new PoDetailsDTO();
				dto.setPdID(rs.getInt("pdID"));
				dto.setPd_invcID(rs.getInt("pd_invcID"));
				dto.setPdPONumber(rs.getLong("pdPONumber"));
				dto.setPd_plID(rs.getInt("pd_plID"));
				dto.setPdQuantity(rs.getInt("pdQuantity"));
				dto.setPdPrice(rs.getDouble("pdPrice"));
				dto.setPdShippingCharge(rs.getDouble("pdShippingCharge"));
				dto.setPdTax(rs.getDouble("pdTax"));
				dto.setPdTag(rs.getString("pdTag"));
				data.put(rs.getString(keyColumn), dto);	
				
			}
			
			rs.close();
			pstmt.close();
			
			if (data != null && data.size() > 0) {
				ro.setData(data);
				ro.setIsSuccessful(true);
			}	
			
			
		}
		catch(Exception e) {
			logger.fatal(e.toString());
		}
		finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
			
		}
		
		return ro;
	}
	
	protected ReturnObject insert(RimsUsersDTO rimsUsersDTO,Object obj) {
		ro = new ReturnObject();
		Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		ArrayList<PoDetailsDTO> poDetailsList = null;
		PoDetailsDTO pdlDTO=null;
		
		if(validate(obj)) {
			
			try {
				dto.setClID((int) DatabaseManagerSuccessful.getInstance().getNextSequenceId(CLIENT_TABLE_NAME));
				dto.setInvcID((int) DatabaseManagerSuccessful.getInstance().getNextSequenceId(INVOICE_TABLE_NAME));
				
				int quantity=0;
				int productID=0;
				double price = 0.0;
				double ship = 0.0;
				double tax = 0.0;
				double total = 0.0;
				double total2 = 0.0;
				double grandTotal = 0.0;
				boolean insertPO = false;
				
				if(dto.getProductDetailsInfo()!=null && dto.getProductDetailsInfo().length>0) {
					
					String str[] = dto.getProductDetailsInfo();
					poDetailsList = new ArrayList<PoDetailsDTO>(str.length);
					for(String s:str) {
						logger.debug(s);
						dto.setPdID((int) DatabaseManagerSuccessful.getInstance().getNextSequenceId(PO_TABLE_NAME));
						pdlDTO = new PoDetailsDTO();
						String arr[] = s.split(":");		
						productID = Integer.parseInt(arr[0]);
						price = Double.parseDouble(arr[1]);
						quantity = Integer.parseInt(arr[2]);
						ship = Double.parseDouble(arr[3]);
						tax = Double.parseDouble(arr[4]);
						total = quantity*price+ship;
						total2 = total*(1+tax*1.0/100);
						grandTotal = grandTotal+total2;
						
						pdlDTO.setPdID(dto.getPdID());
						pdlDTO.setPd_invcID(dto.getInvcID());
						pdlDTO.setPdPONumber(Long.parseLong(dto.getPurchaseOrderNumber()));
						pdlDTO.setPd_plID(productID);
						pdlDTO.setPdQuantity(quantity);
						pdlDTO.setPdPrice(price);					
						pdlDTO.setPdShippingCharge(ship);
						pdlDTO.setPdTax(tax);
						pdlDTO.setPdTag("");
						poDetailsList.add(pdlDTO);					
						
					}
					logger.debug(grandTotal);
					insertPO = true;
				}
				else {
					logger.debug("Product details not found.");
				}
				
				sql = "INSERT INTO "
			            +CLIENT_TABLE_NAME
						+" (clID,clName,clCompanyName,clEmail,"
						+ "clPhone,clShippingAddress,clBillingAddress)"
						+ " VALUES(?,?,?,?,?,?,?)";
				
				connection = DatabaseManagerSuccessful.getInstance()
						.getConnection();
				pstmt = connection.prepareStatement(sql);
				
				int i=1;	
				
				pstmt.setInt(i++, dto.getClID());
				pstmt.setString(i++, dto.getCustomerName());
				pstmt.setString(i++, dto.getCompanyName());
				pstmt.setString(i++, dto.getCustomerEmail());
				pstmt.setString(i++, dto.getCustomerPhone());
				pstmt.setString(i++, dto.getShippingAddress());
				pstmt.setString(i++, dto.getBillingAddress());
				if (pstmt.executeUpdate() > 0) {
					ro.setIsSuccessful(true);
					logger.debug("Data inserted by : "+rimsUsersDTO.getId());
				}
				pstmt.close();
				
				MashupDataRepository mudRepository = MashupDataRepository.getInstance(false);
				sql = "INSERT INTO "
						+INVOICE_TABLE_NAME
						+"(invcID,invcPONumber,invc_clID,invc_cID,invcOrderDate,invcDeliveryDate,"
						+ "invcAmount,invcDue,invcReceived,invcUploadedFileName,invcIsDeleted,"
						+ "invcInvoiceStatus,invcPOStatus)"
						+ " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)"
						;
				
				DateAndTimeHelper ob = new DateAndTimeHelper();
				long d = ob.getLongFromDate(dto.getOrderDate());
				
				pstmt = connection.prepareStatement(sql);
				i=1;
				pstmt.setInt(i++, dto.getInvcID());
				pstmt.setLong(i++, Long.parseLong(dto.getPurchaseOrderNumber()));
				pstmt.setInt(i++, dto.getClID());
				pstmt.setInt(i++,Integer.parseInt(dto.getCurrencyID()));
				pstmt.setLong(i++, d);
				d = ob.getLongFromDate(dto.getDeliveryDate());
				pstmt.setLong(i++, d);
				pstmt.setDouble(i++, grandTotal);
				pstmt.setDouble(i++, grandTotal);
				pstmt.setDouble(i++, 0.0);
				pstmt.setString(i++, dto.getUploadDocFileName());
				pstmt.setInt(i++, 0);
				pstmt.setInt(i++, mudRepository.INVOICE_INITIAL_STATUS);
				pstmt.setInt(i++, mudRepository.PO_INITIAL_STATUS);
				
				if (pstmt.executeUpdate() > 0) {
					ro.setIsSuccessful(true);
				}
				pstmt.close();
				
				if(insertPO) {
					sql = "INSERT INTO "
							+PO_TABLE_NAME
							+" (pdID,pd_invcID,pdPONumber,pd_plID,pdQuantity,"
							+ "pdPrice,pdShippingCharge,pdTax,pdTag)"
							+ " VALUES(?,?,?,?,?,?,?,?,?)";
					pstmt = connection.prepareStatement(sql);				
					for(PoDetailsDTO pDTO:poDetailsList) {
						i=1;
						pstmt.setInt(i++, pDTO.getPdID());
						pstmt.setInt(i++, pDTO.getPd_invcID());
						pstmt.setLong(i++, pDTO.getPdPONumber());
						pstmt.setInt(i++, pDTO.getPd_plID());
						pstmt.setInt(i++, pDTO.getPdQuantity());
						pstmt.setDouble(i++, pDTO.getPdPrice());
						pstmt.setDouble(i++, pDTO.getPdShippingCharge());
						pstmt.setDouble(i++, pDTO.getPdTax());
						pstmt.setString(i++, "");
						pstmt.addBatch();
					}
					
					if (pstmt.executeBatch().length > 0) {
						ro.setIsSuccessful(true);
					} else {
						ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
					}
					pstmt.close();
				}				
				
				
				if(dto.getUploadDocFileName()!=null && dto.getUploadDocFileName().length()>0){
					String fileName = dto.getUploadDocFileName();
					String filePath = ApplicationConstant.PROJECT_PATH + ApplicationConstant.PO_FILES;
					
					logger.debug(filePath);
					try {
						File file= dto.getUploadDoc();
						File uploadFile  = new File(filePath, fileName);
						FileUtils.copyFile(file, uploadFile);
						logger.debug("File Uploaded"+fileName);
					}catch (Exception e) {
						logger.fatal(e.toString());
						
					}
				}
				
				
				
			} catch (NullPointerException e) {
				logger.fatal("NullPointerException", e);
			} catch (SQLException e) {
				ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
				logger.fatal("SQLException", e);
			} catch (JDOMException e) {
				logger.fatal("JDOMException", e);
			} catch (ClassNotFoundException e) {
				logger.fatal("ClassNotFoundException", e);
			} catch (IllegalAccessException e) {
				logger.fatal("IllegalAccessException", e);
			} catch (InstantiationException e) {
				logger.fatal("InstantiationException", e);
			}
			catch (Exception e) {
				logger.fatal("Exception", e);
			}finally {
				FinalCleanUp.closeResource(pstmt);
				FinalCleanUp.closeResource(rs);
				FinalCleanUp.closeDatabaseConnection(connection);				
			}
			
			
			
		}
		
		
		
		return ro;
	}
	
   public ReturnObject deleteInvoice(RimsUsersDTO rimsUserDTO,HashMap<String, ArrayList<String>> vals, int isDeleted) {
	    ro = new ReturnObject();
	    Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		String ids="";
		
		try {
			sql = "update "+INVOICE_TABLE_NAME+" set invcIsDeleted="+isDeleted+" where 1=1 ";
			sql += qh.getQueryFromHashMap(vals);
			ids = qh.getQueryFromHashMap(vals);
			logger.debug(sql);
			
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);
				new RimsUpdateLogHelper(rimsUserDTO.getUsrID(),INVOICE_TABLE_NAME, 0, ids,isDeleted>0 ? "Deleted " : "NoDeleted");
			}
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		
		return ro;
	}
   
   public ReturnObject deletePOProducts(RimsUsersDTO rimsUserDTO,HashMap<String, ArrayList<String>> vals) {
	    ro = new ReturnObject();
	    Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		String ids="";
		
		try {
			sql = "DELETE FROM  "+PO_TABLE_NAME+"  where 1=1 ";
			sql += qh.getQueryFromHashMap(vals);
			ids = qh.getQueryFromHashMap(vals);
			logger.debug(sql);
			
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);
				//new RimsUpdateLogHelper(rimsUserDTO.getUsrID(),INVOICE_TABLE_NAME, 0, ids,isDeleted>0 ? "Deleted " : "NoDeleted");
			}
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		
		return ro;
	}
   
   public ReturnObject updatePOProductInfo(RimsUsersDTO rimsUsersDTO,OrderListDTO dto) {
	    ro = new ReturnObject();
	    Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		PoDetailsDTO productDTO=null;	
		
		try {
			sql = "update "+PO_TABLE_NAME+" set pdTag=?  where pdID=? ";			
			logger.debug(sql);			
			logger.debug(dto.getId());
			//productDTO = dto.getProductDTO();			
			//logger.debug("Tag: "+dto.getProductDTO().getPdTag());
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			pstmt.setString(1, dto.getPdTag());
			pstmt.setInt(2, dto.getId());
			
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);
				//new RimsUpdateLogHelper(rimsUserDTO.getUsrID(),INVOICE_TABLE_NAME, 0, ids,value>0 ? "Deleted " : "NoDeleted");
			}
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		
		return ro;
	}
   
   public ReturnObject updateInvoiceStatus(RimsUsersDTO rimsUsersDTO,OrderListDTO dto) {
	    ro = new ReturnObject();
	    Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		
		try {
			sql = "update "+INVOICE_TABLE_NAME+" set invcInvoiceStatus=?,invcReceived=invcReceived+?,invcDue=invcDue-? where invcID=? ";
			
			
			logger.debug(sql);
			
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, dto.getInvcInvoiceStatus());
			pstmt.setDouble(2, Double.parseDouble(dto.getInvcReceivedAmount()));
			pstmt.setDouble(3, Double.parseDouble(dto.getInvcReceivedAmount()));
			pstmt.setInt(4, dto.getId());
			
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);
				//new RimsUpdateLogHelper(rimsUserDTO.getUsrID(),INVOICE_TABLE_NAME, 0, ids,value>0 ? "Deleted " : "NoDeleted");
			}
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		
		return ro;
	}
   
   public ReturnObject updateOrderDetails(RimsUsersDTO rimsUsersDTO,OrderListDTO dto) {
	    ro = new ReturnObject();
		Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		Statement stmt = null;
		ArrayList<PoDetailsDTO> poDetailsList = null;
		PoDetailsDTO pdlDTO=null;
		
		try {
			
			int quantity=0;
			int productID=0;
			double price = 0.0;
			double ship = 0.0;
			double tax = 0.0;
			double total = 0.0;
			double total2 = 0.0;
			double grandTotal = 0.0;
			boolean insertPO = false;
			
			if(dto.getProductDetailsInfo()!=null && dto.getProductDetailsInfo().length>0) {
				
				String str[] = dto.getProductDetailsInfo();
				poDetailsList = new ArrayList<PoDetailsDTO>(str.length);
				for(String s:str) {
					logger.debug(s);
					dto.setPdID((int) DatabaseManagerSuccessful.getInstance().getNextSequenceId(PO_TABLE_NAME));
					pdlDTO = new PoDetailsDTO();
					String arr[] = s.split(":");		
					productID = Integer.parseInt(arr[0]);
					price = Double.parseDouble(arr[1]);
					quantity = Integer.parseInt(arr[2]);
					ship = Double.parseDouble(arr[3]);
					tax = Double.parseDouble(arr[4]);
					total = quantity*price+ship;
					total2 = total*(1+tax*1.0/100);
					grandTotal = grandTotal+total2;
					
					pdlDTO.setPdID(dto.getPdID());
					pdlDTO.setPd_invcID(dto.getId());
					pdlDTO.setPdPONumber(Long.parseLong(dto.getPurchaseOrderNumber()));
					pdlDTO.setPd_plID(productID);
					pdlDTO.setPdQuantity(quantity);
					pdlDTO.setPdPrice(price);					
					pdlDTO.setPdShippingCharge(ship);
					pdlDTO.setPdTax(tax);
					pdlDTO.setPdTag("");
					poDetailsList.add(pdlDTO);					
					
				}
				logger.debug(grandTotal);
				insertPO = true;
			}
			else {
				logger.debug("Product details not found.");
			}
			
			sql = "UPDATE "
		            +CLIENT_TABLE_NAME
					+" SET clName=?,clCompanyName=?,clEmail=?,"
					+ "clPhone=?,clShippingAddress=?,clBillingAddress=?"
					+ " WHERE clID=?";
			logger.debug(sql);
			logger.debug("Client ID: "+dto.getClID());
			connection = DatabaseManagerSuccessful.getInstance()
					.getConnection();
			pstmt = connection.prepareStatement(sql);
			
			int i=1;	
			pstmt.setString(i++, dto.getCustomerName());
			pstmt.setString(i++, dto.getCompanyName());
			pstmt.setString(i++, dto.getCustomerEmail());
			pstmt.setString(i++, dto.getCustomerPhone());
			pstmt.setString(i++, dto.getShippingAddress());
			pstmt.setString(i++, dto.getBillingAddress());
			pstmt.setInt(i++, dto.getClID());
			
			if (pstmt.executeUpdate() > 0) {
				ro.setIsSuccessful(true);
				logger.debug("Client Data inserted by : "+rimsUsersDTO.getId());
			}
			pstmt.close();
			
			
			sql = "UPDATE "
					+INVOICE_TABLE_NAME
					+" SET invcPONumber=?,invc_cID=?,invcOrderDate=?,invcDeliveryDate=?,"
					+ "invcAmount=?,invcDue=?,invcReceived=?"					
					+ " WHERE invcID=?";
					
			logger.debug(sql);
			DateAndTimeHelper ob = new DateAndTimeHelper();
			long d = ob.getLongFromDate(dto.getOrderDate());
			
			pstmt = connection.prepareStatement(sql);
			i=1;			
			pstmt.setLong(i++, Long.parseLong(dto.getPurchaseOrderNumber()));			
			pstmt.setInt(i++,Integer.parseInt(dto.getCurrencyID()));
			pstmt.setLong(i++, d);
			d = ob.getLongFromDate(dto.getDeliveryDate());
			pstmt.setLong(i++, d);
			pstmt.setDouble(i++, grandTotal);
			pstmt.setDouble(i++, grandTotal);
			pstmt.setDouble(i++, 0.0);
			pstmt.setInt(i++, dto.getId());
	
			
			if (pstmt.executeUpdate() > 0) {
				ro.setIsSuccessful(true);
				logger.debug("Invoice Data inserted by : "+rimsUsersDTO.getId());
			}
			pstmt.close();
			
			if(insertPO) {
				
				sql = "DELETE FROM "+PO_TABLE_NAME +" WHERE pd_invcID="+dto.getId();
				logger.debug(sql);
				
				stmt = connection.createStatement();
				stmt.executeUpdate(sql);
				stmt.close();
				
				sql = "INSERT INTO "
						+PO_TABLE_NAME
						+" (pdID,pd_invcID,pdPONumber,pd_plID,pdQuantity,"
						+ "pdPrice,pdShippingCharge,pdTax,pdTag)"
						+ " VALUES(?,?,?,?,?,?,?,?,?)";
				logger.debug(sql);
				pstmt = connection.prepareStatement(sql);				
				for(PoDetailsDTO pDTO:poDetailsList) {
					i=1;
					pstmt.setInt(i++, pDTO.getPdID());
					pstmt.setInt(i++, pDTO.getPd_invcID());
					pstmt.setLong(i++, pDTO.getPdPONumber());
					pstmt.setInt(i++, pDTO.getPd_plID());
					pstmt.setInt(i++, pDTO.getPdQuantity());
					pstmt.setDouble(i++, pDTO.getPdPrice());
					pstmt.setDouble(i++, pDTO.getPdShippingCharge());
					pstmt.setDouble(i++, pDTO.getPdTax());
					pstmt.setString(i++, "");
					pstmt.addBatch();
					logger.debug("PO number: "+pDTO.getPdPONumber());
				}
				
				if (pstmt.executeBatch().length > 0) {
					ro.setIsSuccessful(true);
				} else {
					ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
				}
				pstmt.close();
				
				
				logger.debug("PRODUCT INFO UPDATED");
			}
			
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		
		return ro;
	}
   
   public ReturnObject updatePOStatus(RimsUsersDTO rimsUsersDTO,OrderListDTO dto) {
	    ro = new ReturnObject();
	    Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		
		try {
			sql = "update "+INVOICE_TABLE_NAME+" set invcPOStatus=? where invcID=? ";		
			
			logger.debug(sql);
			
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			pstmt.setInt(1, dto.getInvcPOStatus());
			pstmt.setInt(2, dto.getId());
			
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);
				//new RimsUpdateLogHelper(rimsUserDTO.getUsrID(),INVOICE_TABLE_NAME, 0, ids,value>0 ? "Deleted " : "NoDeleted");
			}
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		
		return ro;
	}
}
