package com.revesoft.po.revesoft.orderlist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import com.revesoft.po.revesoft.users.RimsUsersDTO;


import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;
import org.apache.log4j.Logger;

public class OrderListService {
	private ReturnObject ro;
	private LinkedHashMap<String, OrderListDTO> data = null;
	LinkedHashMap<String, PoDetailsDTO> pdata = null;
	private RimsUsersDTO rimsUsersDTO;
	Logger logger = Logger.getLogger(OrderListService.class.getName());
	public OrderListService(RimsUsersDTO rimsUsersDTO) {
		this.rimsUsersDTO = rimsUsersDTO;
	}
	
	@SuppressWarnings("unchecked")
	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		
		ro = new OrderListDAO().getMap(vals, condition, keyColumn);
		if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
			try {
				data = (LinkedHashMap<String, OrderListDTO>) ro.getData();
				ro.setIsSuccessful(true);
				ro.setData(data);
			}
			catch(Exception e) {
				
			}
		}
		
	     return ro;
	}
	
	@SuppressWarnings("unchecked")
	public ReturnObject getMapProductDetais(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		
		ro = new OrderListDAO().getMapProductDetais(vals, condition, keyColumn);
		if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
			try {
				pdata = (LinkedHashMap<String, PoDetailsDTO>) ro.getData();
				ro.setIsSuccessful(true);
				ro.setData(pdata);
			}
			catch(Exception e) {
				
			}
		}
		
	     return ro;
	}
	
	
	public ReturnObject deleteInvoice(HashMap<String, ArrayList<String>> vals, int isDeleted) {
		ro = new OrderListDAO().deleteInvoice(rimsUsersDTO, vals, isDeleted);
		if (ro != null && ro.getIsSuccessful()) {
			ro.setIsSuccessful(true);
		}
		return ro;
	}
	
	public ReturnObject deletePOProducts(HashMap<String, ArrayList<String>> vals) {
		ro = new OrderListDAO().deletePOProducts(rimsUsersDTO, vals);
		if (ro != null && ro.getIsSuccessful()) {
			ro.setIsSuccessful(true);
		}
		return ro;
	}
	
	@SuppressWarnings("unchecked")
	public OrderListDTO getDTO(String colName, String colVal,String condition,String keyColumnName){
		OrderListDTO dto=null;
		QueryHelper qh;
		LinkedHashMap<String,OrderListDTO> data =null;
		if (colName != null && colName.length() > 0 && colVal != null && colVal.length() > 0) {
			if (condition == null || condition.length() == 0) {
				condition = "";
			}
			qh = new QueryHelper();
			condition += " AND " + qh.getMysqlRealScapeString(colName) + "='" + qh.getMysqlRealScapeString(colVal) + "' ";
			ro = new OrderListDAO().getMap(null, condition, keyColumnName);
			if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
				data = (LinkedHashMap<String,OrderListDTO>)ro.getData();
				if (data != null && data.containsKey(colVal)) {
					dto = data.get(colVal);
					
				}
			}
		}
		
		return dto;
	}
	
	@SuppressWarnings("unchecked")
	public PoDetailsDTO getPOProductDTO(String colName, String colVal,String condition,String keyColumnName){
		PoDetailsDTO dto=null;
		QueryHelper qh;
		LinkedHashMap<String,PoDetailsDTO> data =null;
		if (colName != null && colName.length() > 0 && colVal != null && colVal.length() > 0) {
			if (condition == null || condition.length() == 0) {
				condition = "";
			}
			qh = new QueryHelper();
			condition += " AND " + qh.getMysqlRealScapeString(colName) + "='" + qh.getMysqlRealScapeString(colVal) + "' ";
			ro = new OrderListDAO().getMapProductDetais(null, condition, keyColumnName);
			if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
				data = (LinkedHashMap<String,PoDetailsDTO>)ro.getData();
				if (data != null && data.containsKey(colVal)) {
					dto = data.get(colVal);
					
				}
			}
		}
		
		return dto;
	}
	
	public ReturnObject updateInvoiceStatus(OrderListDTO dto) {
		ro = new OrderListDAO().updateInvoiceStatus(rimsUsersDTO ,dto);
		if (ro != null && ro.getIsSuccessful()) {
			ro.setIsSuccessful(true);
		}
		return ro;
	}
	
	public ReturnObject updatePOProductInfo(OrderListDTO dto) {
		ro = new OrderListDAO().updatePOProductInfo(rimsUsersDTO ,dto);
		if (ro != null && ro.getIsSuccessful()) {
			ro.setIsSuccessful(true);
		}
		return ro;
	}
	
	public ReturnObject updateOrderDetails(OrderListDTO dto) {
		ro = new OrderListDAO().updateOrderDetails(rimsUsersDTO ,dto);
		if (ro != null && ro.getIsSuccessful()) {
			ro.setIsSuccessful(true);
		}
		return ro;
	}
	
	public ReturnObject updatePOStatus(OrderListDTO dto) {
		ro = new OrderListDAO().updatePOStatus(rimsUsersDTO ,dto);
		if (ro != null && ro.getIsSuccessful()) {
			ro.setIsSuccessful(true);
		}
		return ro;
	}
	
	
}
