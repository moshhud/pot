package com.revesoft.po.revesoft.orderlist;

import java.io.File;

import dev.mashfiq.common.Column;
import dev.mashfiq.common.CommonDTO;
 

public class OrderListDTO extends CommonDTO{
	
	@Column(name="id")
	private int id;	
	
	@Column(name="clName")
	private String customerName;
	
	@Column(name="clCompanyName")
	private String companyName;
	
	@Column(name="clEmail")
	private String customerEmail;
	
	@Column(name="clPhone")
	private String customerPhone;
	
	@Column(name="clShippingAddress")
	private String shippingAddress;
	
	@Column(name="clBillingAddress")
	private String billingAddress;
	
	@Column(name="invcPONumber")
	private String purchaseOrderNumber;	
	
	@Column(name="invcOrderDate")
	private String orderDate;	
	
	@Column(name="invcDeliveryDate")
	private String deliveryDate;
	
		
	@Column(name="invc_cID")
	private String currencyID;
	
	@Column(name="plProductName")
	private String plProductName;
	
	@Column(name="plPriceUSD")
	private String plPriceUSD;
	
	@Column(name="shippingCharge")
	private String shippingCharge;
	
	@Column(name="tax")
	private String tax;
	
	@Column(name="productDetailsInfo")
	private String[] productDetailsInfo;
	
	@Column(name="pdQuantity")
	private String pdQuantity;
	
	
	private File uploadDoc;
	private String uploadDocContentType;
	private String uploadDocFileName;
	
	private int invcID;
	private int clID;
	private int pdID;
	
	private double invcAmount;
	private double invcDue;
	private double invcReceived;
	private int invcIsDeleted;
	private int invcInvoiceStatus;
	private int invcPOStatus;
	
	private int dashboardType;
	
	private String invcReceivedAmount;
	
	private String pdTag;
	
	private PoDetailsDTO productDTO;
	
	private String startDate;
	private String endDate;
	
	
	
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getPdTag() {
		return pdTag;
	}

	public void setPdTag(String pdTag) {
		this.pdTag = pdTag;
	}

	public PoDetailsDTO getProductDTO() {		
		return productDTO;
	}

	public void setProductDTO(PoDetailsDTO productDTO) {		
		this.productDTO = productDTO;
	}

	public int getDashboardType() {
		return dashboardType;
	}

	public void setDashboardType(int dashboardType) {
		this.dashboardType = dashboardType;
	}

	public String getInvcReceivedAmount() {
		return invcReceivedAmount;
	}

	public void setInvcReceivedAmount(String invcReceivedAmount) {
		this.invcReceivedAmount = invcReceivedAmount;
	}

	public double getInvcAmount() {
		return invcAmount;
	}

	public void setInvcAmount(double invcAmount) {
		this.invcAmount = invcAmount;
	}

	public double getInvcDue() {
		return invcDue;
	}

	public void setInvcDue(double invcDue) {
		this.invcDue = invcDue;
	}

	public double getInvcReceived() {
		return invcReceived;
	}

	public void setInvcReceived(double invcReceived) {
		this.invcReceived = invcReceived;
	}

	public int getInvcIsDeleted() {
		return invcIsDeleted;
	}

	public void setInvcIsDeleted(int invcIsDeleted) {
		this.invcIsDeleted = invcIsDeleted;
	}

	public int getInvcInvoiceStatus() {
		return invcInvoiceStatus;
	}

	public void setInvcInvoiceStatus(int invcInvoiceStatus) {
		this.invcInvoiceStatus = invcInvoiceStatus;
	}

	public int getInvcPOStatus() {
		return invcPOStatus;
	}

	public void setInvcPOStatus(int invcPOStatus) {
		this.invcPOStatus = invcPOStatus;
	}

	public int getInvcID() {
		return invcID;
	}

	public void setInvcID(int invcID) {
		this.invcID = invcID;
	}

	public int getClID() {
		return clID;
	}

	public void setClID(int clID) {
		this.clID = clID;
	}

	public int getPdID() {
		return pdID;
	}

	public void setPdID(int pdID) {
		this.pdID = pdID;
	}

	public File getUploadDoc() {
		return uploadDoc;
	}

	public void setUploadDoc(File uploadDoc) {
		this.uploadDoc = uploadDoc;
	}

	
	public String getUploadDocContentType() {
		return uploadDocContentType;
	}

	public void setUploadDocContentType(String uploadDocContentType) {
		this.uploadDocContentType = uploadDocContentType;
	}

	public String getUploadDocFileName() {
		return uploadDocFileName;
	}

	public void setUploadDocFileName(String uploadDocFileName) {
		this.uploadDocFileName = uploadDocFileName;
	}

	public String getPdQuantity() {
		return pdQuantity;
	}

	public void setPdQuantity(String pdQuantity) {
		this.pdQuantity = pdQuantity;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public String getCustomerPhone() {
		return customerPhone;
	}

	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}

	public String getShippingAddress() {
		return v.checkDTO(shippingAddress);
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getBillingAddress() {
		return v.checkDTO(billingAddress);
	}

	public void setBillingAddress(String billingAddress) {
		this.billingAddress = billingAddress;
	}

	public String getPurchaseOrderNumber() {
		return purchaseOrderNumber;
	}

	public void setPurchaseOrderNumber(String purchaseOrderNumber) {
		this.purchaseOrderNumber = purchaseOrderNumber;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public String getCurrencyID() {
		return currencyID;
	}

	public void setCurrencyID(String currencyID) {
		this.currencyID = currencyID;
	}

	public String getPlProductName() {
		return plProductName;
	}

	public void setPlProductName(String plProductName) {
		this.plProductName = plProductName;
	}

	public String getPlPriceUSD() {
		return plPriceUSD;
	}

	public void setPlPriceUSD(String plPriceUSD) {
		this.plPriceUSD = plPriceUSD;
	}

	public String getShippingCharge() {
		return shippingCharge;
	}

	public void setShippingCharge(String shippingCharge) {
		this.shippingCharge = shippingCharge;
	}

	public String getTax() {
		return tax;
	}

	public void setTax(String tax) {
		this.tax = tax;
	}

	public String[] getProductDetailsInfo() {
		return productDetailsInfo;
	}

	public void setProductDetailsInfo(String[] productDetailsInfo) {
		this.productDetailsInfo = productDetailsInfo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	

}
