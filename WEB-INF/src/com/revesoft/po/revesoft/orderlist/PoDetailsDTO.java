package com.revesoft.po.revesoft.orderlist;

import dev.mashfiq.common.Column;
import dev.mashfiq.common.CommonDTO;

public class PoDetailsDTO extends CommonDTO{
	
	private int pdID;
	private int pd_invcID;
	private long pdPONumber;
	private int pd_plID;
	private int pdQuantity;
	private double pdShippingCharge;
	private double pdTax;
	
	@Column(name = "pdTag")
	private String pdTag;
	private double pdPrice;
	
	
	
	public double getPdPrice() {
		return pdPrice;
	}
	public void setPdPrice(double pdPrice) {
		this.pdPrice = pdPrice;
	}
	public int getPdID() {
		return pdID;
	}
	public void setPdID(int pdID) {
		this.pdID = pdID;
	}
	public int getPd_invcID() {
		return pd_invcID;
	}
	public void setPd_invcID(int pd_invcID) {
		this.pd_invcID = pd_invcID;
	}
	public long getPdPONumber() {
		return pdPONumber;
	}
	public void setPdPONumber(long pdPONumber) {
		this.pdPONumber = pdPONumber;
	}
	public int getPd_plID() {
		return pd_plID;
	}
	public void setPd_plID(int pd_plID) {
		this.pd_plID = pd_plID;
	}
	public int getPdQuantity() {
		return pdQuantity;
	}
	public void setPdQuantity(int pdQuantity) {
		this.pdQuantity = pdQuantity;
	}
	public double getPdShippingCharge() {
		return pdShippingCharge;
	}
	public void setPdShippingCharge(double pdShippingCharge) {
		this.pdShippingCharge = pdShippingCharge;
	}
	public double getPdTax() {
		return pdTax;
	}
	public void setPdTax(double pdTax) {
		this.pdTax = pdTax;
	}
	public String getPdTag() {
		return pdTag;
	}
	public void setPdTag(String pdTag) {
		System.out.println("setting pdTag:");
		this.pdTag = pdTag;
	}
	
	
	

}
