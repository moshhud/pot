package com.revesoft.po.revesoft.orderlist;

import java.util.HashMap;

import javax.servlet.ServletOutputStream;

import org.apache.log4j.Logger;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.MasterAction;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.Permissions;
import dev.mashfiq.util.RecordNavigation;
import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.SearchPanels;
import dev.mashfiq.util.SearchQueryBuilder;
import dev.mashfiq.util.Validations;
import java.io.File;
import java.io.FileInputStream;

public class OrderlistAction extends MasterAction implements ModelDriven<OrderListDTO>,Preparable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(OrderlistAction.class.getName());
	private OrderListDTO dto;
	
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		dto = new OrderListDTO();			
	}

	@Override
	public OrderListDTO getModel() {
		// TODO Auto-generated method stub		
		return dto;
	}
	
	
	
	@Override
	public String goToSearch() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String search() {
		// TODO Auto-generated method stub
		
		return searchOrderList("", true);
	}

	@Override
	public String get() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String update() {
		// TODO Auto-generated method stub
		return null;
	}

	

	@Override
	public String add() {
		// TODO Auto-generated method stub
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_THREE) {
			
			if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			
				ro = new OrderListDAO().insert(rimsUsersDTO,dto);
				
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_ADDED);
						new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),OrderListDAO.INVOICE_TABLE_NAME, dto.getId(), "PO invoice Created: ","add");
						
					} else {
						ro.setActionMessage(session);						
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.SYSTEM_ERROR);
				}
			}
			
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For add: "
					+ (System.currentTimeMillis() - startTime));
		}
		
		return result;
	}
	
	public String newOrderList() {
		result = SEARCH;
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_THREE) {
			result = INPUT;
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For newValidDialer: "
					+ (System.currentTimeMillis() - startTime));
		}
		
		return result;
	}


	
	public String poTrackingDetails() {
		return poTrackingDetails(" and invcPONumber=",true);
	}
	 
	public String poTrackingDetails(String additionalCondition, boolean ignoreSearch) {
		result = SEARCH;
		String condition = null;
		int count = 0;
		String[][] searchPanel = SearchPanels.ORDERLIST_DASHBOARD;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			
			condition=additionalCondition;
			if (ignoreSearch || condition.length() > 0) {			
				DateAndTimeHelper ob = new DateAndTimeHelper();
				if (dto != null) {
					if(dto.getPurchaseOrderNumber()!=null&&dto.getPurchaseOrderNumber().length()>0) {
						condition+=dto.getPurchaseOrderNumber();
					}else {
						condition="";
					}
					
					if(dto.getStartDate()!=null&&dto.getStartDate().length()>8) {
						long d = ob.getLongFromDate(dto.getStartDate());
						condition+=" and invcDeliveryDate>="+d;
					}
					if(dto.getEndDate()!=null&&dto.getEndDate().length()>8) {
						long d = ob.getLongFromDate(dto.getEndDate());
						condition+=" and invcDeliveryDate<="+d;
					}
					if(dto.getInvcPOStatus()!=0) {
						condition+=" and invcPOStatus="+dto.getInvcPOStatus();
					}
					condition+=" and invcIsDeleted=0 and clID=invc_clID ";
					
					count = new CommonDAO().getCount(OrderListDAO.INVOICE_TABLE_NAME+","+OrderListDAO.CLIENT_TABLE_NAME,OrderListDAO.DEFAULT_KEY_COLUMN,condition, false);
					logger.debug("Count: "+count);
					if(count>0) {	
						map = rn.getRecordNavigation(count);
						condition += " ORDER BY invcID DESC LIMIT "+ map.get(RecordNavigation.START_INDEX)+ ","+ map.get(RecordNavigation.LIMIT);
						logger.debug("condition22: "+condition);
						ro = new OrderListService(rimsUsersDTO).getMap(null, condition ,OrderListDAO.DEFAULT_KEY_COLUMN);
						if (ro != null) {
							if (ro.getIsSuccessful()) {
								session.setAttribute(ApplicationConstant.ACTION_DATA,ro.getData());
							} else {
								session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
							}
						}
						else {
							session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SYSTEM_ERROR);
						}
					}
					else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
					}
										
				}
				else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
				}
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.USE_SEARCH);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		
		
		return result;
	}
	
	public String searchOrderList(String additionalCondition, boolean ignoreSearch) {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		String[][] searchPanel = SearchPanels.ORDERLIST;
		String condition = null;
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			ro = new SearchQueryBuilder().getSearchQuery(request, session,searchPanel, OrderListDAO.INVOICE_TABLE_NAME, rimsUsersDTO.getUsrID());
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					condition = ro.getData() + "";
					if (ignoreSearch || condition.length() > 0) {
						if (condition.length() == 0) {
							condition = "";
						}
						condition += additionalCondition;						
						logger.debug(condition);
						count = new CommonDAO().getCount(OrderListDAO.INVOICE_TABLE_NAME+","+OrderListDAO.CLIENT_TABLE_NAME,OrderListDAO.DEFAULT_KEY_COLUMN,condition+" and clID=invc_clID", false);
						logger.debug("Count: "+count);
						if (count > 0) {
							map = rn.getRecordNavigation(count);
							condition += " ORDER BY invcID DESC LIMIT "+ map.get(RecordNavigation.START_INDEX)+ ","+ map.get(RecordNavigation.LIMIT);
							ro = new OrderListService(rimsUsersDTO).getMap(null, condition ,OrderListDAO.DEFAULT_KEY_COLUMN);
							if (ro != null) {
								if (ro.getIsSuccessful()) {
									session.setAttribute(ApplicationConstant.ACTION_DATA,ro.getData());
								} else {
									session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
								}
							}
							else {
								session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SYSTEM_ERROR);
							}
						}
						else {
							session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
						}
						
					}
					else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.USE_SEARCH);
					}
				}
				else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
				}
			}
			 else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SYSTEM_ERROR);
				}
		}
		else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		
		
		return result;
	}
	
	public String searchOrderListForDashboard() {
		return searchOrderListForDashboard(" and invcIsDeleted=0 ", true);
	}
	
	public String searchOrderListForDashboard(String additionalCondition, boolean ignoreSearch) {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		String[][] searchPanel = SearchPanels.ORDERLIST_DASHBOARD;
		String condition = "";
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);					
			if (ignoreSearch) {
				if(dto != null) {
					if(dto.getDashboardType()==1) {
						condition+=" and invcPOStatus not in(515) ";
					}
					else if(dto.getDashboardType()==2) {
						condition+=" and invcPOStatus in(515) ";
					}
					else {
						condition+=" and invcInvoiceStatus not in(507) ";
					}
					
					
				}
				if (condition.length() == 0) {
					condition = "";
				}
				condition += additionalCondition;						
				logger.debug(condition);
				count = new CommonDAO().getCount(OrderListDAO.INVOICE_TABLE_NAME+","+OrderListDAO.CLIENT_TABLE_NAME,OrderListDAO.DEFAULT_KEY_COLUMN,condition+" and clID=invc_clID", false);
				logger.debug("Count: "+count);
				if (count > 0) {
					map = rn.getRecordNavigation(count);
					condition += " ORDER BY invcID DESC LIMIT "+ map.get(RecordNavigation.START_INDEX)+ ","+ map.get(RecordNavigation.LIMIT);
					ro = new OrderListService(rimsUsersDTO).getMap(null, condition ,OrderListDAO.DEFAULT_KEY_COLUMN);
					if (ro != null) {
						if (ro.getIsSuccessful()) {
							session.setAttribute(ApplicationConstant.ACTION_DATA,ro.getData());
						} else {
							session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
						}
					}
					else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SYSTEM_ERROR);
					}
				}
				else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
				}
				
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.USE_SEARCH);
			}
				
			
			 
		}
		else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		
		
		return result;
	}
	
	
	
	public String fileDownload() {
		result = ERROR;
		
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_TWO) {
			
			String fileName = dto.getUploadDocFileName();
			String osName = getOSName();
			String projectPath = "";
			if(osName.equals("windows")) {
				projectPath=ApplicationConstant.PROJECT_PATH_WIN;
			}
			else {
				projectPath=ApplicationConstant.PROJECT_PATH;
			}
			String filePath = projectPath+ApplicationConstant.PO_FILES;
			File file = new File(filePath, fileName);
			if(!file.exists()){
	            logger.debug("File not available ");
	            session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.FILE_NOT_FOUND);
	            return result;
	        }
			
			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
			try {
				FileInputStream in = new FileInputStream(new File(filePath, fileName));
				ServletOutputStream out = response.getOutputStream();
				byte[] data = new byte[4096];
				while (in.read(data, 0, 4096) != -1) {
		            out.write(data, 0, 4096);
		        }
	            in.close();
	            out.flush();
	            out.close();
	            
			}catch (Exception e) {
				 
			}
		}
		else {
			result = PERMISSION_DENIED;
			return result;
		}
		
		return null;
	}
	
	public String getOSName() {
	
		String osName = System.getProperty("os.name");
		logger.debug(osName);
		if(osName.startsWith("Windows")) {
			osName="windows";
		}else {
			osName="linux";
		}
		
		return osName;
	}
	
	public String searchProductDetails() {
		return getProductDetails(" and pd_invcID=",true);
	}
	
 
	public String getProductDetails(String additionalCondition, boolean ignoreSearch) {
		
		result = SEARCH;
		String condition = null;
		int count = 0;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_TWO) {
			condition=additionalCondition;
			if (ignoreSearch || condition.length() > 0) {
				
				condition+=dto.getId();
				if (dto != null) {
					logger.debug("ID: "+dto.getId());
					count = new CommonDAO().getCount(OrderListDAO.PO_TABLE_NAME,OrderListDAO.DEFAULT_PO_KEY_COLUMN,condition, false);
					logger.debug("Count: "+count);
					if(count>0) {						
						ro = new OrderListService(rimsUsersDTO).getMapProductDetais(null, condition ,OrderListDAO.DEFAULT_PO_KEY_COLUMN);
						if (ro != null) {
							if (ro.getIsSuccessful()) {
								session.setAttribute(ApplicationConstant.ACTION_DATA,ro.getData());
							} else {
								session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
							}
						}
						else {
							session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SYSTEM_ERROR);
						}
					}
					else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
					}
										
				}
				else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
				}
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.USE_SEARCH);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		
		
		return result;
	}
	
	public String  getInvoiceStatus(){		
		return getInvoiceStatusToEdit();
		
	}
	public String  getInvoiceStatusToEdit(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Edit ID:"+dto.getId());
				dto = new OrderListService(rimsUsersDTO).getDTO(OrderListDAO.DEFAULT_KEY_COLUMN, dto.getId()+"", "",OrderListDAO.DEFAULT_KEY_COLUMN);
				result = INPUT;
				session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
				
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String  getPOProductInfo(){		
		return getPOProductInfoToEdit();
		
	}
	public String  getPOProductInfoToEdit(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Edit ID:"+dto.getId());
				PoDetailsDTO productDTO=null;				
				productDTO= new OrderListService(rimsUsersDTO).getPOProductDTO(OrderListDAO.DEFAULT_PO_KEY_COLUMN, dto.getId()+"", "",OrderListDAO.DEFAULT_PO_KEY_COLUMN);
				
				if(productDTO!=null) {
					dto.setProductDTO(productDTO);
					result = INPUT;
					session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
				}else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
					logger.debug("No Data Found");
				}
				
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String  updatePOProductInfo(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Update ID:"+dto.getId());
				ro = new OrderListService(rimsUsersDTO).updatePOProductInfo(dto);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_UPDATED);
					}else {					
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ro.getActionMessage());
					}
				}
				 else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
				}								
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String  updateInvoiceStatus(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Update ID:"+dto.getId());
				ro = new OrderListService(rimsUsersDTO).updateInvoiceStatus(dto);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_UPDATED);
					}else {					
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ro.getActionMessage());
					}
				}
				 else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
				}								
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String  updateOrderDetails(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Invc ID:"+dto.getId());
				logger.debug("Client ID:"+dto.getClID());
				ro = new OrderListService(rimsUsersDTO).updateOrderDetails(dto);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_UPDATED);
					}else {					
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ro.getActionMessage());
					}
				}
				 else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
				}								
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String  getPOStatus(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Edit ID:"+dto.getId());
				dto = new OrderListService(rimsUsersDTO).getDTO(OrderListDAO.DEFAULT_KEY_COLUMN, dto.getId()+"", "",OrderListDAO.DEFAULT_KEY_COLUMN);
				result = INPUT;
				session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
				
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String  updatePOStatus(){
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Update ID:"+dto.getId());
				ro = new OrderListService(rimsUsersDTO).updatePOStatus(dto);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_UPDATED);
					}else {					
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ro.getActionMessage());
					}
				}
				 else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
				}								
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	
	

}
