package com.revesoft.rims.revesoft.rimsVerificationStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.util.FinalCleanUp;

public class RimsVerificationStatusDAO {

	public static final String TABLE_NAME = "rims_verification_status";
	private Logger logger = Logger.getLogger(RimsVerificationStatusDAO.class
			.getName());
	public static final String VERIFIED = "verified";

	public boolean addVerificationStatus(String type, String val,
			String verifiedBy) {
		boolean status = false;
		Connection connection = null;
		PreparedStatement pstmt = null;
		String sql = null;
		try {
			if (checkVerificationStatus(type, val) == false) {
				sql = "INSERT INTO "
						+ TABLE_NAME
						+ " (field_type,type_val,verification_status,verified_by) VALUES (?,?,?,?)";
				connection = DatabaseManagerSuccessful.getInstance()
						.getConnection();
				pstmt = connection.prepareStatement(sql);
				pstmt.setString(1, type);
				pstmt.setString(2, val);
				pstmt.setString(3, "verified");
				pstmt.setString(4, verifiedBy);
				if (pstmt.executeUpdate() > 0) {
					status = true;
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (SQLException e) {
			logger.fatal("Exception", e);
		} catch (JDOMException e) {
			logger.fatal("Exception", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("Exception", e);
		} catch (IllegalAccessException e) {
			logger.fatal("Exception", e);
		} catch (InstantiationException e) {
			logger.fatal("Exception", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);			
		}
		return status;
	}

	public boolean checkVerificationStatus(String type, String val) {
		boolean status = false;
		HashMap<String, Boolean> data = null;
		ArrayList<String> valList = new ArrayList<String>(Arrays.asList(val));
		if (type != null && type.equals("email")) {
			data = verificationStatus(valList, null);
		} else {
			data = verificationStatus(null, valList);
		}
		if (data != null && data.containsKey(val)) {
			status = data.get(val);
		}
		return status;
	}

	public HashMap<String, Boolean> verificationStatus(
			ArrayList<String> emails, ArrayList<String> phoneNos) {
		HashMap<String, Boolean> data = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			if ((emails != null && emails.size() > 0)
					|| (phoneNos != null && phoneNos.size() > 0)) {
				sql = "SELECT type_val,verification_status FROM " + TABLE_NAME
						+ " WHERE current_status='active' AND (";
//				if (emails != null && emails.size() > 0) {
//					sql += "type_val IN ("
//							+ CollectionHelper.getStringFromArrayList(emails)
//							+ ")";
//					if (phoneNos != null && phoneNos.size() > 0) {
//						sql += " OR type_val IN("
//								+ CollectionHelper
//										.getStringFromArrayList(phoneNos) + ")";
//					}
//				} else if (phoneNos != null && phoneNos.size() > 0) {
//					sql += "type_val IN ("
//							+ CollectionHelper.getStringFromArrayList(phoneNos)
//							+ ")";
//				}
				sql += ") ORDER BY field_type";
				connection = DatabaseManagerSuccessful.getInstance()
						.getConnection();
				stmt = connection.createStatement();
				rs = stmt.executeQuery(sql);
				data = new HashMap<String, Boolean>();
				while (rs.next()) {
					if (rs.getString("verification_status") != null
							&& VERIFIED.equals(rs
									.getString("verification_status"))) {
						data.put(rs.getString("type_val"), true);
					} else {
						data.put(rs.getString("type_val"), false);
					}
				}
				if (rs != null) {
					rs.close();
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (SQLException e) {
			logger.fatal("Exception", e);
		} catch (JDOMException e) {
			logger.fatal("Exception", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("Exception", e);
		} catch (IllegalAccessException e) {
			logger.fatal("Exception", e);
		} catch (InstantiationException e) {
			logger.fatal("Exception", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeDatabaseConnection(connection);			
		}
		return data;
	}

}
