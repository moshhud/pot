package com.revesoft.rims.revesoft.rimsModules;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import com.revesoft.po.revesoft.users.RimsUsersDTO;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.CollectionHelper;
import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.Validations;

public class RimsModulesDAO implements DAOInterface {

	private Logger logger = Logger.getLogger(RimsModulesDAO.class.getName());
	public static final String TABLE_NAME = "poModules";
	public static final String DEFAULT_KEY_COLUMN = "id";
	private QueryHelper queryHelper = new QueryHelper();
	private RimsModulesDTO dto;
	private CommonDAO commonDAO = null;
	private CollectionHelper ch;

	public ReturnObject insert(Object obj) {
		ReturnObject ro = new ReturnObject();
		RimsModulesDTO dto = null;
		String sql = null;
		if (validate(obj)) {
			dto = (RimsModulesDTO) obj;
			if (dto != null && isExisting() == false) {
				sql = "INSERT INTO "
						+ TABLE_NAME
						+ " (created_by,module_name,module_description,level_one,level_two,level_three,level_four,level_five,level_six,creation_time) "
						+ "VALUES ("
						+ dto.getCreatedBy()
						+ ",'"
						+ queryHelper.getMysqlRealScapeString(dto
								.getModuleName())
						+ "','"
						+ queryHelper.getMysqlRealScapeString(dto
								.getModuleDescription())
						+ "','"
						+ queryHelper
								.getMysqlRealScapeString(dto.getLevelOne())
						+ "','"
						+ queryHelper
								.getMysqlRealScapeString(dto.getLevelTwo())
						+ "','"
						+ queryHelper.getMysqlRealScapeString(dto
								.getLevelThree())
						+ "','"
						+ queryHelper.getMysqlRealScapeString(dto
								.getLevelFour())
						+ "','"
						+ queryHelper.getMysqlRealScapeString(dto
								.getLevelFive())
						+ "','"
						+ queryHelper
								.getMysqlRealScapeString(dto.getLevelSix())
						+ "'," + System.currentTimeMillis() + ")";
				if (commonDAO == null) {
					commonDAO = new CommonDAO();
				}
				ro = commonDAO.executeUpdate(sql, false);
			} else {
				ro.setActionMessage(ActionMessages.DUPLICATE_ENTRY);
			}
		} else {
			ro.setActionMessage(ActionMessages.VALIDATION_ERROR);
		}
		return ro;
	}

	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
		String condition, String keyColumn) {
		ReturnObject ro = new ReturnObject();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		LinkedHashMap<String, RimsModulesDTO> data = null;
		RimsModulesDTO dto = null;
		DateAndTimeHelper dth;
		try {
			sql = "SELECT * FROM " + TABLE_NAME + " WHERE id!=0 ";
			sql += queryHelper.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += condition;
			}
			keyColumn = queryHelper.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			stmt = connection.createStatement();
			logger.debug(sql);
			rs = stmt.executeQuery(sql);
			data = new LinkedHashMap<String, RimsModulesDTO>();
			dth = new DateAndTimeHelper();
			while (rs.next()) {
				dto = new RimsModulesDTO();
				
				dto.setId(rs.getInt("id"));
				dto.setCreatedBy(rs.getInt("created_by"));
				dto.setCreationTime(dth.getDateFromLong(rs
						.getLong("creation_time")));
				dto.setCurrentStatus(rs.getString("current_status"));
				dto.setModuleName(rs.getString("module_name"));
				dto.setModuleDescription(rs.getString("module_description"));
				dto.setLevelOne(rs.getString("level_one"));
				dto.setLevelTwo(rs.getString("level_two"));
				dto.setLevelThree(rs.getString("level_three"));
				dto.setLevelFour(rs.getString("level_four"));
				dto.setLevelFive(rs.getString("level_five"));
				dto.setLevelSix(rs.getString("level_six"));
				data.put(rs.getString(keyColumn), dto);
			}
			ch = new CollectionHelper();
			if (ch.checkMap(data)) {
				ro.setIsSuccessful(true);
				ro.setData(data);
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}
	
	@SuppressWarnings("unchecked")
	public RimsModulesDTO getDTO(String colName, String colVal,String condition,String keyColumnName){
		RimsModulesDTO dto=null;
		QueryHelper qh;
		LinkedHashMap<String,RimsModulesDTO> data=null;
		ReturnObject ro = new ReturnObject();
		if (colName != null && colName.length() > 0 && colVal != null && colVal.length() > 0) {
			if (condition == null || condition.length() == 0) {
				condition = "";
			}
			qh = new QueryHelper();
			condition += " AND " + qh.getMysqlRealScapeString(colName) + "='" + qh.getMysqlRealScapeString(colVal) + "' ";
			ro = new RimsModulesDAO().getMap(null, condition, keyColumnName);
			if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
				data = (LinkedHashMap<String,RimsModulesDTO>)ro.getData();
				if (data != null && data.containsKey(colVal)) {
					dto = data.get(colVal);
					
				}
			}
			
		}
		
		return dto;		
	}

	@SuppressWarnings("unchecked")
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		ReturnObject ro = getMap(vals, condition, keyColumn);
		ArrayList<RimsModulesDTO> data = null;
		LinkedHashMap<String, RimsModulesDTO> result = null;
		if (ro != null && ro.getIsSuccessful()) {
			try {
				result = (LinkedHashMap<String, RimsModulesDTO>) ro.getData();
				if (ch == null) {
					ch = new CollectionHelper();
				}
				if (ch.checkMap(result)) {
					data = new ArrayList<RimsModulesDTO>(result.size());
					for (RimsModulesDTO dto : result.values()) {
						if (dto != null) {
							data.add(dto);
						}
					}
					if (ch.checkCollection(data)) {
						ro.setData(data);
					}
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		return ro;
	}
	public ReturnObject deleteModule(RimsUsersDTO rimsUserDTO,HashMap<String, ArrayList<String>> vals) {
		ReturnObject ro = new ReturnObject();
		Connection connection = null;
		String sql = null;
		PreparedStatement pstmt = null;
		 
		QueryHelper qh = new QueryHelper();
		try {
			
			sql = "DELETE FROM  "+TABLE_NAME+"  where 1=1 ";
			sql += qh.getQueryFromHashMap(vals);
			 
			logger.debug(sql);
			
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);				
			}
			
		}catch (Exception e) {
			logger.fatal("Exception", e);
		}finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);				
		}
		return ro;
	}
			

	public ReturnObject update(Object obj) {
         ReturnObject ro = new ReturnObject();		
         RimsModulesDTO dto=null;
         dto = (RimsModulesDTO)obj;
         Connection connection = null;
 		 String sql = null;
 		 PreparedStatement pstmt = null;       
 		 
 		 try
 		 {
 			sql = "update "+TABLE_NAME+" set module_name=?,module_description=?,level_one=?,"
 					+ "level_two=?,level_three=?,level_four=?,level_five=?,level_six=?"
 					+ " where id=? ";		
 			
            logger.debug(sql);
			
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			int i=1;
			pstmt.setString(i++, dto.getModuleName());
			pstmt.setString(i++, dto.getModuleDescription());
			pstmt.setString(i++, dto.getLevelOne());
			pstmt.setString(i++, dto.getLevelTwo());
			pstmt.setString(i++, dto.getLevelThree());
			pstmt.setString(i++, dto.getLevelFour());
			pstmt.setString(i++, dto.getLevelFive());
			pstmt.setString(i++, dto.getLevelSix());
			pstmt.setInt(i++, dto.getId());
			
			if(pstmt.executeUpdate()>0) {
				ro.setIsSuccessful(true);
			}
 			 
 		 }catch (Exception e) {
 			logger.fatal("Exception", e);
 		 }finally {
 			FinalCleanUp.closeResource(pstmt);
 			FinalCleanUp.closeDatabaseConnection(connection);				
 		 }
		return ro;
	}

	public boolean validate(Object obj) {
		boolean isValid = false;
		Validations v;
		if (obj != null && obj instanceof RimsModulesDTO) {
			dto = (RimsModulesDTO) obj;
			v = new Validations();
			if (dto != null // && dto.getCreatedBy() > 0
					&& v.checkInput(dto.getModuleName())
					&& v.checkInput(dto.getModuleDescription())) {
				isValid = true;
			}
		}
		return isValid;
	}

	public boolean isExisting() {
		boolean isExisting = false;
		if (dto != null) {
			commonDAO = new CommonDAO();
			if (commonDAO.getCount(
					TABLE_NAME,
					DEFAULT_KEY_COLUMN,
					" AND module_name='"
							+ queryHelper.getMysqlRealScapeString(dto
									.getModuleName()) + "'", false) == 0) {
				isExisting = false;
			}
		}
		return isExisting;
	}

	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		throw new UnsupportedOperationException();
	}

}
