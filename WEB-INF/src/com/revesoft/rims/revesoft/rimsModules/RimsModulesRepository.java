package com.revesoft.rims.revesoft.rimsModules;

import java.util.ArrayList;

import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.CollectionHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.Validations;

public class RimsModulesRepository {

	private static RimsModulesRepository modulesRepository;
	private ArrayList<RimsModulesDTO> list = null;

	@SuppressWarnings("unchecked")
	private RimsModulesRepository() {
		ReturnObject ro = new RimsModulesDAO().getArrayList(null, null, null);
		if (ro != null && ro.getIsSuccessful()) {
			list = (ArrayList<RimsModulesDTO>) ro.getData();
		}
	}

	public static synchronized RimsModulesRepository getInstance(boolean forceReload) {
		if (forceReload || modulesRepository == null) {
			modulesRepository = new RimsModulesRepository();
		}
		return modulesRepository;
	}

	public ArrayList<RimsModulesDTO> getAll(boolean onlyActive) {
		ArrayList<RimsModulesDTO> data = null;
		Validations v;
		if (new CollectionHelper().checkCollection(list)) {
			data = new ArrayList<RimsModulesDTO>();
			v = new Validations();
			for (RimsModulesDTO dto : list) {
				if (onlyActive
						|| v.checkEqualsIgnoreCase(
								dto.getCurrentStatus(),
								ApplicationConstant.ACTIVE)) {
					data.add(dto);
				}
			}
		}
		return data;
	}

}
