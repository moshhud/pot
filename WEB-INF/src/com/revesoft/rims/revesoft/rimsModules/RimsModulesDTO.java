package com.revesoft.rims.revesoft.rimsModules;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Validations;

public class RimsModulesDTO extends CommonDTO {
	private String moduleName;
	private String moduleDescription;
	private String levelOne;
	private String levelTwo;
	private String levelThree;
	private String levelFour;
	private String levelFive;
	private String levelSix;
	private String currentStatus;

	public String getCurrentStatus() {
		return v.checkDTO(currentStatus, ApplicationConstant.ACTIVE);
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	Validations v = new Validations();

	public String getModuleName() {
		return v.checkDTO(moduleName);
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleDescription() {
		return v.checkDTO(moduleDescription);
	}

	public void setModuleDescription(String moduleDescription) {
		this.moduleDescription = moduleDescription;
	}

	public String getLevelOne() {
		return v.checkDTO(levelOne, "No access");
	}

	public void setLevelOne(String levelOne) {
		this.levelOne = levelOne;
	}

	public String getLevelTwo() {
		return v.checkDTO(levelTwo, "View");
	}

	public void setLevelTwo(String levelTwo) {
		this.levelTwo = levelTwo;
	}

	public String getLevelThree() {
		return v.checkDTO(levelThree, "Add");
	}

	public void setLevelThree(String levelThree) {
		this.levelThree = levelThree;
	}

	public String getLevelFour() {
		return v.checkDTO(levelFour, "Update");
	}

	public void setLevelFour(String levelFour) {
		this.levelFour = levelFour;
	}

	public String getLevelFive() {
		return v.checkDTO(levelFive, "Delete");
	}

	public void setLevelFive(String levelFive) {
		this.levelFive = levelFive;
	}

	public String getLevelSix() {
		return v.checkDTO(levelSix, "View also deleted");
	}

	public void setLevelSix(String levelSix) {
		this.levelSix = levelSix;
	}
}
