package com.revesoft.rims.revesoft.rimsModules;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.MasterAction;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Permissions;
import dev.mashfiq.util.RecordNavigation;
import dev.mashfiq.util.SearchPanels;
import dev.mashfiq.util.SearchQueryBuilder;
import dev.mashfiq.util.Validations;

public class RimsModulesAction extends MasterAction implements
		ModelDriven<RimsModulesDTO>, Preparable {

	private Logger logger = Logger.getLogger(RimsModulesAction.class.getName());
	private static final long serialVersionUID = 1L;
	private RimsModulesDTO dto = null;

	public String newModule() {
		long startTime = System.currentTimeMillis();
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_THREE) {
			result = INPUT;
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For newModule: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String add() {
		long startTime = System.currentTimeMillis();
		result = INPUT;
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_THREE) {
				if (dto != null) {
					dto.setCreatedBy(rimsUsersDTO.getUsrID());
					ro = new RimsModulesDAO().insert(dto);
					if (ro != null) {
						if (ro.getIsSuccessful()) {
							result = INPUT;
							RimsModulesRepository.getInstance(true);
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.SUCCESSFULLY_ADDED);
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_DATA, dto);
							ro.setActionMessage(session);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SYSTEM_ERROR);
					}
				}
			} else {
				result = PERMISSION_DENIED;
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For add: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String search() {
		return searchModules("",true);
	}

	public String get() {
		
		return getModuleToEdit();
	}

	public String update() {
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("ID:"+dto.getId());
				ro = new RimsModulesDAO().update(dto);
				if (ro.getIsSuccessful()) {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.SUCCESSFULLY_UPDATED);
				}else {					
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ro.getActionMessage());
				}
			}else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		return result;
	}

	public void prepare() throws Exception {
		dto = new RimsModulesDTO();
	}

	public RimsModulesDTO getModel() {
		return dto;
	}
	
	public String goToSearch() {
		throw new UnsupportedOperationException();
	}
	
	public String getModuleToEdit() {
		result = SEARCH;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_FOUR) {
			if (dto != null) {
				logger.debug("Edit ID:"+dto.getId());	
				dto = new RimsModulesDAO().getDTO(RimsModulesDAO.DEFAULT_KEY_COLUMN, dto.getId()+"", "", RimsModulesDAO.DEFAULT_KEY_COLUMN);
				session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
				result = INPUT;
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.INVALID_DATA_FORMAT);
			}
		}
		else {
			result = PERMISSION_DENIED;
		}
		
		return result;
	}
	
	public String searchModules(String additionalCondition, boolean ignoreSearch) {
		result = SEARCH;
		String[][] searchPanel = SearchPanels.MODULES_SEARCH;
		String condition = null;
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			ro = new SearchQueryBuilder().getSearchQuery(request, session,searchPanel, RimsModulesDAO.TABLE_NAME, rimsUsersDTO.getUsrID());
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					condition = ro.getData() + "";
					if (ignoreSearch || condition.length() > 0) {
						if (condition.length() == 0) {
							condition = "";
						}
						condition += additionalCondition;						
						logger.debug(condition);
						
						count = new CommonDAO().getCount(RimsModulesDAO.TABLE_NAME,RimsModulesDAO.DEFAULT_KEY_COLUMN,condition, false);
						if (count > 0) {
							map = rn.getRecordNavigation(count);
							condition += " ORDER BY module_name ASC LIMIT "+ map.get(RecordNavigation.START_INDEX)+ ","+ map.get(RecordNavigation.LIMIT);
							ro = new RimsModulesDAO().getMap(null, condition ,RimsModulesDAO.DEFAULT_KEY_COLUMN);
							if (ro.getIsSuccessful()) {
								session.setAttribute(ApplicationConstant.ACTION_DATA,ro.getData());
							} else {
								session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
							}
						}
						else {
							session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
						}
					}
					else {
						session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.USE_SEARCH);
					}
				}
				else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.NO_DATA_FOUND);
				}
			}
			else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SYSTEM_ERROR);
			}
		}else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		
		return result;
	}

}
