package com.revesoft.rims.revesoft.rimsPermissions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import com.revesoft.rims.revesoft.rimsModules.RimsModulesDTO;
import com.revesoft.rims.revesoft.rimsModules.RimsModulesRepository;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.NumericHelper;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.RimsUpdateLogHelper;

public class RimsPermissionsDAO implements DAOInterface {

	public static final String TABLE_NAME = "poPermissions";
	public static final String DEFAULT_KEY_COLUMN = "id";

	private Logger logger = Logger
			.getLogger(RimsPermissionsDAO.class.getName());
	private QueryHelper queryHelper = new QueryHelper();
	private LinkedHashMap<String, RimsPermissionsDTO> data;
	private int createdBy;
	private Thread t;

	public ReturnObject insert(Object obj) {
		ReturnObject ro = new ReturnObject();
		HttpServletRequest request = null;
		Connection connection = null;
		PreparedStatement pstmt = null;
		String sql = null;
		ArrayList<RimsModulesDTO> modulesList = null;
		int roleId = 0;
		long creationTime = 0;
		try {
			if (obj != null && obj instanceof HttpServletRequest) {
				request = (HttpServletRequest) obj;
				modulesList = RimsModulesRepository.getInstance(false).getAll(
						true);
				if (modulesList != null && modulesList.size() > 0) {
					createdBy = NumericHelper.parseInt(request
							.getParameter("createdBy"));
					roleId = NumericHelper.parseInt(request
							.getParameter("roleId"));
					if (createdBy > 0 && roleId > 0) {
						ro = getMap(null,
								" AND permission_type='role' AND table_id="
										+ roleId, null);
						sql = "INSERT INTO "
								+ TABLE_NAME
								+ " (created_by,permission_type,table_id,module_id,permission_level,creation_time) VALUES (?,?,?,?,?,?)";
						connection = DatabaseManagerSuccessful.getInstance()
								.getConnection();
						connection.setAutoCommit(false);
						pstmt = connection.prepareStatement(sql);
						pstmt = connection.prepareStatement(sql);
						creationTime = System.currentTimeMillis();
						for (RimsModulesDTO dto : modulesList) {
							pstmt.setInt(1, createdBy);
							pstmt.setString(2, "role");
							pstmt.setInt(3, roleId);
							pstmt.setInt(4, dto.getId());
							pstmt.setInt(5, NumericHelper.parseInt(request
									.getParameter(ApplicationConstant.ID
											+ dto.getId())));
							pstmt.setLong(6, creationTime);
							pstmt.addBatch();
						}
						if (pstmt.executeBatch().length > 0) {
							connection.commit();
							ro.setIsSuccessful(true);
							if (data != null && data.size() > 0) {
								t = new Thread(new Runnable() {

									@Override
									public void run() {
										String changeDetails;
										String sql;
										for (RimsPermissionsDTO dto : data
												.values()) {
											if (dto != null) {
												changeDetails = "created_by: "
														+ dto.getCreatedBy()
														+ ",creation_time: "
														+ dto.getCreationTime()
														+ ",current_status: "
														+ dto.getCurrentStatus()
														+ ",permission_type: "
														+ dto.getPermissionType()
														+ ",table_id: "
														+ dto.getTableId()
														+ ",module_id: "
														+ dto.getModuleId()
														+ ",permission_level: "
														+ dto.getPermissionLevel();
												new RimsUpdateLogHelper(createdBy,
														TABLE_NAME, dto.getId(),
														changeDetails, "delete");
												sql = "DELETE FROM " + TABLE_NAME
														+ " WHERE id=" + dto.getId();
												new CommonDAO().executeUpdate(sql, false);
											}
										}
									}
								});
								t.start();
							}
						} else {
							ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
						}
					} else {
						ro.setActionMessage(ActionMessages.INVALID_DATA_FORMAT);
					}
				} else {
					ro.setActionMessage(ActionMessages.SYSTEM_ERROR);
				}
			} else {
				ro.setActionMessage(ActionMessages.INVALID_REQUEST);
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (ClassCastException e) {
			logger.fatal("ClassCastException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		ReturnObject ro = new ReturnObject();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		RimsPermissionsDTO dto = null;
		DateAndTimeHelper dth = null;
		try {
			sql = "SELECT * FROM " + TABLE_NAME + " WHERE id!=0 ";
			sql += queryHelper.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += condition;
			}
			connection = DatabaseManagerSuccessful.getInstance()
					.getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			data = new LinkedHashMap<String, RimsPermissionsDTO>();
			keyColumn = queryHelper.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			dth = new DateAndTimeHelper();
			while (rs.next()) {
				dto = new RimsPermissionsDTO();
				dto.setId(rs.getInt("id"));
				dto.setCreatedBy(rs.getInt("created_by"));
				dto.setCreationTime(dth.getDateFromLong(rs
						.getLong("creation_time")));
				dto.setCurrentStatus(rs.getString("current_status"));
				dto.setPermissionType(rs.getString("permission_type"));
				dto.setTableId(rs.getInt("table_id"));
				dto.setModuleId(rs.getInt("module_id"));
				dto.setPermissionLevel(rs.getInt("permission_level"));
				data.put(rs.getString(keyColumn), dto);
			}
			if (data != null && data.size() > 0) {
				ro.setIsSuccessful(true);
				ro.setData(data);
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

	@SuppressWarnings("unchecked")
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		ReturnObject ro = getMap(vals, condition, keyColumn);
		LinkedHashMap<String, RimsPermissionsDTO> result = null;
		ArrayList<RimsPermissionsDTO> data = null;
		if (ro != null && ro.getIsSuccessful()) {
			try {
				result = (LinkedHashMap<String, RimsPermissionsDTO>) ro
						.getData();
				if (result != null && result.size() > 0) {
					data = new ArrayList<RimsPermissionsDTO>(result.size());
					for (RimsPermissionsDTO dto : result.values()) {
						data.add(dto);
					}
					if (data != null && data.size() > 0) {
						ro.setData(data);
					}
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		return ro;
	}

	public ReturnObject update(Object obj) {
		throw new UnsupportedOperationException();
	}

	public boolean validate(Object obj) {
		throw new UnsupportedOperationException();
	}

	public boolean isExisting() {
		throw new UnsupportedOperationException();
	}

	@SuppressWarnings("unchecked")
	public ReturnObject insertAclPermission(RimsPermissionsDTO dto) {
		ReturnObject ro = new ReturnObject();
		String sql = null;
		CommonDAO dao = null;
		LinkedHashMap<String, RimsPermissionsDTO> data = null;
		if (dto != null && dto.getCreatedBy() > 0 && dto.getTableId() > 0
				&& dto.getModuleId() > 0) {
			ro = getMap(null, " AND permission_type='user' AND table_id="
					+ dto.getTableId() + " AND module_id=" + dto.getModuleId(), "table_id");
			if (ro != null && ro.getIsSuccessful()) {
				data = (LinkedHashMap<String, RimsPermissionsDTO>) ro.getData();
			}
			
			sql = "INSERT INTO "
					+ TABLE_NAME
					+ " (created_by,permission_type,table_id,module_id,permission_level) VALUES ("
					+ dto.getCreatedBy() + ",'user'," + dto.getTableId() + ","
					+ dto.getModuleId() + "," + dto.getPermissionLevel() + ")";
			dao = new CommonDAO();
			ro = dao.executeUpdate(sql, false);
			if (ro != null && ro.getIsSuccessful()) {
				if (data != null && data.size() > 0) {
					sql = "0";
					for(RimsPermissionsDTO rpDTO: data.values()) {
						if (rpDTO != null) {
							new RimsUpdateLogHelper(dto.getCreatedBy(), TABLE_NAME, rpDTO.getId(), "id: " + rpDTO.getId() + ", createdBy: " + rpDTO.getCreatedBy() + ", creationTime: " + rpDTO.getCreationTime() + ", permissionType: " + rpDTO.getPermissionType() + ", tableId: " + rpDTO.getTableId() + ", moduleId: " + rpDTO.getModuleId() + ", permissionLevel: " + rpDTO.getPermissionLevel(), "insertAclPermission");
							sql += "," + rpDTO.getId();
						}
					}
					sql = "DELETE FROM " + TABLE_NAME
							+ " WHERE id IN (" + sql + ")";
					ro = dao.executeUpdate(sql, false);
				}
			}
		} else {
			ro.setActionMessage(ActionMessages.VALIDATION_ERROR);
		}
		return ro;
	}

	public LinkedHashMap<Integer, Integer> getPermissionLevel(int role, int user) {
		LinkedHashMap<Integer, Integer> data = null;
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			sql = "SELECT module_id,permission_level FROM " + TABLE_NAME
					+ " WHERE (permission_type='role' AND table_id=" + role
					+ ") OR (permission_type='user' AND table_id=" + user
					+ ") ORDER BY permission_type";
			logger.debug(sql);
			connection = DatabaseManagerSuccessful.getInstance()
					.getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			data = new LinkedHashMap<Integer, Integer>();
			while (rs.next()) {
				data.put(rs.getInt("module_id"), rs.getInt("permission_level"));
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return data;
	}

	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		throw new UnsupportedOperationException();
	}

}
