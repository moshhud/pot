package com.revesoft.rims.revesoft.rimsPermissions;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.ApplicationConstant;

public class RimsPermissionsDTO extends CommonDTO {

	private String permissionType;
	private int tableId;
	private int moduleId;
	private int permissionLevel;
	private String currentStatus;

	public String getCurrentStatus() {
		return v.checkDTO(currentStatus, ApplicationConstant.ACTIVE);
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getPermissionType() {
		return v.checkDTO(permissionType);
	}

	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}

	public int getTableId() {
		return tableId;
	}

	public void setTableId(int tableId) {
		this.tableId = tableId;
	}

	public int getModuleId() {
		return moduleId;
	}

	public void setModuleId(int moduleId) {
		this.moduleId = moduleId;
	}

	public int getPermissionLevel() {
		return permissionLevel;
	}

	public void setPermissionLevel(int permissionLevel) {
		this.permissionLevel = permissionLevel;
	}
}
