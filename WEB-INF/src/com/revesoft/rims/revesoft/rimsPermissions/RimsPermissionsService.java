package com.revesoft.rims.revesoft.rimsPermissions;

import java.util.ArrayList;

import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.util.CollectionHelper;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public class RimsPermissionsService {

	public ReturnObject getPermissionBasedQuery(int permissionLevel, int empId,
			int teamId, int viewOnlyOwn, int viewOnlyTeam, String columnName,
			boolean getAdvanceQuery) {
		ReturnObject ro = new ReturnObject();
		String query = null;
		ArrayList<String> list = null;
		QueryHelper qh = null;
		if (empId > 0 && teamId > 0 && columnName != null
				&& columnName.length() > 0) {
			columnName = new QueryHelper().getMysqlRealScapeString(columnName);
			if (permissionLevel == viewOnlyTeam) {
				// query = " AND account_manager_id IN ("
				// + RimsUsersRepository.getInstance(false)
				// .getEmployeeProfileIdsByTeam(teamId) + ")";
			} else if (permissionLevel == viewOnlyOwn) {
				query = " AND account_manager_id=" + empId;
			} else {
				query = "";
			}
			ro.setIsSuccessful(true);
			if (getAdvanceQuery) {
				list = new CommonDAO().getArrayList(
						"ContactMasterDAO.TABLE_NAME", "id", null, query, true);
				if (new CollectionHelper().checkCollection(list)) {
					qh = new QueryHelper();
					query = " AND " + qh.getMysqlRealScapeString(columnName)
							+ " IN ("
							+ qh.getQueryStringFromArrayList(list, false) + ")";
				} else {
					ro.setIsSuccessful(false);
				}
			}
			ro.setData(query);
		}
		return ro;
	}

}
