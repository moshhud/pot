package com.revesoft.rims.revesoft.rimsPermissions;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import dev.mashfiq.common.MasterAction;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Permissions;
import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.Validations;

public class RimsPermissionsAction extends MasterAction implements
		ModelDriven<RimsPermissionsDTO>, Preparable {

	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(RimsPermissionsAction.class
			.getName());

	private RimsPermissionsDTO dto = null;

	public String add() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			if (rimsUsersDTO
					.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_THREE) {
				ro = new RimsPermissionsDAO().insert(request);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_ADDED);
					} else {
						ro.setActionMessage(session);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.SYSTEM_ERROR);
				}
			} else {
				result = PERMISSION_DENIED;
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String addAclPermission() {
		long startTime = System.currentTimeMillis();
		if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
			dto.setCreatedBy(rimsUsersDTO.getUsrID());
			ro = new RimsPermissionsDAO().insertAclPermission(dto);
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(), RimsPermissionsDAO.TABLE_NAME, dto.getTableId(), "ACL Permission updated", "addAclPermission");
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.SUCCESSFULLY_ADDED);					
				} else {
					ro.setActionMessage(session);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.SYSTEM_ERROR);
			}
		} else {
			session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
					ActionMessages.MULTIPLE_SUBMIT);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String search() {
		return null;
	}

	public String get() {
		return null;
	}

	public String update() {
		return null;
	}

	public void prepare() throws Exception {
		dto = new RimsPermissionsDTO();
	}

	public RimsPermissionsDTO getModel() {
		return dto;
	}
	
	public String goToSearch() {
		throw new UnsupportedOperationException();
	}

}
