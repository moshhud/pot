package com.revesoft.rims.revesoft.rimsUploadedFiles;

import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public class RimsUploadedFilesService {
	
	private Logger logger = Logger.getLogger(RimsUploadedFilesService.class.getName());
	public static final int SUCCESS_STORY = 202;
	public static final int WOW_FACTOR = 203;
			
	
	public RimsUploadedFilesDTO getDTO(int id) {
		return getDTO("id", id + "", null);
	}

	@SuppressWarnings("unchecked")
	public RimsUploadedFilesDTO getDTO(String colName, String value,
			String condition) {
		RimsUploadedFilesDTO dto = null;
		QueryHelper qh;
		ReturnObject ro;
		LinkedHashMap<String, RimsUploadedFilesDTO> data = null;
		if (colName != null && colName.length() > 0 && value != null
				&& value.length() > 0) {
			if (condition == null || condition.length() == 0) {
				condition = "";
			}
			qh = new QueryHelper();
			condition += " AND " + qh.getMysqlRealScapeString(colName) + "='" + qh.getMysqlRealScapeString(value) + "'";
			ro = new RimsUploadedFilesDAO().getMap(null, condition, colName);
			if (ro != null && ro.getIsSuccessful()) {
				try {
					data = (LinkedHashMap<String, RimsUploadedFilesDTO>) ro.getData();
					if (data != null && data.containsKey(value)) {
						dto = data.get(value);
					}
				} catch (ClassCastException e) {
					logger.fatal("ClassCastException", e);
				}
			}
		}
		return dto;
	}

}
