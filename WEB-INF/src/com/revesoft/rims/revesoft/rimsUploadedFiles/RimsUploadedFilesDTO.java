package com.revesoft.rims.revesoft.rimsUploadedFiles;

import dev.mashfiq.common.CommonDTO;
import dev.mashfiq.util.ApplicationConstant;

public class RimsUploadedFilesDTO extends CommonDTO{

	private String moduleType;
	private String title;
	private String originalFileName;
	private String currentStatus;

	public String getCurrentStatus() {
		return v.checkDTO(currentStatus, ApplicationConstant.ACTIVE);
	}

	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getModuleType() {
		return v.checkDTO(moduleType);
	}

	public void setModuleType(String moduleType) {
		this.moduleType = moduleType;
	}

	public String getOriginalFileName() {
		return v.checkDTO(originalFileName);
	}

	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
