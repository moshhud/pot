package com.revesoft.rims.revesoft.rimsUploadedFiles;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.DateAndTimeHelper;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.Messages;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public class RimsUploadedFilesDAO implements DAOInterface {

	public static final String TABLE_NAME = "rims_uploaded_files";
	public static final String DEFAULT_KEY_COLUMN = "id";

	private Logger logger = Logger.getLogger(RimsUploadedFilesDAO.class
			.getName());
	private RimsUploadedFilesDTO dto;
	private ReturnObject ro;
	private CommonDAO commonDAO;
	private String sql;
	private QueryHelper qh = new QueryHelper();

	@Override
	public ReturnObject insert(Object obj) {
		ro = new ReturnObject();
		if (validate(obj)) {
			if (isExisting() == false) {
				sql = "INSERT INTO rims_uploaded_files (ref_table_name,"
						+ "ref_table_id,module_type,original_file_name,"
						+ "file_name,created_by,creation_time) VALUES " + "('"
						+ qh.getMysqlRealScapeString(dto.getTableName())
						+ "'," + dto.getTableId() + ",'"
						+ qh.getMysqlRealScapeString(dto.getModuleType())
						+ "','"
						+ qh.getMysqlRealScapeString(dto.getOriginalFileName())
						+ "','"
						+ qh.getMysqlRealScapeString(dto.getFileFileName())
						+ "'," + dto.getCreatedBy() + ","
						+ System.currentTimeMillis() + ")";
				if (commonDAO == null) {
					commonDAO = new CommonDAO();
				}
				ro = commonDAO.executeUpdate(sql, false);
			}
		}
		return ro;
	}

	@Override
	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		long t, startTime = System.currentTimeMillis();
		ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		LinkedHashMap<String, RimsUploadedFilesDTO> data = null;
		RimsUploadedFilesDTO dto;
		DateAndTimeHelper dth;
		try {
			sql = "SELECT * FROM " + TABLE_NAME + " WHERE id!=0 ";
			sql += qh.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += condition;
			}
			keyColumn = qh.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			t = System.currentTimeMillis();
			connection = DatabaseManagerSuccessful.getInstance()
					.getConnection();
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
			logger.debug("Database Execution Time: "
					+ (System.currentTimeMillis() - t));
			data = new LinkedHashMap<String, RimsUploadedFilesDTO>();
			dth = new DateAndTimeHelper();
			while (rs.next()) {
				dto = new RimsUploadedFilesDTO();
				dto.setId(rs.getInt("id"));
				dto.setTableName(rs.getString("ref_table_name"));
				dto.setTitle(rs.getString("title"));
				dto.setTableId(rs.getInt("ref_table_id"));
				dto.setModuleType(rs.getString("module_type"));
				dto.setOriginalFileName(rs.getString("original_file_name"));
				dto.setFileFileName(rs.getString("file_name"));
				dto.setCreatedBy(rs.getInt("created_by"));
				dto.setCreationTime(dth.getDateFromLong(rs
						.getLong("creation_time")));
				dto.setCurrentStatus(rs.getString("current_status"));
				data.put(rs.getString(keyColumn), dto);
			}
			if (data != null && data.size() > 0) {
				ro.setData(data);
				ro.setIsSuccessful(true);
			}
		} catch (NullPointerException e) {
			logger.fatal("");
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			//FinalCleanUp.closeDatabaseConnection(connection);
			try {
				if(connection!=null){
					connection.close();
				} 
			}
			catch(Exception e) {
				logger.fatal(e.toString());
			}
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getMap: "
					+ (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ReturnObject update(Object obj) {
		ro = new ReturnObject();
		if (validate(obj)) {
			if (isExisting() == false) {
				if (dto.getId() > 0) {
					sql = "UPDATE rims_uploaded_files SET ref_table_name='"
							+ qh.getMysqlRealScapeString(dto.getTableName())
							+ "',ref_table_id="
							+ dto.getTableId()
							+ ",module_type='"
							+ qh.getMysqlRealScapeString(dto.getModuleType())
							+ "',original_file_name='"
							+ qh.getMysqlRealScapeString(dto
									.getOriginalFileName())
							+ "',file_name='"
							+ qh.getMysqlRealScapeString(dto.getFileFileName())
							+ "',current_status='"
							+ qh.getMysqlRealScapeString(dto.getCurrentStatus())
							+ "' WHERE id=" + dto.getId();
					if (commonDAO == null) {
						commonDAO = new CommonDAO();
					}
					ro = commonDAO.executeUpdate(sql, false);
				} else {
					ro.setActionMessage(ActionMessages.INVALID_REQUEST);
				}
			}
		}
		return ro;
	}

	@Override
	public boolean validate(Object obj) {
		boolean isValid = false;
		ArrayList<Messages> list = new ArrayList<Messages>();
		if (obj != null && obj instanceof RimsUploadedFilesDTO) {
			dto = (RimsUploadedFilesDTO) obj;
			if (dto != null) {
				isValid = true;
				if (dto.getFile() == null) {
					isValid = false;
					list.add(new Messages()
							.setErrorMessage("Select file for upload"));
				}
				if (dto.getModuleType() == null
						|| dto.getModuleType().length() == 0
						|| dto.getModuleType().equalsIgnoreCase("select")) {
					isValid = false;
					list.add(new Messages().setErrorMessage("Select module"));
				}
				if (dto.getFileFileName() == null
						|| dto.getFileFileName().length() == 0) {
					isValid = false;
					list.add(new Messages()
							.setErrorMessage("Inavlid new generated file name!"));
				}
				if (dto.getOriginalFileName() == null
						|| dto.getOriginalFileName().length() == 0) {
					isValid = false;
					list.add(new Messages()
							.setErrorMessage("Inavlid original file name!"));
				}
				if (dto.getCreatedBy() == 0) {
					isValid = false;
					list.add(new Messages()
							.setErrorMessage("Can identify you!"));
				}

			} else {
				list.add(new Messages().setErrorMessage("Inavlid Input Data!"));
			}
		} else {
			list.add(new Messages().setErrorMessage("Inavlid Argument!"));
		}
		ro.setMsgList(list);
		return isValid;
	}

	@Override
	public boolean isExisting() {
		boolean isExisting = true;
		if (dto != null) {
			isExisting = false;
		}
		return isExisting;
	}
}
