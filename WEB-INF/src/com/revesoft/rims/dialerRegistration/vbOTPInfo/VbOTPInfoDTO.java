package com.revesoft.rims.dialerRegistration.vbOTPInfo;

import dev.mashfiq.common.CommonDTO;

public class VbOTPInfoDTO extends CommonDTO {

	private int otpSerial;
	private String otpSNInfo;

	public int getOtpSerial() {
		return otpSerial;
	}

	public void setOtpSerial(int otpSerial) {
		this.otpSerial = otpSerial;
	}

	public String getOtpSNInfo() {
		return v.checkDTO(otpSNInfo);
	}

	public void setOtpSNInfo(String otpSNInfo) {
		this.otpSNInfo = otpSNInfo;
	}

}
