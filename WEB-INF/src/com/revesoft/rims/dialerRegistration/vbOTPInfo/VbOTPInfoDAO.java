package com.revesoft.rims.dialerRegistration.vbOTPInfo;

import java.sql.Connection;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManagerReseller;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public class VbOTPInfoDAO implements DAOInterface {
	
	public static final String TABLE_NAME = "vbOTPInfo";
	public static final String DEFAULT_KEY_COLUMN = "otpSerial";

	private Logger logger = Logger.getLogger(VbOTPInfoDAO.class.getName());
	private QueryHelper qh = new QueryHelper();

	@Override
	public ReturnObject insert(Object obj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		long startTime = System.currentTimeMillis(), t;
		ReturnObject ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		LinkedHashMap<String, VbOTPInfoDTO> data = null;
		VbOTPInfoDTO dto;
		try {
			sql = "SELECT * FROM vbOTPInfo WHERE otpSerial!=0 ";
			sql += qh.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += condition;
			}
			keyColumn = qh.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			t = System.currentTimeMillis();
			connection = DatabaseManagerReseller.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
			logger.debug("Database Execution Time " + startTime + " : " + (System.currentTimeMillis() - t));
			data = new LinkedHashMap<String, VbOTPInfoDTO>();
			while(rs.next()) {
				dto = new VbOTPInfoDTO();
				dto.setOtpSerial(rs.getInt("otpSerial"));
				dto.setOtpSNInfo(rs.getString("otpSNInfo"));
				data.put(rs.getString(keyColumn), dto);
			}
			if (data != null && data.size() > 0) {
				ro.setData(data);
				ro.setIsSuccessful(true);
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (SQLException e) {
			logger.fatal("Exception", e);
		} catch (JDOMException e) {
			logger.fatal("Exception", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("Exception", e);
		} catch (IllegalAccessException e) {
			logger.fatal("Exception", e);
		} catch (InstantiationException e) {
			logger.fatal("Exception", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ReturnObject update(Object obj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean validate(Object obj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isExisting() {
		throw new UnsupportedOperationException();
	}

}
