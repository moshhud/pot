package com.revesoft.rims.dialerRegistration.mdProductList;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import dev.mashfiq.util.ReturnObject;

public class MdProductListRepository {
	private static MdProductListRepository mdProductListRepository;
	private ArrayList<MdProductListDTO> list;

	static {
		mdProductListRepository = new MdProductListRepository();
	}
	@SuppressWarnings("unchecked")
	private MdProductListRepository() {
		ReturnObject ro = new MdProductListDAO().getArrayList(null, " ORDER BY plProductName ");
		if (ro != null && ro.getIsSuccessful()) {
			list = (ArrayList<MdProductListDTO>) ro.getData();
		}
	}

	public static MdProductListRepository getInstance(boolean forceReload) {
		if (forceReload || mdProductListRepository == null) {
			mdProductListRepository = new MdProductListRepository();
		}
		return mdProductListRepository;
	}

	public ArrayList<MdProductListDTO> getAllData() {
		return list;
	}
	
	public  LinkedHashMap<Integer, String> getProductIDName(String plCurrentStatus, int isDeleted){
		LinkedHashMap<Integer, String> data = null;
		if (list != null && list.size() > 0) {
			data = new LinkedHashMap<Integer, String>();
			for (MdProductListDTO dto : list) {				
				if (dto.getPlCurrentStatus().equals(plCurrentStatus)) {
					data.put(dto.getPlProductID(), dto.getPlProductName());
				}
			}
		}
		
		return data;
	}
	public  LinkedHashMap<Integer, Float> getProductIDPrice(int id){
		LinkedHashMap<Integer, Float> data = null;
		if (list != null && list.size() > 0) {
			data = new LinkedHashMap<Integer, Float>();
			for (MdProductListDTO dto : list) {				
				if (dto.getPlProductID()==id) {
					data.put(dto.getPlProductID(), dto.getPlPriceUSD());
				}
			}
		}
		
		return data;
	}

	public MdProductListDTO getDTOByPlProductID(int id) {
		MdProductListDTO data = null;
		if (list != null && list.size() > 0) {
			for (MdProductListDTO dto : list) {
				if (dto.getPlProductID() == id) {
					data = dto;
					break;
				}
			}
		}
		return data;
	}
	
	public String getPlProductStatusByPlProductID(int id) {
		String plProductStatus = "N/A";
		if (list != null && list.size() > 0) {
			for (MdProductListDTO dto : list) {
				if (dto.getPlProductID() == id) {
					plProductStatus = dto.getPlProductStatus();
					break;
				}
			}
		}
		return plProductStatus;
	}

	public String getPlProductNameByPlProductID(int id) {
		String name = "N/A";
		if (list != null && list.size() > 0) {
			for (MdProductListDTO dto : list) {
				if (dto.getPlProductID() == id) {
					name = dto.getPlProductName();
					break;
				}
			}
		}
		return name;
	}
	public float getPlPriceUSDByPlProductID(int id) {
		float price = 0.0f;
		if (list != null && list.size() > 0) {
			for (MdProductListDTO dto : list) {
				if (dto.getPlProductID() == id) {
					price = dto.getPlPriceUSD();
					break;
				}
			}
		}
		return price;
	}

	
}
