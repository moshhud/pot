package com.revesoft.rims.dialerRegistration.mdProductList;

import java.util.HashMap;

import org.apache.log4j.Logger;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.MasterAction;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.Permissions;
import dev.mashfiq.util.RecordNavigation;
import dev.mashfiq.util.RimsUpdateLogHelper;
import dev.mashfiq.util.SearchPanels;
import dev.mashfiq.util.SearchQueryBuilder;
import dev.mashfiq.util.Validations;

public class MdProductListAction extends MasterAction implements ModelDriven<MdProductListDTO>, Preparable {

	private static final long serialVersionUID = 1L;
	private Logger logger = Logger.getLogger(MdProductListAction.class
			.getName());
	private MdProductListDTO dto;

	@Override
	public void prepare() throws Exception {
		dto = new MdProductListDTO();
	}

	@Override
	public MdProductListDTO getModel() {
		return dto;
	}

	public String newMdProductList() {
		long startTime = System.currentTimeMillis();
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_THREE) {
			result = INPUT;
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For newMdProductList " + startTime
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	@Override
	public String add() {
		long startTime = System.currentTimeMillis();
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_THREE) {
			dto.setPlCreatedBy(rimsUsersDTO.getUsrID());
			ro = new MdProductListDAO().insert(dto);
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					MdProductListRepository.getInstance(true);
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,ActionMessages.SUCCESSFULLY_ADDED);
				} else {
					ro.setActionMessage(session);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.SYSTEM_ERROR);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For add " + startTime
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	public String goToSearch() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_TWO) {
			new RecordNavigation(SearchPanels.MDPRODUCTLIST, request,
					"search-mdproductlist.html");
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For goToSearch: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	@Override
	public String search() {
		long startTime = System.currentTimeMillis();
		result = SEARCH;
		String[][] searchPanel = SearchPanels.MDPRODUCTLIST;
		String condition = " and 1=1 ";
		int count = 0;
		HashMap<String, Integer> map = null;
		RecordNavigation rn = null;
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_TWO) {
			rn = new RecordNavigation(searchPanel, request);
			ro = new SearchQueryBuilder().getSearchQuery(request, session,
					searchPanel, MdProductListDAO.TABLE_NAME,
					rimsUsersDTO.getUsrID());
			if (ro != null) {
				if (ro.getIsSuccessful()) {
					condition = ro.getData() + "";
					if (true) {
						count = new CommonDAO().getCount(
								MdProductListDAO.TABLE_NAME,
								MdProductListDAO.DEFAULT_KEY_COLUMN, condition,
								false);
						if (count > 0) {
							map = rn.getRecordNavigation(count);
							if (map != null && map.size() > 0) {
								condition += " ORDER BY plProductID DESC LIMIT "
										+ map.get(RecordNavigation.START_INDEX)
										+ "," + map.get(RecordNavigation.LIMIT);
								ro = new MdProductListDAO().getMap(null,
										condition, null);
								if (ro != null) {
									if (ro.getIsSuccessful()) {
										session.setAttribute(
												ApplicationConstant.ACTION_DATA,
												ro.getData());
									} else {
										session.setAttribute(
												ApplicationConstant.ACTION_MESSAGE,
												ActionMessages.NO_DATA_FOUND);
									}
								} else {
									session.setAttribute(
											ApplicationConstant.ACTION_MESSAGE,
											ActionMessages.SYSTEM_ERROR);
								}
							} else {
								session.setAttribute(
										ApplicationConstant.ACTION_MESSAGE,
										ActionMessages.SYSTEM_ERROR);
							}
						} else {
							session.setAttribute(
									ApplicationConstant.ACTION_MESSAGE,
									ActionMessages.NO_DATA_FOUND);
						}
					} else {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.USE_SEARCH);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.NO_DATA_FOUND);
				}
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.SYSTEM_ERROR);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For search: "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	@Override
	public String get() {
		long startTime = System.currentTimeMillis();
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_TWO) {
			dto = MdProductListRepository.getInstance(false)
					.getDTOByPlProductID(dto.getPlProductID());
			if (dto != null) {
				new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
						MdProductListDAO.TABLE_NAME, dto.getId(), "viewed",
						"get");
				session.setAttribute(ApplicationConstant.ACTION_DATA, dto);
				result = INPUT;
			} else {
				session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
						ActionMessages.NO_DATA_FOUND);
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For get" + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

	@Override
	public String update() {
		long startTime = System.currentTimeMillis();
		MdProductListDTO oldDTO = null;
		if (rimsUsersDTO
				.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_THREE) {
			if (Validations.checkCaptcha(dto.getCaptcha(), session)) {
				oldDTO = MdProductListRepository.getInstance(false)
						.getDTOByPlProductID(dto.getPlProductID());
				dto.setPlCreatedBy(rimsUsersDTO.getUsrID());
				ro = new MdProductListDAO().update(dto);
				if (ro != null) {
					if (ro.getIsSuccessful()) {
						session.setAttribute(
								ApplicationConstant.ACTION_MESSAGE,
								ActionMessages.SUCCESSFULLY_ADDED);
						MdProductListRepository.getInstance(true);
						new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(),
								MdProductListDAO.TABLE_NAME, dto.getId(),
								oldDTO, dto, "update");
					} else {
						ro.setActionMessage(session);
					}
				} else {
					session.setAttribute(ApplicationConstant.ACTION_MESSAGE,
							ActionMessages.SYSTEM_ERROR);
				}
			}
		} else {
			result = PERMISSION_DENIED;
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For update " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return result;
	}

}
