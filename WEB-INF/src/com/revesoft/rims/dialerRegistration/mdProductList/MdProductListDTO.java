package com.revesoft.rims.dialerRegistration.mdProductList;

import java.io.File;

import dev.mashfiq.common.CommonDTO;

public class MdProductListDTO extends CommonDTO {

	private int plProductID;
	private String plProductName;
	private float plPriceUSD;
	private String plFileName;
	private int plProductType;
	private int plProductSubType;
	private int plParentProductID;
	private int plStatusType;
	private String plCurrentStatus;
	private String plSupportPortalShow;
	private String plHasAfterSalesService;
	private String isRecursive;
	private int noOfMonths;
	private File file;
	private int plCreatedBy;
	private String currentProductName;
	private String plUpdateExpireDate;
	private String plProductStatus;
	private String showInInvoiceList;
	private String showInProductStatusList;
	private String plShowExpireDate;
	private int plConcurrentCallRegistrationLimit;
	private int plInvoiceLifeTime;
	private String plIsPlatinum;
	private String plDescription;	
	private String plVendorName;
	private String isDeleted;
	
	
	

	public String getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}

	public String getPlVendorName() {
		return v.checkDTO(plVendorName,"");
	}

	public void setPlVendorName(String plVendorName) {
		this.plVendorName = plVendorName;
	}

	public String getPlDescription() {
		return v.checkDTO(plDescription,"");
	}

	public void setPlDescription(String plDescription) {
		this.plDescription = plDescription;
	}

	public String getPlIsPlatinum() {
		return v.checkDTO(plIsPlatinum,"0");
	}

	public void setPlIsPlatinum(String plIsPlatinum) {
		this.plIsPlatinum = plIsPlatinum;
	}

	public int getPlProductID() {
		return plProductID;
	}

	public void setPlProductID(int plProductID) {
		this.plProductID = plProductID;
	}

	public String getPlProductName() {
		return v.checkDTO(plProductName);
	}

	public void setPlProductName(String plProductName) {
		this.plProductName = plProductName;
	}

	public float getPlPriceUSD() {
		return plPriceUSD;
	}

	public void setPlPriceUSD(float plPriceUSD) {
		this.plPriceUSD = plPriceUSD;
	}

	public String getPlFileName() {
		return v.checkDTO(plFileName);
	}

	public void setPlFileName(String plFileName) {
		this.plFileName = plFileName;
	}

	public int getPlProductType() {
		return plProductType;
	}

	public void setPlProductType(int plProductType) {
		this.plProductType = plProductType;
	}

	public int getPlParentProductID() {
		return plParentProductID;
	}

	public void setPlParentProductID(int plParentProductID) {
		this.plParentProductID = plParentProductID;
	}

	public int getPlStatusType() {
		return plStatusType;
	}

	public void setPlStatusType(int plStatusType) {
		this.plStatusType = plStatusType;
	}

	public String getPlCurrentStatus() {
		return v.checkDTO(plCurrentStatus);
	}

	public void setPlCurrentStatus(String plCurrentStatus) {
		this.plCurrentStatus = plCurrentStatus;
	}
	
	public String getPlSupportPortalShow() {
		return plSupportPortalShow;
	}

	public void setPlSupportPortalShow(String plSupportPortalShow) {
		this.plSupportPortalShow = plSupportPortalShow;
	}

	public String getPlHasAfterSalesService() {
		return v.checkDTO(plHasAfterSalesService);
	}

	public void setPlHasAfterSalesService(String plHasAfterSalesService) {
		this.plHasAfterSalesService = plHasAfterSalesService;
	}

	public String getIsRecursive() {
		return v.checkDTO(isRecursive);
	}

	public void setIsRecursive(String isRecursive) {
		this.isRecursive = isRecursive;
	}

	public int getNoOfMonths() {
		return noOfMonths;
	}

	public void setNoOfMonths(int noOfMonths) {
		this.noOfMonths = noOfMonths;
	}

	public int getPlCreatedBy() {
		return plCreatedBy;
	}

	public void setPlCreatedBy(int plCreatedBy) {
		this.plCreatedBy = plCreatedBy;
	}

	public String getCurrentProductName() {
		return v.checkDTO(currentProductName);
	}

	public void setCurrentProductName(String currentProductName) {
		this.currentProductName = currentProductName;
	}

	public String getPlUpdateExpireDate() {
		return v.checkDTO(plUpdateExpireDate);
	}

	public void setPlUpdateExpireDate(String plUpdateExpireDate) {
		this.plUpdateExpireDate = plUpdateExpireDate;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getPlProductStatus() {
		return v.checkDTO(plProductStatus);
	}

	public void setPlProductStatus(String plProductStatus) {
		this.plProductStatus = plProductStatus;
	}

	public String getShowInInvoiceList() {
		return v.checkDTO(showInInvoiceList, "no");
	}

	public void setShowInInvoiceList(String showInInvoiceList) {
		this.showInInvoiceList = showInInvoiceList;
	}

	public String getShowInProductStatusList() {
		return v.checkDTO(showInProductStatusList,"no");
	}

	public void setShowInProductStatusList(String showInProductStatusList) {
		this.showInProductStatusList = showInProductStatusList;
	}

	public String getPlShowExpireDate() {
		return v.checkDTO(plShowExpireDate, "no");
	}

	public void setPlShowExpireDate(String plShowExpireDate) {
		this.plShowExpireDate = plShowExpireDate;
	}

	public int getPlConcurrentCallRegistrationLimit() {
		return plConcurrentCallRegistrationLimit;
	}

	public void setPlConcurrentCallRegistrationLimit(
			int plConcurrentCallRegistrationLimit) {
		this.plConcurrentCallRegistrationLimit = plConcurrentCallRegistrationLimit;
	}

	public int getPlInvoiceLifeTime() {
		return plInvoiceLifeTime;
	}

	public void setPlInvoiceLifeTime(int plInvoiceLifeTime) {
		this.plInvoiceLifeTime = plInvoiceLifeTime;
	}

	public int getPlProductSubType() {
		return plProductSubType;
	}

	public void setPlProductSubType(int plProductSubType) {
		this.plProductSubType = plProductSubType;
	}

}
