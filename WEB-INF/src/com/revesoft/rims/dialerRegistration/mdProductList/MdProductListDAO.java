package com.revesoft.rims.dialerRegistration.mdProductList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.jdom.JDOMException;

import databasemanager.DatabaseManager;
import databasemanager.DatabaseManagerReseller;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.CommonDAO;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ActionMessages;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.Messages;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;
import dev.mashfiq.util.RimsUpdateLogHelper;

public class MdProductListDAO implements DAOInterface {

	public static final String TABLE_NAME = "poProductList";
	public static final String DEFAULT_KEY_COLUMN = "plProductID";

	private Logger logger = Logger.getLogger(MdProductListDAO.class.getName());
	private MdProductListDTO dto;
	private QueryHelper qh = new QueryHelper();
	private CommonDAO commonDAO;
	private ReturnObject ro;

	@SuppressWarnings({ "resource"})
	@Override
	public ReturnObject insert(Object obj) {
		ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		try {
			if (validate(obj)) {
				if (isExisting() == false) {
					sql = "SELECT MAX(plProductID)+1 AS id FROM "+TABLE_NAME;
					connection = DatabaseManagerSuccessful.getInstance().getConnection();
					pstmt = connection.prepareStatement(sql);
					rs = pstmt.executeQuery(sql);
					if (rs.next()) {
						sql = "INSERT INTO "+TABLE_NAME
								+ " (plProductID,plProductName,"
								+ "plDescription,plVendorName,"
								+ "plPriceUSD,plIsDeleted,"
								+ "plCurrentStatus) VALUES (?,?,?,?,?,?,?)";
						int i=1;
						pstmt = connection.prepareStatement(sql);
						pstmt.setLong(i++, rs.getLong("id"));
						pstmt.setString(i++, dto.getPlProductName());
						pstmt.setString(i++, dto.getPlDescription());
						pstmt.setString(i++, dto.getPlVendorName());
						pstmt.setFloat(i++, dto.getPlPriceUSD());
						pstmt.setInt(i++, 0);
						pstmt.setString(i++, dto.getPlCurrentStatus());
						
						
						if (pstmt.executeUpdate() > 0) {
							new RimsUpdateLogHelper(dto.getPlCreatedBy(), TABLE_NAME, rs.getInt("id"), "added", "insert");
							ro.setIsSuccessful(true);
						}
					} else {
						ro.setMessage(new Messages()
								.setSuccessMessage("No value found while geting MAX(plProductID)+1"));
					}
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("Exception", e);
		} catch (SQLException e) {
			ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
			logger.fatal("Exception", e);
		} catch (JDOMException e) {
			logger.fatal("Exception", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("Exception", e);
		} catch (IllegalAccessException e) {
			logger.fatal("Exception", e);
		} catch (InstantiationException e) {
			logger.fatal("Exception", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		return ro;
	}

	@Override
	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		long startTime = System.currentTimeMillis(), t;
		ro = new ReturnObject();
		Connection connection = null;
		Statement stmt = null;
		ResultSet rs = null;
		String sql = null;
		MdProductListDTO dto = null;
		LinkedHashMap<String, MdProductListDTO> data;
		try {
			sql = "SELECT * FROM " + TABLE_NAME + " WHERE plProductName!='' ";
			sql += qh.getQueryFromHashMap(vals);
			if (condition != null && condition.length() > 0) {
				sql += condition;
			}
			keyColumn = qh.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			t = System.currentTimeMillis();
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			stmt = connection.createStatement();
			rs = stmt.executeQuery(sql);
			logger.debug("Database Execution Time " + startTime + " : "
					+ (System.currentTimeMillis() - t));
			data = new LinkedHashMap<String, MdProductListDTO>();
			while (rs.next()) {
				dto = new MdProductListDTO();
				dto.setPlProductID(rs.getInt("plProductID"));
				dto.setPlProductName(rs.getString("plProductName"));		
				dto.setPlDescription(rs.getString("plDescription"));
				dto.setPlVendorName(rs.getString("plVendorName"));
				dto.setPlPriceUSD(rs.getFloat("plPriceUSD"));
				dto.setPlCurrentStatus(rs.getString("plCurrentStatus"));
				dto.setIsDeleted(rs.getString("plIsDeleted"));			
				data.put(rs.getString(keyColumn), dto);
			}
			if (data.size() > 0) {
				ro.setData(data);
				ro.setIsSuccessful(true);
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(stmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getMap " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		long startTime = System.currentTimeMillis();
		ro = getMap(vals, condition, null);
		LinkedHashMap<String, MdProductListDTO> result;
		ArrayList<MdProductListDTO> data;
		if (ro != null && ro.getIsSuccessful()) {
			try {
				result = (LinkedHashMap<String, MdProductListDTO>) ro.getData();
				if (result != null && result.size() > 0) {
					data = new ArrayList<MdProductListDTO>(result.size());
					for (MdProductListDTO dto : result.values()) {
						if (dto != null) {
							data.add(dto);
						}
					}
					if (data != null && data.size() > 0) {
						ro.setData(data);
					}
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getArrayList " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	@Override
	public ReturnObject update(Object obj) {
		long startTime = System.currentTimeMillis();
		ro = new ReturnObject();
		Connection connection = null;
		PreparedStatement pstmt = null;
		String updateQuery = null;
		try {
			if (validate(obj)) {
				if (dto.getPlProductName() != null
						&& dto.getPlProductName().equalsIgnoreCase(
								dto.getCurrentProductName())
						|| isExisting() == false) {
					updateQuery = "UPDATE "+TABLE_NAME+" SET plProductName=?,"
							+ "plDescription=?,plVendorName=?,plPriceUSD=?,"
							+ "plIsDeleted=?,plCurrentStatus=?"							
							+ "  WHERE plProductID=?";
					connection = DatabaseManagerSuccessful.getInstance().getConnection();
					pstmt = connection.prepareStatement(updateQuery);
					int i=1;
					pstmt.setString(i++, dto.getPlProductName());
					pstmt.setString(i++, dto.getPlDescription());	
					pstmt.setString(i++, dto.getPlVendorName());	
					pstmt.setFloat(i++, dto.getPlPriceUSD());		
					pstmt.setString(i++, dto.getIsDeleted());
					pstmt.setString(i++, dto.getPlCurrentStatus());
					
					
					pstmt.setInt(i++, dto.getPlProductID());
					if (pstmt.executeUpdate() > 0) {
						ro.setIsSuccessful(true);
					} else {
						ro.setMessage(new Messages()
								.setErrorMessage("Not updated for plProductID "
										+ dto.getPlProductID()));
					}
				}
			}
		} catch (NullPointerException e) {
			logger.fatal("NullPointerException", e);
		} catch (SQLException e) {
			ro.setActionMessage(ActionMessages.MYSQL_EXCEPTION);
			logger.fatal("SQLException", e);
		} catch (JDOMException e) {
			logger.fatal("JDOMException", e);
		} catch (ClassNotFoundException e) {
			logger.fatal("ClassNotFoundException", e);
		} catch (IllegalAccessException e) {
			logger.fatal("IllegalAccessException", e);
		} catch (InstantiationException e) {
			logger.fatal("InstantiationException", e);
		} finally {
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For update " + startTime + " : "
					+ (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	@Override
	public boolean validate(Object obj) {
		boolean isValid = false;
		ArrayList<Messages> msgList = new ArrayList<Messages>();
		if (obj != null && obj instanceof MdProductListDTO) {
			dto = (MdProductListDTO) obj;
			if (dto != null) {
				isValid = true;
				if (dto.getPlProductName() == null
						|| dto.getPlProductName().length() == 0) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Enter product name"));
				}
				
				if (dto.getPlPriceUSD() < 0) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Enter product price"));
				}
				if (dto.getPlCurrentStatus().length() == 0) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Select Current Status"));
				}
				if (dto.getPlCreatedBy() <= 0) {
					isValid = false;
					msgList.add(new Messages()
							.setErrorMessage("Could not identify you!"));
				}
			} else {
				msgList.add(new Messages()
						.setErrorMessage(ActionMessages.INVALID_DATA_FORMAT
								.getMsg()));
			}
		} else {
			msgList.add(new Messages()
					.setErrorMessage(ActionMessages.INVALID_DATA_FORMAT
							.getMsg()));
		}
		if (isValid == false && msgList != null && msgList.size() > 0) {
			ro = ReturnObject.clearInstance(ro);
			ro.setMsgList(msgList);
		}
		return isValid;
	}

	@Override
	public boolean isExisting() {
		boolean isExisting = true;
		if (dto != null) {
			if (commonDAO == null) {
				commonDAO = new CommonDAO();
			}
			if (commonDAO
					.getCount(
							TABLE_NAME,
							DEFAULT_KEY_COLUMN,
							" AND plProductName='"
									+ qh.getMysqlRealScapeString(dto
											.getPlProductName()) + "' ", false) == 0) {
				isExisting = false;
			} else {
				ro.setMessage(new Messages()
						.setErrorMessage("Duplicate Product Name '"
								+ dto.getPlProductName() + "'"));
			}
		}
		return isExisting;
	}

}
