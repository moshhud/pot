package com.revesoft.rims.dialerRegistration.mdProductTypes;

import dev.mashfiq.common.CommonDTO;


public class MdProductTypesDTO  extends CommonDTO{

	private String name;
	private String description;
	private int deleteID;

	public String getName() {
		return v.checkDTO(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return v.checkDTO(description);
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getDeleteID() {
		return deleteID;
	}

	public void setDeleteID(int deleteID) {
		this.deleteID = deleteID;
	}

}
