package com.revesoft.rims.dialerRegistration.mdProductTypes;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;

import databasemanager.DatabaseManagerReseller;
import databasemanager.DatabaseManagerSuccessful;
import dev.mashfiq.common.DAOInterface;
import dev.mashfiq.util.ApplicationConstant;
import dev.mashfiq.util.FinalCleanUp;
import dev.mashfiq.util.QueryHelper;
import dev.mashfiq.util.ReturnObject;

public class MdProductTypesDAO implements DAOInterface {

	public static final String TABLE_NAME = "mdProductTypes";
	public static final String DEFAULT_KEY_COLUMN = "id";
	private Logger logger = Logger.getLogger(MdProductTypesDAO.class.getName());
	private QueryHelper qh = new QueryHelper();
	
	@Override
	public ReturnObject insert(Object obj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public ReturnObject getMap(HashMap<String, ArrayList<String>> vals,
			String condition, String keyColumn) {
		ReturnObject ro = new ReturnObject();
		long startTime = System.currentTimeMillis(), t;
		Connection connection = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		String sql = null;
		LinkedHashMap<String, MdProductTypesDTO> data = null;
		MdProductTypesDTO dto;
		try {
			sql = "SELECT * FROM " + TABLE_NAME + " ORDER BY NAME;";
			t = System.currentTimeMillis();
			connection = DatabaseManagerSuccessful.getInstance().getConnection();
			pstmt = connection.prepareStatement(sql);
			rs = pstmt.executeQuery();
			logger.debug("Database Execution Time " + startTime + " : " + (System.currentTimeMillis() - t));
			data = new LinkedHashMap<String, MdProductTypesDTO>();
			keyColumn = qh.getKeyColumn(keyColumn, DEFAULT_KEY_COLUMN);
			while(rs.next()){
				dto = new MdProductTypesDTO();
				dto.setId(rs.getInt("id"));
				dto.setName(rs.getString("NAME"));
				//dto.setDescription(rs.getString("description"));
				//dto.setDeleteID(rs.getInt("deleteID"));
				data.put(rs.getString(keyColumn), dto);
			}
			if (data != null && data.size() > 0) {
				ro.setData(data);
				ro.setIsSuccessful(true);
			}
		} catch (Exception e) {
			logger.fatal("Exception", e);
		}finally{
			FinalCleanUp.closeResource(rs);
			FinalCleanUp.closeResource(pstmt);
			FinalCleanUp.closeDatabaseConnection(connection);
		}
		if (ApplicationConstant.KEEP_DEBUGGING) {
			logger.debug("Execution Time For getMap " + startTime + " : " + (System.currentTimeMillis() - startTime));
		}
		return ro;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ReturnObject getArrayList(HashMap<String, ArrayList<String>> vals,
			String condition) {
		ReturnObject ro = getMap(vals, condition, null);
		LinkedHashMap<String, MdProductTypesDTO> result = null;
		ArrayList<MdProductTypesDTO> data = null;
		if (ro != null && ro.getIsSuccessful()) {
			try {
				result = (LinkedHashMap<String, MdProductTypesDTO>) ro.getData();
				if (result != null && result.size() > 0) {
					data = new ArrayList<MdProductTypesDTO>(result.size());
					for(MdProductTypesDTO dto: result.values()) {
						data.add(dto);
					}
					ro.setData(data);
				}
			} catch (ClassCastException e) {
				logger.fatal("ClassCastException", e);
			}
		}
		return ro;
	}

	@Override
	public ReturnObject update(Object obj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean validate(Object obj) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isExisting() {
		throw new UnsupportedOperationException();
	}

}
