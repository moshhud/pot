package com.revesoft.rims.dialerRegistration.mdProductTypes;

import java.util.ArrayList;

import org.apache.log4j.Logger;

import dev.mashfiq.util.ReturnObject;


public class MdProductTypesRepository {

	static Logger logger = Logger.getLogger(MdProductTypesRepository.class
			.getName());
	private static MdProductTypesRepository mdProductTypesRepository;
	private ArrayList<MdProductTypesDTO> list;

	@SuppressWarnings("unchecked")
	private MdProductTypesRepository() {
		ReturnObject ro = new MdProductTypesDAO().getArrayList(null, null);
		if (ro != null && ro.getIsSuccessful()) {
			list = (ArrayList<MdProductTypesDTO>) ro.getData();
		}
	}

	public static MdProductTypesRepository getInstance(boolean forceReload) {
		if (forceReload || mdProductTypesRepository == null) {
			mdProductTypesRepository = new MdProductTypesRepository();
		}
		return mdProductTypesRepository;
	}

	public ArrayList<MdProductTypesDTO> getProductTypes() {
		return list;
	}

	public synchronized String getProductTypeNameByID(int id) {
		String name = "N/A";
		if (id > 0 && list != null && list.size() > 0) {
			for (MdProductTypesDTO dto : list) {
				if (dto != null && dto.getId() == id) {
					name = dto.getName();
				}
			}
		}
		return name;
	}

}
