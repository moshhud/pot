
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Collections"%>

<%@page import="dev.mashfiq.util.URLHelper"%>

<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="databasemanager.DatabaseManagerSuccessful"%>
<%@page import="dev.mashfiq.util.FinalCleanUp"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Connection"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="java.util.Calendar"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.DateAndTimeHelper"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDAO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListService"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>

<script
	src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/raphael-min.js"
	type="text/javascript"></script>
<script
	src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/morris.js"
	type="text/javascript"></script>
<link rel="stylesheet"
	href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/morris.css">

<%
	DateAndTimeHelper dth = new DateAndTimeHelper();
	/* Thread t = new Thread(new Runnable() {
		
		public void run() {
			new URLHelper().readWriteToURl("https://potracker.revesoft.com/apis/update-unread-leads.jsp", null, false);
		}
		
	});
	t.start(); */
%>
<div class="panel-group" id="accordion">

	<%
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
			.getAttribute(ApplicationConstant.LOGIN_INFO);
	long currentTime = System.currentTimeMillis();
	String td = dth.getDateFromLong(currentTime);
	td = td.substring(3);
	String monthName = dth.getMonthNameFromLongDate(currentTime);
	
	MashupDataRepository mudRepository = MashupDataRepository
			.getInstance(false);
	UIHelper uiHelper = new UIHelper();
	
	if (rimsUsersDTO != null && rimsUsersDTO.getPermissionLevelByModuleId(Permissions.REPORT_DASHBOARD) == 6) {
		try {
	 %>
	
	<%
	Connection connection = null;
	ResultSet rs = null;
	String sql = null;
	CommonDAO commonDAO = null;
	int invcPOStatus=0;
	int invcInvoiceStatus=0;
	int ongoingCount=0;
	int deliveryConfirmCount=0;
	int pendingInvoiceCount=0;
	try{
		connection = DatabaseManagerSuccessful.getInstance().getConnection();
		commonDAO = new CommonDAO();
		sql = "select invcPOStatus,count(*) as c from poInvoice where invcPOStatus not in(515) and invcIsDeleted=0";
		rs = commonDAO.executeQuery(sql, connection);
		if (rs != null) {
			while (rs.next()) {
				invcPOStatus=rs.getInt("invcPOStatus");
				ongoingCount=rs.getInt("c");
			}
		}
		rs.close();
		
		sql = "select invcPOStatus,count(*) as c from poInvoice where invcPOStatus in(515) and invcIsDeleted=0";
		rs = commonDAO.executeQuery(sql, connection);
		if (rs != null) {
			while (rs.next()) {
				invcPOStatus=rs.getInt("invcPOStatus");
				deliveryConfirmCount=rs.getInt("c");
			}
		}
		rs.close();
		
		sql = "select invcInvoiceStatus,count(*) as c from poInvoice where invcInvoiceStatus not in(507) and invcIsDeleted=0";
		rs = commonDAO.executeQuery(sql, connection);
		if (rs != null) {
			while (rs.next()) {
				invcInvoiceStatus=rs.getInt("invcInvoiceStatus");
				pendingInvoiceCount=rs.getInt("c");
			}
		}
		rs.close();
		
		connection.close();
	}
	catch(Exception e){
		
	}
	%>
	<div class="panel panel-info">
			<div class="panel-heading">
			   <h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion"
						href="#dashboard">Dashboard</a>
				</h4>
			</div>
		   <div id="dashboard" class="panel-collapse collapse in">
			  <div class="panel-body">
			   <div class="row">
			    
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><%=ongoingCount %></div>
                                    <div>Ongoing Orders</div>
                                </div>
                            </div>
                        </div>
                        <a href="order-list/search-dashboard-details.html?dashboardType=1">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-home fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><%=deliveryConfirmCount %></div>
                                    <div>Delivery Confirmation</div>
                                </div>
                            </div>
                        </div>
                        <a href="order-list/search-dashboard-details.html?dashboardType=2">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge"><%=pendingInvoiceCount %></div>
                                    <div>Pending Invoice</div>
                                </div>
                            </div>
                        </div>
                        <a href="order-list/search-dashboard-details.html?dashboardType=3">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>                
            </div>
			   </div>
		   </div>
	
	    </div>
	
	   <div class="panel panel-info">
			<div class="panel-heading">
			   <h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion"
						href="#tracking">Order Tracking</a>
				</h4>
			</div>
		   <div id="tracking" class="panel-collapse collapse in">
			  <div class="panel-body">
			   <form action="order-list/po-tracking-details.html" method="post" >
			  
			    <div  class="form-group col-lg-6 col-md-6">
			        <label >PO Number</label>
			        <input name="purchaseOrderNumber" class="form-control"   placeholder="PO Number"/>
			    </div>
			    <div  class="form-group col-lg-6 col-md-6">
			        <label >PO Status</label>
			        <select name="invcPOStatus"  class="form-control">					        
							<%=uiHelper.getSelectFromMashup(mudRepository.getByFieldType(mudRepository.PO_INITIAL, true), true, 0, null,"None") %>
						</select>
			    </div>
			    <div  class="form-group col-lg-6 col-md-6">
			        <label >Delivery Start Date</label>
			        <input name="startDate" class="datepicker form-control"  placeholder="dd/mm/yyyy"/>
			         
			    </div>
			    <div  class="form-group col-lg-6 col-md-6">
			        <label >Delivery End Date</label>
			         
			        <input name="endDate" class="datepicker form-control" placeholder="dd/mm/yyyy"/>
			    </div>
			     <div class="form-group">
			         <div  class="col-lg-3 col-md-2">
					     <input type="submit" value="Submit" class="btn btn-reve" />
					</div>
			      </div>
			     
			   
			   </form>
               </div>
	        </div>
	    </div>
	
	    <div class="panel panel-info">
			<div class="panel-heading">
			   <h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion"
						href="#last10">Last 10 Orders</a>
				</h4>
			</div>
		   <div id="last10" class="panel-collapse collapse in">
			  <div class="panel-body">
               <div class="table-responsive">
                  <table id="dataTables-example" class="table-bordered table-search table-hover table-striped">
                     <thead>
                        <tr>
                            <th>PO</th>
							<th>Client Name</th>
							<th>Company Name</th>
							<th>Email</th>						
							<th>Shipping Address</th>	
							<!-- <th>Billing Address</th> -->							
							<th>Delivery Date</th>		
							<th>Invoice Status</th>	
							<th>PO Status</th>	
                        </tr>   
                        </thead>                     
                      <tbody>
                           <%
					     String condition = " ORDER BY invcID DESC LIMIT 0,10";
					     ReturnObject ro = new ReturnObject();
					     LinkedHashMap<String, OrderListDTO> data = null;	
					     
					     ro = new OrderListService(rimsUsersDTO).getMap(null, condition ,OrderListDAO.DEFAULT_KEY_COLUMN);
					     if (ro.getIsSuccessful()) {
					    	 data = (LinkedHashMap<String, OrderListDTO>) ro.getData();
					    	 if (data != null && data.size() > 0) {	
					    		 mudRepository = MashupDataRepository.getInstance(false);
					    		 for (OrderListDTO dto : data.values()) {	
					    			 %>
							    <tr>
							        <td><%=dto.getPurchaseOrderNumber()%></td>
							        <td><%=dto.getCustomerName()%></td>
									<td><%=dto.getCompanyName()%></td>
									<td><%=dto.getCustomerEmail()%></td>
									<td><%=dto.getShippingAddress()%></td>
									<%-- <td><%=dto.getBillingAddress()%></td> --%>
									<td><%=dto.getDeliveryDate()%></td>
									<td><%=mudRepository.getLabelById(dto.getInvcInvoiceStatus()) %></td>
									<td><%=mudRepository.getLabelById(dto.getInvcPOStatus()) %></td>
							    </tr>
					    			 <%
					    		 }
					    	 }
					      }
	                     %>                           
                        </tbody>                     
                  </table>
               </div>
               
               </div>
               <div class="panel-footer">
                 <a class="btn btn-info btn-sm" href="order-list/search-order-details-order-list.html">More...</a>
               </div>
	        </div>
	    </div>
	    

	<%		  
		} 
		catch (Exception e) {
		}		
	  }
	else{
		%>
		<div class="panel panel-info">
			<div class="panel-heading">
			   <h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordion"
						href="#welcome">Dashboard</a>
				</h4>
			</div>
			<div id="welcome" class="panel-collapse collapse in">
			   <div class="panel-body">
			   <h3>No Data Found</h3>
			   </div>
			</div>
		</div>
		<%
	}
	session.removeAttribute(ApplicationConstant.ACTION_MESSAGE);
	%>
</div>
