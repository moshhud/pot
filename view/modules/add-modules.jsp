<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesDTO"%>
<%@page import="org.apache.log4j.Logger"%>
<form action="<%=ApplicationConstant.getBaseURL(session)%>modules/add-module.html"
	method="post" class="form-horizontal">
	<%@ include file="/view/includes/captcha.jsp"%>
	<%
		RimsModulesDTO dto = null;
		Object object = null;
		String selected = null;
		try {
			object = session.getAttribute(ApplicationConstant.ACTION_DATA);
			session.getAttribute(ApplicationConstant.ACTION_DATA);
			if (object != null && object instanceof RimsModulesDTO) {
				dto = (RimsModulesDTO) object;
			}
			if (dto == null) {
				dto = new RimsModulesDTO();
			}
	%>
   <div class="panel panel-info">
	    <div class="panel-body">
	       <div class="form-group">
				<label class="col-lg-2 col-md-2">Module Name <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <input type="text" name="moduleName"
						class="form-control" value="<%=dto.getModuleName()%>" required />
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			 <div class="form-group">
				<label class="col-lg-2 col-md-2">Module Description <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="moduleDescription"
						class="form-control"   required ><%=dto.getModuleDescription()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Level One <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="levelOne"
						class="form-control"   required ><%=dto.getLevelOne()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Level Two <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="levelTwo"
						class="form-control"   required ><%=dto.getLevelTwo()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Level Three <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="levelThree"
						class="form-control"   required ><%=dto.getLevelThree()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Level Four <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="levelFour"
						class="form-control"   required ><%=dto.getLevelFour()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Level Five <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="levelFive"
						class="form-control"   required ><%=dto.getLevelFive()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Level Six <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <textarea rows="5" name="levelSix"
						class="form-control"   required ><%=dto.getLevelSix()%></textarea>
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
	    </div>
	    <div class="panel-footer">
			<input type="submit" class="btn btn-warning" value="Submit" /> <span
				class="mandatory">*</span> Fields are mandatory
		</div>
	</div>	
	<%
		} catch (RuntimeException e) {
			Logger.getLogger("add-rims-modules").fatal("Exception", e);
		}
	%>
</form>