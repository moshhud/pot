<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.QueryHelper"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>

	
<script type="text/javascript">
$(document).ready(function() {	
	$('#checkAll').click(function() {		
		if ($('#checkAll').is(':checked')) {
			
			 $("input[name~=ids]").each(function() {
				$(this).prop("checked", true);	
			
			});			
			

		} else {
			
			$("input[name~=ids]").each(function() {
				$(this).prop("checked", false);
			});
		}

	});
	 $('input[name~=ids]').click(function() {		 
		if ($(this).is(':checked') == false) {
			$('#checkAll').prop('checked', false);
		}
	}); 	
	
	$('.UpdateAll').click(function(event) {
		try {
			event.preventDefault();
			var ids = '';
			var c=0;
			var type = $(this).attr('href');
			$('input[name~=ids]:checked').each(function() {ids += $(this).val()+ ',';c++;});		
			var con = getConfirmation(c,"Do you want to delete?");
			if (ids.length > 0 && con) {
				$.post(BASE_URL + 'resources/ajax-files/delete-module.jsp',
					{
					    ids : ids,
						type : type						
					},
					function(data) {
						data = $.trim(data);
						if (data == 'successful') {
							alert('Operation Successful');
							$('#data').html('');							 
							location.reload();
						} else {
							alert(data);
						}
				});
			} 
		} catch (e) {
			alert(e);
		}
	});
		
	function getConfirmation(count,msg) {
	    
	    if(count==0){
	    	alert("Please select at least one item to delete.");
	    	return false;
	    }	
	    var stat = confirm("Total Record: "+count+". "+msg);
	    if (stat==true)
	    	return true;
	    else
	    	return false;
	    	
	}
	
	});
</script>
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		LinkedHashMap<String, RimsModulesDTO> data = null;	
		RimsUsersRepository ruRepository = null;
		MashupDataRepository mudRepository = null;
		
		
		%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Search Result</h1>
		<div class="pull-right"> 
			<%
 				if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) > Permissions.LEVEL_FOUR) {
 			%>
			    <a href="new-module.html" class="add btn btn-info">Add</a> 
				<a href="delete" class="UpdateAll btn btn-danger">Delete</a>
				<!-- <span class="alert alert-danger span-legend">Blocked</span>   -->				
			<%
  								}
  							%>			 
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panle-body">
		<div class="table-responsive">
			<table id="dataTables-example" class="table-bordered table-search table-hover table-striped">
				<thead>
					<tr>
						<th><input type="checkbox" id="checkAll" name="checkAll" /></th>						
						<th>Module Name</th>
						<th>Description</th>
						<th>Level One</th>						
						<th>Level Two</th>	
						<th>Level Three</th>
						<th>Level Four</th>
						<th>Level Five</th>
						<th>Level Six</th>					
						<th>Action</th>														
					</tr>
				</thead>
				<tbody>
					<%
						if (obj != null && obj instanceof LinkedHashMap) {
									ruRepository = RimsUsersRepository.getInstance(false);
									data = (LinkedHashMap<String, RimsModulesDTO>) obj;
									if (data != null && data.size() > 0) {							
										mudRepository = MashupDataRepository.getInstance(false);
										for (RimsModulesDTO dto : data.values()) {											
					%>
					<tr >
						<td><input type="checkbox" value="<%=dto.getId()%>" name="ids" /></td>
						<td><%=dto.getModuleName()%></td>
						<td><%=dto.getModuleDescription()%></td>
						<td><%=dto.getLevelOne()%></td>
						<td><%=dto.getLevelTwo()%></td>	
						<td><%=dto.getLevelThree()%></td>
						<td><%=dto.getLevelFour()%></td>
						<td><%=dto.getLevelFive()%></td>
						<td><%=dto.getLevelSix()%></td>	
						<td>
						<%
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_FOUR) {
						%>
			                 <a href="get-module.html?id=<%=dto.getId()%>" title="Edit">Edit</a>
			    
			                <%
			                }else{
								%>
								N/A
								<% 
							}
			                %>
						</td>
						
					</tr>
				<%}%>
			<%}
			
			
		}%>
	</tbody>
</table>
</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-vbregistration").fatal("RuntimeException", e);
	}
%>
