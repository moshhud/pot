<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<td>Department</td>
<td><select name="dept" class="form-control">
		<%=new UIHelper().getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(MashupDataRepository.getInstance(false).DEPARTMENT, false), true, NumericHelper.parseInt(session.getAttribute("dept") + ""), null) %>
		<% session.removeAttribute("dept"); %>
</select></td>