<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<td>Active Status</td>
<td>
	<select name="plCurrentStatus" class="form-control">
		<%= new UIHelper().getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(MashupDataRepository.getInstance(false).CURRENT_STATUS, true), false, 0, session.getAttribute("plCurrentStatus") + "")%>
	</select>
	<% session.removeAttribute("plCurrentStatus"); %>
</td>