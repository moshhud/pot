<td>End Date</td>
<td>
	<%
		String endDate = (String) session.getAttribute("endDate");
		session.removeAttribute("endDate");
		if(endDate == null || endDate.length() == 0) {
			endDate = "";
		}
	%>
	<input autocomplete="off" class="datepicker form-control" name="endDate" value="<%=endDate %>" placeholder="dd/mm/yyyy"/>
</td>