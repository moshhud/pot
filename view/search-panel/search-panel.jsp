<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="java.security.Permission"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="dev.mashfiq.util.RecordNavigator"%>
<script type="text/javascript">
	$(document).ready(function() {
		$('table#navTable td a').click(function(event) {
			event.preventDefault();
			id = this.id;
			form = document.forms[0];
			currentPage = form['pageno'].value;
			if (id == 'first') {
				form['pageno'].value = 1;
			} else if (id == 'previous') {
				if (currentPage * 1 > 1) {
					form['pageno'].value = currentPage * 1 - 1;
				} else {
					form['pageno'].value = 1;
				}
			} else if (id == 'next') {
				if (currentPage < form['totalPages'].value * 1) {
					form['pageno'].value = currentPage * 1 + 1;
				} else {
					form['pageno'].value = form['totalPages'].value
				}
			} else if (id == 'last') {
				form['pageno'].value = form['totalPages'].value;
			}
			form.submit();
		});
		$('#searchBtn').click(function() {
			form = document.forms[0];
			form['pageno'].value = 1;
			this.submit();
		});
		$('#goBtn').click(function() {
			form = document.forms[0];
			form['pageno'].value = form['temp'].value;
			this.submit();
		});
	});
</script>
<%
	try {
		RecordNavigator rn = (RecordNavigator) session.getAttribute(ApplicationConstant.RECORD_NAVIGATOR);
		session.removeAttribute(ApplicationConstant.RECORD_NAVIGATOR);
		String searchPanel[][] = rn.getSearchPanel();
		String value = "";
		int noOfColmns = 4;
		if (searchPanel != null 
				// to allow notifications
				//&& searchPanel.length > 0
				) {
%>
<form action="<%=rn.getActionName()%>" method="post" autocomplete="on">
	<div id="accordion" class="panel-group">
		<div class="panel panel-default">
			<div class="panel-heading">
				<a data-parent="#accordion" data-toggle="collapse" href="#searchPanel">Search Panel</a>
			</div>
			<div id="searchPanel" class="panel-collapse collapse in">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-borderless table-condensed table-hover" id="table-search-panel">
						<tr>
							<%int i = 0;
							String pName = null;
							for (i = 0; i < searchPanel.length; i++) {
								if (searchPanel[i][0].endsWith(".jsp")) {
								pName = "/view/search-panel/" + searchPanel[i][0];
							%>
								<jsp:include
								page="<%=pName%>"
								flush="true"></jsp:include>
							<%
								} 
								else {
									value = (String) session.getAttribute(searchPanel[i][1]);
									session.removeAttribute(searchPanel[i][1]);
									if (value == null) {
										value = "";
									}
									%>
									<td><%=searchPanel[i][0]%></td>
									<td><input class="form-control" name="<%=searchPanel[i][1]%>" value="<%=value%>" /></td>
									<%
								}
								if (i > 0 && (i + 1) % noOfColmns == 0) { %>
									</tr>
									<tr>
								<%}
							}%>
							
									<td>Records Per Page</td>
									<td><select name="<%=ApplicationConstant.RECORD_PER_PAGE%>" class="form-control">
										<option value="30">30</option>
										<%try {
											String selected = null;
											String rpp = session.getAttribute(ApplicationConstant.RECORD_PER_PAGE)+ "";
											session.removeAttribute(ApplicationConstant.RECORD_PER_PAGE);
											if (rpp == null || rpp.length() == 0) {
												rpp = "30";
											}
											if (rpp != null && rpp.equals("40")) {
												selected = "selected=\"selected\"";
											} else {
												selected = "";
											}%>
											
											<option <%=selected%> value="40">40</option>
											<%if (rpp != null && rpp.equals("50")) {
												selected = "selected=\"selected\"";
												} 
											else {
												selected = "";
											}%>
											<option <%=selected%> value="50">50</option>
										
										<%
											RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
											int multiplier = 1;
											
											if((rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RECORD_PER_PAGE) > Permissions.LEVEL_ONE) &&  (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RECORD_PER_PAGE) < Permissions.LEVEL_FIVE)) { 
												multiplier = 75;
											}
											else
												multiplier = 450;
											
											int val = 0;
											Validations v = new Validations();
											int recordPerPage = NumericHelper.parseInt(rpp);
											
											for(int x=1; x <= rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RECORD_PER_PAGE); x++) {
												val = (x) * multiplier;
											%>
											<option <%=v.checkSelected(recordPerPage, val)%> value="<%=val %>"><%=val %></option>
											<%			
											}
										
										} catch (Exception e) {}
									%>
							</select></td>
						</tr>
					</table>
				</div>
			</div>
			<div class="panel-footer table-responsive">
			  
			  <div class="col-lg-5">			
				 <input class="left search" type="image"
								src="<%=ApplicationConstant.getBaseURL(session)%>/resources/images/search-btn.jpg"
								value="Search" />
			   </div>				
				<div class="col-lg-7">
				      <%@ include file="nav-panel.jsp"%>
				</div>
				
			</div>
			</div>
		</div>
	</div>
</form>
<%
	}
	} catch (RuntimeException e) {
		Logger.getLogger("search-panel").fatal("RuntimeException", e);
	}
%>