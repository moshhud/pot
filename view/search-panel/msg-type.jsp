<td>Message Type</td>
<td>
<select name="msgType" class="form-control">

	<% 
		String msgType = session.getAttribute("msgType") + "";
		session.removeAttribute("msgType");
		
	%>
	<option value="">Select</option>
	<option value="incoming" <%= "incoming".equalsIgnoreCase(msgType) ? "selected=\"selected\"": "" %>>Incoming</option>
	<option value="outgoing" <%= "outgoing".equalsIgnoreCase(msgType) ? "selected=\"selected\"": "" %>>Outgoing</option>
</select>
</td>