<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<script type="text/javascript">
            $(document).ready(function() {
                SearchText();
            });
            function SearchText() {
            	<%
            		String productID =(String) session
    						.getAttribute("name");
            		session.removeAttribute("name");
            		if(productID == null) {
            			productID = "";
            		}
            		String values = "";
            		for(RimsUsersDTO dto: RimsUsersRepository.getInstance(false).getAll("active")) {
            			
            			values += "{ label: '" + dto.getUsrName() + "', value: '" + dto.getUsrName() + "' },";
            		}
            		values = values.substring(0, values.length()-1);
            	%>
                $(".autosuggest").autocomplete({
                    source: [<%=values %>]
                });
            }
        </script>
<td>Name</td>
<td>
<input type="text" name="name" id="txtSearch" class="autosuggest form-control" placeholder="Type Name" value="<%=productID %>"/>
</td>