<%@page import="com.revesoft.rims.dialerRegistration.mdProductTypes.MdProductTypesDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductTypes.MdProductTypesRepository"%>
<td>Product Type</td>
<td>
	<select name="productType" class="form-control">
		<option value="">All</option>
		<%
			String selectedType = (String) session.getAttribute("productType");
			session.removeAttribute("productType");
			String selected = null;
			ArrayList<MdProductTypesDTO> productTypes = MdProductTypesRepository.getInstance(false).getProductTypes();
			for(MdProductTypesDTO dto: productTypes){
				if(selectedType != null && selectedType.length() > 0 && selectedType.equals(dto.getId() + "")){
					selected = "selected=\"selected\"";
				} else {
					selected = "";
				}
		%>
		<option <%=selected %> value="<%=dto.getId() %>"><%=dto.getName() %></option>
		<% } %>
	</select>
</td>