<td>Start Date</td>
<td>
	<%
		String startDate = (String) session.getAttribute("startDate");
		session.removeAttribute("startDate");
		if(startDate == null || startDate.length() == 0) {
			startDate = "";
		}
	%>
	<input autocomplete="off" class="datepicker form-control" name="startDate" value="<%=startDate %>" placeholder="dd/mm/yyyy"/>
</td>