<%@page import="com.revesoft.rims.dialerRegistration.country.CountryDTO"%>
<%@page import="com.revesoft.rims.dialerRegistration.country.CountryRepository"%>
<%@page import="java.util.ArrayList"%>
<td>Country</td>
<td>
	<select name="country" class="form-control">
		<option value="">Select</option>
		<%
			ArrayList<CountryDTO> countries = CountryRepository.getInstance(false).getAll();
			String selected = null;
			String fmCountry = null;
			if(countries != null && countries.size() > 0) {
				fmCountry = session.getAttribute("country") + "";
				session.removeAttribute("country");
				for(CountryDTO country: countries) {
					if(country != null && fmCountry != null && fmCountry.length() > 0 && fmCountry.equals(country.getName())) {
						selected = "selected=\"selected\""; 
					} else {
						selected = "";
					}
		%>
		<option <%=selected %> value="<%=country.getName() %>"><%=country.getName()%></option>
		<%
				}
			}
		%>
	</select>
</td>