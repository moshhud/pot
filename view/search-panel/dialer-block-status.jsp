<%@page import="dev.mashfiq.util.Validations"%>

<td>Block Status</td>
<td>
	<select name="isBlocked" class="form-control">
	    <%
			String blockType = session.getAttribute("isBlocked") + "";
			session.removeAttribute("isBlocked");
			Validations v = new Validations();
		%>
		
		<option value="">All</option>
		<option <%=v.checkSelected(blockType, "0") %> value="0">Unblocked</option>
		<option <%=v.checkSelected(blockType, "1") %> value="1">blocked</option>
	</select>
</td>