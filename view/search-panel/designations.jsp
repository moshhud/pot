<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<td>Designation</td>
<td><select name="desg" class="form-control">
		<%=new UIHelper().getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(MashupDataRepository.getInstance(false).DESIGNATION, false), true, NumericHelper.parseInt(session.getAttribute("desg") + ""), null) %>
		<% session.removeAttribute("desg"); %>
</select></td>