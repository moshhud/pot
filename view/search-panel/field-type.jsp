<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<td>Field Type</td>
<td>
	<select name="fieldType" class="form-control">
	<%=new UIHelper().getSelectFromMashup(MashupDataRepository
					.getInstance(false).getByFieldType(1, true), true,
					NumericHelper.parseInt(session.getAttribute("fieldType")
							+ ""), null, "Field Type")%>
</select>
</td>
<%
	session.removeAttribute("fieldType");
%>