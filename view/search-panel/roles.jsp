<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<td>Role</td>
<td>
<select name="role" class="form-control">
	<%=new UIHelper().getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(MashupDataRepository.getInstance(false).ROLE, false), true, NumericHelper.parseInt(session.getAttribute("role") + ""), null) %>
	<% session.removeAttribute("role"); %>
</select>
</td>