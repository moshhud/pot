<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<td>Product Type</td>
<td>
<select name="pType" class="form-control">
	<%=new UIHelper().getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(MashupDataRepository.getInstance(false).OSC_PRODUCT_TYPE, true), false, 0, session.getAttribute("pType") + "") %>
	<% session.removeAttribute("pType"); %>
</select>
</td>