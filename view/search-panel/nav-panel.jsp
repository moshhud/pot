<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="dev.mashfiq.util.RecordNavigation"%>
<table style="width: 450px;" id="navTable" class=" table table-borderless table-hover table-condensed table-sm">
	<tr>
		<td style="width: 220px;"><a href="?<%=RecordNavigation.ID%>=<%=RecordNavigation.FIRST_PAGE%>&<%=RecordNavigation.CURRENT_PAGE_NO%>=<%=rn.getCurrentPageNo()%>" id="first">
		<img border="0" src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/first.png"/></a>
		<a href="?<%=RecordNavigation.ID%>=<%=RecordNavigation.PREVIOUS_PAGE%>&<%=RecordNavigation.CURRENT_PAGE_NO%>=<%=rn.getCurrentPageNo()%>" id="previous"> <img border="0"
			src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/previous.png"/></a>
		<a href="?<%=RecordNavigation.ID%>=<%=RecordNavigation.NEXT_PAGE%>&<%=RecordNavigation.CURRENT_PAGE_NO%>=<%=rn.getCurrentPageNo()%>" id="next"> <img border="0"
			src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/next.png"/></a>
		<a href="?<%=RecordNavigation.ID%>=<%=RecordNavigation.LAST_PAGE%>&<%=RecordNavigation.CURRENT_PAGE_NO%>=<%=rn.getCurrentPageNo()%>" id="last"> <img border="0"
			src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/last.png"/></a>
		</td>
		<td id="pagesLbl">&nbsp;Page&nbsp;</td>
		<td>
			<input class="numeric" type="text" name="<%=RecordNavigation.CURRENT_PAGE_NO %>" value="<%= rn.getCurrentPageNo() %>" size="3" style="width: 40px;" />&nbsp;of
		</td>
		<td><input type="text" name="totalPages" readonly="readonly" value="<%= rn.getTotalPages()%>" style="width: 30px; border: none;"/>&nbsp;</td>
		<td>
			<input type="hidden" value="yes" name="go">
			<input id="goBtn" type="image" value="Go" src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/go.png">
		</td>
	</tr>
</table>