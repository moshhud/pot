<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page
	import="com.revesoft.rims.revesoft.rimsActionTakenOptions.RimsActionTakenOptionsRepository"%>
<%@page
	import="com.revesoft.rims.revesoft.rimsActionTakenOptions.RimsActionTakenOptionsDTO"%>
<%@page import="java.util.ArrayList"%>
<td>Last Action Taken</td>
<td><select name="actionTaken" class="form-control">
		<option value="">Select</option>
		<%
			int actionTaken = NumericHelper.parseInt(session
					.getAttribute("actionTaken") +"");
			Validations v = null;
			ArrayList<RimsActionTakenOptionsDTO> ratoDTOList = RimsActionTakenOptionsRepository
					.getInstance(false).getListByType(
							RimsActionTakenOptionsRepository.TYPE_ACTION_TAKEN,
							"active");
			if (ratoDTOList != null && ratoDTOList.size() > 0) {
				v = new Validations();
				for (RimsActionTakenOptionsDTO val : ratoDTOList) {
		%>
		<option <%=v.checkSelected(val.getId(), actionTaken)%>
			value="<%=val.getId()%>"><%=val.getLabel()%></option>
		<%
			}
			}
		%>
</select></td>