<%@page import="dev.mashfiq.util.DateAndTimeHelper"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>

<td>Select Month</td>
<%
	int mon = 0;
	String selectedMon = ""+session.getAttribute("birthmonth");
	session.removeAttribute("birthmonth");
	if(selectedMon != null && selectedMon.length() > 0 && selectedMon.equalsIgnoreCase("null") == false){
		mon = NumericHelper.parseInt(selectedMon);
	}
%>
<td>
<select class="form-control" name="birthmonth">
<option value="">Select Month</option>
	<%
	DateAndTimeHelper dth = new DateAndTimeHelper();
	for(int i = 1;i<13;i++){
	%>
	<option <%if(mon == i) out.println("selected = selected"); %> value="<%=i %>"><%=dth.getMonthNameFromIndex(i) %></option>
	<%
	}
	%>
</select>
</td>