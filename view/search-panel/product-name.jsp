        <%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDTO"%>


<script type="text/javascript">
            $(document).ready(function() {
                SearchText();
            });
            function SearchText() {
            	<%
            		String productID =(String) session
    						.getAttribute("plProductName");
            		session.removeAttribute("plProductName");
            		if(productID == null) {
            			productID = "";
            		}
            		String values = "";
            		for(MdProductListDTO pd: MdProductListRepository.getInstance(false).getAllData()) {
            			
            			values += "{ label: '" + pd.getPlProductName() + "', value: '" + pd.getPlProductName() + "' },";
            		}
            		values = values.substring(0, values.length()-1);
            	%>
                $(".autosuggest").autocomplete({
                    source: [<%=values %>]
                });
            }
        </script>
<td>Product Name</td>
<td>
<input type="text" name="plProductName" id="txtSearch" class="autosuggest form-control" placeholder="Type Product Name" value="<%=productID %>"/>
</td>