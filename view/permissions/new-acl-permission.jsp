<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page
	import="com.revesoft.rims.revesoft.rimsModules.RimsModulesRepository"%>
<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page
	import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="java.util.ArrayList"%>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						$('[name~=moduleId]')
								.change(
										function() {
											var usrID = $('[name~=tableId]')
													.val();
											var moduleId = $(this).val();
											$
													.ajax({
														url : BASE_URL
																+ 'resources/ajax-files/get-current-permission-level.jsp',
														type : 'POST',
														data : ({
															usrID : usrID,
															moduleId : moduleId
														}),
														dataType : 'JSON',
														success : function(data) {
															if (data.status == 'success') {
																$('#cpl')
																		.html(
																				data.msg);
															} else {
																$('#cpl').html(
																		'');
																alert(data.msg);
															}
														},
														error : function(x, y,
																z) {
															alert(z)
														}
													});
										});
					});
</script>
<%
	try {
		ArrayList<RimsUsersDTO> list = RimsUsersRepository.getInstance(
				false).getAll(ApplicationConstant.ACTIVE);
		ArrayList<RimsModulesDTO> modulesList = RimsModulesRepository
				.getInstance(false).getAll(true);
%>
<form action="add-acl-permission.html" method="post"
	class="form-horizontal">
	<div class="panel panel-info">
		<div class="panel-heading">
			<h1 class="panel-title">Update User Permission</h1>
		</div>
		<div class="panel-body">
			<%@ include file="/view/includes/captcha.jsp"%>
			<div class="form-group">
				<label class="col-lg-2">User</label>
				<div class="col-lg-2">
					<select class="form-control" name="tableId" required>
						<option value="">Select</option>
						<%
							if (list != null && list.size() > 0) {
									for (RimsUsersDTO dto : list) {
										if (dto != null) {
						%>
						<option value="<%=dto.getUsrID()%>"><%=dto.getUsrEmail()%></option>
						<%
							}
									}
								}
						%>
					</select>
				</div>
				<label class="col-lg-2">Module Name</label>
				<div class="col-lg-2">
					<select name="moduleId" required class="form-control">
						<option value="">Select</option>
						<%
							if (modulesList != null && modulesList.size() > 0) {
									for (RimsModulesDTO dto : modulesList) {
										if (dto != null) {
						%>
						<option value="<%=dto.getId()%>"><%=dto.getModuleName()%></option>
						<%
							}
									}
								}
						%>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-2">Current Permission Level</label>
				<div class="col-lg-2">
					<span id="cpl"></span>
				</div>
				<label class="col-lg-2">Permission Level</label>
				<div class="col-lg-2">
					<select name="permissionLevel" required class="form-control">
						<%=new UIHelper().getSelectFromMashup(
						MashupDataRepository
								.getInstance(false)
								.getByFieldType(
										MashupDataRepository.getInstance(false).PERMISSION_LEVEL,
										true), false, 0, null)%>
					</select>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<input type="submit" class="btn btn-primary" value="Update">
		</div>
	</div>
</form>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("new-acl-permission").fatal(
				"RuntimeException", e);
	}
%>