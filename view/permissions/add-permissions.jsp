<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesRepository"%>
<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesDTO"%>
<%@page import="com.revesoft.rims.revesoft.rimsPermissions.RimsPermissionsDAO"%>
<%@page import="com.revesoft.rims.revesoft.rimsPermissions.RimsPermissionsDTO"%>
<%@page import="dev.mashfiq.util.CollectionHelper"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>
<form action="add-permissions.html" method="post">
	<%@ include file="/view/includes/captcha.jsp"%>
	<%
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
	%>
	<input type="hidden" name="createdBy" value="<%=rimsUsersDTO.getUsrID() %>" />
	<table class="table table-bordered table-striped">
		<tr>
			<th>Role</th>
			<th colspan="7">
				<%
					int roleId = NumericHelper.parseInt(request
															.getParameter(ApplicationConstant.ID));
				%><input type="hidden" name="roleId" value="<%=roleId%>" />
				<input type="text" readonly="readonly" class="input"
				value="<%=MashupDataRepository.getInstance(false).getLabelById(
				roleId)%>" />
			</th>
		</tr>
		<%
			LinkedHashMap<String, RimsPermissionsDTO> permissionsDTOMap = null;
			RimsPermissionsDTO permissionDTO = null;
			Validations v = null;
			ReturnObject ro = new RimsPermissionsDAO().getMap(null,
					" AND permission_type='role' AND table_id=" + roleId,
					"module_id");
			if (ro != null && ro.getIsSuccessful()) {
				permissionsDTOMap = (LinkedHashMap<String, RimsPermissionsDTO>) ro
						.getData();
			}
			ArrayList<RimsModulesDTO> modulesList = RimsModulesRepository.getInstance(
					false).getAll(true);
			if (new CollectionHelper().checkCollection(modulesList)) {
				v = new Validations();
				for (RimsModulesDTO dto : modulesList) {
					if (permissionsDTOMap != null
							&& permissionsDTOMap.containsKey(dto.getId() + "")) {
						permissionDTO = permissionsDTOMap.get(dto.getId() + "");
					} else {
						permissionDTO = new RimsPermissionsDTO();
					}
		%>
		<tr>
			<td rowspan="2"><%=dto.getModuleName()%></td>
			<td rowspan="2"><%=dto.getModuleDescription()%></td>
			<td align="center"><input type="radio"
				name="<%=ApplicationConstant.ID + dto.getId()%>" value="1"
				checked="checked" /></td>
			<td align="center"><input type="radio"
				name="<%=ApplicationConstant.ID + dto.getId()%>" value="2"
				<%=v.checkChecked(
						permissionDTO.getPermissionLevel(), 2)%> /></td>
			<td align="center"><input type="radio"
				name="<%=ApplicationConstant.ID + dto.getId()%>" value="3"
				<%=v.checkChecked(
						permissionDTO.getPermissionLevel(), 3)%> /></td>
			<td align="center"><input type="radio"
				name="<%=ApplicationConstant.ID + dto.getId()%>" value="4"
				<%=v.checkChecked(
						permissionDTO.getPermissionLevel(), 4)%> /></td>
			<td align="center"><input type="radio"
				name="<%=ApplicationConstant.ID + dto.getId()%>" value="5"
				<%=v.checkChecked(
						permissionDTO.getPermissionLevel(), 5)%> /></td>
			<td align="center"><input type="radio"
				name="<%=ApplicationConstant.ID + dto.getId()%>" value="6"
				<%=v.checkChecked(
						permissionDTO.getPermissionLevel(), 6)%> /></td>
		</tr>
		<tr>
			<td align="center"><%=dto.getLevelOne()%></td>
			<td align="center"><%=dto.getLevelTwo()%></td>
			<td align="center"><%=dto.getLevelThree()%></td>
			<td align="center"><%=dto.getLevelFour()%></td>
			<td align="center"><%=dto.getLevelFive()%></td>
			<td align="center"><%=dto.getLevelSix()%></td>
		</tr>
		<%}} %>
	</table>
	<% if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_THREE) { %>
		<input type="submit" value="Submit" class="btn btn-info"/>
	<% } %>
	
</form>