<%@page import="com.revesoft.rims.revesoft.rimsNotificationRecipients.RimsNotificationRecipientsRepository"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="com.revesoft.rims.revesoft.rimsNotificationRecipients.RimsNotificationRecipientsDAO"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.revesoft.rims.revesoft.rimsNotificationRecipients.RimsNotificationRecipientsDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%
	try {
%>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">Search Result</h1>
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-bordered table-hover">
				<thead>
					<tr>
						<th>SN</th>
						<th>Type</th>
						<th>Message</th>
					</tr>
				</thead>
				<tbody>
					<%
						RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
								.getAttribute(ApplicationConstant.LOGIN_INFO);
						Object obj = session
									.getAttribute(ApplicationConstant.ACTION_DATA);
						session.removeAttribute(ApplicationConstant.ACTION_DATA);
						LinkedHashMap<String, RimsNotificationRecipientsDTO> data = null;
						RimsNotificationRecipientsRepository repository;
						int i = 1;
						String ids = "0";
						String condition;
						if (rimsUsersDTO != null && obj != null && obj instanceof LinkedHashMap) {
							data = (LinkedHashMap<String, RimsNotificationRecipientsDTO>) obj;
							repository = RimsNotificationRecipientsRepository.getInstance(false);
							for (RimsNotificationRecipientsDTO value : data.values()) {
								if("unseen".equalsIgnoreCase(value.getCurrentStatus())) {
									ids += "," + value.getId();
									repository.removeFromData(value);
								}
					%>
					<tr>
						<td><%=i++%></td>
						<td><%=value.getModule()%></td>
						<td><%=value.getMessage()%></td>
					</tr>
					<%
							}
							condition = " AND id IN (" + ids + ") AND user_id=" + rimsUsersDTO.getUsrID();
							new CommonDAO().update(RimsNotificationRecipientsDAO.TABLE_NAME, null, "current_status='seen',seen_time=" + System.currentTimeMillis(), "1", "1", "=", condition, RimsNotificationRecipientsDAO.IS_DIALERREGISTRATION);
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-rims-rims-notification-recipients").fatal(
				"RuntimeException", e);
	}
%>