<div class="panel panel-info">
	<form action="add-notifications.html" method="post">
		<div class="panel-body">
			<div class="form-group col-lg-4">
				<label>Table Name</label>
				<div>
					<input  class="form-control" name="tableName"/>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label>Table Id</label>
				<div>
					<input class="form-control" name="tableId"/>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label>Module</label>
				<div>
					<input class="form-control" name="module"/>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label>Type</label>
				<div>
					<textarea name="messageType" class="form-control" placeholder="{json: format}"></textarea>
				</div>
			</div>
			<div class="form-group col-lg-4">
				<label>Message</label>
				<div>
					<textarea class="form-control" name="message" ></textarea>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<input type="submit" value="Submit" class="btn btn-info" />
		</div>
	</form>
</div>