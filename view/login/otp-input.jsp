<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="org.apache.log4j.Logger"%>

   <script type="text/javascript">
			window.onload = function() {
				var frame = document.getElementById('r2fa_iframe');
				var receiver = frame.contentWindow;
				var apiKey = frame.getAttribute('data-apiKey');
				var username = frame.getAttribute('data-username');
				var apiHmac = frame.getAttribute('data-hmacSign');
				var uniqueId = frame.getAttribute('data-uniqueId');
				var target = frame.getAttribute('data-submitUrl');
				var loginUrl = frame.getAttribute('data-loginUrl');
				var msg = {};
				msg.apiKey = apiKey;
				msg.username = username;
				msg.apiHmac = apiHmac;
				msg.uniqueId = uniqueId;
				try {
					receiver.postMessage(msg, 'https://dashboard.revesecure.com/iframe/');
				} catch (err) {
					console.log(err);
				}
				function receiveMessage(e) {
					if (e.data.post !== undefined) {
						var input = document.createElement('input');
						input.type = 'hidden';
						input.name = "auth";
						input.value = e.data.post;
						
						var form = document.createElement('form');
						form.method = 'POST';
						form.action = target;
						form.appendChild(input);						

						frame.parentElement.insertBefore(form, frame.nextSibling);

						form.submit();
					} else if (e.data.redirect !== undefined) {
						window.location.href = loginUrl;
					}
				}

				window.addEventListener('message', receiveMessage);
			}

	</script>

<%
long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
String otpTokenUniqueID= null;
session.setAttribute("otpTokenUniqueID", number+ "");
%>

<div class="container">
	<div class="col-lg-12 loginFormContainer">
		<div class="paddingDiv"></div>
		<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 loginForm">
			<%
				try {
					String username;
					RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
					System.out.println(rimsUsersDTO.getUsrEmail());
					username= rimsUsersDTO.getUsrEmail();
			%>
			
				<%@ include file="/view/includes/captcha.jsp"%>
				<div class="panel panel-default" style="margin-top: 60px;">
					<div class="panel-heading"><h1 class="panel-title">Login Security Token</h1></div>
					<div class="panel-body">
					 
					 <iframe id="r2fa_iframe" src="https://dashboard.revesecure.com/iframe/"	width="100%" height="340px" 
						data-uniqueId="<%=number %>"							
						data-apiKey=<%=ApplicationConstant.apiKey %>
						data-hmacSign=<%=ApplicationConstant.hmacSign %>
						data-username="<%=username %>"
						data-submitUrl="otp-validation.html"
						data-loginUrl="login.html"							
					  >
			        </iframe>
					
				  </div>
				</div>
			
			<%
				} catch (RuntimeException e) {
					Logger.getLogger("otp-input").fatal("RuntimeException", e);
				}
			%>
		</div>
	</div>
</div>
<script type="text/javascript">
document.getElementById("usrPassword").focus();
</script>
