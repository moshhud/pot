<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="org.apache.log4j.Logger"%>

<div class="container">
	<div class="col-lg-12 loginFormContainer">
		<div class="paddingDiv"></div>
		<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 loginForm">
			<%
				try {
					RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.TEMPORARY_LOGIN_INFO);
			%>
			<form id="form" action="otp-validation.html" method="post" class="col-lg-12">
				<%@ include file="/view/includes/captcha.jsp"%>
				<div class="panel panel-default" style="margin-top: 200px;">
					<div class="panel-heading"><h1 class="panel-title">Login Security Token</h1></div>
					<div class="panel-body">
						<%@ include
								file="/view/includes/action-message.jsp"%>
						<div class="alert alert-primary hidden">
							<center><a class="btn btn-success" href="<%=ApplicationConstant.getBaseURL(session) %>resources/common/rims-apk.zip">Download RIMS Android APP (Beta Version)</a></center>
						</div>
						<div class="form-group">
							<input type="text" value="" required="required" pattern="\d{6,}" title="6 digit number" id="usrPassword" name="usrPassword" autocomplete="off" class="form-control" placeholder="Security Token" />
						</div>
					</div>
					<div class="panel-footer">
						<input type="submit" class="btn btn-warning" value="Submit"/>
						
					</div>
				</div>
			</form>
			<%
				} catch (RuntimeException e) {
					Logger.getLogger("otp-input").fatal("RuntimeException", e);
				}
			%>
		</div>
	</div>
</div>
<script type="text/javascript">
document.getElementById("usrPassword").focus();
</script>
