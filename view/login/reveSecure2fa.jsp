<%@ page language="java" %>
<%@ taglib uri="../WEB-INF/struts-bean.tld" prefix="bean" %><%@ taglib uri="../WEB-INF/struts-html.tld" prefix="html" %><%@ taglib uri="../WEB-INF/struts-logic.tld" prefix="logic" %>
<%@page import="org.apache.log4j.Logger"%>
<%
Logger logger = Logger.getLogger("reveSecure 2FA jsp");

String otpTokenUniqueID = (String)session.getAttribute("otpTokenUniqueID");
String otpAuth = request.getParameter("auth");
String otpToken = "-1"; //just need to bypass previous code flow
System.out.println("--OTP auth: " + request.getParameter("auth"));
logger.debug("--OTP auth: " + request.getParameter("auth"));
logger.debug("--otpTokenUniqueID: " + otpTokenUniqueID);

%>
<html>

	<head>
		<html:base/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Billing System::OTP Token</title>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/stylesheets/styles.css"/>
		<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/stylesheets/login.css"/>
		
		<script  src="<%=request.getContextPath().toString()%>/scripts/util.js"></script>
		<script type="text/javascript">
			window.onload = function(){
				  document.forms[0].submit();
				}
		</script>
		
	</head>
	
	<body class="body_center_align style2BG" >
	
		<div id="style2Wrapper">
		
			<div id="style2Header"> </div>
			<div id = "style2Logo">
				<img alt="Logo" align="center" src="<%= request.getContextPath() %>/images/common/login_logo.png"/>
			</div>
		
		
			<div id = "mainContent">
				<img id="secDeviceImg"  align="center" src="<%= request.getContextPath() %>/images/sedvce_txt.jpg" alt="Security Device" />
				<form id="form" action="otp-validation.html" method="post" class="col-lg-12">
				
					<div id= "otpDiv" >					
						<div style="color: green;"> OTP token verification is in progress. Please wait... </div>
						<input type="hidden" name="otpTokenUniqueID" value="<%=otpTokenUniqueID%>"/> 
						<input type="hidden" name="otpAuth" value="<%=otpAuth%>"/>
						<input type="hidden" name="otpToken" value="<%=otpToken%>"/>
						<ul>
							
						</ul>
						
						
					</div>
				
				</form>
				<img id="secDeviceBannerImg" src="<%= request.getContextPath() %>/images/otp_baner.jpg" alt="Security Device Banner" />
			</div>
			
		</div>
		<div id="style2Footer" style="" >
			<%@ include file="loginfooter.jsp"%>
		</div>
	
		</body>
		
	
</html>