<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<script type="text/javascript">
$(document).ready(function() {
	$('form').submit(function(e){
		var passWord = $('#passWord').val();
		if(passWord.length < 6) {
			alert('Please enter valid password');
			e.preventDefault();
		}
	});
});
</script>
<div id="login-box" class="container">
	
	<div class="col-lg-12">
		<div class="col-lg-offset-3 col-lg-6 col-lg-offset-3 loginForm">
			<form action="login.html" method="post" class="col-lg-12">
				<%@ include file="/view/includes/captcha.jsp"%>
				<%
					Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
					session.removeAttribute(ApplicationConstant.ACTION_DATA);
					RimsUsersDTO dto = null;
					if (obj != null && obj instanceof RimsUsersDTO) {
						dto = (RimsUsersDTO) obj;
					} else {
						dto = new RimsUsersDTO();
					}
				%>
				<div class="panel panel-default">
					<div class="panel-heading"><h1 class="panel-title">Login to PO Tracker</h1></div>
					<div class="panel-body">
					<%@ include file="/view/includes/action-message.jsp"%>
				<div class="form-group">
					<input type="email" title="Email"
							name="usrEmail" class="form-control" placeholder="Email"
							value="<%=dto.getUsrEmail()%>" required />
				</div>
				<div class="form-group">
					<input id="passWord" type="password" pattern=".{6,}" title="Password must be at least 6 characters long" placeholder="Password"
							name="usrPassword" class="form-control" required />
				</div>
					</div>
					<div class="panel-footer">
						<input type="submit" class="btn btn-warning" value="Login"/>
						
					</div>
				</div>
			</form>
			

		</div>
	</div>
</div>