<%--

<%@page import="com.revesoft.rims.revesoft.pr.PrDTO"%>
<%@page import="com.revesoft.rims.revesoft.pr.PrDAO"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="com.revesoft.rims.revesoft.rimsUploadedFiles.RimsUploadedFilesService"%>
<%@page import="com.revesoft.rims.revesoft.rimsUploadedFiles.RimsUploadedFilesDAO"%>
<%@page import="dev.mashfiq.util.StringHelper"%>
<%@page import="com.revesoft.rims.revesoft.rimsUploadedFiles.RimsUploadedFilesDTO"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="com.revesoft.rims.revesoft.rimsPost.RimsPostService"%>
<%@page import="com.revesoft.rims.revesoft.rimsPost.RimsPostDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="org.apache.log4j.Logger"%>
<%
	try {
%>
<link rel='stylesheet' id='camera-css'  href='css/camera.css' type='text/css' media='all'> 
<script type='text/javascript' src='<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/jquery.mobile.customized.min.js'></script>
<script type='text/javascript' src='<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/jquery.easing.1.3.js'></script> 
<script type='text/javascript' src='<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/camera.js'></script> 
<style>
.title {
	font-size: 14px;
}
.yellow {
	color: yellow;
}
.pr-title {color: #e05fef;}
.pr-date{ font-size: 11px;}
.purple-bd {background: #e05fef; color: white; padding: 2px 5px; font-weight: bold;}
</style>
<script>
jQuery(function(){
	jQuery('#camera_wrap_1').camera({
		pagination: false,
		time : 7000,
		hover: true
	});
	jQuery('#success-story').camera({
		pagination: false,
		time : 3000,
		hover: true,
		height: '310px',
		autoAdvance: false
	});
	jQuery('#wow-factor').camera({
		pagination: false,
		time : 3000,
		hover: true,
		height: '310px',
		autoAdvance: false
	});
});
</script>
    <%
    	ReturnObject ro = new RimsPostService().getMap(null, " AND current_status='published' ORDER BY id DESC LIMIT 5", null, true);
    	LinkedHashMap<String, RimsPostDTO> rimsPostData = null;
    	StringHelper sh = null;
    	LinkedHashMap<String, RimsUploadedFilesDTO> rufData = null;
    	LinkedHashMap<String, PrDTO> prData = null;
    	RimsUploadedFilesDAO rufDAO = null;
    	if (ro != null && ro.getIsSuccessful()) {
    		rimsPostData = (LinkedHashMap<String, RimsPostDTO>) ro.getData();
    		if (rimsPostData != null && rimsPostData.size() > 0) {
    			sh = new StringHelper();
    %>
    <div class="fluid_container camera_wrap camera_azure_skin" id="camera_wrap_1">
    <%
    			for(RimsPostDTO dto: rimsPostData.values()) {
    				if(dto != null && dto.getRimsUploadedFilesDTOList() != null && dto.getRimsUploadedFilesDTOList().size() > 0) {
    					for(RimsUploadedFilesDTO rufDTO: dto.getRimsUploadedFilesDTOList()) {
    						if(rufDTO != null && rufDTO.getFileFileName() != null && rufDTO.getFileFileName().length() > 0) {
    							dto.setFileFileName(rufDTO.getFileFileName());
    							break;
    						}
    					}
    %>
		<div data-src="<%=ApplicationConstant.getBaseURL(session)%>resources/upload-folder/uploaded-files/<%=dto.getFileFileName()%>">
			<div class="camera_caption fadeFromBottom">
				<h1 class="title yellow"><%=dto.getHeading() %></h1>
				<p class="story-content"><%=sh.getSubString(dto.getContent(), 200, "... <a href=\"get-cover-story.html?id=" + dto.getId() + "\" class=\"yellow\">read more</a>") %></p>
			</div>
		</div>
		<%
					}
				}
		%>
   	</div><!-- fluid_container -->
   	<%
    		}
    	}// end of cover story
    %>
    <div class="row" id="ss-wf-container">
    <%
    	rufDAO = new RimsUploadedFilesDAO();
    	ro = rufDAO.getMap(null, " AND current_status='active' AND module_type IN (" + RimsUploadedFilesService.SUCCESS_STORY + "," + RimsUploadedFilesService.WOW_FACTOR +  ") ORDER BY id DESC ", null);
    	if(ro != null && ro.getIsSuccessful()) {
    		rufData = (LinkedHashMap<String, RimsUploadedFilesDTO>) ro.getData();
    		if(rufData != null && rufData.size() > 0) {
    %>
    <div class="col-lg-6">
    	<div id="success-story-container">
    		<img id="ss-top" src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/success-icon.jpg" />
    		<div id="success-story"  class="fluid_container camera_wrap camera_azure_skin stories">
    			<%
    				for(RimsUploadedFilesDTO rufDTO: rufData.values()) {
    					if(NumericHelper.parseInt(rufDTO.getModuleType()) == RimsUploadedFilesService.SUCCESS_STORY) {
    					
    			%>
    			<div data-src="<%=ApplicationConstant
									.getBaseURL(session)%>resources/upload-folder/uploaded-files/<%=rufDTO.getFileFileName()%>">
				</div>
    			<%	
    					}
    				}
    			%>
    		</div>
    	</div>
    </div>
    <div class="col-lg-6">
    	<div id="wow-factor-container">
    		<img id="ss-top" src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/wow-icon.png" />
    		<div id="wow-factor"  class="fluid_container camera_wrap camera_azure_skin stories">
    			<%
    				for(RimsUploadedFilesDTO rufDTO: rufData.values()) {
    					if(NumericHelper.parseInt(rufDTO.getModuleType()) == RimsUploadedFilesService.WOW_FACTOR) {
    					
    			%>
    			<div data-src="<%=ApplicationConstant
									.getBaseURL(session)%>resources/upload-folder/uploaded-files/<%=rufDTO.getFileFileName()%>">
				</div>
    			<%	
    					}
    				}
    			%>
    		</div>
    	</div>
    </div>
    <%			
    		}
    	}// end of ss-wf
    	ro = new PrDAO().getMap(null, " AND delete_status=0 ORDER BY id DESC LIMIT 3 ", null);
    	if(ro != null && ro.getIsSuccessful()) {
    		prData = (LinkedHashMap<String, PrDTO>) ro.getData();
    		if(prData != null && prData.size() > 0) {
    %>
     <div class="col-lg-12">
    	<div id="pr-container">
    		<img id="ss-top" class="pr-img" src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/press-icon.png" />
    		<div class="line"></div>
   			<div id="pr">
   				<%
    				for(PrDTO prDTO: prData.values()) {
    					
    			%>
    			<div>
    				<h1 class="title pr-title"><%=prDTO.getTitle() %></h1>
    				<span class="pr-date"><%=prDTO.getCreated() %></span>&nbsp;
    				<a class="purple-bd" href="get-pr.html?id=<%=prDTO.getId() %>">read more</a>
    			</div>
    			<%	
    				}
    			%>
   			</div>
   			<div class="line"></div>
    	</div>
    </div>
    <%			
    		}
    	}
    %>
    </div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("home").fatal("RuntimeException", e);
	}
%>


--%>