<%@page import="com.revesoft.po.revesoft.orderlist.PoDetailsDTO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.QueryHelper"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>

	
<script type="text/javascript">

$(document).ready(function() {	
	$('#checkAll').click(function() {
		
		if ($('#checkAll').is(':checked')) {
			
			 $("input[name~=ids]").each(function() {
				$(this).prop("checked", true);	
			
			});			
			

		} else {
			
			$("input[name~=ids]").each(function() {
				$(this).prop("checked", false);
			});
		}

	});
	 $('input[name~=ids]').click(function() {		 
		if ($(this).is(':checked') == false) {
			$('#checkAll').prop('checked', false);
		}
	}); 
	
	
	
	$('.UpdateAll').click(function(event) {
		try {
			event.preventDefault();
			var ids = '';
			var c=0;
			var type = $(this).attr('href');
			$('input[name~=ids]:checked').each(function() {ids += $(this).val()+ ',';c++;});		
			var con = getConfirmation(c,"Do you want to delete?");
			if (ids.length > 0 && con) {
				$.post(BASE_URL + 'resources/ajax-files/po-product-update.jsp',
					{
					    ids : ids,
						type : type						
					},
					function(data) {
						data = $.trim(data);
						if (data == 'successful') {
							alert('Operation Successful');
							$('#data').html('');							 
							location.reload();
						} else {
							alert(data);
						}
				});
			} 
		} catch (e) {
			alert(e);
		}
	});

	
	function getConfirmation(count,msg) {
	    
	    if(count==0){
	    	alert("Please select at least one item to delete.");
	    	return false;
	    }	
	    var stat = confirm("Total Record: "+count+". "+msg);
	    if (stat==true)
	    	return true;
	    else
	    	return false;
	    	
	}

	
	});
</script>
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		LinkedHashMap<String, PoDetailsDTO> data = null;	
		RimsUsersRepository ruRepository = null;
		MashupDataRepository mudRepository = null;
		
		
		%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Listed Products</h1>
		<div class="pull-right"> 
			<%
 				if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) > Permissions.LEVEL_FOUR) {
 			%>
				<!-- <a href="delete" class="UpdateAll btn btn-danger">Delete</a> -->
			<%}
  			 %>			 
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panle-body">
		<div class="table-responsive">
			<table id="dataTables-example" class="table-bordered table-search table-hover">
				<thead>
					<tr>
						<!-- <th><input type="checkbox" id="checkAll" name="checkAll" /></th> -->					 
						<th>PO</th>
						<th>Product Name</th>
						<th>Tag</th>
						<th>Quantity</th>
						<th>Unit Price</th>						
						<th>Shipping Charge</th>	
						<th>Price</th>
						<th>Tax</th>
						<th>Total Price</th>
						<th>Action</th>	
														
					</tr>
				</thead>
				<tbody>
					<%
						if (obj != null && obj instanceof LinkedHashMap) {
									ruRepository = RimsUsersRepository.getInstance(false);
									data = (LinkedHashMap<String, PoDetailsDTO>) obj;
									if (data != null && data.size() > 0) {
										
										int quantity=0;
										double price = 0.0;
										double ship = 0.0;
										double tax = 0.0;
										double total = 0.0;
										double total2 = 0.0;
										double grandTotal = 0.0;
										String productName="";
										
										mudRepository = MashupDataRepository.getInstance(false);
										MdProductListRepository plistRepo=null;
										plistRepo = MdProductListRepository.getInstance(false);
										for (PoDetailsDTO dto : data.values()) {
											price = dto.getPdPrice();
											quantity = dto.getPdQuantity();
											ship = dto.getPdShippingCharge();
											tax =dto.getPdTax();
											total = quantity*price+ship;
											total2 = total*(1+tax*1.0/100);
											grandTotal = grandTotal+total2;
											productName = plistRepo.getPlProductNameByPlProductID(dto.getPd_plID());
					%>
					<tr >
						<%-- <td><input type="checkbox" value="<%=dto.getPdID()%>" name="ids" /></td>	 --%>
														
						<td>
							<%=dto.getPdPONumber()%>
						</td>
						<td><%=productName%></td>
						<td><%=dto.getPdTag()%></td>
						<td><%=quantity%></td>
						<td><%=price%></td>
						<td><%=ship%></td>
						<td><%=total %></td>
						<td><%=tax%></td>
						<td><%=total2 %></td>
						<td>
						<%
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
						%>
			                 <a href="get-po-to-edit.html?id=<%=dto.getPdID()%>" title="Edit">Edit Tag</a>
			    
			                <%
			                }else{
			                	%>
			                	N/A
			                	<%
			                }
			                %>
						</td>
						
					</tr>
				<%}%>
				<tr >
				   
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>
				   <td></td>				   
				   <th>Total</th>
				   <th><%=grandTotal %></th>
				   <td></td>
				  
				   
				</tr>
			<%}
			
			
		}%>
	</tbody>
</table>
</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-vbregistration").fatal("RuntimeException", e);
	}
%>
