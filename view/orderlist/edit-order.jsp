<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.revesoft.po.revesoft.orderlist.PoDetailsDTO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDAO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListService"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<script type="text/javascript" src="../resources/scripts/orders.js"></script>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<div>
<script type="text/javascript">

$(document).ready(function() {
	 getProductList();	 
});

function validate()
{ 
	 var f = document.forms[0];
	 var ob = f.checksum;	 
	 if (isNaN(ob.value)) {
		 alert("Please put Integer value as checksum");
		 return false;
	 }
	 	 
	 return true;
}
function selectAllOptions() {
	len = document.forms[0].productDetailsInfo.length;
	for (i = 0; i < len; i++) {
		document.forms[0].productDetailsInfo.options[i].selected = true;
	}
	return true;
}

</script>

	<%
		try {
			RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
			Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
			session.removeAttribute(ApplicationConstant.ACTION_DATA);
			ReturnObject ro = new ReturnObject();
			OrderListDTO dto = null;
			MashupDataRepository mudRepository = MashupDataRepository.getInstance(false);
			UIHelper uiHelper = new UIHelper();	
			if (obj != null && obj instanceof OrderListDTO) {
				dto = (OrderListDTO) obj;
				if (dto != null) {
					%>
	<form autocomplete="off" action="update-order-details-order-list.html" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="selectAllOptions(); return validate();" >
		<%@ include file="../includes/captcha.jsp"%>
		<input type="hidden" name="id" value="<%=dto.getInvcID()%>" />
		<input type="hidden" name="clID" value="<%=dto.getClID()%>" />
		<div class="panel panel-info">
			<div class="panel-body">
				
								
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Customer Name<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="customerName" value="<%=dto.getCustomerName() %>" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Company Name</label>
					<div  class="col-lg-3 col-md-5">
						<input name="companyName" value="<%=dto.getCompanyName() %>" class="form-control" />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Email<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="customerEmail" value="<%=dto.getCustomerEmail() %>" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Phone<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="customerPhone" value="<%=dto.getCustomerPhone() %>" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Shipping Address<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<textarea name="shippingAddress" rows="5"   class="form-control required" required><%=dto.getShippingAddress() %></textarea>
					</div>
					 <label class="col-lg-2 col-md-5"> </label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Billing Address<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<textarea name="billingAddress" rows="5" class="form-control required" required><%=dto.getBillingAddress().trim() %></textarea>
					</div>
					 <label class="col-lg-2 col-md-5"> </label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Order Number<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="purchaseOrderNumber" value="<%=dto.getPurchaseOrderNumber() %>" class="form-control"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Date of Order<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">						
						<input name="orderDate" value="<%=dto.getOrderDate() %>" class="form-control datepicker" placeholder="ex. 03/10/2018" required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Delivery Date<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">						
						<input name="deliveryDate" value="<%=dto.getDeliveryDate() %>" class="form-control datepicker" placeholder="ex. 03/10/2018" required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Upload File</label>
					<div  class="col-lg-3 col-md-5">
						<input  type="file" name="uploadDoc" class="form-control-file" />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Currency</label>
					<div  class="col-lg-3 col-md-5">						
						<select name="currencyID" id="currencyID" class="form-control" >
							<!-- <option value="1">USD</option>
							<option value="2">BDT</option> -->
							<%=uiHelper.getSelectFromMashup(mudRepository.getByFieldType(mudRepository.CURRENCY, true), true, Integer.parseInt(dto.getCurrencyID()), null) %>
						</select>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
				
				   <label class="col-lg-2 col-md-2">Products Details</label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Product Name</label>
					<div  class="col-lg-3 col-md-5">						
						<select name="plProductName" id="plProductName" class="form-control">
							<option value="">Select Product</option>
						</select>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Unit Price</label>
					<div  class="col-lg-3 col-md-5">
						<input name="plPriceUSD" id="plPriceUSD" type="number" step="any" class="form-control" />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Quantity</label>
					<div  class="col-lg-3 col-md-5">
						<input name="pdQuantity" id="pdQuantity" type="number" value="1" class="form-control"  />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Shipping Charge</label>
					<div  class="col-lg-3 col-md-5">
						<input name="shippingCharge" id="shippingCharge" type="number" step="any" class="form-control" value="0.00" />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Total Price</label>
					<div  class="col-lg-3 col-md-5">
						<input name="totalPriceUSD" id="totalPriceUSD" type="number" step="any" class="form-control"  readonly="readonly"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Tax</label>
					<div  class="col-lg-3 col-md-5">
						<input name="tax" id="tax" type="number" step="any" value="0.00" class="form-control"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Total Price</label>
					<div  class="col-lg-3 col-md-5">
						<input name="totalPriceUSD2" id="totalPriceUSD2" type="number" step="any" class="form-control"  readonly="readonly"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2"></label>
					<div  class="col-lg-3 col-md-5">
					      <button  type="button"  class="addProducts btn btn-info">Add</button>
						  <button  type="button"  class="removeProducts btn btn-info">Remove</button>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				
				
				
				
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2"></label>
					<div  class="table-responsive col-lg-8 col-md-8">
					     <table id="ProductList"  class="table table-hover table-striped table-bordered table-condensed">								 
								  <tr>
									<td>Name</td>
									<td>Unit Price</td>
									<td>Quantity</td>
									<td>Shipping Charge</td>
									<td>Price</td>
									<td>Tax</td>
									<td>Total Price</td>																									
								  </tr>
								<%
								String condition=" and pd_invcID="+dto.getInvcID();
								ro = new OrderListService(rimsUsersDTO).getMapProductDetais(null, condition ,OrderListDAO.DEFAULT_PO_KEY_COLUMN);
								LinkedHashMap<String, PoDetailsDTO> pdata = null;
								ArrayList<String> list = new ArrayList<String> ();
								
								if (ro != null && ro.getIsSuccessful() && ro.getData() instanceof LinkedHashMap) {
									pdata = (LinkedHashMap<String, PoDetailsDTO>) ro.getData();
									if (pdata != null && pdata.size() > 0) {

										int quantity=0;
										double price = 0.0;
										double ship = 0.0;
										double tax = 0.0;
										double total = 0.0;
										double total2 = 0.0;
										double grandTotal = 0.0;
										String productName="";
										MdProductListRepository plistRepo=null;
										plistRepo = MdProductListRepository.getInstance(false);
										
										
										for (PoDetailsDTO pdto : pdata.values()) {
											String str = "";
											price = pdto.getPdPrice();
											quantity = pdto.getPdQuantity();
											ship = pdto.getPdShippingCharge();
											tax =pdto.getPdTax();
											total = quantity*price+ship;
											total2 = total*(1+tax*1.0/100);
											grandTotal = grandTotal+total2;
											
											str = pdto.getPd_plID()+":"+price+":"+quantity+":"+ship+":"+tax;
											list.add(str);
											productName = plistRepo.getPlProductNameByPlProductID(pdto.getPd_plID());
											
											%>
											<tr>
											  <td><%=productName%></td>
											  <td><%=price %></td>
											  <td><%=quantity %></td>
											  <td><%=ship %></td>
											  <td><%=total %></td>
											  <td><%=tax %></td>
											  <td><%=total2 %></td>
											</tr>
											
											<%
										}
										
									}
									
								}
									%>
								  
								</table>
					</div>
					 
				</div>
				<div style="display:none;" class="form-group">
					<label  class="col-lg-2 col-md-2"></label>
					<div  class="col-lg-3 col-md-5">
					      <select name="productDetailsInfo" id="productDetailsInfo" multiple="multiple" class="form-control">
							<%
							for (int i=0; i<list.size(); i++){
								%>
								<option value ="<%=list.get(i) %>" ><%=list.get(i) %></option>								
								<%
								
							}
								
							%>
						</select>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				
				<div class="clearfix"></div>
				
			</div>
			
			<div class="panel-footer">
				<input type="submit" value="Submit" class="btn btn-reve" />
				<span class="mandatory">*</span> Fields are mandatory
			</div>
		</div>
	</form>
					<%
				}
			}
			
	%>
	
	<%
		} catch (RuntimeException e) {
			Logger.getLogger("add-rims-users").fatal("RuntimeException", e);
		}
	%>
</div>