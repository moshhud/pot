<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.QueryHelper"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>

  
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		LinkedHashMap<String, OrderListDTO> data = null;	
		RimsUsersRepository ruRepository = null;
		MashupDataRepository mudRepository = null;
		int sln=1;
		String type = request.getParameter("dashboardType");
		if(type.equals("1")){
			type = "Ongoing Order";
		}else if(type.equals("2")){
			type = "Delivery Confirmation";
		}
		else{
			type = "Pending Invoice";
		}
		%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Result for <%=type %></h1>
	
	</div>
	<div class="panle-body">
		<div class="table-responsive">
			<table id="dataTables-example" class="table-bordered table-search table-hover table-striped">
				<thead>
					<tr>
					    <th>SN</th>
						<!-- <th><input type="checkbox" id="checkAll" name="checkAll" />Check All</th> -->
						<!-- <th>ID</th> -->						
						<th>PO</th>
						<th>Client Name</th>
						<th>Company Name</th>
						<th>Email</th>						
						<th>Shipping Address</th>	
						<!-- <th>Billing Address</th> -->
						<th>File</th>
						<th>Currency</th>
						<th>Amount</th>
						<th>Due</th>	
						<th>Received</th>
						<th>Order date</th>	
						<th>Delivery Date</th>		
						<th>Invoice Status</th>	
						<th>PO Status</th>	
						<th>Action</th>														
					</tr>
				</thead>
				<tbody>
					<%
						if (obj != null && obj instanceof LinkedHashMap) {
									ruRepository = RimsUsersRepository.getInstance(false);
									data = (LinkedHashMap<String, OrderListDTO>) obj;
									if (data != null && data.size() > 0) {							
										mudRepository = MashupDataRepository.getInstance(false);
										for (OrderListDTO dto : data.values()) {											
					%>
					<tr >
					    <td><%=sln %></td>
						<%-- <td><input type="checkbox" value="<%=dto.getInvcID()%>" name="ids" /></td>	 --%>
						<%-- <td><%=dto.getId()%></td>	 --%>										
						<td>
							<a href="get-product-details.html?id=<%=dto.getInvcID()%>" title="Invoice Status">
							    <%=dto.getPurchaseOrderNumber()%>
							</a>
						</td>
						<td><%=dto.getCustomerName()%></td>
						<td><%=dto.getCompanyName()%></td>
						<td><%=dto.getCustomerEmail()%></td>
						<td><%=dto.getShippingAddress()%></td>
						<%-- <td><%=dto.getBillingAddress()%></td> --%>
						<td>
						<%-- <a class="btn btn-info btn-sm" href="<%=ApplicationConstant.getBaseURL(session) + ApplicationConstant.PO_FILES + dto.getUploadDocFileName() %>">Download</a> --%>
						<a class="btn btn-info btn-sm" href="<%= "file-download.html?uploadDocFileName=" + dto.getUploadDocFileName() %>">Download</a>
						</td>	
						<td><%=mudRepository.getLabelById(Integer.parseInt(dto.getCurrencyID())) %></td>
						<td><%=dto.getInvcAmount()%></td>
						<td><%=dto.getInvcDue()%></td>
						<td><%=dto.getInvcReceived()%></td>
						<td><%=dto.getOrderDate()%></td>
						<td><%=dto.getDeliveryDate()%></td>
						<td>
						<%
						if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
							%>
							<a href="get-invoice-status.html?id=<%=dto.getInvcID()%>" title="Invoice Status">
							    <%=mudRepository.getLabelById(dto.getInvcInvoiceStatus()) %>
                            </a>
							<%
						}else{
							%>
							<%=mudRepository.getLabelById(dto.getInvcInvoiceStatus()) %>
							<% 
						}
						%>
						</td>
						<td>
						<%
						if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
							%>
							<a href="get-po-status.html?id=<%=dto.getInvcID()%>" title="Po Status">
							    <%=mudRepository.getLabelById(dto.getInvcPOStatus()) %>
                            </a>							
							<%
						}else{
							%>
							<%=mudRepository.getLabelById(dto.getInvcPOStatus()) %>
							<% 
						}
						%>
						</td>
						<td>
						<%
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
						%>
			                 <a href="get-order-details-order-list.html?id=<%=dto.getInvcID()%>" title="Edit">Edit</a>
			    
			                <%}else{
								%>
								N/A
								<% 
							}
			                %>
						</td>
						
					</tr>
				<%
				sln++;
				}%>
			<%}
			
			
		}%>
	</tbody>
</table>
</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-vbregistration").fatal("RuntimeException", e);
	}
%>
