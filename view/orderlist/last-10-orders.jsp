<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>

<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.QueryHelper"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>

   
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
	<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>  -->
	
<script type="text/javascript">

$(document).ready(function() {	
	$('#checkAll').click(function() {		
		if ($('#checkAll').is(':checked')) {
			
			 $("input[name~=ids]").each(function() {
				$(this).prop("checked", true);	
			
			});			
			

		} else {
			
			$("input[name~=ids]").each(function() {
				$(this).prop("checked", false);
			});
		}

	});
	 $('input[name~=ids]').click(function() {		 
		if ($(this).is(':checked') == false) {
			$('#checkAll').prop('checked', false);
		}
	}); 	
	
	$('.UpdateAll').click(function(event) {
		try {
			event.preventDefault();
			var ids = '';
			var c=0;
			var type = $(this).attr('href');
			$('input[name~=ids]:checked').each(function() {ids += $(this).val()+ ',';c++;});		
			var con = getConfirmation(c,"Do you want to delete?");
			if (ids.length > 0 && con) {
				$.post(BASE_URL + 'resources/ajax-files/po-update.jsp',
					{
					    ids : ids,
						type : type						
					},
					function(data) {
						data = $.trim(data);
						if (data == 'successful') {
							alert('Operation Successful');
							$('#data').html('');							 
							location.reload();
						} else {
							alert(data);
						}
				});
			} 
		} catch (e) {
			alert(e);
		}
	});
		
	function getConfirmation(count,msg) {
	    
	    if(count==0){
	    	alert("Please select at least one item to delete.");
	    	return false;
	    }	
	    var stat = confirm("Total Record: "+count+". "+msg);
	    if (stat==true)
	    	return true;
	    else
	    	return false;
	    	
	}
	
	});
</script>
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		LinkedHashMap<String, OrderListDTO> data = null;	
		RimsUsersRepository ruRepository = null;
		MashupDataRepository mudRepository = null;
		
		
		%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Search Result</h1>
		<div class="pull-right"> 
			<%
 				if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) > Permissions.LEVEL_FOUR) {
 			%>
			    		
			<%}%>	
					 
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panle-body">
		<div class="table-responsive">
			<table id="dataTables-example" class="table-bordered table-search table-hover">
				<thead>
					<tr>
						<th><input type="checkbox" id="checkAll" name="checkAll" /></th>
						<!-- <th>ID</th> -->
						<th>PO</th>
						<th>Client Name</th>
						<th>Company Name</th>
						<th>Email</th>						
						<th>Shipping Address</th>	
						<th>Billing Address</th>
						<th>File</th>
						<th>Currency</th>
						<th>Amount</th>
						<th>Due</th>	
						<th>Received</th>
						<th>Order date</th>	
						<th>Delivery Date</th>		
						<th>Invoice Status</th>	
						<th>PO Status</th>	
						<th>Action</th>														
					</tr>
				</thead>
				<tbody>
					<%
						if (obj != null && obj instanceof LinkedHashMap) {
									ruRepository = RimsUsersRepository.getInstance(false);
									data = (LinkedHashMap<String, OrderListDTO>) obj;
									if (data != null && data.size() > 0) {							
										mudRepository = MashupDataRepository.getInstance(false);
										for (OrderListDTO dto : data.values()) {											
					%>
					<tr >
						<td><input type="checkbox" value="<%=dto.getInvcID()%>" name="ids" /></td>	
						<%-- <td><%=dto.getId()%></td>	 --%>										
						<td>
							<a href="get-product-details.html?id=<%=dto.getInvcID()%>" title="Invoice Status">
							    <%=dto.getPurchaseOrderNumber()%>
							</a>
						</td>
						<td><%=dto.getCustomerName()%></td>
						<td><%=dto.getCompanyName()%></td>
						<td><%=dto.getCustomerEmail()%></td>
						<td><%=dto.getShippingAddress()%></td>
						<td><%=dto.getBillingAddress()%></td>
						<td>
						<%-- <a class="btn btn-info btn-sm" href="<%=ApplicationConstant.getBaseURL(session) + ApplicationConstant.PO_FILES + dto.getUploadDocFileName() %>">Download</a> --%>
						<a class="btn btn-info btn-sm" href="<%= "file-download.html?uploadDocFileName=" + dto.getUploadDocFileName() %>">Download</a>
						</td>	
						<td><%=mudRepository.getLabelById(Integer.parseInt(dto.getCurrencyID())) %></td>
						<td><%=dto.getInvcAmount()%></td>
						<td><%=dto.getInvcDue()%></td>
						<td><%=dto.getInvcReceived()%></td>
						<td><%=dto.getOrderDate()%></td>
						<td><%=dto.getDeliveryDate()%></td>
						<td>
						<%
						if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
							%>
							<a href="get-invoice-status.html?id=<%=dto.getInvcID()%>" title="Invoice Status">
							    <%=mudRepository.getLabelById(dto.getInvcInvoiceStatus()) %>
                            </a>
							<%
						}else{
							%>
							<%=mudRepository.getLabelById(dto.getInvcInvoiceStatus()) %>
							<% 
						}
						%>
						</td>
						<td>
						<%
						if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
							%>
							<a href="get-po-status.html?id=<%=dto.getInvcID()%>" title="Po Status">
							    <%=mudRepository.getLabelById(dto.getInvcPOStatus()) %>
                            </a>							
							<%
						}
						%>
						</td>
						<td>
						<%
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR) {
						%>
			                 <a href="get-order-details-order-list.html?id=<%=dto.getInvcID()%>" title="Edit">Edit</a>
			    
			                <%}%>
						</td>
						
					</tr>
				<%}%>
			<%}
			
			
		}%>
	</tbody>
</table>
</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-vbregistration").fatal("RuntimeException", e);
	}
%>
