<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<script type="text/javascript" src="../resources/scripts/orders.js"></script>

<div>
<script type="text/javascript">

$(document).ready(function() {
	 getProductList();	 
});

function validate()
{ 
	 var f = document.forms[0];
	 var ob = f.checksum;	 
	 if (isNaN(ob.value)) {
		 alert("Please put Integer value as checksum");
		 return false;
	 }
	 	 
	 return true;
}
function selectAllOptions() {
	len = document.forms[0].productDetailsInfo.length;
	for (i = 0; i < len; i++) {
		document.forms[0].productDetailsInfo.options[i].selected = true;
	}
	return true;
}

</script>

	<%
		try {
			RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
					.getAttribute(ApplicationConstant.LOGIN_INFO);
			MashupDataRepository mudRepository = MashupDataRepository
					.getInstance(false);
			UIHelper uiHelper = new UIHelper();
	%>
	<form autocomplete="off" action="add-order-details-order-list.html" method="post" enctype="multipart/form-data" class="form-horizontal" onsubmit="selectAllOptions(); return validate();" >
		<%@ include file="../includes/captcha.jsp"%>
		<div class="panel panel-info">
			<div class="panel-body">
				
				<div  class="form-group">
					<label  class="col-lg-2 col-md-2">Form Type</label>
					<div  class="col-lg-3 col-md-5">
						<input type="radio" name="formType" id="fullFormButton" value="fullFormButton" checked="checked"/>
						 <label >Full Form</label>&nbsp;&nbsp;
						 <input type="radio" name="formType" id="shortFormButton" value="shortFormButton" />
					     <label >Short Form</label>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Customer Name<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="customerName" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Company Name</label>
					<div  class="col-lg-3 col-md-5">
						<input name="companyName" class="form-control" />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Email<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="customerEmail" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Phone<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="customerPhone" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Shipping Address<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<textarea name="shippingAddress" class="form-control required" required="required"/></textarea>
					</div>
					 <label class="col-lg-2 col-md-5"> </label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Billing Address<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<textarea name="billingAddress" class="form-control required" required="required"/></textarea>
					</div>
					 <label class="col-lg-2 col-md-5"> </label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Order Number<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="purchaseOrderNumber" class="form-control"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Date of Order<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">						
						<input name="orderDate" class="form-control datepicker" placeholder="ex. 03/10/2018" required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Delivery Date<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">						
						<input name="deliveryDate" class="form-control datepicker" placeholder="ex. 03/10/2018" required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Upload File<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input  type="file" name="uploadDoc" class="form-control-file" required/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div class="form-group">
					<label  class="col-lg-2 col-md-2">Currency</label>
					<div  class="col-lg-3 col-md-5">						
						<select name="currencyID" id="currencyID" class="form-control" >
							<!-- <option value="1">USD</option>
							<option value="2">BDT</option> -->
							<%=uiHelper.getSelectFromMashup(mudRepository.getByFieldType(mudRepository.CURRENCY, true), true, mudRepository.CURRENCY_DEFAULT, null) %>
						</select>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
				
				   <label class="col-lg-2 col-md-2">Products Details</label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Product Name<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">						
						<select name="plProductName" id="plProductName" class="form-control required" required="required">
							<option value="">Select Product</option>
						</select>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Unit Price<span class="glyphicon glyphicon-asterisk mandatory"></span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="plPriceUSD" id="plPriceUSD" type="number" step="any" class="form-control required"  required="required"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Quantity</span></label>
					<div  class="col-lg-3 col-md-5">
						<input name="pdQuantity" id="pdQuantity" type="number" value="1" class="form-control"  />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Shipping Charge</label>
					<div  class="col-lg-3 col-md-5">
						<input name="shippingCharge" id="shippingCharge" type="number" step="any" class="form-control" value="0.00" />
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Total Price</label>
					<div  class="col-lg-3 col-md-5">
						<input name="totalPriceUSD" id="totalPriceUSD" type="number" step="any" class="form-control"  readonly="readonly"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Tax</label>
					<div  class="col-lg-3 col-md-5">
						<input name="tax" id="tax" type="number" step="any" value="0.00" class="form-control"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Total Price</label>
					<div  class="col-lg-3 col-md-5">
						<input name="totalPriceUSD2" id="totalPriceUSD2" type="number" step="any" class="form-control"  readonly="readonly"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2"></label>
					<div  class="col-lg-3 col-md-5">
					      <button  type="button"  class="addProducts btn btn-info">Add</button>
						  <button  type="button"  class="removeProducts btn btn-info">Remove</button>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2"></label>
					<div  class="table-responsive col-lg-8 col-md-8">
					     <table id="ProductList"  class="table table-hover table-striped table-bordered table-condensed">								 
								  <tr>
									<td>Name</td>
									<td>Unit Price</td>
									<td>Quantity</td>
									<td>Shipping Charge</td>
									<td>Price</td>
									<td>Tax</td>
									<td>Total Price</td>																									
								  </tr>
								</table>
					</div>
					 
				</div>
				<div style="margin: 0px 0px 2px 0px;display:none;" class="form-group">
					<label  class="col-lg-2 col-md-2"></label>
					<div  class="col-lg-3 col-md-5">
					      <select name="productDetailsInfo" id="productDetailsInfo" multiple="multiple" class="form-control">
							
						</select>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>
				
				<div class="clearfix"></div>
				
			</div>
			
			<div class="panel-footer">
				<input type="submit" value="Submit" class="btn btn-reve" />
				<span class="mandatory">*</span> Fields are mandatory
			</div>
		</div>
	</form>
	<%
		} catch (RuntimeException e) {
			Logger.getLogger("add-rims-users").fatal("RuntimeException", e);
		}
	%>
</div>