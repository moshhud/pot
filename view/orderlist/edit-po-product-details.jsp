<%@page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="com.revesoft.po.revesoft.orderlist.PoDetailsDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<div>
	<%
		try {
			RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
					.getAttribute(ApplicationConstant.LOGIN_INFO);
			OrderListDTO dto=null;
			PoDetailsDTO pdto = null;
			Object obj = session
					.getAttribute(ApplicationConstant.ACTION_DATA);
			session.removeAttribute(ApplicationConstant.ACTION_DATA);
			 
			MdProductListRepository plistRepo=null;
			String productName="";
			
			if (obj != null && obj instanceof OrderListDTO) {
				dto = (OrderListDTO) obj;
				if (dto != null) {
					pdto = dto.getProductDTO();
					if(pdto==null){
						pdto = new PoDetailsDTO();
						dto.setProductDTO(pdto);
						System.out.println("Created New pdto ");
					}
					plistRepo = MdProductListRepository.getInstance(false);
					productName = plistRepo.getPlProductNameByPlProductID(pdto.getPd_plID());
					System.out.println("Product Name: "+productName);
					System.out.println("Product price: "+pdto.getPdPrice());
	%>
	<form action="update-po-product-details.html" method="post" class="form-horizontal">
		<%@ include file="../includes/captcha.jsp" %>
		<input type="hidden" name="id" value="<%=pdto.getPdID()%>" />
		
		<div class="panel panel-info">
			<div class="panel-body">
			
			<div  class="form-group">
						<label  class="col-lg-2 col-md-2">Product Name</label>
						<div  class="col-lg-3 col-md-5">
							<input type="text" name="plProductName"    value="<%=productName %>"  class="form-control"/>
						</div>
						 <label class="col-lg-2 col-md-5"></label>
			</div>	
			
			<div  class="form-group">
						<label  class="col-lg-2 col-md-2">Tag</label>
						<div  class="col-lg-3 col-md-5">
							<input type="text" name="pdTag"   class="form-control" value="<%=pdto.getPdTag() %>"  />
						</div>
						 <label class="col-lg-2 col-md-5"></label>
			</div>	
		
			</div>
			<div class="panel-footer">
				<input type="submit" value="Submit" class="btn btn-warning" />
				<span class="mandatory">*</span> Fields are mandatory
			</div>
		</div>

	</form>
	<%
				}
			}
		} catch (RuntimeException e) {
			Logger.getLogger("edit-users").fatal("RuntimeException", e);
		}
	%>
</div>