<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<div>
	<%
		try {
			RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
					.getAttribute(ApplicationConstant.LOGIN_INFO);
			OrderListDTO dto = null;
			Object obj = session
					.getAttribute(ApplicationConstant.ACTION_DATA);
			session.removeAttribute(ApplicationConstant.ACTION_DATA);
			MashupDataRepository mudRepository = MashupDataRepository
					.getInstance(false);
			UIHelper uiHelper = new UIHelper();
			Validations v = new Validations();
			if (obj != null && obj instanceof OrderListDTO) {
				dto = (OrderListDTO) obj;
				if (dto != null) {
					
					System.out.println("invoice status: "+dto.getInvcInvoiceStatus());
	%>
	<form action="update-invoice-status.html" method="post"
		  class="form-horizontal">
		<%@ include file="../includes/captcha.jsp" %>
		<input type="hidden" name="id" value="<%=dto.getInvcID()%>" />
		
		<div class="panel panel-info">
			<div class="panel-body">
			
		<div id="fullForm" class="form-group">
					<label  class="col-lg-2 col-md-2">Amount Received</label>
					<div  class="col-lg-3 col-md-5">
						<input name="invcReceivedAmount" id="invcReceived" type="number" value="0" step="any" class="form-control"/>
					</div>
					 <label class="col-lg-2 col-md-5"></label>
				</div>		
		
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Invoice Status</label>
			<div class="col-lg-3 col-md-5">
				<select name="invcInvoiceStatus" required class="form-control">
					<%=uiHelper.getSelectFromMashup(mudRepository.getByFieldType(mudRepository.INVOICE_INITIAL, true), true, dto.getInvcInvoiceStatus(), null) %>
				</select>
			</div>
			<div class="col-lg-1">
				<span class="glyphicon glyphicon-asterisk mandatory"></span>
			</div>
		</div>
		
		
		
		
			</div>
			<div class="panel-footer">
				<input type="submit" value="Submit" class="btn btn-warning" />
				<span class="mandatory">*</span> Fields are mandatory
			</div>
		</div>

	</form>
	<%
				}
			}
		} catch (RuntimeException e) {
			Logger.getLogger("edit-users").fatal("RuntimeException", e);
		}
	%>
</div>