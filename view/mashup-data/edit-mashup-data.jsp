<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataDTO"%>
<%
	try {
		Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
		MashupDataDTO dto  = null;
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		UIHelper uIHelper = null;
		if(obj != null && obj instanceof MashupDataDTO) {
			dto = (MashupDataDTO) obj;
			if(dto != null) {
				uIHelper = new UIHelper();
%>
<div class="profile">
	<div class="data-section">
		<form action="update-mashup-data.html" 
			method="post" class="form-horizontal">
			<%@ include file="/view/includes/captcha.jsp"%>
			<input type="hidden" name="id" value="<%=dto.getId() %>" />
			<div class="form-group">
		        <label class="control-label label-control col-xs-2" >Field Type</label>
		        <div class="col-xs-3"> 
		  			  <select name="fieldType" required="required"
					class="form-control form_bg">
						<%=uIHelper.getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(1, true), true, dto.getFieldType(), null, "Select") %>
					</select> 
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="control-label label-control col-xs-2" >Label</label>
		        <div class="col-xs-3"> 
	  			   <input type="text" name="label" class="form-control form_bg"
						value="<%=dto.getLabel() %>" />
		        </div>
		    </div><div class="form-group">
		        <label class="control-label label-control col-xs-2" >Key Value</label>
		        <div class="col-xs-3"> 
		  			  <input type="text" name="keyValue" class="form-control form_bg"
						value="<%=dto.getKeyValue() %>" /> 
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="control-label label-control col-xs-2" >Field Type</label>
		        <div class="col-xs-3"> 
		  			  <select name="currentStatus" required="required"
					class="form-control form_bg">
						<%=uIHelper.getSelectFromMashup(MashupDataRepository.getInstance(false).getByFieldType(MashupDataRepository.getInstance(false).CURRENT_STATUS, true), false, 0, dto.getCurrentStatus(), "Select") %>
					</select> 
		        </div>
		    </div>
		    <div class="form-group">
		        <label class="control-label label-control col-xs-2" ></label>
		        <div class="col-xs-3"> 
		  			  <input type="submit" class="btn btn-info" value="Update" /> 
		        </div>
		    </div>
		</form>
	</div>
</div>
<% }}} catch (Exception e) {
	out.println(e);
}%>