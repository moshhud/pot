<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.mashupData.MashupDataDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%
	try {
		Object object = session
				.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		RimsUsersRepository rimsUsersRepository = null;
		RimsUsersDTO epDTO = (RimsUsersDTO) session
				.getAttribute(ApplicationConstant.LOGIN_INFO);
		LinkedHashMap<String, MashupDataDTO> data = null;
		MashupDataRepository mudRepository = null;
		if (object != null && object instanceof LinkedHashMap
				&& epDTO != null) {
			data = (LinkedHashMap<String, MashupDataDTO>) object;
			rimsUsersRepository = RimsUsersRepository.getInstance(false);
%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Search Result</h1></div>
	<div class="panel-body">
	<div class="table-responsive">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Field Type</th>
					<th>Label</th>
					<th>Key Value</th>
					<th>Created By</th>
					<th>Current Status</th>
					<% if (epDTO.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_FOUR) { %>
					<th>Edit</th>
					<% } %>
				</tr>
			</thead>
			<tbody>
				<%
					if (data != null && data.size() > 0 && rimsUsersRepository != null) {
						mudRepository = MashupDataRepository.getInstance(false);
						for (MashupDataDTO dto : data.values()) {
							if (dto != null) {
				%>
				<tr>
					<td><%=dto.getId()%></td>
					<td><%=mudRepository.getLabelById(dto.getFieldType())%></td>
					<td class="capitalize"><%=dto.getLabel()%></td>
					<td><%=dto.getKeyValue()%></td>
					<td class="capitalize"><%=rimsUsersRepository.getUsrUserIDByUsrID(dto.getCreatedBy())%></td>
					<td class="capitalize"><%=dto.getCurrentStatus()%></td>
					<% if (epDTO.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_FOUR) { %>
					<td><a href="get-mashup-data.html?id=<%=dto.getId()%>">Edit</a></td>
					<% } %>
				</tr>
				<% }}
				} %>
			</tbody>
		</table>
		</div>
	</div>
</div>
<%
	}} catch (RuntimeException e) {Logger.getLogger("search-mashup-data").fatal("RuntimeException", e);}
%>