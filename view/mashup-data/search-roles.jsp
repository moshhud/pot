<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.mashupData.MashupDataDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%
	try {
		Object object = session
				.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		RimsUsersRepository rimsUsersRepository = null;
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
				.getAttribute(ApplicationConstant.LOGIN_INFO);
		LinkedHashMap<String, MashupDataDTO> data = null;
		if (object != null && object instanceof LinkedHashMap
				&& rimsUsersDTO != null) {
			data = (LinkedHashMap<String, MashupDataDTO>) object;
			rimsUsersRepository = rimsUsersRepository.getInstance(false);
%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Search Result</h1></div>
	<div class="panel-body">
		<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>Label</th>
				<th>Created By</th>
				<th>Current Status</th>
				<% if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_TWO) { %>
				<th>Update Permissions</th>
				<% } %>
			</tr>
		</thead>
		<tbody>
			<%
				if (data != null && data.size() > 0 && rimsUsersDTO != null) {
					for (MashupDataDTO dto : data.values()) {
						if (dto != null) {
			%>
			<tr>
				<td><%=dto.getLabel()%></td>
				<td class="capitalize"><%=rimsUsersRepository.getUsrUserIDByUsrID(dto.getCreatedBy())%></td>
				<td class="capitalize"><%=dto.getCurrentStatus()%></td>
				<% if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_TWO) { %>
				<td><a href="<%=ApplicationConstant.getBaseURL(session) %>permissions/new-permissions.html?id=<%=dto.getId()%>">View Permissions</a></td>
				<% } %>
			</tr>
			<% }}} %>
		</tbody>
	</table>
	</div>
</div>
<%
	}} catch (RuntimeException e) {Logger.getLogger("search-roles").fatal("RuntimeException", e);}
%>