<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductTypes.MdProductTypesDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDTO"%>
<%
	Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
	session.removeAttribute(ApplicationConstant.ACTION_DATA);
	MdProductListDTO dto = null;
	Validations v = null;
	UIHelper uih = new UIHelper();
	MashupDataRepository mudRepository = MashupDataRepository.getInstance(false);
	ArrayList<MdProductTypesDTO> list = null;
	ArrayList<MdProductListDTO> plList = null;
	if (obj != null && obj instanceof MdProductListDTO) {
		dto = (MdProductListDTO) obj;
	}
	if (dto == null) {
		dto = new MdProductListDTO();
		dto.setPlCurrentStatus("0");
	}
	System.out.println(dto.getPlCurrentStatus());
	
%>
<form action="add-mdproductlist.html" method="post" class="form-horizontal">
	<%@ include file="/view/includes/captcha.jsp"%>
	<div class="panel panel-info">
		<div class="panel-body">
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Product Name <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <input type="text" id="plProductName" name="plProductName"
						class="form-control" value="<%=dto.getPlProductName()%>" required />
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Description</label>
				<div  class="col-lg-3 col-md-5">
				    <input type="text" id="plDescription" name="plDescription"
						class="form-control" value="<%=dto.getPlDescription()%>"  />
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Vendor</label>
				<div  class="col-lg-3 col-md-5">
				    <input type="text" id="plVendorName" name="plVendorName"
						class="form-control" value="<%=dto.getPlVendorName()%>"  />
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Product Price <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div  class="col-lg-3 col-md-5">
				    <input type="number" step="any" id="plPriceUSD" name="plPriceUSD"
						class="form-control" value="<%=dto.getPlPriceUSD()%>" required />
				 </div>
				 <label class="col-lg-2 col-md-5"></label>
			</div>
			
			<div class="form-group">
				<label class="col-lg-2 col-md-2">Current Status <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div class="col-lg-3 col-md-5">
					<select name="plCurrentStatus" required class="form-control">	
					 <%=uih.getSelectFromMashup(mudRepository.getByFieldType(
						mudRepository.CURRENT_STATUS, true), false, 0, dto.getPlCurrentStatus())%> 
					</select>
				</div>
				<label class="col-lg-2 col-md-5"></label>
				
			</div>
			
			
			<div class="clearfix"></div>
			
			
		</div>
		<div class="panel-footer">
			<input type="submit" class="btn btn-warning" value="Submit" /> <span
				class="mandatory">*</span> Fields are mandatory
		</div>
	</div>
</form>
