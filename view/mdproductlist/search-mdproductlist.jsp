<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>

<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		LinkedHashMap<String, MdProductListDTO> data;	
		MashupDataRepository mashupDataRepository = MashupDataRepository.getInstance(false);
%>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1 class="panel-title">Search Result</h1>
	</div>
	<div class="panel-body">
	 <div class="table-responsive">
		<table class="table table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>					
					<th>Description</th>
					<th>Vendor Name</th>
					<th>Price</th>
					<th>Deleted Status</th>
					<th>Active Status</th>
					
					<%
						if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_FOUR) {
					%>
					<th>Edit</th>
					<%
						}
					%>
				</tr>
			</thead>
			<tbody>
				<%
					if (obj != null && obj instanceof LinkedHashMap) {
							data = (LinkedHashMap<String, MdProductListDTO>) obj;
							if (data != null && data.size() > 0) {
								for (MdProductListDTO dto : data.values()) {
				%>
				<tr>
					<td><%=dto.getPlProductID()%></td>
					<td><%=dto.getPlProductName()%></td>
					<td><%=dto.getPlDescription()%></td>
					<td><%=dto.getPlVendorName()%></td>
					<td><%=dto.getPlPriceUSD()%></td>
					<%
					  String delete="NO";
					  if(dto.getIsDeleted().equals("1")){
						  delete="YES";
					  }
					%>
					<td><%=delete%></td>
					<td class="capitalize"><%=dto.getPlCurrentStatus()%></td>
					<%
						if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_FOUR) {
					%>
					<td><a class="btn btn-default"
						href="get-mdproductlist.html?plProductID=<%=dto.getPlProductID()%>">Edit</a></td>
					<%
						}
					%>
				</tr>
				<% }}} %>
			</tbody>
		</table>
	</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-mdproductlist").fatal(
				"RuntimeException", e);
	}
%>