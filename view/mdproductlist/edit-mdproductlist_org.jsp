<%@page
	import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page
	import="com.revesoft.rims.dialerRegistration.mdProductTypes.MdProductTypesRepository"%>
<%@page
	import="com.revesoft.rims.dialerRegistration.mdProductTypes.MdProductTypesDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page
	import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDTO"%>
<%
	Object obj = session.getAttribute(ApplicationConstant.ACTION_DATA);
	session.removeAttribute(ApplicationConstant.ACTION_DATA);
	MdProductListDTO dto = null;
	Validations v = null;
	UIHelper uih = null;
	MashupDataRepository mudRepository = null;
	ArrayList<MdProductTypesDTO> list = null;
	ArrayList<MdProductListDTO> plList = null;
	if (obj != null && obj instanceof MdProductListDTO) {
		dto = (MdProductListDTO) obj;
		if (dto != null) {
			uih = new UIHelper();
%>
<form action="update-mdproductlist.html" method="post">
	<%@ include file="/view/includes/captcha.jsp"%>
	<input type="hidden" value="<%=dto.getPlProductID()%>"
		name="plProductID" /> <input type="hidden"
		value="<%=dto.getPlProductName()%>" name="currentProductName" />
	<div class="panel panel-info">
		<div class="panel-body">
			<div class="form-group col-lg-12">
				<label>Product Name <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<input type="text" id="plProductName" name="plProductName"
						class="form-control" value="<%=dto.getPlProductName()%>" required />
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Product Price (USD) <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<input type="number" name="plPriceUSD" class="form-control"
						value="<%=dto.getPlPriceUSD()%>"  required/>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Concurrent Registration </label>
				<div>
					<input type="number" name="plConcurrentCallRegistrationLimit" class="form-control"
						value="<%=dto.getPlConcurrentCallRegistrationLimit()%>"  />
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Product Category <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plProductType" required class="form-control">
						<option value="select">Select</option>
						<%
							v = new Validations();
									list = MdProductTypesRepository.getInstance(false)
											.getProductTypes();
									if (list != null && list.size() > 0) {
										for (MdProductTypesDTO productType : list) {
						%><option
							<%=v.checkSelected(dto.getPlProductType(),
									productType.getId())%>
							value="<%=productType.getId()%>"><%=productType.getName()%></option>
						<%
							}
									}
						%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6 col-lg-6">
				<label>Sub Category <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plProductSubType" required class="form-control">
						<%=uih
							.getSelectFromMashup(
									MashupDataRepository.MDPRODUCTLIST_PRODUCT_SUB_TYPE,
									true, dto.getPlProductSubType(), null)%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Product Status <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plProductStatus" class="form-control">
						<%
							uih = new UIHelper();
									mudRepository = MashupDataRepository.getInstance(false);
						%>
						<%=uih.getSelectFromMashup(
							mudRepository.getByFieldType(354, true), false, 0,
							dto.getPlProductStatus())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Has After Sales Service <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plHasAfterSalesService" required class="form-control">
						<%=uih.getSelectFromMashup(mudRepository
							.getByFieldType(mudRepository.DROP_DOWN_YES_NO,
									true), false, 0, dto
							.getPlHasAfterSalesService())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Update Product Support Expire Date <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plUpdateExpireDate" required class="form-control">
						<%=uih.getSelectFromMashup(mudRepository
							.getByFieldType(mudRepository.DROP_DOWN_YES_NO,
									true), false, 0, dto
							.getPlUpdateExpireDate())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Is Recursive <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="isRecursive" id="isRecursive" required
						class="form-control">
						<%=uih.getSelectFromMashup(mudRepository
							.getByFieldType(mudRepository.DROP_DOWN_YES_NO,
									true), false, 0, dto.getIsRecursive())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>No Of Months <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="noOfMonths" class="form-control">
						<%
							for (int i = 0; i <= 12; i++) {
						%>
						<option <%=v.checkSelected(dto.getNoOfMonths(), i)%>
							value="<%=i%>"><%=i%></option>
						<%
							}
						%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Show in invoice list <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="showInInvoiceList" class="form-control" required>
						<%=uih.getSelectFromMashup(mudRepository
							.getByFieldType(mudRepository.DROP_DOWN_YES_NO,
									true), false, 0, dto.getShowInInvoiceList())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Show in product status list <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="showInProductStatusList" class="form-control"
						required>
						<%=uih.getSelectFromMashup(mudRepository
							.getByFieldType(mudRepository.DROP_DOWN_YES_NO,
									true), false, 0, dto
							.getShowInProductStatusList())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Show in Support Portal <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plSupportPortalShow" class="form-control" required>
						<%=uih.getSelectFromMashup(mudRepository
							.getByFieldType(mudRepository.DROP_DOWN_YES_NO,
									true), false, 0, dto
							.getPlSupportPortalShow())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Invoice Life Time <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<input type="number" name="plInvoiceLifeTime" class="form-control"
						value="<%=dto.getPlInvoiceLifeTime()%>" required />
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Current Status <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<select name="plCurrentStatus" required class="form-control">
						<%
							uih = new UIHelper();
									mudRepository = MashupDataRepository.getInstance(false);
						%>
						<%=uih.getSelectFromMashup(
							mudRepository.getByFieldType(
									mudRepository.CURRENT_STATUS, true), false,
							0, dto.getPlCurrentStatus())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Product Type <span
					class="glyphicon glyphicon-asterisk mandatory"></span></label>
				<div>
					<input type="radio" name="productType" value="0" id="standAlone"
						checked="checked" /> <label for="standAlone">Independent</label>
					<input type="radio" name="productType" value="1" id="dependent" />
					<label for="dependent">Dependent</label>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="form-group col-lg-6" id="parentProduct">
				<label>Parent Product</label>
				<div>
					<select name="plParentProductID" class="form-control">
						<option value="-2">Select</option>
						<%
							plList = MdProductListRepository.getInstance(false)
											.getAllData();
									if (plList != null && plList.size() > 0) {
										for (MdProductListDTO plDTO : plList) {
						%>
						<option value="<%=plDTO.getPlProductID()%>"><%=plDTO.getPlProductName()%></option>
						<%
							}
									}
						%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Show Expire Date</label>
				<div>
					<select name="plShowExpireDate" class="form-control">
						<%=uih.getSelectFromMashup(
							mudRepository.getByFieldType(
									mudRepository.DROP_DOWN_YES_NO, false), false,
							0, dto.getPlShowExpireDate())%>
					</select>
				</div>
			</div>
			<div class="form-group col-lg-6">
				<label>Platinum Status</label>
				<div>
					<select name="plIsPlatinum" class="form-control">
						<%=uih.getSelectFromMashup(
							mudRepository.getByFieldType(
									mudRepository.YES_1_NO_0, false), false,
							0, dto.getPlIsPlatinum())%>
					</select>
				</div>
			</div>
			
		</div>
		<div class="panel-footer">
			<input type="submit" class="btn btn-info" value="Submit" /> <span
				class="mandatory">*</span> Fields are mandatory
		</div>
	</div>
</form>
<%
	}
	}
%>
