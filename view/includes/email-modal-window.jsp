<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<script type="text/javascript"	src="<%=ApplicationConstant.getBaseURL(session) %>resources/tiny_mce/jquery.tinymce.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.send-mail-btn').click(function(event){
			if(countCheck("idCheckBox")){
				$('#customer-mail').val("");
				var IDs="";
				var emailIDs="";
				$('.idCheckBox').each(function(){
					if($(this).is(':checked')){
						if($(this).attr('title')!=""){
							IDs+=$(this).attr('value')+",";
							emailIDs+=$(this).attr('title')+",";
						}
					}
				});
				if(IDs!="" && emailIDs!=""){
					$('#customer-mail').val(emailIDs).attr("readonly","readonly");
					$('.short-warn').html("");
				}else{
					event.preventDefault();
					alert("No Email Record Found!");
				}
			}else{
				alert("First Check at least one record!");
				event.preventDefault();
			}
		});
		function countCheck(className){
			var status=true;
			var totalChecked=0;
			$('.'+className).each(function(){
				if($(this).is(':checked')){
					totalChecked++;
				}
			});
			if(totalChecked==0){
				return false;
			}
			return status;
		}
		$('#sendMail').click(function(event){
			var tableName=$('.send-mail-btn').attr('accesskey');
			var subject=$('.subject').val();
			var mailCC=$('.mailcc').val();
			var content=$('.mail-body-text').val();
			var pageURL=window.location.href;
			var isAjax=$(this).attr('accesskey');
			if(subject!="" && content!=""){
				var IDs="";
				$('.idCheckBox').each(function(){
					if($(this).is(':checked')){
						if($(this).attr('title')!=""){
							IDs+=$(this).attr('value')+",";
						}
					}
				});
				if(IDs!=""){
					IDs=IDs.substring(0,IDs.length-1);
					if(tableName!="" && IDs!=""){
						pageURL=pageURL.split("#");
						var splitURL=pageURL[0];
						var agree=confirm("Are you confirm to send mail?");
						if(agree){
							try {
								$('.loading-point').html("").html('<img height="16" width="16" src="../resources/images/loading.gif"/>');
								$("#sendMail").attr("disabled",true).css('opacity',.2);
								$.ajax({url:BASE_URL + "resources/ajax-files/send-mail.jsp",data:({refTableId:IDs,refTableName:tableName,
									subject:subject,content:content,mailCC:mailCC}),type:"POST",dataType:"html",success:function(response)
									{
										$('.loading-point').html("");
										$('#openEmailModal').css('opacity',0);
										$('.emailModalDialog:target').css('pointer-events','none');
										$("#sendMail").removeAttr("disabled").css('opacity',0);
										if(response.trim()=="successful"){
											alert(" Your Mail has been successfully sent");
											if(isAjax != 'isAjax') {
												window.location.href=""+splitURL+"";
											}
										}else{
											if(response.trim()=="noAccess"){
												alert("Sorry You have no access to use this feature!");
											}else{
												alert(response.trim());
											}
										}
									}
								,error:function(xhr,textStatus,error)
								{
								    alert(xhr + textStatus + error);
								}
								});
							} catch(err) {
								alert(err);
							}
						}else{
							return;
						}
					}else{
						alert("Unexpected error! Please close the modal and try again");
					}
				}else{
					event.preventDefault();
					alert("No Email Record Found!");
				}
			}else{
				alert("Subject and Content is required.");
			}
		});
		function emailformValidation(){
			var subject=$('.subject').val();
			var cc=$('.mailcc').val();
			var msg=$('.mail-body-text').html();
			if(cc!=""){
				var ccLenght=cc.length;
				if(cc.charAt(ccLenght-1)=="," || cc.charAt(ccLenght-1)==";" || cc.charAt(ccLenght-1)=="."){
					cc=cc.slice(0,-1);
				}
				$('.mailcc').val("").val(cc);
			}
			return true;
		};
		$('.mail-content-type').change(function(){
			var emailBodyFor=$(this).attr('accesskey');
			mailContent(emailBodyFor);
		});
		$(window).load(function(){
			var emailBodyFor=$('input[name~=mail-content-type]:checked').attr('accesskey');
			
			mailContent(emailBodyFor);
		});
		function mailContent(emailBodyFor){
			var customerName=$('.customer-name').val();
			if(emailBodyFor=="email-dialer-body"){
				$('.subject').val("").val("New features to iTel Mobile Dialer Express/ Follow up on your Demo for iTel Mobile Dialer Express");
			}else if(emailBodyFor=="email-switch-body"){
				$('.subject').val("").val("New features to iTel Switch Plus/ Follow up on your Demo for iTel Switch Plus");
			} else {
				$('.subject').val("").val("Greetings From REVE Systems");
				emailBodyFor = "email-default-body"
			}
			if(emailBodyFor!=""){
				$.ajax({url: BASE_URL + "resources/ajax-files/email-body-text.jsp",data:({emailBody:emailBodyFor}),
					type:"POST",dataType:"html",cache:false,success:function(response)
					{
						$('.mail-body-text').html("").html(response.trim());
					}
				, error:function(xhr,textStatus,error) {
					alert("Error: " + xhr.readyState+xhr.status + error);
				}
				});
			}
		}
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '<%=ApplicationConstant.getBaseURL(session) %>resources/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,|,insertdate,inserttime,preview,|,forecolor,backcolor",
			theme_advanced_buttons3 : "tablecontrols,|,hr,|,sub,sup,|,charmap,advhr,|,print,|,ltr,rtl",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,

			// Replace values for the template plugin
			template_replace_values : {
				username : "",
				staffid : ""
			}
		});
		//$('#customer-mail').val($('.email').val());
	});
</script>
<style type="text/css">
.modal-top {
    background: url("../resources/images/stripe.png") repeat-x scroll center top transparent;
    border-radius: 5px 5px 0 0;
    padding:1px 5px 5px;
}
.modal-top h1 {
	color: orange;
    font-size: 15px;
}
.email-text{padding: 0px 10px;}
#openEmailModal input[type="text"], #openEmailModal input[type="email"], #openEmailModal input[type="password"] {
	width: 450px;
}
#elm1_tbl{width: 100% !important;}
#openModal h2,#openEmailModal h2{
	font-size:12px;
	background:url(../resources/images/stripe.png) repeat-x top center transparent;
	border-radius: 10px 10px 0 0;
	-mox-border-radius: 10px 10px 0 0;
	-webkit-border-radius: 10px 10px 0 0;
	-o-border-radius: 10px 10px 0 0;
	padding:10px;
}
#openModal .btn_icon,#openEmailModal .btn_icon{
	font-weight:bold;
	cursor:pointer;
}
#openModal a,#openEmailModal a{text-decoration:none}
.modalDialog,.emailModalDialog {
	position: fixed;
	font-family: Arial, Helvetica, sans-serif;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: rgba(212,212,212,0.5);
	z-index: 99999;
	opacity:0;
	-webkit-transition: opacity 400ms ease-in;
	-moz-transition: opacity 400ms ease-in;
	transition: opacity 400ms ease-in;
	pointer-events: none;
}
.modalDialog:target,.emailModalDialog:target {
	opacity:1;
	pointer-events: auto;
}

.modalDialog > div,.emailModalDialog > div {
	width: 430px;
	position: relative;
	margin: 10% auto;
	padding: 0px 0px 13px 0px;
	border-radius: 10px;
	background: #fff;
}
.emailModalDialog > div{min-width: 720px}
.close {
	background: #606061;
	line-height: 25px;
	position: absolute;
	right: -12px;
	text-align: center;
	top: -10px;
	width: 24px;
	text-decoration: none;
	font-weight: bold;
	-webkit-border-radius: 12px;
	-moz-border-radius: 12px;
	border-radius: 12px;
	-moz-box-shadow: 1px 1px 3px #000;
	-webkit-box-shadow: 1px 1px 3px #000;
	box-shadow: 1px 1px 3px #000;
}
.close:hover { background: #00d9ff; }
-webkit-transition: opacity 400ms ease-in;
-moz-transition: opacity 400ms ease-in;
transition: opacity 400ms ease-in;
.mailField {
    height: 21px;
    margin-bottom: 3px;
}
#sendMail{float: none;width: auto;}
.loading-point img{width:40px;height: 40px;}
.do-load{
	margin: 0px 0 0 110px;
}
</style>
<div id="openEmailModal" class="emailModalDialog">
	
	<div>	<a href="#close" title="Close" class="close">X</a>
		<div class="modal-top">
		</div>
		<div class="email-text">
			<table cellpadding="0" cellspacing="0" width="700" border="0">
				<tr>
					<td><input type="hidden" name="tableName" class="modal-ref-table" value=""/></td>
					<td><input type="hidden" name="mailRefTableId" class="customer-id" value="" /></td>
				</tr>
				<tr>
					<td class="label" width="50"><strong>To</strong></td>
					<td width="20">:</td>
					<td>
					<input class="mailField" id="customer-mail" type="text" name="mailTo"  value="" size="80" readonly="readonly"/>
					<br/>
					</small></em></td>
				</tr>
				<br />
				<tr class="cc-tr">
				<td class="label" width="50"><strong>CC</strong></td>
					<td width="20">:</td>
					<td><input class="mailField mailcc"  type="text" name="cc" value="" size="80" placeholder="For multiple email plz use comma(,)"/><br/>
				</td>
				</tr>
				<tr>
					<td class="label" width="50"><strong>Subject</strong></td>
					<td width="20">:</td>
					<td>
						<input class="mailField subject" type="text" name="subject" value="Greetings From REVE Systems" size="80" required=""/>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td></td>
				</tr>
				<tr>
					<td class="label" width="100%"><strong>Mail Content</strong></td>
					<td colspan="3">
						<input type="radio" name="mail-content-type" value="1" accesskey="email-default-body" class="mail-content-type" checked="checked"/>
						Basic Text&nbsp;&nbsp;
						<input type="radio" name="mail-content-type" value="2" accesskey="email-dialer-body" class="mail-content-type" style="visibility: hidden"/>
						&nbsp;&nbsp;
						<input type="radio" name="mail-content-type" value="3" accesskey="email-switch-body" class="mail-content-type" style="visibility: hidden"/>
						&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td></td>
				</tr>
			</table>
		
		<div class="mail-contents">
				Dear X,<em><small class=""><br/>(Please dont add recipient name in email body,system will take care of it)</small><br/>
				<textarea class="mail-body-text tinymce" id="elm1"  name="content"></textarea>
			</div>
			<br/>
				<div class="loading-point"></div>
				<input class="update-btn action-btn" id="sendMail"  type="submit" value="Send Mail"/>	
				<a href="#closeModal">
					<input class="update-btn action-btn" type="button" value="Cancel"/>
				</a>
		</div>
	</div>
</div>
