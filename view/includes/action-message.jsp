<%@page import="dev.mashfiq.util.Messages"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.util.ActionMessages"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%
	String msgClass = "errorMessage";
	String msg = null;
	Object msgObj = session
			.getAttribute(ApplicationConstant.ACTION_MESSAGE);
	session.removeAttribute(ApplicationConstant.ACTION_MESSAGE);
	ActionMessages actionMessage = null;
	dev.mashfiq.util.Messages message = null;
	ArrayList<dev.mashfiq.util.Messages> amlist = null;
	if (msgObj != null) {
		if (msgObj instanceof ActionMessages) {
			actionMessage = (ActionMessages) msgObj;
			msg = actionMessage.getMsg();
			msgClass = actionMessage.getType();
		} else if (msgObj instanceof dev.mashfiq.util.Messages) {
			message = (Messages) msgObj;
			msg = message.getMsg();
			msgClass = message.getType();
		} else if (msgObj instanceof ArrayList) {
			amlist = (ArrayList<Messages>) msgObj;
			msg = "";
			for (Messages m : amlist) {
				msg += m.getMsg() + "<br/>";
				msgClass = m.getType();
			}
		} else {
			msg = msgObj + "";
		}
		if (msg != null && msg.length() > 0) {
%>
<div class="<%=msgClass%>"><%=msg%></div>
<%
	}
	}
%>