
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%
	if (dto.getRcDTO() != null) {
%>
<div class="panel panel-info">
	<div class="panel panel-heading">
		<a href="#clientInfo" data-toggle="collapse" data-parent="#accordion">Client
			Information</a>
	</div>
	<div id="clientInfo" class="panel-collapse collapse">
		<div class="panel panel-body">
			<div class="table-responsive">
				<table class="table table-borderless table-hover">
					<tr>
						<td class="col-label">Name</td>
						<td class="col-colon">:</td>
						<td class="nameField"><%=dto.getRcDTO().getRcClientName()%></td>
						<td class="col-label">Email ID</td>
						<td class="col-colon">:</td>
						<td><span id="emailAddress" class="emailCheck"><%=dto.getRcDTO().getRcEmail()%></span></td>
					</tr>
					<tr>
						<td class="col-label">Cell Phone</td>
						<td class="col-colon">:</td>
						<td><%=dto.getRcDTO().getRcCellPhone()%></td>
						<td class="col-label">Country</td>
						<td class="col-colon">:</td>
						<td class="nameField"><%=dto.getRcDTO().getRcCountry()%></td>
					</tr>
					<tr>
						<td class="col-label">Company Name</td>
						<td class="col-colon">:</td>
						<td class="nameField"><%=v.checkString(dto.getRcDTO().getRcCompanyName(),
						"N/A")%></td>
						<td class="col-label">Account Manager</td>
						<td class="col-colon">:</td>
						<td class="capitalize"><%=RimsUsersRepository.getInstance(false).getUsrUserIDByUsrID(dto
						.getRcDTO().getRcAccountManagerID())%></td>
					</tr>
					<tr>
						<td class="col-label">Registration Date</td>
						<td class="col-colon">:</td>
						<td class="nameField"><%=dto.getRcDTO().getRcTimeOfReg()%></td>
						<td class="col-label" colspan="3">
							<% if(rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PORTFOLIO_PERMISSION) == Permissions.LEVEL_SIX) { %>
							<a class="btn btn-info"
							href="<%=ApplicationConstant.getBaseURL(session)%>reve-clients/get-reve-clients-portfolio.html?rcClientID=<%=dto.getRcDTO().getRcClientID()%>">View
								Portfolio</a>
							<%} %>
						</td>
						<td class="col-label">Is Premium Client</td>
						<td class="col-colon">:</td>
						<td class="capitalize"><%=dto.getRcDTO().getRcPremiumClient()%></td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<%
	}
%>