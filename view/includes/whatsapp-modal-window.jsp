<script type="text/javascript">

	$(document).ready(function() {
		try {
			$('#to').val($('.contact_no').val());
			$('#send').click(function() {
				var tableName=$('.ref_table_name').attr('accesskey');
				var content=$('#content').val();
				var to=$('#to').val();
				var refTableId=$('.ref_table_id').val();
				if(tableName!=""){
					if(confirm("Are you confirm?")){
							$('.loading-point').html("").html('<img height="16" width="16" src="../resources/images/loading.gif"/>');
							$("#send").attr("disabled",true).css('opacity',.2);
							$.ajax({url: BASE_URL + "resources/ajax-files/send-whatsapp-message.jsp",data:({tId:refTableId,tName:tableName,content:content,to: to}),type:"POST",dataType:"JSON",success:function(data)
								{
									$('.loading-point').html("");
									$('#openWhatsappModal').css('opacity',0);
									$('.modalDialog:target').css('pointer-events','none');
									$("#sendMail").removeAttr("disabled").css('opacity',0);
									alert(data.msg);
								}
							,error:function(xhr,textStatus,error)
							{
							    alert(xhr + textStatus + error);
							}
							});
					}else{
						return false;
					}
				} else{
					alert("Unexpected error! Please close the modal and try again");
				}
			})
		} catch (err) {
			alert(err);
		}
	});
	
</script>
<style type="text/css">
.modal-top {
	background: url("../resources/images/stripe.png") repeat-x scroll center
		top transparent;
	border-radius: 5px 5px 0 0;
	padding: 1px 5px 5px;
	color: white;
	font-size: 15px;
}

.email-text {
	padding: 20px 10px;
}

#openModal a,#openWhatsappModal a {
	text-decoration: none
}

.modalDialog {
	position: fixed;
	font-family: Arial, Helvetica, sans-serif;
	top: 0;
	right: 0;
	bottom: 0;
	left: 0;
	background: rgba(212, 212, 212, 0.5);
	z-index: 99999;
	opacity: 0;
	-webkit-transition: opacity 400ms ease-in;
	-moz-transition: opacity 400ms ease-in;
	transition: opacity 400ms ease-in;
	pointer-events: none;
}

.modalDialog:target {
	opacity: 1;
	pointer-events: auto;
}

.modalDialog>div {
	width: 430px;
	position: relative;
	margin: 10% auto;
	padding: 0px 0px 0px 0px;
	border-radius: 10px;
	background: #fff;
}

.close {
	background: #606061;
	line-height: 25px;
	position: absolute;
	right: -12px;
	text-align: center;
	top: -10px;
	width: 24px;
	text-decoration: none;
	font-weight: bold;
	-webkit-border-radius: 12px;
	-moz-border-radius: 12px;
	border-radius: 12px;
	-moz-box-shadow: 1px 1px 3px #000;
	-webkit-box-shadow: 1px 1px 3px #000;
	box-shadow: 1px 1px 3px #000;
}

.close:hover {
	background: #00d9ff;
}

-webkit-transition: opacity 400ms ease-in ;
-moz-transition: opacity 400ms ease-in ;
transition: opacity 400ms ease-in ;
.mailField {
	height: 21px;
	margin-bottom: 3px;
}

#sendMail {
	float: none;
	width: auto;
}

.loading-point img {
	width: 40px;
	height: 40px;
}
</style>
<div id="openWhatsappModal" class="modalDialog">
<input type="hidden" name="tableName" class="modal-ref-table" value="" />
<input type="hidden" name="mailRefTableId" class="customer-id" value="" />
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<div class="modal-top">Whatsapp Messaging</div>
		<div class="email-text">
			<table>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td><strong>To</strong></td>
					<td>:</td>
					<td><input id="to" readonly="readonly" /></td>
				</tr>
				<tr>
					<td>Message</td>
					<td>:</td>
					<td><textarea id="content" class="form-control" name="content" cols="50" rows="5"></textarea></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2"></td>
					<td><input id="send" type="button" value="Send Message" /></td>
				</tr>
			</table>
			<div class="loading-point"></div>
		</div>
	</div>
</div>
