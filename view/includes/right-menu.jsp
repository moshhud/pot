
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<script>
jQuery(function(){
	jQuery('#camera_wrap_3').camera({
		pagination: false,
		time : 3000,
		hover: true
	});
});
</script>
<style>
#upcoming-events{
	background: white;
}
#upcoming-events .title {
	padding: 5px;
	text-align: center;
	font-weight: bold;
	color: gray;
}
</style>
<div style="float: right;">
	<a href="<%=ApplicationConstant.getBaseURL(session)%>dashboard.html">
		<img
		src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/cms_btn.jpg"
		width="330px" alt="CMS">
	</a>
	<div id="upcoming-events">
		<h1 class="title">Upcoming Events</h1>
		<div id="events" style="margin: 10px 0">
			<div class="fluid_container camera_wrap camera_azure_skin"
				id="camera_wrap_3">
				<%-- <%
					int i = 0;
					for (CmEventsDTO dto : CmEventsRepository.getInstance(false)
							.getAll()) {
						if (dto != null) {
							if (dto.getFileFileName() != null
									&& dto.getFileFileName().length() > 0) {
								i++;
								if(i > 3) {
									break;
								}
				%>
				<div
					data-src="https://www.revesoft.com/contentmanagement/asset/events/<%=dto.getFileFileName()%>"></div>
				<%
							}
						}
					}
				%> --%>
			</div>
		</div>
	</div>
</div>
