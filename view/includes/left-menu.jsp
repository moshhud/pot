<%@page import="dev.mashfiq.util.DateAndTimeHelper"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>

<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/metisMenu.css" type="text/css" rel="stylesheet" />
<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/font-awesome-4.1.0/css/font-awesome.min.css" type="text/css" rel="stylesheet" />

<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/metisMenu.min.js" type="text/javascript"></script>
<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/sideMenu.js" type="text/javascript"></script>

<%
	RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
	DateAndTimeHelper dth = new DateAndTimeHelper();
%>
<div class="navbar-default sidebar" role="leftnavigation" >
	<div class="sidebar-nav navbar-collapse leftnavbar-collapse">
		<%
		    
			
			
				if(rimsUsersDTO.getPermissionLevelByModuleId(Permissions.REPORT_DASHBOARD) == 6) { %>
					<div>
						<a class="btn btn-dashboard block" href="<%=ApplicationConstant.getBaseURL(session)%>home.html">Dashboard </a>
					</div>
					<br/>
				<%}%>
			
				<ul class="nav" id="side-menu">
				 
				  <%if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MASTER_CREATION) == Permissions.LEVEL_SIX) { %>
					<li class="list-group-item"><a class="active menuitem submenuheader"  href="#">User & Role<span class="fa arrow"></span></a>
						<ul class="nav nav-second-level">							
							<%
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_THREE) { %>
								<li><a href="<%=ApplicationConstant.getBaseURL(session)%>users/new-users.html">Add User</a></li>
							<%}
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_TWO) { %>
								<li><a href="<%=ApplicationConstant.getBaseURL(session)%>users/search-users.html">Search Users</a></li>
							<%}
							if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_TWO) {%>	
							    <li><a href="<%=ApplicationConstant.getBaseURL(session)%>roles/search-roles.html">Search Roles</a></li>
							<%} %>						
							
				         </ul> <!-- /.nav-second-level -->
				     </li>
			      <%}%>
				  
				  
			<%
				if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MASTER_SEARCH) == Permissions.LEVEL_SIX) {
			%>
			<li class="list-group-item"><a href="#" class="menuitem submenuheader"> Products<span class="fa arrow"></span></a>
				<ul class="nav nav-second-level">
					<%if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_THREE) { %>
						<li><a href="<%=ApplicationConstant.getBaseURL(session)%>mdproductlist/new-mdproductlist.html">Add Products</a></li>
					<%}
					if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MDPRODUCTLIST) >= Permissions.LEVEL_TWO) { %>
						<li><a href="<%=ApplicationConstant.getBaseURL(session)%>mdproductlist/search-mdproductlist.html">Search Products</a></li>
					<%}
					%>
					
				  </ul> <!-- /.nav-second-level -->
				</li>
			<%
				}
			%>
		
		<%
					if (rimsUsersDTO
								.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER) >= Permissions.LEVEL_TWO) {
				%>
			<li class="list-group-item"><a href="#" class="menuitem submenuheader">Orders<span class="fa arrow"></span></a>
			 <ul class="nav nav-second-level">
			    <%
			    	if (rimsUsersDTO
			    				.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_THREE) {
			    %>
				<li><a
					href="<%=ApplicationConstant.getBaseURL(session)%>order-list/new-order-details-order-list.html">Add order</a></li>
				<%
					}
					if(rimsUsersDTO
								.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER) >= Permissions.LEVEL_TWO){
				%>				
				<li><a
					href="<%=ApplicationConstant.getBaseURL(session)%>order-list/search-order-details-order-list.html">Search Order</a></li>
				
			    <%
					}
			    %>
			 </ul> 
		    </li>	
					
		   <%	
			}		   
		   %>
		
			
		<%
		if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_ADMIN) == Permissions.LEVEL_SIX) {
		%>
		<li class="list-group-item"><a href="#"
			class="menuitem submenuheader">WEB Admin<span class="fa arrow"></span></a>
			<ul class="nav nav-second-level submenu">
				<%
					if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_THREE) {
				%>
				<li><a href="<%=ApplicationConstant.getBaseURL(session)%>mashup-data/new-mashup-data.html">Add Mashup Data</a></li>
				<%
				     }
					if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.MASHUP_DATA) >= Permissions.LEVEL_TWO) {
				%>
				<li><a href="<%=ApplicationConstant.getBaseURL(session)%>mashup-data/search-mashup-data.html">Search Mashup Data</a></li>
				<%
					}						
					if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_THREE) {
				%>
				<li><a href="<%=ApplicationConstant.getBaseURL(session)%>modules/new-module.html">Add Modules</a></li>
				<li><a href="<%=ApplicationConstant.getBaseURL(session)%>modules/search-modules.html">Search Modules</a></li>				
				<%
					}
					if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.PERMISSIONS) >= Permissions.LEVEL_FOUR) {
						%>
						<li><a href="<%=ApplicationConstant.getBaseURL(session)%>permissions/new-acl-permission.html">ACL Permission</a></li>
						<%
					}
				%>
			</ul> <!-- /.nav-second-level --></li>
		<%
			}
			%>
			</ul>				
		   
		 
	</div>
	<!-- /.sidebar-collapse -->
</div>
<%-- <script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/notifications.js"></script> --%>