<div class="modal fade" id="pidateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Installment Date Modification</h4>
      </div>
      <div class="modal-body">
        <form role="form">
          <div class="form-group">
            <label for="installment-date" class="control-label">New Installment Date</label>
            <input type="text" class="datepicker-modal form-control" id="installment-date">
            <input type="hidden" id="pi-id">
            <input type="hidden" id="invc-id">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Comment:</label>
            <textarea class="form-control" id="comment-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="save-btn">Save</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.call-modal').click(function(){
		var obj =  $(this);
		$('#pi-id').val(obj.attr('id'));
		$('#invc-id').val(obj.attr('title'));
		$('#pidateModal').modal('show');
		$(".datepicker-modal").datepicker({
			minDate : obj.text(),
			maxDate : '+5D',
			dateFormat : 'dd/mm/yy',
			showAnim : 'blind'
		});
	});
	$('#save-btn').click(function(){
		var newDt = $('#installment-date').val();
		var cmt = $('#comment-text').val();
		var piID = $('#pi-id').val();
		var invcID = $('#invc-id').val();
		if(newDt.length> 0 && cmt.length > 0 && piID.length > 0)
		{
			$.ajax({
				url : BASE_URL
					+ 'resources/ajax-files/payment-installment-date-modification.jsp',
				dataType : 'JSON',
				type : 'POST',
				data : ({
					newDt : newDt,
					cmt : cmt,
					piID : piID,
					invcID : invcID
				}),
				success : function(
						data) {
					
					if (data.status == 'success') {
						alert(data.msg + ' Refresh the page to see the change');
						$(
								'#pidateModal')
								.modal(
										'hide');
					} else {
						alert(data.msg);
					}
				},
				error : function(
						x, h, e) {
					alert(e);
				}
			});
		}
		else
		{
			alert("Enter input fields first!");
		}
	});
});
</script>