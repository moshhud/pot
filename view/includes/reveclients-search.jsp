<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<script type="text/javascript"
	src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/reveclients-search.js"></script>
<div id="accordion" class="panel-group">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h1 class="panel-title">
				<a href="#clientInfo" data-toggle="collapse"
					data-parent="#accordion">Search Client</a>
			</h1>
		</div>
		<div id="clientInfo" class="collapse panel-collapse in">
			<div class="panel-body">
				<div class="table-responsive">
					<table class="table table-borderless">
						<tr>
<!-- 							<td><input type="radio" name="searchType" id="clientID" -->
<!-- 								value="rcClientID" checked="checked" /><label for="clientID">&nbsp; -->
<!-- 									Client ID</label></td> -->
							<td><input type="radio" name="searchType" id="email"
								value="rcEmail" /><label for="email">&nbsp; Email</label></td>
							<td><input type="radio" name="searchType" id="companyName"
								value="rcCompanyName" /><label for="companyName">&nbsp;
									Company Name</label></td>
							<td><input type="radio" name="searchType" id="operatorCode"
								value="operator_code" /><label for="operatorCode">&nbsp;
									Operator Code</label></td>
							<td><input type="radio" name="searchType" id="referenceNo"
								value="siReference" /><label for="referenceNo">&nbsp;
									Reference No</label></td>
							<td><input name="searchValue" class="form-control"
								placeholder="Search Value" /></td>
							<td><input type="button" id="search" type="button"
								value="Search" class="btn btn-info" /></td>
						</tr>
					</table>
				</div>
				<div class="panel panel-info">
					<div class="panel-heading">Client Details</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-borderless">
								<tr>
									<td class="col-label">Client ID</td>
									<td class="col-colon">:</td>
									<td><span id="rcClientID"></span> <input type="hidden"
										id="invcRcClientID" name="invcRcClientID" readonly="readonly" />
										<input type="hidden" id="hsRcClientID" name="hsRcClientID"
										readonly="readonly" /></td>
									<td class="col-label">Client Name</td>
									<td class="col-colon">:</td>
									<td><span id="rcClientName"></span></td>
								</tr>
								<tr>
									<td class="col-label">Email</td>
									<td class="col-colon">:</td>
									<td><span id="rcEmail"></span></td>
									<td class="col-label">Contact No</td>
									<td class="col-colon">:</td>
									<td><span id="rcCellPhone"></span></td>
								</tr>
								<tr>
									<td class="col-label">Company Name</td>
									<td class="col-colon">:</td>
									<td><span id="rcCompanyName"></span></td>
									<td class="col-label capitalize">Account Manager</td>
									<td class="col-colon">:</td>
									<td><span id="rcAccountManagerName"></span> <input
										type="hidden" id="rcAccountManagerID"
										name="rcAccountManagerID">
										<input
										type="hidden" id="hsAccountManagerID"
										name="hsAccountManagerID"></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>