<%@page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<meta http-equiv="Cache-control" content="public">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/bootstrap.min.css"type="text/css" rel="stylesheet" />
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/bootstrap-responsive.css"type="text/css" rel="stylesheet" />
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/cms-style.css"rel="stylesheet" media="screen" type="text/css" />
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/magazine-style.css"type="text/css" rel="stylesheet" />
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery.min.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/bootstrap.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/magazine-scripts.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/ckeditor/ckeditor.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery.blockUI.js" type="text/javascript"></script>
		<script type="text/javascript">
			var jsBASE_URL = "<%=ApplicationConstant.getBaseURL(session)%>";
		</script>
		<link rel="icon" href="<%=ApplicationConstant.getBaseURL(session)%>resources/images/favicon.jpg">
	</head>
	<body>
		<%
			try {
		%>
		<tiles:insertAttribute name="header" />
		<div class="container-fluid main-content">
			<div class="container mid-content">
				<div class="row">
					<div class="col-lg-8"><tiles:insertAttribute name="contentLeft" /></div>
					<div class="col-lg-4"><tiles:insertAttribute name="contentRight" /></div>
				</div>
			</div>
		</div>
		<%
			} catch (RuntimeException e) {
				Logger.getLogger("base-template").fatal("RuntimeException", e);
			}
		%>
	</body>
</html>