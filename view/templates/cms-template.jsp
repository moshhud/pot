<%@page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width ,initial-scale=1.0" />
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<meta http-equiv="Cache-control" content="public">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/bootstrap.min.css"type="text/css" rel="stylesheet" />
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/cms-responsive-style.css" rel="stylesheet"media="screen" type="text/css" />
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/jquery-ui.css" rel="stylesheet" media="screen" type="text/css"/>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery-1.11.0.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/bootstrap.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/bootstrap-filestyle.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/page-resize.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/input-validation.js" type="text/javascript" ></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery-ui.js" type="text/javascript" ></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/common-script.js" type="text/javascript" ></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery.blockUI.js" type="text/javascript"></script>
		<script type="text/javascript">
			var BASE_URL = "<%=ApplicationConstant.getBaseURL(session)%>";
			
		</script>
		<link rel="icon" href="<%=ApplicationConstant.getBaseURL(session)%>resources/images/favicon.jpg">
	</head>
	<body>
		<%
			try {
		%>
		<tiles:insertAttribute name="header" />
		<div class="container-fluid" id="cmsPage">
			<div class="col-lg-12 content-fluid">
				<div class="row" id="page-content">
					<div class="col-lg-2 col-md-3 col-sm-4">
						<tiles:insertAttribute name="contentLeft" />
					</div>
					<div class="col-lg-10 col-md-9 col-sm-8">	
						<div class="panel panel-default">
							<div class="panel-heading div-page-title"><tiles:getAsString name="title" ignore="true" /></div>
							<div class="panel-body">
								<%@ include file="/view/includes/action-message.jsp" %>
								<tiles:insertAttribute name="contentRight" />
							</div>
						</div>		
					</div>
				</div>
			</div>
		</div>
		<tiles:insertAttribute name="footer" />
		<%
			} catch (RuntimeException e) {
				Logger.getLogger("cms-template").fatal("RuntimeException", e);
			}
		%>
	</body>
</html>