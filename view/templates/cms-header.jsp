
<%@page import="java.util.HashMap"%>
<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%
	RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
			.getAttribute(ApplicationConstant.LOGIN_INFO);
%>
<input type="hidden" id="usr" value="<%=rimsUsersDTO.getUsrID()%>" />
<%@ include file="/view/common/notifications.jsp" %>
<nav class="navbar-wrapper navbar-default navbar-static-top nav headerTop" role="navigation">
	<!--Top-bar Start-->
	<div class="container-fluid ">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle navtoggle"
				data-toggle="collapse" data-target=".leftnavbar-collapse" >
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> 
					<span class="icon-bar">
					</span> <span
					class="icon-bar"></span>
			</button>
			<div class="row">
			<img id="reveLogo" class="img-responsive img-rounded center-block col-lg-4 col-md-4 col-sm-4"
				src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/reve_logo.png"/>
			</div>
			<div class="row">			 
				 <img id="imsLogo" class="img-responsive img-rounded  col-lg-12 col-md-12 col-sm-12"
				src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/title_txt.png" />
			</div>	
		</div>
		
	</div>
	<!--End Top-menubar-->
</nav>
<!--End Navbar-->

<!--Top-menubar Start-->
<nav class="navbar-wrapper navbar-default navbar-static-top nav " role="navigation" id="navMenu">
	<div class="container pull-right">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"data-target=".top2navbar-collapse">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse top2navbar-collapse">
			<ul class="nav navbar-nav navbar-right right-nav">
				<%-- <li>
					<a class="btn btn-success apk-download-tracker" style="color: white" href="<%=ApplicationConstant.getBaseURL(session) %>resources/common/rims-apk.zip">Download RIMS Android APP (Beta Version)</a>
				</li> --%>
				<li><a
					href="<%=ApplicationConstant.getBaseURL(session)%>home.html">HOME</a></li>
				
				<li><a
					href="<%=ApplicationConstant.getBaseURL(session)%>logout.html">LOGOUT</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!--End Top-menubar-->
</nav>
<!--End Navbar-->
<script type="text/javascript">

	/* 
	//this function is used for expanding selected menu after loading a new page
	
	$(function () {
	    setNavigation();
	});
	
	function setNavigation() {
	    var path = window.location;
	    
	    $(".nav a").each(function () {
	        var href = $(this).attr('href');
	        if (path == href) {
	            $(this).closest('ul').addClass('active in');
	        }
	    });
	} */

	$(document).ready(function(){
		$('.apk-download-tracker').click(function(e){
			$.ajax({
				url: BASE_URL + 'resources/ajax-files/apk-download-tracker.jsp'
			})
		})
	});



</script>
