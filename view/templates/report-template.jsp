<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="org.apache.log4j.Logger"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width ,initial-scale=1.0" />
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<meta http-equiv="Cache-control" content="public">
		<link href='https://fonts.googleapis.com/css?family=Open+Sans'rel='stylesheet' type='text/css'>
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/bootstrap.min.css"type="text/css" rel="stylesheet" />
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/jquery-ui.css" rel="stylesheet" media="screen" type="text/css"/>
		<link href="<%=ApplicationConstant.getBaseURL(session)%>resources/css/cms-responsive-style.css" rel="stylesheet"media="screen" type="text/css" />
		<script src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/jquery-1.11.0.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/left-menu-script.js" type="text/javascript" ></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/bootstrap.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/bootstrap-filestyle.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/page-resize.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery-ui.js" type="text/javascript" ></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/tableExport.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/jquery.base64.js" type="text/javascript"></script>
		<script src="<%=ApplicationConstant.getBaseURL(session)%>resources/scripts/common-script.js" type="text/javascript" ></script>
		<script type="text/javascript">
			var BASE_URL = "<%=ApplicationConstant.getBaseURL(session)%>";
		</script>
	</head>
	<body>
		<%
			try {
		%>
		<tiles:insertAttribute name="header" />
		<div class="container-fluid">
			<div class="col-lg-12 content-fluid">
				<div class="row" id="page-content">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading div-page-title"><tiles:getAsString name="title" ignore="true" /></div>
							<div class="panel-body">
								<%@ include file="/view/includes/action-message.jsp" %>
								<tiles:insertAttribute name="searchPanel" />
								<tiles:insertAttribute name="searchData" />
							</div>			
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="footer" class="centered"><tiles:insertAttribute name="footer" /></div>
		<%
			} catch (RuntimeException e) {
				Logger.getLogger("cms-template").fatal("RuntimeException", e);
			}
		%>
	</body>
</html>