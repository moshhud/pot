<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<footer>
	<div class="footer-area">
		<div class="text-container">
			<font>Copyright &copy;<%=new SimpleDateFormat("yyyy").format(new Date())%>, Project Management. All rights reserved.</font>
		</div>
	</div>
</footer>