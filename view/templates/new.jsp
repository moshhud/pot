<%@page import="com.reveantivirus.products.Products"%>
<%@page import="java.util.List"%>
<%@page import="com.reveantivirus.products.ProductsRepository"%>
<%@page import="dev.mashfiq.util.StringHelper"%>
<%@page import="dev.mashfiq.common.SpringApplicationContext"%>
<%@page import="dev.mashfiq.common.CommonDTO"%>
<%@page import="dev.mashfiq.factory.DTOFactory"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.annotations.FieldDefination"%>
<%@page import="java.lang.reflect.Field"%>
<%@page import="javax.persistence.Column"%>
<%@page import="java.lang.annotation.Annotation"%>
<%@page import="dev.mashfiq.mashupData.MashupData"%>
<%@page import="dev.mashfiq.common.MasterController"%>
<%@page import="dev.mashfiq.util.Modules"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@ include file="../common/ajax-form-submit.jsp"%>
<%
	String module = session
	.getAttribute(ApplicationConstant.MODULE_NAME) + "";
	DTOFactory dtoFactory = (DTOFactory) SpringApplicationContext
	.getBean("dtoFactory");
	CommonDTO dto = dtoFactory.getDTO(module);
	Class dtoClass = dto.getClass();
	Annotation[] annotations = dtoClass.getAnnotations();
	FieldDefination fieldDefination = null;
	Field[] fields = dtoClass.getDeclaredFields();
	StringHelper stringHelper = null;
	UIHelper uiHelper = null;
	MashupDataRepository mudRepo = null;
%>
<div class="panel panel-default">
	<form id="ajax-form" method="post" class="form-horizontal"
		action="<%=ApplicationConstant.getBaseURL(session) + "ajax/"
					+ module + "/" + MasterController.ADD%>.htm">
		<div class="panel-body">
			<div class="col-lg-12">
				<div class="err-msg err-general"></div>
				<%
					if (fields != null && fields.length > 0) {
										mudRepo = MashupDataRepository.getInstance(false);
										uiHelper = new UIHelper();
										stringHelper = new StringHelper();
										for (Field field : fields) {
											annotations = field.getAnnotations();
											for (Annotation annotation : annotations) {
												if (annotation instanceof FieldDefination) {
													fieldDefination = (FieldDefination) annotation;
													if (fieldDefination.useFor().contains("input")
															|| fieldDefination.useFor().contains("all")) {
														if ("hidden".equalsIgnoreCase(fieldDefination
																.type())) {
				%>
				<input name="<%=field.getName()%>"
					id="<%=stringHelper
											.getIdFromFieldDefination(
													fieldDefination, field)%>"
					type="<%=fieldDefination.type()%>"
					class="form-control <%=field.getName()%>"
					placeholder="<%=fieldDefination.placeHolder()%>"
					value="<%=fieldDefination.defaultValue()%>"
					<%=fieldDefination.required() ? "required"
											: ""%>
					<%=fieldDefination.readonly() ? "readonly"
											: ""%> />
				<%
					continue;
										}
				%>
				<div class="form-group col-lg-4 col-md-6 <%=field.getName()%>">
					<label class="control-label"
						for="<%=stringHelper
										.getIdFromFieldDefination(
												fieldDefination, field)%>"><%=stringHelper
										.getLabelFromFieldDefination(
												fieldDefination, field)%>
						<%
							if (fieldDefination.required()) {
						%><i class="fa fa-asterisk mandatory"></i> <%
 	}
 %> </label>
					<%
						if ("text".equalsIgnoreCase(fieldDefination
													.type())
													|| "number"
															.equalsIgnoreCase(fieldDefination
																	.type())
													|| "email"
															.equalsIgnoreCase(fieldDefination
																	.type())
													|| "password"
															.equalsIgnoreCase(fieldDefination
																	.type())
													|| "file"
															.equalsIgnoreCase(fieldDefination
																	.type())) {
					%>
					<input name="<%=field.getName()%>"
						id="<%=stringHelper
											.getIdFromFieldDefination(
													fieldDefination, field)%>"
						type="<%=fieldDefination.type()%>"
						class="form-control <%=field.getName()%> <%=fieldDefination.cssClass() %>"
						placeholder="<%=fieldDefination.placeHolder()%>"
						value="<%=fieldDefination.defaultValue()%>"
						<%=fieldDefination.required() ? "required"
											: ""%>
						<%=fieldDefination.readonly() ? "readonly"
											: ""%> />
					<%
						} else if ("select"
													.equalsIgnoreCase(fieldDefination
															.type())) {
					%>
					<select name="<%=field.getName()%>"
						id="<%=stringHelper
											.getIdFromFieldDefination(
													fieldDefination, field)%>"
						class="form-control <%=field.getName()%>"
						<%=fieldDefination.required() ? "required"
											: ""%>
						<%=fieldDefination.readonly() ? "readonly"
											: ""%>>
						<%= uiHelper.getSelect(fieldDefination, null)%>
					</select>
					<%
						} else if ("textarea"
													.equalsIgnoreCase(fieldDefination
															.type())) {
					%>
					<textarea name="<%=field.getName()%>"
						id="<%=stringHelper
											.getIdFromFieldDefination(
													fieldDefination, field)%>"
						class="form-control <%=field.getName()%>"
						placeholder="<%=fieldDefination.placeHolder()%>"
						<%=fieldDefination.required() ? "required"
											: ""%>
						<%=fieldDefination.readonly() ? "readonly"
											: ""%>><%=fieldDefination.defaultValue()%></textarea>
					<%
						}
					%>
					<div class="control-label err-msg err-<%=field.getName()%>"></div>
				</div>
				<%
					}
								}
							}
						}
					}
				%>
			</div>
		</div>
		<div class="panel-footer">
			<input type="submit" class="btn btn-default submit" value="Submit" />
			<i class="fa fa-asterisk mandatory"></i> fields are mandatory
		</div>
	</form>
</div>