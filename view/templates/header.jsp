<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%
	String pageN = request.getParameter("page") == null ? "0" : request
			.getParameter("page");
%>
<nav class="navbar-wrapper navbar-default navbar-static-top nav"
	role="navigation">
	<!--Top-bar Start-->
	<div class="container-fluid top-bar-bg">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle navtoggle"
					data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
			</div>
			<div class="collapse navbar-collapse">
				<!-- Collect the nav links, forms, and other content for toggling -->
				<ul class="nav navbar-nav navbar-right">
					<li><a class="li-fb" href="#"><span><img
								src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/fb-icon.png"
								alt=""></span></a></li>
					<li><a class="li-twt" href="#"><span><img
								src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/twitter.png"
								alt=""></span></a></li>
					<li><a class="li-linkd" href="#"><span><img
								src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/linkedin.png"
								alt=""></span></a></li>
					<li><a class="li-utube" href="#"><span><img
								src="<%=ApplicationConstant.getBaseURL(session)%>resources/images/utube.png"
								alt=""></span></a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
	</div>
	<!--End Top-menubar-->
</nav>
<!--End Navbar-->