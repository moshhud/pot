<%@page contentType="text/html; charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<%@page import="dev.mashfiq.util.StringHelper"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%
	String url = request.getRequestURL().toString();
	if(url.contains("potracker.revesoft.com")) {
		url = new StringHelper().getSubString(url, url.lastIndexOf("potracker.revesoft.com")+ 22, "/");
	} else {
		url = new StringHelper().getSubString(url, url.lastIndexOf("pot")+ 4, "");
	}
	System.out.println(url);
	session.setAttribute(ApplicationConstant.BASEURL, url);
%>
<!DOCTYPE html>
<html>
	<head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width ,initial-scale=1.0" />
		
		<title><tiles:insertAttribute name="title" ignore="true" /></title>
		<meta http-equiv="Cache-control" content="public">
		<link href="<%=ApplicationConstant.getBaseURL(session) %>resources/css/bootstrap.css" type="text/css" rel="stylesheet" media="screen" />
		<link href="<%=ApplicationConstant.getBaseURL(session) %>resources/css/cms-responsive-style.css" type="text/css" rel="stylesheet" media="screen" />
		<script type="text/javascript" src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/jquery.min.js"></script>
		<script type="text/javascript" src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/bootstrap.js"></script>
		<script type="text/javascript" src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/bootstrap.min.js"></script>
<%-- 		<script type="text/javascript" src="<%=ApplicationConstant.getBaseURL(session) %>resources/scripts/bootstrap-filestyle.js"></script>  --%>
		<tiles:insertAttribute name="additionalHeader" ignore="true" />
		<link rel="icon" href="resources/images/favicon.jpg">
		<script type="text/javascript"> //<![CDATA[ 
			var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.comodo.com/" : "http://www.trustlogo.com/");
			document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
			//]]>
		</script>
		
	</head>
<body class="out_login">
	<%
		try {
	%>
	<tiles:insertAttribute name="header" />
	<div id="main" class="hundred">
		<tiles:insertAttribute name="contentLeft" />
		<tiles:insertAttribute name="contentRight" />
		
	</div>
	<div class="clear"></div>
	<%
		} catch (Exception e) {
			out.println(e);
		}
	%>
	
	<!-- <script language="JavaScript" type="text/javascript">
	TrustLogo("http://potracker.revesoft.com/resources/images/comodo_secure_seal_100x85_transp.png", "CL1", "none");
	</script>
	<a  href="https://ssl.comodo.com" id="comodoTL">Comodo SSL</a> -->
</body>
</html>