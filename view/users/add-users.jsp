<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<div>
	<%
		try {
			RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
					.getAttribute(ApplicationConstant.LOGIN_INFO);
			RimsUsersDTO dto = null;
			Object obj = session
					.getAttribute(ApplicationConstant.ACTION_DATA);
			session.removeAttribute(ApplicationConstant.ACTION_DATA);
			if (obj != null && obj instanceof RimsUsersDTO) {
				dto = (RimsUsersDTO) obj;
			}
			if (dto == null) {
				dto = new RimsUsersDTO();
			}
			MashupDataRepository mudRepository = MashupDataRepository
					.getInstance(false);
			UIHelper uiHelper = new UIHelper();
			Validations v = new Validations();
	%>
	<form action="add-users.html" method="post"
		enctype="multipart/form-data" class="form-horizontal">
		<%@ include file="../includes/captcha.jsp" %>
	  <div class="panel panel-info">
		<div class="panel-body">
			
			
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Full Name</label>
			<div class="col-lg-3 col-md-5"><input type="text" name="usrName"
			value="<%=dto.getUsrName()%>" required class="form-control"/></div>
			<div class="col-lg-1">
				<span class="glyphicon glyphicon-asterisk mandatory"></span>
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Email</label>
			<div class="col-lg-3 col-md-5"><input type="email" name="usrEmail"
			value="<%=dto.getUsrEmail()%>" required class="form-control"/></div>
			<div class="col-lg-1">
				<span class="glyphicon glyphicon-asterisk mandatory"></span>
			</div>
		</div>
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Mobile Number</label>
			<div class="col-lg-3 col-md-5"><input type="number" name="officePhone" 
			value="<%=dto.getOfficePhone()%>"  class="form-control"/></div>
			<div class="col-lg-1">
				<!-- <span class="glyphicon glyphicon-asterisk mandatory"></span> -->
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Department</label>
			<div class="col-lg-3 col-md-5">
				<select name="department"  class="form-control">
					<%=uiHelper.getSelectFromMashup(mudRepository.getByFieldType(mudRepository.DEPARTMENT, true), true, dto.getDepartment(), null) %>
				</select>
			</div>
			<div class="col-lg-1">
				
			</div>
		</div>
		
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Role</label>
			<div class="col-lg-3 col-md-5">
				<select name="usrRoleId" required class="form-control">
					<%=uiHelper.getSelectFromMashup(mudRepository.getByFieldType(mudRepository.ROLE, true), true, dto.getUsrRoleId(), null) %>
				</select>
			</div>
			<div class="col-lg-1">
				<span class="glyphicon glyphicon-asterisk mandatory"></span>
			</div>
		</div>
		
		
		<%-- <div class="form-group">
			<label class="col-lg-2">Security Token</label>
			<div class="col-lg-2"><input type="number" name="usrSecurityToken" required class="form-control" value="<%=dto.getUsrSecurityToken() %>"/></div>
			<div class="col-lg-1">
				<span class="glyphicon glyphicon-asterisk mandatory"></span>
			</div>
		</div> --%>
		
		<div class="form-group">
			<label class="col-lg-2 col-md-2">Current Status</label>
			<div class="col-lg-3 col-md-5">
				<select name="currentStatus" required="required" class="form-control">
					<%=uiHelper.getSelectFromMashup(mudRepository
						.getByFieldType(mudRepository.CURRENT_STATUS, true),
						false, 0, dto.getCurrentStatus())%>
				</select>
			</div>
			<div class="col-lg-1">
				<span class="glyphicon glyphicon-asterisk mandatory"></span>
			</div>
		</div>
		
			</div>
			<div class="panel-footer">
				<input type="submit" value="Submit" class="btn btn-warning" />
				<span class="mandatory">*</span> Fields are mandatory
			</div>
		</div>

	</form>
	<%
		} catch (RuntimeException e) {
			Logger.getLogger("add-rims-users").fatal("RuntimeException", e);
		}
	%>
</div>