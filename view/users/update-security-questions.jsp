<%@page import="dev.mashfiq.mashupData.MashupDataDTO"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<form action="rims-users/update-security-questions.html" method="post">
<%@ include file="/view/includes/captcha.jsp" %>
	<div class="panel panel-info">
		<div class="alert alert-warning text-center">
			if you update your security question answer, you can use them to login without secuirty devices.
		</div>
		<div class="panel-body">
			<%
				MashupDataRepository mashupDataRepository = MashupDataRepository.getInstance(false);
				for(MashupDataDTO question: mashupDataRepository.getByFieldType(MashupDataRepository.SECURITY_QUESTIONS, true)) {
					%>
					<div class="form-group col-lg-4">
						<label><%=question.getLabel() %> <span class="glyphicon glyphicon-asterisk mandatory"></span></label>
						<div>
							<input type="text" name="q_<%=question.getId() %>" class="form-control" required="required"/>
						</div>
					</div>
					<%
				}
			%>
		</div>
		<div class="panel-footer">
			<input type="submit" value="Update" class="btn btn-success" />
		</div>
	</div>
</form>