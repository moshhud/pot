<%@page import="dev.mashfiq.util.FileUploadHelper"%>
<%@page
	import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<div>
	<%
		try {
			RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
					.getAttribute(ApplicationConstant.LOGIN_INFO);
			RimsUsersDTO dto = null;
			Object obj = session
					.getAttribute(ApplicationConstant.ACTION_DATA);
			session.removeAttribute(ApplicationConstant.ACTION_DATA);
			MashupDataRepository mudRepository = MashupDataRepository
					.getInstance(false);
			UIHelper uiHelper = new UIHelper();
			Validations v = new Validations();
			if (obj != null && obj instanceof RimsUsersDTO) {
				dto = (RimsUsersDTO) obj;
				if (dto != null) {
	%>
	<form action="update-profile.html" method="post"
		enctype="multipart/form-data" class="form-horizontal">
		<%@ include file="../includes/captcha.jsp"%>
		<input type="hidden" name="usrID" value="<%=dto.getUsrID()%>" /> <input
			type="hidden" name="currentEmail" value="<%=dto.getUsrEmail()%>" />
		<input type="hidden" name="currentPassword"
			value="<%=dto.getUsrPassword()%>" />
		<div class="panel panel-info">
			<div class="panel-body">
				<%-- <div class="row">
					<img class="img-rounded  col-lg-3" src="<%=ApplicationConstant.getBaseURL(session) + FileUploadHelper.RIMS_USER_FILES + dto.getProfileImageFileName() %>">
				</div> 
				<br/>--%>
				<div class="form-group">
				   <label class="col-lg-2 col-md-3">Profile Picture</label>
				   <div class="col-lg-3 col-md-9">
				        
				        <%
				        if(dto.getUsrSignatureFileName()!=null && dto.getUsrSignatureFileName().length()>0){
				        	%>
				        	<img class="img-responsive img-rounded  " src="<%=ApplicationConstant.getBaseURL(session) + FileUploadHelper.RIMS_USER_FILES + dto.getProfileImageFileName() %>">
				        	<%
				        }
				        else{
				        	%>
				        	Profile image not available
				        	<%
				        }
				      %>
				   </div>				   
				   <label class="col-lg-2 col-md-3">Signature File</label>
				   <div class="col-lg-3 col-md-9">
				      <%
				        if(dto.getUsrSignatureFileName()!=null && dto.getUsrSignatureFileName().length()>0){
				        	%>
				        	<img class="img-responsive img-rounded  " src="<%=ApplicationConstant.getBaseURL(session) + FileUploadHelper.RIMS_USER_SIGNATURE + dto.getUsrSignatureFileName() %>">
				        	<%
				        }
				        else{
				        	%>
				        	Signature file not available
				        	<%
				        }
				      %>
				       
				   </div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Gender</label>
					<div class="col-lg-2">
						<select name="gender" required class="form-control">
							<%=uiHelper.getSelectFromMashup(mudRepository
								.getByFieldType(mudRepository.GENDER, true),
								false, 0, dto.getGender())%>
						</select>
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
					<label class="col-lg-2">Employee Name</label>
					<div class="col-lg-2">
						<input type="text" name="usrName" value="<%=dto.getUsrName()%>"
							required class="form-control" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Official Email</label>
					<div class="col-lg-2">
						<input type="email" name="usrEmail" value="<%=dto.getUsrEmail()%>"
							required class="form-control" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
					<label class="col-lg-2">Password</label>
					<div class="col-lg-2">
						<input type="password" name="usrPassword"
							value="<%=dto.getUsrPassword()%>" required class="form-control" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Official Mobile Number</label>
					<div class="col-lg-2">
						<input type="number" name="officePhone"
							value="<%=dto.getOfficePhone()%>" required class="form-control" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
					<label class="col-lg-2">Office Location</label>
					<div class="col-lg-2">
						<select name="officeLocation" required class="form-control"
							id="officeLocation">
							<%=uiHelper.getSelectFromMashup(mudRepository
								.getByFieldType(mudRepository.OFFICE_LOCATION,
										true), false, 0, dto
								.getOfficeLocation())%>
						</select>
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Department</label>
					<div class="col-lg-2">
						<select name="department" required class="form-control">
							<%=uiHelper.getSelectFromMashup(
								mudRepository.getByFieldType(
										mudRepository.DEPARTMENT, true), true,
								dto.getDepartment(), null)%>
						</select>
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
					<label class="col-lg-2">Designation</label>
					<div class="col-lg-2">
						<select name="designation" required class="form-control">
							<%=uiHelper.getSelectFromMashup(
								mudRepository.getByFieldType(
										mudRepository.DESIGNATION, true), true,
								dto.getDesignation(), null)%>
						</select>
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Security Token</label>
					<div class="col-lg-2">
						<input type="number" name="usrSecurityToken" required
							class="form-control" value="<%=dto.getUsrSecurityToken()%>" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
					<label class="col-lg-2">Joining Date</label>
					<div class="col-lg-2">
						<input type="text" name="joinDate" class="form-control datepicker"
							value="<%=dto.getJoinDate()%>" required="required" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Date of Birth</label>
					<div class="col-lg-2">
						<input type="text" name="birthDate"
							class="form-control datepicker" value="<%=dto.getBirthDate()%>"
							required="required" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
					<label class="col-lg-2">Skype ID</label>
					<div class="col-lg-2">
						<input type="text" name="skypeId" required class="form-control"
							value="<%=dto.getSkypeId()%>" />
					</div>
					<div class="col-lg-1">
						<span class="glyphicon glyphicon-asterisk mandatory"></span>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Marital Status</label>
					<div class="col-lg-2">
						<select name="maritalStatus" class="form-control">
							<%=uiHelper.getSelectFromMashup(mudRepository
								.getByFieldType(mudRepository.MARITAL_STATUS,
										true), false, 0, dto.getMaritalStatus()
								+ "")%>
						</select>
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Father's Name</label>
					<div class="col-lg-2">
						<input type="text" name="fatherName" class="form-control"
							value="<%=dto.getFatherName()%>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Mother's Name</label>
					<div class="col-lg-2">
						<input type="text" name="motherName" class="form-control"
							value="<%=dto.getMotherName()%>" />
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Personal Email</label>
					<div class="col-lg-2">
						<input type="email" name="personalEmail" class="form-control"
							value="<%=dto.getPersonalEmail()%>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Country</label>
					<div class="col-lg-2">
						<select name="country" class="form-control"><%=uiHelper.getCountryOptions(dto.getCountry(),
								"Select")%></select>
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Current Address</label>
					<div class="col-lg-2">
						<textarea name="currentAddress" class="form-control"><%=dto.getCurrentAddress()%></textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Permanent Address</label>
					<div class="col-lg-2">
						<textarea name="permanentAddress" class="form-control"><%=dto.getPermanentAddress()%></textarea>
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Alternate Number</label>
					<div class="col-lg-2">
						<input type="number" name="alternatePhone"
							value="<%=dto.getAlternatePhone()%>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Emergency Contact Name</label>
					<div class="col-lg-2">
						<input type="text" name="emerContName" class="form-control"
							value="<%=dto.getEmerContName()%>" />
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Relation</label>
					<div class="col-lg-2">
						<select name="emerContRelation" class="form-control"
							id="emerContRelation">
							<option value="">Select</option>
							<%=uiHelper.getSelectFromMashup(mudRepository
								.getByFieldType(mudRepository.RELATIONSHIP,
										true), false, 0, dto
								.getEmerContRelation())%>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Mobile No</label>
					<div class="col-lg-2">
						<input type="number" class="form-control" name="emerContMobileNo"
							value="<%=dto.getEmerContMobile()%>" />
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Telephone No</label>
					<div class="col-lg-2">
						<input type="number" name="emerContTelephone"
							value="<%=dto.getEmerContTelephone()%>" class="form-control" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Address</label>
					<div class="col-lg-2">
						<input type="text" name="emerContAddress" class="form-control"
							value="<%=dto.getEmerContAddress()%>" />
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Spouse's Name</label>
					<div class="col-lg-2">
						<input type="text" name="spouseName" class="form-control"
							value="<%=dto.getSpouseName()%>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Marriage Date</label>
					<div class="col-lg-2">
						<input type="text" name="marriageDate"
							class="form-control datepicker"
							value="<%=dto.getMarriageDate()%>" />
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">No Of Children</label>
					<div class="col-lg-2">
						<input type="text" name="noOfChildren" id="noOfChildren"
							class="form-control" value="<%=dto.getNoOfChildren()%>" />
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-2">Upload Digital Signature</label>
					<div class="col-lg-2">
						<input type="hidden" name="usrSignatureFileName" value="<%=dto.getUsrSignatureFileName() %>" />
						<input type="file" name="usrSignature" class="form-control"/>
						<%
							if(dto.getUsrSignatureFileName() != null && dto.getUsrSignatureFileName().length() > 0) {
						%>
						<a class="btn btn-default" href="<%=ApplicationConstant.getBaseURL(session) + FileUploadHelper.RIMS_USER_SIGNATURE + dto.getUsrSignatureFileName() %>" target="_blank">Download</a>
						<%		
							}
						%>
					</div>
					<div class="col-lg-1"></div>
					<label class="col-lg-2">Upload Profile Picture</label>
					<div class="col-lg-2">
						<input type="hidden" name="profileImageFileName" value="<%=dto.getProfileImageFileName() %>" />
						<input type="file" name="profileImage" class="form-control"/>
					</div>					
				</div>
				<div class="form-group">
			      <label class="col-lg-2 col-md-6 col-sm-6 col-xs-6">Security Question</label>
			      <div class="col-lg-3 col-md-6 col-sm-6  col-xs-6">
					<a href="../security-update.html?usrID=<%=dto.getUsrID()%>">Security Question Update</a>
				  </div>
			   </div>
			   
			</div>
			
			<div class="panel-footer">
				<input type="submit" value="Submit" class="btn btn-info" />
			</div>
		</div>

	</form>
	<%
		}
			}
		} catch (RuntimeException e) {
			Logger.getLogger("edit-profile").fatal("RuntimeException", e);
		}
	%>
</div>