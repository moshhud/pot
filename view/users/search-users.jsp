<%@page import="dev.mashfiq.util.Permissions"%>
<%@page import="dev.mashfiq.mashupData.MashupDataRepository"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="org.apache.log4j.Logger"%>
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session
				.getAttribute(ApplicationConstant.LOGIN_INFO);
		Object obj = session
				.getAttribute(ApplicationConstant.ACTION_DATA);
		session.removeAttribute(ApplicationConstant.ACTION_DATA);
		LinkedHashMap<String, RimsUsersDTO> data = null;
		MashupDataRepository mudRepository = null;
		
%>
<div class="panel panel-default">
	<div class="panel-heading"><h1 class="panel-title">Search Result</h1>
	
	</div>
	<div class="panel-body">
		<div class="table-responsive">
			<table class="table table-bordered table-hover table-search">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Contact No</th>	
						<th>Role</th>
						<th>Department</th>
						<th>Status</th>	
						<% if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_FOUR) { %>						 
						<th>Edit</th>
						<% } %>
							
					</tr>
				</thead>
				<tbody id="data">
					<%
						if (obj != null && obj instanceof LinkedHashMap) {
								data = (LinkedHashMap<String, RimsUsersDTO>) obj;
								if (data != null && data.size() > 0) {
									mudRepository = MashupDataRepository.getInstance(false);
									for (RimsUsersDTO dto : data.values()) {
										if (dto != null) {
					%>
					<tr>
						<td class="capitalize"><%=dto.getUsrName() %></td>
						<td><%=dto.getUsrEmail() %></td>
						<td><%=dto.getOfficePhone() %></td>
						<td><%=mudRepository.getLabelById(dto.getUsrRoleId()) %></td>
						<td><%=mudRepository.getLabelById(dto.getDepartment()) %></td>
						<td><%=dto.getCurrentStatus() %></td>
						<% if (rimsUsersDTO.getPermissionLevelByModuleId(Permissions.RIMS_USERS) >= Permissions.LEVEL_FOUR) { %>						 
						<td><a href="get-users.html?usrID=<%=dto.getUsrID()%>">Edit</a></td>
						<% } %>
						
					</tr>
					<%
							}
						}
					}
				}
		%>
				</tbody>
			</table>
		</div>
	</div>
</div>
<%
	} catch (RuntimeException e) {
		Logger.getLogger("search-users").fatal("RuntimeException", e);
	}
%>
