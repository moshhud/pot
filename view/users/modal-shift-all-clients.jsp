<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.UIHelper"%>
<script type="text/javascript">
$(document).ready(function(e) {
	$('#shiftAllClients').submit(function(e) {
		e.preventDefault();
		currentAccountManager = $('#currentAccountManager').val();
		newAccountManager = $('#newAccountManager').val();
		cc = $('#cc').val();
		comment = $('#comment').val();
		try{
			if(currentAccountManager > 0) {
				if(newAccountManager > 0) {
					if(comment.length > 0) {
						try {
							$.ajax({url:BASE_URL + 'resources/ajax-files/rims-users.jsp', type: 'POST', data: ({type: "shiftAllClients", currentAccountManager: currentAccountManager, newAccountManager: newAccountManager, cc: cc, comment: comment}), dataType: 'JSON', success: function(data){
								alert(data.msg);
								if(data.status == 'success') {
									$('#shiftAllClients').modal("hide");
									$('#currentAccountManager').val('');
									$('#newAccountManager').val('');
									$('#cc').val('');
									$('#comment').val('');
								}
							}, error: function(x, y, z){}});
						} catch (err) {
							alert(err)
						}
					} else {
						alert('Enter Comment');
					}
				} else {
					alert('Select New Account Manager');
				}
			} else {
				alert('Select Current Account Manager');
			}
		} catch (err) {
			alert(err)
		}
	});
});
</script>
<%
	RimsUsersDTO currentRimsUserDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
	UIHelper uiHelper = new UIHelper();
%>
<div id="shiftAllClients" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <form id="shiftAllClients">
      	<div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Shift Clients</h4>
	      </div>
	      <div class="modal-body">
	        <div class="form-group">
	        	<select class="form-control" id="currentAccountManager" required>
	        		<%= uiHelper.getAccountManagerOptions(0, currentRimsUserDTO.getUsrID(), 6, "Current Account Manager", false) %>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<select class="form-control" id="newAccountManager" required>
	        		<%= uiHelper.getAccountManagerOptions(0, currentRimsUserDTO.getUsrID(), 6, "New Account Manager") %>
	        	</select>
	        </div>
	        <div class="form-group">
	        	<textarea id="comment" placeholder="Description *" class="form-control" required></textarea>
	        </div>
	        <div class="form-group">
	        	<textarea id="cc" placeholder="comma separated emails to keep in cc" class="form-control"></textarea>
	        </div>
	      </div>
	      <div class="modal-footer">
	        <input type="submit" id="getDeletedInvoice" class="btn btn-default" value="Update" />
	      </div>
      </form>
    </div>
  </div>
</div>