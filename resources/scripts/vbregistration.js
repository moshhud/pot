var displayed = false;

function getDealingOfficer(val) {
	selectDealingOfficer(val);
}

function addMMAI() {
	var form = document.forms[0];
	val = form.txtImei.value + ":" + form.txtMmIP.value + ":" + form.txtMmPort.value;
	if(form.txtImei.value.length < 15) {
		alert('Invalid IMEI');
		return false;
	}

	var ob = form.txtMmIP;
	var obPort = form.txtMmPort;

	if (isEmpty(ob.value) && isEmpty(obPort.value)) {
		ob.focus();
		return false;
	}

	if (!isEmpty(ob.value) && !isValidIP(ob.value)) {
		alert("IP is invalid");
		ob.focus();
		return false;
	}
	
	if(obPort.value*1 <=0) {
		alert("Port must be great than zero");
		obPort.focus();
		return false;
	}

	if (!isEmpty(ob.value)) {
		if (isEmpty(obPort.value) || !validateInteger(obPort.value)) {
			alert("Please put a valid port.");
			obPort.focus();
			return false;
		}
	}

	for (i = 0; i < form.vbMobileMoneyAgentInfo.length; i++) {
		index = form.vbMobileMoneyAgentInfo[i].text.indexOf(":");
		str = form.vbMobileMoneyAgentInfo[i].text.substring(0, index);
		if (str == form.txtImei.value) {
			alert('IMEI ' + form.txtImei.value + ' already added.');
			ob.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.vbMobileMoneyAgentInfo[form.vbMobileMoneyAgentInfo.options.length] = new Option(
				val, val, false, false);
		form.txtImei.value = '';
		form.txtMmIP.value = '';
		form.txtMmPort.value = '';
		return true;
	}
	return false;
}

function addSwitch() {
	var form = document.forms[0];
	val = form.txtSwitch.value + ":" + form.txtPort.value;

	var ob = form.txtSwitch;
	var obPort = form.txtPort;

	if (isEmpty(ob.value) && isEmpty(obPort.value)) {
		ob.focus();
		return false;
	}

	if (!isEmpty(ob.value) && !isValidIP(ob.value) && !checkDomain(ob.value)) {
		alert("IP/DNS name is invalid.");
		ob.focus();
		return false;
	}

	if (!isEmpty(ob.value)) {
		if (isEmpty(obPort.value) || !validateInteger(obPort.value)) {
			alert("Please put a valid port.");
			obPort.focus();
			return false;
		}
	}

	if (isEmpty(ob.value) && !isEmpty(obPort.value)) {
		alert("Please put a valid switch IP.");
		ob.focus();
		return false;
	}

	for (i = 0; i < form.additionalSwitches.length; i++) {
		index = form.additionalSwitches[i].text.indexOf(":");
		str = form.additionalSwitches[i].text.substring(0, index);
		if (str == form.txtSwitch.value) {
			alert('Additional Switch ' + form.txtSwitch.value + ' already added.');
			ob.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.additionalSwitches[form.additionalSwitches.options.length] = new Option(val, val, false, false);
		form.txtSwitch.value = '';
		form.txtPort.value = '';
		return true;
	}

	return false;
}






function addFieldNameValue(fieldNameIP, fieldNamePort, nameTag){
	var form = document.forms[0]; 
	var fieldNameIP = form.fieldNameIP;
	var fieldNamePort = form.fieldNamePort;
	var nameTag = form.nameTag;
	
	val = fieldNameIP.value + ":" + fieldNamePort.value;

	//var ob = form.fieldName;
	//var obPort = form.fieldNamePort;

	if (isEmpty(fieldNameIP.value) && isEmpty(fieldNamePort.value)) {
		fieldNameIP.focus();
		return false;
	}

	if (!isEmpty(fieldNameIP.value) && !isValidIP(fieldNameIP.value) && !checkDomain(fieldNameIP.value)) {
		alert("IP is invalid.");
		fieldNameIP.focus();
		return false;
	}

	if (!isEmpty(fieldNameIP.value)) {
		if (isEmpty(fieldNamePort.value) || !validateInteger(fieldNamePort.value)) {
			alert("Please put a valid port.");
			fieldNamePort.focus();
			return false;
		}
	}

	if (isEmpty(fieldNameIP.value) && !isEmpty(fieldNamePort.value)) {
		alert("Please put a valid IP.");
		fieldNameIP.focus();
		return false;
	}

	for (i = 0; i < form.nameTag.length; i++) {
		index = form.nameTag[i].text.indexOf(":");
		str = form.nameTag[i].text.substring(0, index);
		if (str == form.fieldNameIP.value) {
			alert('Additional Switch ' + form.txtSwitch.value + ' already added.');
			fieldNameIP.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.nameTag[form.nameTag.options.length] = new Option(val, val, false, false);
		form.fieldNameIP.value = '';
		form.fieldNamePort.value = '';
		return true;
	}

	return false;
	
	
}

function addAccessNumber() {
	var form = document.forms[0];
	val = form.accNum.value + ":" + form.accAtr.value + ":" + form.accDefault.value;

	var ob = form.accNum;
	var obPort = form.accAtr;

	if (isEmpty(ob.value) && isEmpty(obPort.value)) {
		alert('Enter Value');
		ob.focus();
		return false;
	}

	for (i = 0; i < form.vbAccessNumber.length; i++) {
		index = form.vbAccessNumber[i].text.indexOf(":");
		str = form.vbAccessNumber[i].text.substring(0, index);
		if (str == form.accNum.value) {
			alert('Access Number ' + form.vbAccessNumber.value + ' already added.');
			ob.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.vbAccessNumber[form.vbAccessNumber.options.length] = new Option(
				val, val, false, false);
		form.accNum.value = '';
		form.accAtr.value = '';
		return true;
	}

	return false;
}

function addVbRegionalSwitch() {
	try {
		var form = document.forms[0];
		val = form.region.value + ":" + form.ssIP.value + ":" + form.ssPort.value;
		var ob = form.region;
		var obIp = form.ssIP;
		var obPort = form.ssPort;

		if (isEmpty(ob.value) || isEmpty(obPort.value) || isEmpty(obIp.value)) {
			alert('Enter Value');
			ob.focus();
			return false;
		}
		for (i = 0; i < form.vbRegionalSwitch.length; i++) {
			index = form.vbRegionalSwitch[i].text.indexOf(":");
			str = form.vbRegionalSwitch[i].text.substring(0, index);
			if (str == form.region.value) {
				alert(form.region.value + ' already added.');
				ob.focus();
				return false;
			}
		}

		if (val.length != 0) {
			form.vbRegionalSwitch[form.vbRegionalSwitch.options.length] = new Option(
					val, val, false, false);
			form.ssIP.value = '';
			form.ssPort.value = '';
			return true;
		}
	} catch(err) {
		alert(err)
	}

	return false;
}

function addProtocolDesc() {
	try {
		var form = document.forms[0];
		val = form.protocol.value + "-" + form.protoIP.value + ":" + form.protoPort.value;
		
		var ob = form.protocol;
		var obIp = form.protoIP;
		var obPort = form.protoPort;

		if (isEmpty(ob.value) || isEmpty(obPort.value) || isEmpty(obIp.value)) {
			alert('Enter Value');
			ob.focus();
			return false;
		}
		if (!isValidIP(obIp.value)) {
			alert("IP is invalid.");
			ob.focus();
			return false;
		}
		if(!validateNum(obPort.value,1, 65535)){
			alert("Port is invalid.");
			ob.focus();
			return false;
		}
		for (i = 0; i < form.protocolDescription.length; i++) {
			//index = form.protocolDescription[i].text.indexOf("-");
			//str = form.protocolDescription[i].text.substring(0, index);
			str = form.protocolDescription[i].text;
			if (str == val) {
				alert(val + ' already added.');
				ob.focus();
				return false;
			}
		}

		if (val.length != 0) {
			form.protocolDescription[form.protocolDescription.options.length] = new Option(
					val, val, false, false);
			form.protoIP.value = '';
			form.protoPort.value = '';
			return true;
		}
	} catch(err) {
		alert(err)
	}

	return false;
}

function removeProtocolDesc() {
	var form = document.forms[0];
	index = form.protocolDescription.selectedIndex;
	if (index != -1) {
		if (form.protocolDescription.options[index].selected)
			form.protocolDescription.remove(index);
	}
}

function validateNum(input, min, max) {
    var num = +input;
    return num >= min && num <= max && input === num.toString();
}

function addLanguage() {
	var form = document.forms[0];
	val = form.langAtr.value + ":" + form.langId.value + ":" + form.langDefault.value;

	var ob = form.langAtr;
	var obPort = form.LangId;

	if (isEmpty(ob.value) && isEmpty(obPort.value)) {
		alert('Enter Value');
		ob.focus();
		return false;
	}

	for (i = 0; i < form.vbCallThroughLanguage.length; i++) {
		index = form.vbCallThroughLanguage[i].text.indexOf(":");
		str = form.vbCallThroughLanguage[i].text.substring(0, index);
		if (str == form.langAtr.value) {
			alert('Access Number ' + form.vbCallThroughLanguage.value + ' already added.');
			ob.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.vbCallThroughLanguage[form.vbCallThroughLanguage.options.length] = new Option(
				val, val, false, false);
		form.langAtr.value = '';
		form.LangId.value = '';
		return true;
	}

	return false;
}

function removeSwitch() {
	var form = document.forms[0];
	index = form.additionalSwitches.selectedIndex;
	if (index != -1) {
		if (form.additionalSwitches.options[index].selected)
			form.additionalSwitches.remove(index);
	}
}



function removeMMAI() {
	var form = document.forms[0];
	index = form.vbMobileMoneyAgentInfo.selectedIndex;
	if (index != -1) {
		if (form.vbMobileMoneyAgentInfo.options[index].selected)
			form.vbMobileMoneyAgentInfo.remove(index);
	}
}

function removeAccessNumber() {
	var form = document.forms[0];
	index = form.vbAccessNumber.selectedIndex;
	if (index != -1) {
		if (form.vbAccessNumber.options[index].selected)
			form.vbAccessNumber.remove(index);
	}
}

function removeVbRegionalSwitch() {
	var form = document.forms[0];
	index = form.vbRegionalSwitch.selectedIndex;
	if (index != -1) {
		if (form.vbRegionalSwitch.options[index].selected)
			form.vbRegionalSwitch.remove(index);
	}
}

function removeLanguage() {
	var form = document.forms[0];
	index = form.vbCallThroughLanguage.selectedIndex;
	if (index != -1) {
		if (form.vbCallThroughLanguage.options[index].selected)
			form.vbCallThroughLanguage.remove(index);
	}
}

function selectAllOptions() {
	var form = document.forms[0];
	var ob = form.switchIP;
	if (!isValidIP(ob.value) && !checkDomain(ob.value)) {
		alert("Switch IP/DNS is invalid.");
		ob.focus();
		return false;
	}

	var ob = form.amrSwitchIP;
	if (!isEmpty(ob.value) && !isValidIP(ob.value) && !checkDomain(ob.value)) {
		alert("AMR Switch IP/DNS is invalid.");
		ob.focus();
		return false;
	}

	var ob = form.jitterBufferLength;

	if (isEmpty(ob.value)) {
		alert("Jitter Buffer Length won't be empty.");
		ob.focus();
		return false;
	}

	if (!validateInteger(ob.value)) {
		alert("Jitter Buffer Length must be integer.");
		ob.focus();
		return false;
	}
	if (form.rtpProtocolType.value == 1) {
		if (ob.value != 0) {
			alert("Jitter Buffer Length must be 0");
			ob.focus();
			return false;
		}
	} else {
		if (ob.value < 0 || ob.value > 8) {
			alert("Jitter Buffer Length must be a value between 0 to 8");
			form.jitterBufferLength.focus();
			return false;
		}
	}

	var ob = form.outgoingFramesPerPacket;
	if (isEmpty(ob.value)) {
		alert("Outgoing Frames Per Packet won't be empty.");
		ob.focus();
		return false;
	}
	if (!validateInteger(ob.value)) {
		alert("Outgoing Frames Per Packet must be integer.");
		ob.focus();
		return false;
	}

	if (ob.value < 2 || ob.value > 100) {
		alert("Outgoing Frames Per Packet must be an even value between 2 to 100");
		ob.focus();
		return false;
	}

	var ob = form.primaryOperatorCode;
	if (isEmpty(ob.value)) {
		alert("Primary operator code won't be empty.");
		ob.focus();
		return false;
	}

	var ob = form.rtpHeader;
	if (ob.value.length > 0) {
		var hex_check_string = ob.value.substring(0, 2);
		if (!validateHEXNumeric(ob.value.substring(2))) {
			alert("After 0x, RTP header characters will be ranging from 0 to 9 and A to F");
			ob.focus();
			return false;
		} else if (hex_check_string != "0x") {
			alert("RTP header value must starts with 0x (zero x)");
			ob.focus();
			return false;
		}
	}

	var ob = form.signalingKeys;
	if (ob.value.length > 0) {
		var hex_check_string = ob.value.substring(0, 2);
		if (!validateHEXNumeric(ob.value.substring(2))) {
			alert("After 0x, Signaling Keys characters will be ranging from 0 to 9 and A to F");
			ob.focus();
			return false;
		} else if (hex_check_string != "0x") {
			alert("Signaling Keys value must starts with 0x (zero x)");
			ob.focus();
			return false;
		}
	}

	var ob = form.signalingHeader;
	if (ob.value.length > 0) {
		var hex_check_string = ob.value.substring(0, 2);
		if (!validateHEXNumeric(ob.value.substring(2))) {
			alert("After 0x, Signaling Header characters will be ranging from 0 to 9 and A to F");
			ob.focus();
			return false;
		} else if (hex_check_string != "0x") {
			alert("Signaling Header value must starts with 0x (zero x)");
			ob.focus();
			return false;
		}
	}

	var ob = form.signalingFooter;
	if (ob.value.length > 0) {
		var hex_check_string = ob.value.substring(0, 2);
		if (!validateHEXNumeric(ob.value.substring(2))) {
			alert("After 0x, Signaling Footer characters will be ranging from 0 to 9 and A to F");
			ob.focus();
			return false;
		} else if (hex_check_string != "0x") {
			alert("Signaling Footer value must starts with 0x (zero x)");
			ob.focus();
			return false;
		}
	}

	var len = document.forms[0].additionalSwitches.length;
	if (len > 0) {
		for (i = 0; i < len; i++)
			document.forms[0].additionalSwitches.options[i].selected = true;
	}
	var len = document.forms[0].additionalFooters.length;
	if (len > 0) {
		for (i = 0; i < len; i++)
			document.forms[0].additionalFooters.options[i].selected = true;
	}
	
	var len = document.forms[0].vbAccessNumber.length;
	if (len > 0) {
		for (i = 0; i < len; i++)
			document.forms[0].vbAccessNumber.options[i].selected = true;
	}
	
	var len = document.forms[0].vbCallThroughLanguage.length;
	if (len > 0) {
		for (i = 0; i < len; i++)
			document.forms[0].vbCallThroughLanguage.options[i].selected = true;
	}
	return false;
}

function checkByteSaver() {
	var form = document.forms[0];
	var ob_byte_saver = form.byteSaver;
	if (ob_byte_saver.value == "Normal Dialer") {
		alert("Please enable byte saver first.");
		form.rtpProtocolType.value = '0';
		return false;
	}
	if (form.rtpProtocolType.value == 1) {
		form.jitterBufferLength.value = '0';
		form.jitterBufferLength.readOnly = true;
	} else
		form.jitterBufferLength.readOnly = false;
	return true;
}
function init() {
	if (document.all.audioSet.value == "0")
		document.all.audioSetting[0].click();
	else
		document.all.audioSetting[1].click();

	if (document.all.balanceServer.value.length > 1) {
		document.all.chkBalanceServer.checked = true;
		showIPPort();
	}

	if (document.all.smsServerIP.value.length > 1) {
		document.all.chkSMSServer.checked = true;
		showSMSIPPort();
	}

	showPullDown('pullDownClient');
	window.document.getElementById('mainTable').style.height = parseInt(window.document
			.getElementById('mainTable').style.height) + 250;
}

function showIPPort() {
	var f = document.forms[0];
	if (f.chkBalanceServer.checked == true) {
		document.getElementById("td1").style.display = '';
		document.getElementById("td2").style.display = '';
		document.getElementById("td3").style.display = '';
		document.getElementById("td4").style.display = '';
		document.getElementById("td5").style.display = '';
		document.getElementById("td6").style.display = '';
		if (document.getElementById("balance_server_tr").style.display == '') {
			document.all.balanceServer.focus();
		}
	} else {
		document.getElementById("td1").style.display = 'none';
		document.getElementById("td2").style.display = 'none';
		document.getElementById("td3").style.display = 'none';
		document.getElementById("td4").style.display = 'none';
		document.getElementById("td5").style.display = 'none';
		document.getElementById("td6").style.display = 'none';
		if (document.getElementById("balance_server_tr").style.display == 'none') {
			document.all.chkBalanceServer.focus();
		}
	}
}

function showSMSIPPort() {
	var f = document.forms[0];
	if (f.chkSMSServer.checked == true) {
		document.getElementById("td7").style.display = '';
		document.getElementById("td8").style.display = '';
		document.getElementById("td9").style.display = '';
		document.getElementById("td10").style.display = '';
		document.getElementById("td11").style.display = '';
		document.getElementById("td12").style.display = '';
		if (document.getElementById("sms_server_tr").style.display == '') {
			document.all.smsServerIP.focus();
		}
	} else {
		document.getElementById("td7").style.display = 'none';
		document.getElementById("td8").style.display = 'none';
		document.getElementById("td9").style.display = 'none';
		document.getElementById("td10").style.display = 'none';
		document.getElementById("td11").style.display = 'none';
		document.getElementById("td12").style.display = 'none';
		if (document.getElementById("sms_server_tr").style.display == 'none') {
			document.all.chkSMSServer.focus();
		}
	}

}

$(document).ready(function(){
//	$('form').submit(function(){
//		var len = document.forms[0].additionalSwitches.length;
//		if(len>0)
//		{
//	 	 	for( i=0;i<len;i++)
//	 	 	{
//	  			document.forms[0].additionalSwitches.options[i].selected = true;
//	 	 	}
//		}
//	});
});