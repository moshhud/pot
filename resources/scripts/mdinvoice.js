function selectAllOptions()
{
	var len = document.forms[0].invcInstallments.length;
    for( i=0;i<len;i++)
    	document.forms[0].invcInstallments.options[i].selected = true;
    
}
$(document).ready(function(){
	$("#product").change(getProduct);// end of product change
	$('[name~=category]').click(getProduct);
	
	function getProduct(){
		var clientID_val = $("#invcRcClientID").val() * 1;
		var product_val = $("#product").val();
		var clientEmail_val = $("#rcEmail").html();
		var category_val = $('[name~=category]:checked').val();
		var one_done = false;
		$('#level-one').addClass('hidden');
		$('#level-two').addClass('hidden');
		$('#level-three').addClass('hidden');
		$('#level-four').addClass('hidden');
		$.blockUI();
		if(clientID_val > 0) {
			if(product_val == "111")
			{
				$('#level-one').removeClass('hidden');
				$("select#productType").html("<option value='111'>After Sales Service</option>");
				$('.supportExtension').css('display', 'block');
				updateCode(category_val);
				try {
					if(category_val != undefined && category_val.length > 0) {
						$.post(BASE_URL + "resources/ajax-files/select-commercial-products.jsp",{email:clientEmail_val, category: category_val},function(options)
						{
							options = $.trim(options);
							options = $.parseJSON(options);
							if(options.status == 'success') {
								$("select#idFromOtherTable").html(options.msg);
								$('#level-two').removeClass('hidden');
								$('.div-product-name').addClass('hidden');
							} else {
								alert(options.msg);
							}
							$.unblockUI();
						});
					} else {
						$.unblockUI();
						$('[name~=category]').val();
					}
				} catch(err){
					alert(err);
					$.unblockUI();
				}
				
			}
			else
			{
				$.post(BASE_URL + "resources/ajax-files/select-current-product.jsp",{email:clientEmail_val,product:product_val},function(options)
				{
					options = $.trim(options);
					options = $.parseJSON(options);
					if(options.status == 'success') {
						$("select#idFromOtherTable").html("").html(options.msg);
						
					} else {
						$("select#idFromOtherTable").html("");
						$("select#productType").html("");
						alert(options.msg);
					}
					if(one_done) {
						$.unblockUI();
					} else {
						one_done = true;
					}
				});
				$.post(BASE_URL + "resources/ajax-files/select-invoice-product.jsp",{product:product_val},function(options){
					options = $.trim(options);
					options = $.parseJSON(options);
					if(options.status == 'success') {
						$("select#productType").html("").html(options.msg);		
					} else {
						$("select#idFromOtherTable").html("");
						$("select#productType").html("");
						alert(options.msg);
					}
					if(one_done) {
						$.unblockUI();
					} else {
						one_done = true;
					}
				});
				$('#level-one').removeClass('hidden');
				$('#level-two').removeClass('hidden');
				updateCode(product_val);
				$('.supportExtension').css('display', 'none');
				$('.productTypes').css('display', 'block');
			}
		} else{
			$("select#idFromOtherTable").html("");
			$("select#productType").html("");
			alert("Client ID Not Found");
		}
	}
	
	function updateCode(product_val) {
		if(product_val == "501" || product_val == 501 || product_val == 'switch')
		{
			$("span#code").html("Reference");
		}
		else if(product_val == "3" || product_val == 3 || product_val == 'mobileMoneyAgent')
		{
			$("span#code").html("IMEI");
		}
		else 
		{
			$("span#code").html("Operator Code");
		}
	}
	$('#productType').change(getPrice);
	$('#currency').change(getPrice);
	function getPrice(){
		$.blockUI();
		var prodID = $('#productType').val();
		var currency = $('#currency').val();
		var type;
		if(prodID.length> 0 && currency.length > 0)
		{
			$.ajax({url: BASE_URL + "resources/ajax-files/get-product-price-by-id.jsp",
				dataType: 'JSON', type: 'POST', data: ({productID:prodID,currency:currency}), success: function(data){
					if(data.status == 'success')
					{
						$('#invoiceCharge').val(data.msg);
						$('#discount').val(0);
						$('#totPayable').val(($.trim(data.msg) * 1).toFixed(2));
						$('#level-four').removeClass('hidden');
						var operatorCode = $('#idFromOtherTable option:selected').text();
						var url = '';
						var icon = 'photoIcon';
						var splash = 'photoSplash';
						if(data.subtype == 'signing') {
							$('.signing').removeClass('hidden');
//							$('.signing input').attr('required', true);
							type = data.type;
							try {
								if(type == 'symbian') {
									$('.playstorelink').css('display', 'none');
									$('#playstorelink').attr('required', false);
									$('.af-cf').removeClass('clearfix').css('display', 'none');
									$('.file').removeClass('file');
								}
								$.ajax({url: BASE_URL + 'resources/ajax-files/rims-platinum-signing.jsp', type: 'POST', dataType: 'JSON', data: ({operatorCode: operatorCode, type: type}),
									success: function(data){
										try {
										if(data.status == 'success') {
											$('#applicationname').val(data.applicationName);
											$('#iconname').val(data.applicationName);
											if(data.id > 0) {
												$('#playstorelink').val(data.playstorelink);
												$('#icon').css('display', 'none');
												$('#icon').attr('required', false);
												$('#splashscreen').css('display', 'none');
												$('#splashscreen').attr('required', false);
												$('#additionalfiles').attr('required', false);
												if(type == 'symbian') {
													url = BASE_URL + 'resources/images/previous.jpg';
													$('#splashImg').attr('src', url);
													$('#iconImg').attr('src', url);
													$('#iconname').css('display', none);
												} else {
													$('#iconname').attr('required', true);
													url = BASE_URL + 'resources/ajax-files/get-file-from-database.jsp?db=2&id='+ data.id + '&name=' + splash + '&type=' + type
													$('#splashImg').attr('src', url);
													url = BASE_URL + 'resources/ajax-files/get-file-from-database.jsp?db=2&id='+ data.id + '&name=' + icon + '&type=' + type;
													$('#iconImg').attr('src', url);
												}
											} else {
												$('.signing-img').css('display', 'none');
											}
										} else {
											alert(data.msg);						
										}
										$.unblockUI();
									} catch(err) {
										alert('kichumichu ' + err);
										$.unblockUI();
									}
									}, error: function (x, y, z){
										alert('Techinal error occured. Please Inform RIMS Admin');
										$.unblockUI();
								}});
							} catch(err) {
								alert('hmm ' + err);
								$.unblockUI();
							}
						} else {
							$.unblockUI();
							$('.signing input').attr('required', false);
							$('.signing').addClass('hidden');
						}
					} else {
						 $.unblockUI();
						alert(data.msg);
					}
				}, error: function(x,h,e){alert('r ki ' + e); $.unblockUI();}});
		}
	}
	$('.btn-change').click(function(e) {
		e.preventDefault();
		var type = $(this).attr('accesskey');
		$('.img-' + type).css('display', 'none');
		$('.input-' + type).css('display', 'block');
		$('.input-' + type).attr('required', true)
	})
	$('#discount').change(getTotalPayable);
	$('#tax').change(getTotalPayable)
	$('#insmntAmount').keyup(calculateBalance);
	$('#addInstallment').click(addInstallment);
	function getTotalPayable()
	{
		var basicCharge = $('#invoiceCharge').val()*1;
		var discount = $('#discount').val()*1;
		var tax = $('#tax').val()*1;
		if(basicCharge == 0 || basicCharge > discount)
		{
			$('#totPayable').val((eval(basicCharge-discount+tax) * 1).toFixed(2));
		}
		else
		{
			alert('Discount can not be more than basic charge!');
			$('#discount').val('');
			$('#totPayable').val((eval(basicCharge+tax) * 1).toFixed(2));
		}
	}
	function addInstallment()
	{
		var form=document.forms[0];
		var totPayable = form.totPayable.value;
		var insmntDate = form.insmntDate.value;
		var insmntAmount = form.insmntAmount.value;
		var insmntBalance = form.insmntBalance.value;
		advancedAddInstallment(totPayable, insmntDate, insmntAmount, insmntBalance, true);
		return false;
	}
	
	function advancedAddInstallment(totPayable,insmntDate,insmntAmount,insmntBalance, addToTable)
	{
		var form=document.forms[0];
		if(totPayable >= 0){
			try {
				if(insmntBalance>=0)
				{
					val= insmntDate+":"+insmntAmount+":"+insmntBalance;				
					if(calculateBalance())
					{
						form.invcInstallments.appendChild(new Option(val,val));
						if(addToTable) 
						{
							var installmentNo = form.invcInstallments.length;
							if($('[name~=downPayment]').val() * 1 > 0) {
								installmentNo--;
							}
							addInstallmentIntoTable(insmntDate,insmntAmount,insmntBalance,installmentNo);
						}
					}
					form.insmntDate.value = '';
					form.insmntAmount.value = '';
				}
				else
				{
					alert('Balance can not be 0');
				}
			} catch (err){alert(err)}
		}
		else{
			alert('Total payable must be > 0');
		}
		return false;
	}
	function addInstallmentIntoTable(date,amount,due,installmentNo)
	{
		var str = document.getElementById("divInstalmentTable").innerHTML;
		var index = str.indexOf('</tbody>');
		str = str.substring(0,index);
		str+='<tr id="productno'+installmentNo+'">';
		str+='<td>'+date+'</td><td>'+amount+'</td><td>'+due+'</td>';
		str+='<td>'+'<a href="#" onclick="removeInst('+installmentNo+'); return false;">Remove</a>'+'</td>';
		str+='</tr></tbody></table>';
		document.getElementById("divInstalmentTable").innerHTML = str;
	}
	function calculateBalance()
	{
		var form=document.forms[0];
		var installmentAmount = form.insmntAmount.value*1;
		return advancedCalculateBalance(installmentAmount);
	}
	function advancedCalculateBalance(installmentAmount)
	{
		var form=document.forms[0];
		var len = form.invcInstallments.length;
		var price=form.totPayable.value*1;
		var downPayment = $('[name~=downPayment]').val()*1;
		var amount = 0;
		var balance = 0;
		if(len==0)
		{
			balance = price-installmentAmount*1;
			if(installmentAmount == 0) {
				 balance -= downPayment;
			}
		}
		else
		{
			try {
				for(j=0;j<len;j++ )
				{
					form.invcInstallments.options[j].selected = true;
				}
				for (i=0;i<len;i++)
				{
					if(form.invcInstallments.options[i].selected)
					{
						chosen = form.invcInstallments[i].value;
						values=chosen.split(":");
						amount+=parseFloat(values[1]);
					}
				}
				balance=price-(amount+installmentAmount);
			} catch(err){alert('here ' +err)}
		}
		if(balance >= 0)
		{
			form.insmntBalance.value = balance;
			return true;
		}
		else
		{
			alert("Balance Must Be >=0. ");
			form.insmntAmount.value = 0;
			return false;
		}
	}
});
$(document).ready(function(){
	
	$(".file").change(function (e) {
		if($(this).hasClass('file') == false) {
			return;
		}
		var _URL = window.URL || window.webkitURL;
	    var file, img, dimension;
	    var obj = $(this);
	    try {
	    	dimension = "{" + $(this).attr('title') + "}";
	    	dimension = $.parseJSON(dimension);
	    	if ((file = this.files[0])) {
		        try {
		        	img = new Image();
		        	img.onload = function() {
		        		if (dimension.minHeight > this.height) {
		        			alert("Min allowed height is "+  dimension.minHeight + "px and you selected " + this.height + "px. Please select correct file");
		        			obj.val('');
		        		} else if(dimension.maxHeight < this.height) {
		        			alert("Max allowed height is "+  dimension.maxHeight + "px and you selected " + this.height + "px. Please select correct file");
		        			obj.val('');
		        		}
		            };
		            img.onerror = function() {
		                alert( "not a valid file: " + file.type);
		            };
		            img.src = _URL.createObjectURL(file);
		        } catch (err) {
			    	alert(err);
			    }
		    } else {
		    	alert(':(');
		    }
	    } catch (err) {
	    	alert(err);
	    }
	});
	
	
	$('form').submit(function(){
		$('#submitButton').attr('disabled', true);
		try {
			if($('#invcRcClientID').val().length > 0){
				if($('#idFromOtherTable').val() != 'select'){
					if($('#productType').val() != 'select'){
						if($('#insmntBalance').val()*1 == 0){
							selectAllOptions();
							return true;
						}else{alert('Installment balance should be 0');}
					}else{alert('Please select a product first');}
				}else{alert('Please select an operator code or reference');}
			}else{alert('Please select a customer first!');}
			$('#submitButton').attr('disabled', false);
		} catch(err) {
			alert(err);
		}
		return false;
	});
	$('#idFromOtherTable').change(function(){
		$.blockUI();
		var email = $("#rcEmail").html();
		var prodTableID = $('#idFromOtherTable').val();
		var currency = $('#currency').val();
		var category = $('[name~=category]:checked').val();
		if($('#productType').val() == '111')
		{
			$.post(BASE_URL + "resources/ajax-files/get-invoice-list.jsp",{email: email,prodTableID: prodTableID, category: category}, function(data){
				data = $.parseJSON($.trim(data));
				if(data.status == 'success') {
					$("select#parentInvcID").html(data.msg);
					$('#level-three').removeClass('hidden');
				} else {
					$("select#parentInvcID").html('');
					alert(data.msg);
				}
				$.unblockUI();
			});
		} else {
			$('#level-three').removeClass('hidden');
			 $.unblockUI();
		}
	});
	$('#parentInvcID').change(function(){
		$.blockUI();
		var invcID = $('#parentInvcID').val();
		var currency = $('#currency').val();
		if($('#productType').val() == '111')
		{
			$('.signing').addClass('hidden');
			$('.signing input').attr('required', false);
			$.post(BASE_URL + "resources/ajax-files/get-after-sales-service-charge.jsp",{invcID: invcID, currency: currency}, function(data){
				data = $.parseJSON($.trim(data));
				if(data.status = 'success') {
					$('#invoiceCharge').val(data.msg);
					$('#totPayable').val((data.msg * 1).toFixed(2));
					$('#invoiceCharge').val(data.msg);
					$('#invcID').val(invcID);
					$('#level-four').removeClass('hidden');
				} else {
					alert(data.msg)
				}
				$.unblockUI();
			});
		}
	});
	$('.seFields').change(function(){
		var days = $('#days').val();
		var months = $('#months').val();
		var years = $('#years').val();
		var invoiceCharge = $('#invoiceCharge').val()*1;
		if(invoiceCharge > 0)
		{
			var basicCharge = (invoiceCharge*1/365) * (years*365+months*30+days*1);
			if(basicCharge > 0)
			{
				$('#invoiceCharge').val(Math.round(basicCharge));
				$('#totPayable').val((Math.round(basicCharge) * 1).toFixed(2));
			}
		}
	});
});