$(document).ready( function() {
	var Stype = $('#serverType').val();
	$.post(BASE_URL + 'resources/ajax-files/get-hosted-product-details.jsp',{type: Stype},function(xml){
		var message_nodes = xml.getElementsByTagName("message"); 
		var n_messages = message_nodes.length;
		var serviceDesc = document.getElementById("productID");
		$("#productID").html("");
		for (i = 0; i < n_messages; i++){
			var product_id = message_nodes[i].getElementsByTagName("product_id");
			var product_name = message_nodes[i].getElementsByTagName("product_name");
			serviceDesc.options[serviceDesc.options.length] = new Option(product_name[0].firstChild.nodeValue, product_id[0].firstChild.nodeValue, false, false);
		}
	});
	if (Stype * 1 == 1) {
		$('#maxHostTr').css('display', 'table-row');
	} else {
		$('#maxHostTr').css('display', 'none');
	}
	$('#addIP').click(function() {
		try {
			var form = document.forms[0];
			val = form.txtSwitch.value;
			var ob = form.txtSwitch;
			if (val == '' || isValidIP(val) == false) {
				alert('Enter Valid IP')
				ob.focus();
				return false;
			}
			for (i = 0; i < form.serverIP.length; i++) {
				index = form.serverIP[i].text.indexOf(":");
				str = form.serverIP[i].text.substring(0, index);
				if (str == form.txtSwitch.value) {
					alert('IP: ' + form.txtSwitch.value + ' already added.');
					ob.focus();
					return false;
				}
			}

			if (val.length != 0) {
				form.serverIP[form.serverIP.options.length] = new Option(
						val, val, false, false);
				form.txtSwitch.value = '';
				return true;
			}
		} catch(err) {
			alert(err);
		}
		return false;
	});
	$('#removeIP').click(function() {
		var form = document.forms[0];
		index = form.serverIP.selectedIndex;
		if (index != -1) {
			if (form.serverIP.options[index].selected)
				form.serverIP.remove(index);
		}
	});
	$('#serverType').change(function(){
		var type = $('#serverType').val();
		$.ajax({url:BASE_URL+"resources/ajax-files/get-rims-hosted-products-details.jsp",type:"POST",dataType:"html",data:({serverType:type}),success:function(value)
			{
				if(value!=""){
					$('#serviceDesc').html("").html("<option value=''>Select</option>"+value.trim());
				}else{
					$('#serviceDesc').html("").html("<option value=''>Select</option>");
				}
				$('.loading-point').html("").html("");
			},error:function(xhr,textStatus,error){alert(error)}
		});
		if (type * 1 == 1) {
			$('#maxHostTr').css('display', 'table-row');
		} else {
			$('#maxHostTr').css('display', 'none');
		}
	});
	/*SERVER ADDITIONAL IP*/
	$('#totalHosted').keyup(function(){
		var totalH=$('#totalHosted').val();
		var mxH=$('#maxHosting').val();
		if(totalH!=0 && totalH>mxH){
			$('.err-msg').html('').html('Can not be greater than maximum hosting').addClass('alert alert-warning')
			$('#totalHosted').effect("shake", 1000)
			$('#totalHosted').val(mxH);
		} else {
			$('.err-msg').html('').removeClass('alert alert-warning');
		}
	});
	
	/*******/
});