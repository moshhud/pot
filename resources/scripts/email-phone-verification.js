$(document).ready( function() {
	$('.verify').click( function(event) {
		event.preventDefault();
		var obj = $(this);
		var val = $(obj).html();
		var checkStat = false;
		var country = $(obj).attr('id');
		if(country != '') {
			$.ajax( {
				type : "POST",
				data : {
					country : country,
					phoneNo : val
				},
				url : BASE_URL + "resources/ajax-files/phone-no-and-country-check.jsp",
				success : function(data) {
					status = $.trim(data);
					if (status == 'invalid') {
						if(confirm('Country And Phone No Does Not Match! Continue?')) {
							verify();
						}
					} else {
						verify();
					}
				}
			});
		} else {
			verify();
		}
		function verify() {
			$.ajax( {
				type : "POST",
				data : {
					val : val
				},
				url : BASE_URL + "resources/ajax-files/email-phone-verification.jsp",
				success : function(data) {
					status = $.trim(data);
					if (status == true || status == 'true') {
						$(obj).attr('class', 'verified');
						$(obj).css({'color': 'green!important', 'text-decoration': 'none'});
						alert('Successfully Verified');
					} else {
						$(obj).attr('class', 'unverified');
						alert('Sorry, verification could not be performed');
					}
				}, error: function(xhr, textStatus, error) {
					alert(error);
				}
			});
		}
	});

});