
function addIPChanger() {
	var form = document.forms[0];
	val = form.ipChangerIPTxt.value;

	var ob = form.ipChangerIPTxt;

	if (isEmpty(ob.value)) {
		ob.focus();
		return false;
	}

	if (!isEmpty(ob.value) && !isValidIP(ob.value) && !checkDomain(ob.value)) {
		alert("Please put a valid IP.");
		ob.focus();
		return false;
	}

	for (i = 0; i < form.ipPortOfIPChangerSelect.length; i++) {
		index = form.ipPortOfIPChangerSelect.options[i].text;
		str = form.ipPortOfIPChangerSelect[i].text;
		if (str == form.ipChangerIPTxt.value) {
			alert('IP ' + form.ipChangerIPTxt.value + ' already added.');
			ob.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.ipPortOfIPChangerSelect[form.ipPortOfIPChangerSelect.options.length] = new Option(
				val, val, false, false);
		form.ipChangerIPTxt.value = '';
		return true;
	}

	return false;
}

function addIPChangerPublicIp() {
	var form = document.forms[0];
	val = form.ipChangerPublicIPTxt.value;

	var ob = form.ipChangerPublicIPTxt;

	if (isEmpty(ob.value)) {
		ob.focus();
		return false;
	}

	if (!isEmpty(ob.value) && !isValidIP(ob.value) && !checkDomain(ob.value)) {
		alert("Please put a valid IP.");
		ob.focus();
		return false;
	}

	for (i = 0; i < form.ipPortOfIPChangerPublicIpSelect.length; i++) {
		str = form.ipPortOfIPChangerPublicIpSelect[i].text;

		if (str == form.ipChangerPublicIPTxt.value) {
			alert('IP ' + form.ipChangerPublicIPTxt.value + ' already added.');
			ob.focus();
			return false;
		}
	}

	if (val.length != 0) {
		form.ipPortOfIPChangerPublicIpSelect[form.ipPortOfIPChangerPublicIpSelect.options.length] = new Option(
				val, val, false, false);
		form.ipChangerPublicIPTxt.value = '';
		return true;
	}

	return false;
}

function removeIPChanger() {
	var form = document.forms[0];
	index = form.ipPortOfIPChangerSelect.selectedIndex;
	if (index != -1) {
		if (form.ipPortOfIPChangerSelect.options[index].selected)
			form.ipPortOfIPChangerSelect.remove(index);
	}
}

function removeIPChangerPublicIp() {
	var form = document.forms[0];
	index = form.ipPortOfIPChangerPublicIpSelect.selectedIndex;
	if (index != -1) {
		if (form.ipPortOfIPChangerPublicIpSelect.options[index].selected)
			form.ipPortOfIPChangerPublicIpSelect.remove(index);
	}
}
