$(document).ready(function(){
	check_for_notifications();
	
	function check_for_notifications() {
		$.ajax({url: BASE_URL + 'resources/ajax-files/notifications.jsp',
			type: 'POST',
			data: ({type: 'getNotifications'}),
			dataType: 'JSON',
			success: function(data) {
				try {
					if(data.status) {
						$('#notification_count').html('').html(data.count);
						a = JSON.parse(data.notifications);
						for(i = 0; i<a.length; i++) {
							tag = '{"id":' + a[i].id + ', "module": "' + a[i].module + '", "table_id": ' + a[i].table_id + '}';
							show(a[i].module, a[i].message, tag);
						}
					}
					setTimeout(check_for_notifications, 60000);
				} catch (err) {
//					alert(JSON.stringify(data));
				}
			},
			error: function (x, y, z) {
//				alert('Error occured, Update Web Admin');
			}
		});
//		
	}
	
	function show(title, body, tag) {
		Notification.requestPermission(function() {
			var notification = new Notification(title, {
			  body: body,
			  tag: tag,
			});
			notification.onclick = function() {
				try {
					tag = JSON.parse(notification.tag);
					$.ajax({url: BASE_URL + 'resources/ajax-files/notifications.jsp',
						type: 'POST',
						data: ({type: 'seen', id: tag.id}),
					});
					if(tag.module == 'Platinum (Android) Signing') {
						window.location = BASE_URL + 'rims-platinum-android/search-rims-platinum-android.html'
					} else if (tag.module == 'Invoice Delete') {
						window.location = BASE_URL + 'mdinvoice/get-mdinvoice-delete.html?invcID=' + tag.table_id
					} else if (tag.module == 'Hosted Service') {
						window.location = BASE_URL + 'rims-hosted-service/get-rims-hosted-service.html?hsHostingID=' + tag.table_id
					}
					notification.close();
				} catch(err) {
					alert(err);
				}
			};
//			notification.onclose = function () {
//				tag = JSON.parse(notification.tag);
//				$.ajax({url: BASE_URL + 'resources/ajax-files/notifications.jsp',
//					type: 'POST',
//					data: ({type: 'seen', id: tag.id}),
//				});
//				notification.close();
//			};
		})
	}
	
});


