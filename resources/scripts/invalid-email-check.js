$(document).ready(function(){
	var email = $('.emailCheck').html();
	$.ajax({url: BASE_URL + 'resources/ajax-files/check-invalid-email.jsp', dataType: 'JSON', type: 'POST', data:({email: email}), success: function(data){
		if(data.status == 'invalid') {
			$('.emailCheck').addClass('label label-danger');
			$('.emailCheck').attr('title', 'Invalid Email');
			alert('Email ' + email + ' has been found invalid. Please update')
		}
	}, error: function(e, x, h){
		alert(h);
	}});
});