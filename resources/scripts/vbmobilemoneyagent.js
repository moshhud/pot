$(document).ready(function(){
	rcEmail = $('#rcEmail')
	rcClientID = $('#rcClientID');
	rcClientName = $('#rcClientName');
	rcCellPhone = $('#rcCellPhone');
	rcCompanyName = $('#rcCompanyName');
	rcCountry = $('#rcCountry');
	rcAccountManagerID = $('#rcAccountManagerID');
	rcCountryVal= 0;
	rcAccountManagerIDVal = 0;
	rcEmail.focus(reset_form);
	selectedValues = $('#selectedValues');
	itemIndex = 0;
	
	rcAccountManagerID.change(function (e) {
		if(rcClientID.val() * 1 > 0) {
			e.preventDefault();
			alert('To edit client details use Master Creation>Search Customer>Edit');
			rcAccountManagerID.val(rcAccountManagerIDVal);
		}
	});
	
	rcCountry.change(function (e) {
		if(rcClientID.val() * 1 > 0) {
			e.preventDefault();
			alert('To edit client details use Master Creation>Search Customer>Edit')
			rcCountry.val(rcCountryVal);
		}
	});
	
	$('#submit').click(function(e){
    	e.preventDefault();
    	$('#submit').attr('disabled', false);
    	$.blockUI();
    	if(validate()) { 
    		try {
    			$.ajax({url: BASE_URL + 'resources/ajax-files/vbmobilemoneyagent.jsp', type: 'POST', dataType: 'JSON',
        			data:({type: 'add', values: selectedValues.val(), rcClientID: rcClientID.val(), rcEmail: rcEmail.val(), rcClientName: rcClientName.val(), rcCellPhone: rcCellPhone.val(), rcCompanyName: rcCompanyName.val(), rcCountry: rcCountry.val(), rcAccountManagerID: rcAccountManagerID.val() }),
        			success: function(data) {
        				if(data.status == 'success') {
        					alert('Successfully added');
        					location.reload();
        				} else {
        					alert(data.msg);
        				}
        	    		$.unblockUI();
        	    		$('#submit').attr('disabled', false);
        			}, error: function (x, y, z) {
        	    		$.unblockUI();
        	    		$('#submit').attr('disabled', false);
        				alert(x + ' ' + y + ' ' + z);
        			}
        		})
    		} catch (err) {
    			alert(err);
    		}
    	} else {
    		$.unblockUI();
    		$('#submit').attr('disabled', false);
    	}
    });
    
    function validate() {
    	isValid = false;
    	try {
    		if(rcEmail.val().length > 0) {
        			if(rcClientName.val().length > 0) {
        				if(rcCellPhone.val().length > 0) {
        					if(rcCompanyName.val().length > 0) {
        						if(rcCountry.val().length > 0) {
        							if(selectedValues.val().length > 0) {
        								isValid = true;
        					    	} else {
        					    		rcEmail.focus();
        					    		alert("Enter IMEI, IP and PORT");
        					    	}
        				    	} else {
        				    		rcCountry.focus();
        				    		alert("Select Country");
        				    	}
        			    	} else {
        			    		rcEmail.focus();
        			    		alert("Enter Company Name");
        			    	}
        		    	} else {
        		    		rcCellPhone.focus();
        		    		alert("Enter Contact Number");
        		    	}
        	    	} else {
        	    		rcClientName.focus();
        	    		alert("Enter Client Name");
        	    	}
        	} else {
        		rcEmail.focus();
        		alert("Enter email");
        	}
    	} catch(err) {alert(err)}
    	return isValid;
    }
	
	
	
	$('.btn-check').click(function(){
		var email = rcEmail.val();
		if(email != undefined && email.indexOf('@') > 0) {
			$.ajax({url: BASE_URL + 'resources/ajax-files/vbmobilemoneyagent.jsp', type: 'POST', dataType: 'JSON',
				data: ({type: 'rc', rcEmail: email}), success: function(data) {
					if(data.status == 'success') {
						rcClientID.val(data.rcClientID)
						rcClientName.val(data.rcClientName);
						rcCellPhone.val(data.rcCellPhone);
						rcCompanyName.val(data.rcCompanyName);
						rcCountry.val(data.rcCountry);
						rcAccountManagerID.val(data.rcAccountManagerID);
						rcCountryVal = data.rcCountry;
						rcAccountManagerIDVal = data.rcAccountManagerID;
						show_hideme(true);
					} else if(data.status == 'no_data_found') {
 						if(confirm('No client found with this email. Would you like to add new?')) {
							show_hideme(false);
 						}
					} else {
						alert(data.msg);
					}
				}, error: function (x, y, z) {
				}});
			
		} else {
			alert('Enter a valid email ID');
		}
		
	});
	
	function show_hideme(isReadOnly) {
		$('.check-container').addClass('hidden');
		$('.hideme').removeClass('hidden');
		if(isReadOnly) {
			$('.rc').attr('readonly', true);
		} else {
			$('.rcClientID').addClass('hidden');
		}
	}
	function reset_form() {
		$('.check-container').removeClass('hidden');
		$('.resetme').each(function(index){
			$(this).val('');
			$(this).attr('readonly', false);
		});
		$('#details').html('');
		$('.hideme').addClass('hidden');
		$('#submit').attr('disabled', true);
	}
	
	$('#add').click(function(){
		try {
			imei = $('#imei').val();
			ip = $('#ip').val();
			port = $('#port').val();
			if(imei != undefined && imei.length > 0) {
				if(ip != undefined && ip.length >= 7 && ip.length <= 15) {
					if(port != undefined && port.length > 0) {
						add_details(imei, ip, port);
					} else {
						$('#port').focus();
						alert('Enter valid Port');
					}
				} else {
					$('#ip').focus();
					alert('Enter valid IP');
				}
			} else {
				$('#imei').focus();
				alert('Enter valid IMEI No');
			}
		} catch (err) {alert(err)}
	});
	
	
	function add_details(imei, ip, port)
    {
        var currentValue = selectedValues.val();
        
        if(currentValue != undefined && currentValue.length > 0 && find_it($.parseJSON(currentValue), 'imei', imei)) {
        	alert('IMEI already exists!');
        	return false;
        }
        if (currentValue === undefined || currentValue.length == 0) {
            currentValue = '[';
        } else {
            currentValue = currentValue.substr(0, currentValue.length - 1) + ",";
        }
        itemIndex++;
        currentValue += '{"imei": "' + imei + '","ip": "' + ip + '","port": "' + port + '","id": "' + itemIndex + '"}]';
        selectedValues.val(currentValue);
        add_to_table(imei, ip, port);
        reset_details();
        return true;
    }
	
	function add_to_table(imei, ip, port)
    {
        var str = document.getElementById("details").innerHTML;
        str += '<tr id="tr_' + itemIndex + '">';
        str += '<td>' + imei + '</td><td>' + ip + '</td><td>' + port + '</td><td><a class="btn btn-danger remove" accesskey="' + itemIndex + '">Remove</a></td>';
        str += '</tr>';
        document.getElementById("details").innerHTML = str;
        $('#submit').attr('disabled', false);
    }
	
	function reset_details() {
		$('.details').each(function(index){
			$(this).val('');
		});
	};
	
	$(document).on('click', '.remove' , function(e){
        try {
            e.preventDefault();
            var id = $(this).attr('accesskey');
            var selectedItems = $.parseJSON(selectedValues.val());
            selectedItems = removeItem(selectedItems, 'id', id);
            selectedItems = JSON.stringify(selectedItems);
            selectedItems = selectedItems.replace("null,", "");
            selectedValues.val(selectedItems);
            remove_row_from_table(id);
        } catch(err) {alert('removeItem ' + err)};
        
    });
    
    function removeItem(obj, prop, val) {
        var c, found=false;
        for(c in obj) {
            if(obj[c] != null) {
                if(obj[c][prop] == val) {
                    found=true;
                    break;
                }
            }
        }
        if(found){
            delete obj[c];
        }
        return obj;
    }
    
    function find_it(obj, prop, val) {
        var c, found=false;
        for(c in obj) {
            if(obj[c] != null) {
                if(obj[c][prop] == val) {
                    found=true;
                    break;
                }
            }
        }
        return found;
    }
    
    function remove_row_from_table(id) {
        $("#tr_" + id).remove();
    }
    
    
    $('.btn-remove').click(function(e){
    	
    	try {
    	    var mmIMEI = $(this).attr('accesskey');   	    
    	
    		if(mmIMEI > 0) {
    			$.blockUI();
        		if(confirm('This action will delete the record. Do you want to continue?')) {
        			$.ajax({url: BASE_URL + '/resources/ajax-files/vbmobilemoneyagent.jsp', type: 'POST', dataType: 'JSON', data: ({type:'delete', mmIMEI: mmIMEI}),
        				success: function(data) {
        					alert(data.msg);
        					if(data.status == 'success') {
        						$("#tr_" + mmIMEI).remove();
        					}
        					$.unblockUI();
        				}, error: function (x, y, z) {
        					alert('ops Technical Error occured, Please contact RIMS Admin');
        					$.unblockUI();
        				}
    				})
        		} else {
        			$.unblockUI();
        		}
        	} else {
        		alert('Technical Error occured, Please contact RIMS Admin');
        	}
    	} catch (err) {
    		alert('Technical Error occured, Please contact RIMS Admin' + err);
    	}
    	
    });
    
})