$(document).ready(function(){
	$('#serverType').change(function(){
		$('#price').val("0.00");
		$('#serviceDesc').html("");
		var selectedType=$(this).val();
		if(selectedType != null && selectedType!=""){
			$.ajax({url:BASE_URL+"resources/ajax-files/get-rims-hosted-products-details.jsp",type:"POST",dataType:"html",data:({serverType:selectedType}),success:function(value)
				{
					if(value!=""){
						$('#serviceDesc').html("").html("<option value=''>Select</option>"+value.trim());
					}else{
						$('#serviceDesc').html("").html("<option value=''>Select</option>");
					}
					$('.loading-point').html("").html("");
				},error:function(xhr,textStatus,error){alert(error)}
			});
		}else{
			$('#serviceDesc').html("<option value=''>Select</option>").css({'border':'1px solid #E8E6E6'});
			$('#serverType').css({'border':'1px solid #E8E6E6'});
			$('.loading-point').html("").html("");
		}
	});
	$('#serverID').change(function() {
		var val = $(this).val() * 1;
		if (val > 0) {
			$.post(BASE_URL + 'resources/ajax-files/get-rims-colo-servers-details.jsp', {
				type : "serverIP",
				val : val
			}, function(data) {
				$('#serverIP').html($.trim(data));
			});
		} else {
			alert('Invalid Data!');
		}
	});
	$('#serviceDesc').change(function(){
		var id=$(this).val();
		if(id>0){
			$.ajax({url: BASE_URL + "resources/ajax-files/get-rims-hosted-products-price.jsp",type:"POST",dataType:"JSON",data:({id:id}),success:function(value)
				{
					if(value.status = 'success'){
						$('#price,.invoice-amount').val("").val(value.msg);
					}else{
						alert(value.msg);
						$('#price,.invoice-amount').val("0.00");
					}
				},error:function(xhr,textStatus,error){alert('error' + error)}
			});
		}else{
			//to do here//
		}
	});
	$('.rent-slot').click(function(){
		var monthlyRent = $('#price').val();
		var multiplyBy = $(this).attr('accesskey');
		var invoiceAmount = monthlyRent*multiplyBy;
		$('.invoice-amount').val("0.00").val(invoiceAmount.toFixed(2));
	});
	
	$('#coupleWith').change(function(){
		var productType=$(this).val();
		var customerEmail=$('#rcEmail').html();
		if(productType!=""){
			if(customerEmail!=""){
				if(productType==1){
					$('.product-type').html("").html("Operator Code");
				}else {
					$('.product-type').html("").html("Reference");
				}
				$.ajax({url:BASE_URL+"resources/ajax-files/select-current-product.jsp",type:"POST",dataType:"JSON",data:({email:customerEmail,product:productType, module: "hosted_service"}),success:function(value)
					{
						$('#product-code').html("").html(value.msg);
					},error:function(xhr,textStatus,error){alert(xhr.status)}
				});
			}else{
				alert("Please search a customer to see his existing product.");
				$('.js_note').html("").html("Please search a customer to see his existing product.").css({'color':'red','padding-left':'27px'})
				$('#coupleWith').val("");
			}
		}else{
			$('.product-type').html("").html("Reference");
		}
	});
	$('.request-for-discontinue').click(function(evt){
		evt.preventDefault();
		if(checkSelectBox('select-box')){
			var con = confirm("Are you sure you want to discontinue?");
			if(con){
				var checkedIDs = "";
				$('.select-box').each(function(){
					if($(this).is(':checked')){
						checkedIDs +=$(this).attr('accesskey')+",";
					}
				});
				if(checkedIDs != ""){
					$(this).css('visibility','hidden');
					checkedIDs = checkedIDs.substring(0,checkedIDs.length-1);
					$.ajax({url: BASE_URL + "resources/ajax-files/request-for-hs-discontinue.jsp",data:({IDs: checkedIDs, type: $(this).attr('title')}),type:"POST",dataType:"JSON",success:function(value)
						{
							alert(value.msg);
							location.reload();
						},error:function(xhr,textStatus,error){alert(xhr.status)}
					});
				}
			}else{
				return;
			}
		}else{
			alert("Select at least one data");
		}
	});
	function checkSelectBox(className){
		var checked=0;
		var result = false;
		$('.'+className).each(function(){
			if($(this).is(':checked')){
				checked++;
			}
		});
		if(checked>0){
			return true;
		}
		return result;
	}
});

