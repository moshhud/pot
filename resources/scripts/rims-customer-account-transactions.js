$(document).ready(function() {
	$('.ids').click(function(){
		var prefix = $(this).attr('title');
		var value = $(this).val();
		var name = null;
		var quickBook = null;
		var desc = null;
		var qbiNo = null;
		if(value.length > 0)
		{
			temp = value.split(":");
			if(temp.length > 0)
			{
				name = '[name~=' + prefix + 'invc_'+temp[0] +']';
				quickBook = '[name~=' + prefix + 'qbn_'+temp[0] +']';
				desc = '[name~=' + prefix + 'desc_'+temp[0] +']';
				qbiNo = '[name~=' + prefix + 'qbiNo_'+temp[0] +']';
				if($(this).is(':checked'))
				{
					$(name).attr('disabled', false);
					$(quickBook).attr('disabled', false);
					$(desc).attr('disabled', false);
					$(qbiNo).attr('disabled', false);
				}
				else
				{
					$(name).attr('disabled', true);
					$(quickBook).attr('disabled', true);
					$(desc).attr('disabled', true);
					$(qbiNo).attr('disabled', true);
				}
			}
		}
		else
		{
			alert('Invalid value!');
		}
	});
	$('[name~=paymentMethod]').change(function(){
		var value = $(this).val();
		$('.bank').css('display', 'none');
		if(value == 'bank')
		{
			$('.bank').css('display', 'block');
		}
		else if(value == 'select')
		{
			alert('Please Select A Payment Method');
		}
	});
})