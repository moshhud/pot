function addModule() 
{
	var form = document.forms[0];
	var ob = form.module;

	for (i = 0; i < form.modules.length; i++) {
		str = form.modules[i].text;
		if (str == ob.value) {
			alert(ob.value + ' already added.');
			document.forms[0].module[0].selected = true;
			return false;
		}
	}

	if (ob.value.length != 0) {
		form.modules[form.modules.options.length] = new Option(ob.value,
				ob.value, false, false);
		for ( var i = form.modules.length - 1; i >= 0; i--) {
			form.modules.options[i].selected = true;
		}
		return true;
	}
	return false;
}

function validateInteger(text)
{
	var charpos = text.search("[^0-9]"); 
	if( eval(text.length) > 0 &&  charpos >= 0 ) 
	{ 
		return false; 
	}

	return true;
}

function removeModule() 
{
	var form = document.forms[0];
	index = form.modules.selectedIndex;
	if (index != -1) {
		if (form.modules.options[index].selected)
			form.modules.remove(index);
	}
	for ( var i = form.modules.length - 1; i >= 0; i--) {
		form.modules.options[i].selected = true;
	}
}
function validate() 
{
	return true;
}

function showPortCapacity(val) 
{
	if (val == "Server Base") {
		document.getElementById("portCapacity").style.display = "none";
		document.forms[0].portCapacity.value = 800;
		document.forms[0].switchType.focus();
	} else {
		document.getElementById("portCapacity").style.display = "";
		document.forms[0].portCapacity.value = 0;
		document.forms[0].portCapacity.focus();
	}
}
$(document).ready(function(){
	$('form').submit(function(){
		if(this.serverIP.value.length > 0)
		{if(this.port.value.length > 0)
			{if(this.ivr.value.length > 0){var len = document.forms[0].modules.length;if(len>0){for( i=0;i<len;i++){document.forms[0].modules.options[i].selected = true;}}return true;}
				else{alert('Please enter a valid IVR.');}}
			else{alert('Please enter a valid Port.');}
		}else{alert('Please enter a valid Server IP.');}return false;
	});
});