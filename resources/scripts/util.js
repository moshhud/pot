function getSelectedIndex(displayObject, value) {
	var flag = false;
	for(var i=0; i<displayObject.options.length; i++) {
		if(displayObject.options[i].value == value) {
			displayObject.selectedIndex = i;
			flag = true;
			break;
		}
	} 
	if(flag == false) {
		displayObject.options[0].value = value;
		displayObject.options[0].text = value;
	}
}


function validateRequired(text) 
{
	if(eval(text.length) == 0) 
    { 
		return false; 
    }
    return true;
}

function notNull(text) 
{
	if(eval(text.length) == 0) 
    { 
		alert("Enter ClientID.."); 
		return false; 
    }
    return true;
}

function validateMaxLength(text,len)
{
	if(eval(text.length) >  eval(len)) 
    { 
		return false;
	}	
	return true;
}

function validateMinLength(text,len)
{
	if(eval(text.length) <  eval(len)) 
    { 
		return false;
	}	
	return true;
}

function validateEmail(text)
{
	var email = text;
    var splitted = email.match("^(.+)@(.+)$");
    if(splitted == null) return false;
    if(splitted[1] != null )
    {
      var regexp_user=/^\"?[\w-_\.]*\"?$/;
      if(splitted[1].match(regexp_user) == null) return false;
    }
    if(splitted[2] != null)
    {
      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
      if(splitted[2].match(regexp_domain) == null) 
      {
	    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
	    if(splitted[2].match(regexp_ip) == null) return false;
      }// if
      return true;
    }
	return false;
}


function validateGT(text,value) 
{
	if(isNaN(text)) 
    { 
         return false; 
    }
    
    if(eval(text) <=  eval(value)) 
    { 
		return false;
	}
	return true;
}

function validateLT(text,value) 
{
	if(isNaN(text)) 
    { 
         return false; 
    }
    
    if(eval(text) >=  eval(value)) 
    { 
		return false;
	}
	return true;
}

function validateInteger(text)
{
	var charpos = text.search("[^0-9]"); 
	if( eval(text.length) > 0 &&  charpos >= 0 ) 
	{ 
		return false; 
	}

	return true;
}

function validateDecimal(text) 
{
	if(!text.match("^([0-9]*)(.[0-9]+)?$")) 
	{
		return false;
	}
	return true;
}

function validateAlpha(text)
{
	var charpos = text.search("[^A-Za-z]"); 
	if( eval(text.length) > 0 &&  charpos >= 0 ) 
	{ 
		return false; 
	}
	return true;
}

function validateAlphaNumeric(text)
{
	var charpos = text.search("[^A-Za-z0-9]"); 
	if( eval(text.length) > 0 &&  charpos >= 0 ) 
	{ 
		return false; 
	}
	return true;
}

function validateHEXNumeric(text)
{
	var charpos = text.search("[^A-F0-9]"); 
	if( eval(text.length) > 0 &&  charpos >= 0 ) 
	{ 
		return false; 
	}
	return true;
}


/*
case "alnumhyphen":
 search("[^A-Za-z0-9\-_]") 
*/

   function isEmpty(s)
   {
     for(var i=0;i<s.length;i++)
         if(s.charAt(i)!=' ')break;
     
     if(i==s.length)
         return true;
     else
         return false;      
   }

   function isNum(s)
   {
     for(var i=0;i<s.length;i++)
     {  
         if(s.charAt(i)=='1'||s.charAt(i)=='2'||
		s.charAt(i)=='3'||s.charAt(i)=='4'|| 
		s.charAt(i)=='5'||s.charAt(i)=='6'||
		s.charAt(i)=='7'||s.charAt(i)=='8'||
		s.charAt(i)=='9'||s.charAt(i)=='0'||
		s.charAt(i)=='.')
           continue;
         else 
           break;   
     }
     if(i==s.length)
         return true;
     else
         return false;      
   }

  function checkFromToDateForValidation(yy,mm,dd,hh,min,sec,
	                         yyTo,mmTo,ddTo,hhTo,minTo,secTo)
  {

	//var s = yy + " " + mm + " " + dd + " " + hh + " "+min + " " + sec;  
	//s += "\n";		
	//s += yyTo + " " + mmTo + " " + ddTo + " " + hhTo + " "+minTo + " " + secTo;  
    //alert(s);

	var yy = 23; //;,mm,dd,hh,min,sec,
	//yyTo,mmTo,ddTo,hhTo,minTo,secTo	
	alert(yy);
	
	var flag = false;
    if(!isYearMonthDateValid(yy, mm, dd)
       ||    !isYearMonthDateValid(yyTo, mmTo,ddTo)) {
       //alert("month check ");
       return false;
    }
      
	


    if( yy < yyTo )
      return true;
    else if( yy == yyTo )
    {
	  //alert("year equel");
      
      if( mm < mmTo )
        return true;
      else if( mm == mmTo)
      {
	    // alert("month equel");

        if( dd < ddTo )
          return true;
        else if( dd == ddTo )
        {
	     //alert("day equal");

          if( hh < hhTo)
            return true;
          else if(hh== hhTo)
          {
            if(min < minTo)
              return true;
            else if(min == minTo)
            {
              if( sec <= secTo)
                return true;
            }
          }
        }
      }
    }
    return false;
  }

  function isYearMonthDateValid(year,month,date)
  {
    var endOfMonth = -1;
    //alert("M="+month);
	
    switch(month)
    {
      case "0"://January
      case "2"://March
      case "4"://May
      case "6"://July
      case "7"://August
      case "9"://October
      case "11"://December
        endOfMonth = 31;
        break;

      case "1"://February
        endOfMonth = isLeapYear(year) ? 29 : 28;
        break;

      case "3"://April
      case "5"://June
      case "8"://September
      case "10"://November
        endOfMonth = 30;
        break;
    }
    //alert("EOM ="+endOfMonth);
    return !( date < 1 || date > endOfMonth);
  }
  
  function isLeapYear(year)
  {
    return (year % 4 == 0 && year % 100 != 0 ) || year % 400 ==0;
  }
  
  
//Sleep time in milliseconds  
function sleep(slepptime){
var sleeping=true;
var start = new Date();
var star_ms = start.getTime();
	
	while(sleeping){
	end = new Date();
    end_ms = end.getTime();
		if((end_ms-star_ms)>=slepptime )
		sleeping=false;
	}
}





// DATE Validation
var dtCh= "/";
var minYear=1900;
var maxYear=2100;

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){	
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strDay =dtStr.substring(0,pos1)
	var strMonth=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		alert("The date format should be : mm/dd/yyyy")
		return false
	}
	
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("Please enter a valid day")
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		alert("Please enter a valid month")
		return false
	}	
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("Please enter a valid 4 digit year between "+minYear+" and "+maxYear)
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
		return false
	}
return true
}


function fileFilter(filename,exts){
	//var exts='gif,jpg,pdf,doc,docx,xls,xlsx';
	//new Array('gif','jpg','pdf','doc','docx','xls','xlsx');
	var tmp=filename.split('.');
	var ext=tmp[tmp.length-1];
	if(exts.match(ext.toLowerCase()))
	return true;
	else{
	alert('You have selected '+ext+' file.\nPlease select '+exts+' file.');
	return false;
	}
	
}

function isValidIP (IPvalue) {
	IPvalue = $.trim(IPvalue);
	var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
	var ipArray = IPvalue.match(ipPattern);

	if (ipArray == null){
	return false;
	}
	else{
		for(i = 1; i < 5; i++){
			segment = parseInt(ipArray[i],10);
			if(segment > 255){
			return false;
			}
		  }
	}
	return true;
	}