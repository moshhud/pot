$(document).ready(function(){
	$('.likeUnlike').click(function(e){
		e.preventDefault();
		var postID = $(this).attr('accesskey');
		if(postID > 0){
			$.post(jsBASE_URL+'resources/ajax-files/like-count.jsp', {postID: postID}, function(data){
   	   	        var obj = jQuery.parseJSON(data);
                var status = obj.status;
                var likes = obj.countNo;
                if(status == "success"){
                	$('#likeUnlikebtn-'+postID).html("").html("Liked");
                	$('.total-like').html("").html(likes);
                }else{
                	alert(status);
                }
                $('.likeNO-'+btnSerial).html(val+" likes");   	       	
   	   	   	});
		}else{
			alert("Unexpected error! Please reload the page.");
		}
	});
	
	 $('.comments').click(function(e){
	   	   	e.preventDefault();
	   	   	var cmtSerial = $(this).attr('accesskey');
	  	   	var id = $('.idComment-'+cmtSerial).val();
	  	    var tableName = $('.tblComment-'+cmtSerial).val();
	   	   	var comment = $('.cmtComment-'+cmtSerial).val();
	   		$.post(jsBASE_URL+'resources/ajax-files/comments-post.jsp', {id: id, comment: comment, tableName: tableName}, function(data){
	   	   	$('.cmtComment-'+cmtSerial).val(""); 
	   	   	});

	 	});
	 
	 /*popular posts*/
	 $('.pr-posts-change').click(function(e){
		 e.preventDefault();
		 var postType = $(this).attr('accesskey');
		 if(postType.length > 0 ){
			 $('.pr-posts-change').css({'color':'#428BCA','text-decoration':'none','border':'none'});
			 $(this).css('color','#FF6C00');
			 if(postType == "popular" || postType == "recent"){
				 $.ajax({url:jsBASE_URL+"resources/ajax-files/magazine-pr-post.jsp",data:({postType:postType}),type:"POST",dataType:"html",success:function(response)
						{
					 		$('#pr-list').html("").html(response);
						}
					,error:function(xhr,textStatus,error)
					{
						//alert("readyState: "+xhr.readyState+"\nstatus: "+xhr.status);
					    alert(error);
					}
					});
			 }else{
				 
			 }
		 }
	 });
	 
	 //testimonials slide//
	 /*function frontBox(){
		 $('.testimonal-slot-1').css({"position":"relative"}).animate({"left":"-=100%"},4000);
		 setTimeout(function(){
			 $('.testimonal-slot-1').css({"position":"absolute","visibility":"hidden"})
		 },4000);
	 }
	 function hiddenBox(){
		 $('.testimonal-slot-2').css({"visibility":"visible","position":"absolute"}).animate({"left":"0%"},4000);
		 setTimeout(function(){
			 $('.testimonal-slot-2').css({"position":"static"})
		 },4000);
	 }
	 function frontBox2(){
		 $('.testimonal-slot-1').css({"visibility":"visible","position":"relative"}).animate({"left":"0%"},4000);
		 setTimeout(function(){
			 $('.testimonal-slot-1').css({"position":"static"})
		 },4000);
	 }
	 function hiddenBox2(){
		 $('.testimonal-slot-2').css({"position":"absolute"}).animate({"left":"+=100%"},4000);
		 setTimeout(function(){
			 $('.testimonal-slot-2').css({"visibility":"hidden"})
		 },4000);
	 }
	 $('.testimonials-next-hov-arrow').click(function(e){
		 e.preventDefault();
		 frontBox();hiddenBox();
		 $(this).css({"visibility":"hidden"});
		 $('.testimonials-prev-arrow').css({"visibility":"visible"});
	 });
	 $('.testimonials-prev-arrow').click(function(e){
		 e.preventDefault();
		 hiddenBox2();frontBox2();
		 $(this).css({"visibility":"hidden"});
		 $('.testimonials-next-hov-arrow').css({"visibility":"visible"});
	 });*/
});