$(document).ready(function(){
	$('#cmp-sending-option,#cmp-content,#cmp-recipient').removeClass('hide');
	$('.recipient-search-module').removeAttr('checked');
	if(BASE_URL.length > 0 == false){
		alert("Base URL is undefined in javascript. Please assign a javascript variable name 'BASE_URL' and set the base url ");
	}
	//load search panel//
	$('.recipient-search-module').click(function(){
		var keyVale = $(this).val();
		var checkedCount = 0;
		var searchPanelNo = $(this).attr('accesskey');
		$('.recipient-search-module').each(function(){
			if($(this).is(":checked")){
				checkedCount ++;
			}
		});
		if($(this).is(":checked")){
			var loadingDiv = "<div class='loading-img-container col-md-12 align-center'><img src='"+BASE_URL+"resources/images/campaign/loading.gif' alt='Please wait.Panel loading...'/></div>";
			$('#recipient-expand-search-panel').append(loadingDiv);
			$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/load-recipient-search-panel.jsp",
				type:"POST",dataType:"HTML",data:({searchPanelNo:searchPanelNo,keyVale:keyVale}),success:function(htmlresponse){
				if(htmlresponse.trim().length > 0){
					$('#recipient-expand-search-panel').append(htmlresponse.trim());
				}
				$('.loading-img-container').remove();
			},error:function(xhr,textStatus,error){
				alert(error);
			}
			});
		}else{
			$('#panel-'+searchPanelNo).remove();
		}
		if(checkedCount > 0){
			$('#recipient-search-area,.recipient-search-record').removeClass('hide');
		}else{
			$('#recipient-search-area,.recipient-search-record').addClass('hide');
		}
		
	});
	
	//search form submit//
	$('#recipient-search-form').submit(function(evt){
	  evt.preventDefault();
	  //console.log( $( this ).serialize() );
	  $('.recipient-search-notification').html("Please wait. We are searching.....");
	  $('.recipient-search-btn').val("Please wait...").prop('disabled',true);
	  $.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/load-recipient-search-data.jsp",
		type:"POST",dataType:"html",data:$(this).serialize(),success:function(jsonResponse){
		  	var json = $.parseJSON(jsonResponse);
		  	$('.session-key').val(json.sessionKey);
		  	$('.recipient-search-notification').html("Search Result :");
		  	if(json.status == "success"){
		  		$('.recipient-search-record').html("");
		  		for(k in json){
		  			if(k != "msg" && k != "sessionKey" && k != "status"){
		  				$('.recipient-search-record').append("<b>"+json[k]+"</b> records found from "+k+" Search <br/>");
		  			}
		  		}
		  	}else{
		  		alert(json.msg);
		  		$('.recipient-search-record').html(json.msg);
		  	}
		  	$('.recipient-search-btn').val("Search Contact").removeAttr('disabled');
			},error:function(xhr,textStatus,error){
				alert(error);
			}
		});
	});
	
	
	/****************Basic Info Form submit *****************/
	$('#basic-info-form').submit(function(evt){
		evt.preventDefault();
		if(checkValidation()){
			//console.log $(this).serialize());
			$('#basic-info-save-btn').val("Please wait...").prop('disabled',true);
			$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/campaign-basic-info-save.jsp",
				type:"POST",dataType:"html",data:$(this).serialize(),success:function(jsonResponse){
			  	var json = $.parseJSON(jsonResponse);
			  	$('.session-key').val(json.sessionKey);
			  	$('#basic-info-save-btn').val("Save & Continue").removeAttr('disabled');
			  	if(json.status == "success"){
			  		$('.cmp-basic-info-notification').html(json.msg).css('color','green');
			  		var tabCount = json.activeTab;
			  		if(tabCount > 1){
			  			for(var c = 0;c <=tabCount;c++){
			  				$("#campaign-tabs").tabs("enable",c);
			  			}
			  		}else{
			  			$("#campaign-tabs").tabs("option","disabled",[2,3]);
			  		}
			  		$("#campaign-tabs").tabs({
			  			active:1
			  		});
			  	}else{
			  		alert(json.msg);
			  		$('.cmp-basic-info-notification').html(json.msg).css('color','red');
			  	}
				},error:function(xhr,textStatus,error){
					alert(error);
				}
			});
		}else{
			alert("check form inputs!");
		}
	});

	/*******Add Link in campaign*******/
	$('.add-link-btn').click(function(){
		var link = $('.add-link-input').val();
		if(link != "" && link.length > 0){
			$('.add-link-btn').val("Wait...").prop('disabled',true);
			$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/campaign-included-link-save.jsp",
				type:"POST",dataType:"html",data:({cmpLink:link}),success:function(jsonResponse){
			  	var json = $.parseJSON(jsonResponse);
			  	$('.add-link-btn').val("ADD").removeAttr('disabled');
			  	if(json.status == "success"){
			  		$('.cmp-content-notification').html(json.msg).css('color','green');
			  		$('.links-added').prepend("<p class='"+json.processedLink+"'><span accesskey='"+json.processedLink+"' class='btn-remove'>x</span> "+json.originalLink+" > <span style='color:blue'>"+BASE_URL+"campaign/campaign-response.html?cmplid="+json.processedLink+"</span></p>");
			  		$('.add-link-input').val("");
			  	}else{
			  		alert(json.msg);
			  		$('.cmp-content-notification').html(json.msg).css('color','red');
			  	}
				},error:function(xhr,textStatus,error){
					alert(error);
				}
			});
		}else{
			alert("Please enter a link!");
		}
	});
	
	/************Content save****************/
	$('.cmp-content-save').click(function(){
		var content = CKEDITOR.instances.htmlEditor.getData();
		if(content !="" && content.length > 0){
			$('.cmp-content-save').val("Please Wait...").prop('disabled',true);
			$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/campaign-content-save.jsp",
				type:"POST",dataType:"html",data:({cmpContent:content}),success:function(jsonResponse){
			  	var json = $.parseJSON(jsonResponse);
			  	$('.cmp-content-save').val("Save & Continue").removeAttr('disabled');
			  	if(json.status == "success"){
			  		$('.cmp-content-notification').html(json.msg).css('color','green');
			  		var tabCount = json.activeTab;
			  		if(tabCount > 2){
			  			for(var c = 0;c <=tabCount;c++){
			  				$("#campaign-tabs").tabs("enable",c);
			  			}
			  		}else{
			  			$("#campaign-tabs").tabs("option","disabled",[3]);
			  		}
			  		$("#campaign-tabs").tabs({
			  			active:2
			  		});
			  	}else{
			  		alert(json.msg);
			  		$('.cmp-content-notification').html(json.msg).css('color','red');
			  	}
				},error:function(xhr,textStatus,error){
					alert(error);
				}
			});
		}else{
			alert("Please create your campaign content");
		}
	});
	
	/*******************Recipient Search Save*********************/
	$('#recipient-search-save').click(function(){
		var additionalContacts = $('.additional-emails').val();
		
		$('#recipient-search-save').val("Wait...").prop('disabled',true);
		$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/campaign-recipient-search-save.jsp",
			type:"POST",dataType:"html",data:({ajax:"ajax",additionalContacts:additionalContacts}),success:function(jsonResponse){
		  	var json = $.parseJSON(jsonResponse);
		  	$('#recipient-search-save').val("Continue").removeAttr('disabled');
		  	$('.session-key').val(json.sessionKey);
		  	if(json.status == "success"){
		  		var tabCount = json.activeTab;
		  		for(var c = 0;c <=tabCount;c++){
	  				$("#campaign-tabs").tabs("enable",c);
	  			}
		  		$("#campaign-tabs").tabs({
		  			active:3
		  		});
		  	}else{
		  		alert(json.msg);
		  	}
			},error:function(xhr,error,textStatus){
				alert(xhr.status);
			}
		});
	});
	
	/*******************Add Campaign Condition Panel*********************************/
	
	$('#cmp-get-option').click(function(){
		var checkedCount = 0;
		var cmpID = $('.choose-campaign').val();
		var cmpName = $(".choose-campaign option[value='"+cmpID+"']").text();
		$('.individual-search-panel').each(function(){
			checkedCount ++;
		});
		
		if(cmpID > 0 && cmpName.length > 0){
			if($('.cmp-search-panel-'+cmpID).length > 0){
				alert("Search Panel for Campaign *"+cmpName+"* already loaded!");
			}else{
				var loadingDiv = "<div class='loading-img-container col-md-12 align-center'><img src='"+BASE_URL+"resources/images/campaign/loading.gif' alt='Please wait.Panel loading...'/></div>";
				$('#filtering-area').append(loadingDiv);
				$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/load-campaign-base-search-panel.jsp",
					type:"POST",dataType:"HTML",data:({searchPanelNo:cmpID,keyVale:"campaign filter",cmpName:cmpName}),success:function(htmlresponse){
					if(htmlresponse.trim().length > 0){
						$('#filtering-area').append(htmlresponse.trim());	
					}
					$('.loading-img-container').remove();
				},error:function(xhr,error,textStatus){
					alert(xhr.status);
				}
				});
				checkedCount ++;
			}
		}else{
			alert("Campaign Reference Missing!");
		}
		if(checkedCount > 0){
			$('#capaign-search-btn-area').removeClass('hide');
		}else{
			$('#capaign-search-btn-area').addClass('hide');
		}
	});
	
	/************Campaign Search Form Submit************/
	//search form submit//
	$('#campaign-search-form').submit(function(evt){
	  evt.preventDefault();
	  //console.log( $( this ).serialize() );
	  $('.campaign-search-notification').html("Please wait. We are searching.....");
	  $('.campaign-search-btn').val("Please wait...").prop('disabled',true);
	  $.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/load-campaign-search-data.jsp",
		type:"POST",dataType:"html",data:$(this).serialize(),success:function(jsonResponse){
		  	var json = $.parseJSON(jsonResponse);
		  	$('.session-key').val(json.sessionKey);
		  	$('.campaign-search-notification').html("Search Result :");
		  	$('.campaign-search-btn').val("Search Contact").removeAttr('disabled');
		  	if(json.status == "success"){
		  		$('.campaign-search-record').html("");
		  		for(k in json){
		  			if(k != "msg" && k != "sessionKey" && k != "status"){
		  				$('.campaign-search-record').append("<b>"+json[k]+"</b> records found from "+k+" Search <br/>");
		  			}
		  		}
		  	}else{
		  		alert(json.msg);
		  		$('.campaign-search-record').html(json.msg);
		  	}
			},error:function(xhr,textStatus,error){
				alert(error);
			}
		});
	});
	
	/****************Sending time radio**************/
	$('.sending-time').click(function(){
		var val = $(this).val();
		if(val == "immediate"){
			$("input[name='sendingTimeSet']").val("").css('display','none');
		}else{
			$("input[name='sendingTimeSet']").val("").css('display','block');
		}
	})
	
	/*******************Create Campaign***************/
	
	$('.create-campaign').click(function(){
		var formSucess = true;
		var datetime = "";
		var sendingOption = $("input[type='radio'][name='sendTime']:checked").val();
		if(sendingOption == "immediate" || sendingOption == "schedule"){
			if(sendingOption == "schedule"){
				datetime = $("input[name='sendingTimeSet']").val();
				if(datetime == "" || datetime.length == 0){
					alert("Please leave the campaing sending time!");
					formSucess = false;
				}
			}
			if(formSucess){
				var con = confirm("Are you all done?System will take few minutes to create this campaign. To avoid data missing dont close/reload the browser!");
				if(con){
					
					var key = $('#session-key-cmp-create').val();
					$('.create-campaign').val("Processing...").prop('disabled',true);
					$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/create-campaign.jsp",
						type:"POST",dataType:"HTML",data:({key:key,sendtime:sendingOption,datetime:datetime}),success:function(jsonResponse){
						var json = $.parseJSON(jsonResponse);
						$('.create-campaign').val("Proceed").removeAttr('disabled');
						if(json.status == "success"){
							alert(json.msg);
							window.location.reload(true);
						}else{
							alert(json.msg);
						}
					},error:function(xhr,error,textStatus){
						alert(xhr.status);
					}
					});
				}
			}
		}else{
			alert("Please select campaing sending option");
		}
	});
	
	
	$('.datetimepicker').datetimepicker({
		step:15,
		format:'d/m/Y H:i',
	});
	
});


/********Remove campaign search panel************/


function removeCmpPanel(panelNo){
	if(panelNo > 0){
		var con = confirm("Are you sure");
		if(con){
			$('.cmp-search-panel-'+panelNo).remove();
		}
	}
};

	/*******Remove Included Links***********/
	
	//will do later
	/***********************************/
	
	

/*
 * will do later
 */
function checkValidation(){
	var isValid = false;
	
	
	return true;
}

function isValidEmail(email){
	var splitted = email.match("^(.+)@(.+)$");
	if (splitted == null)
		return false;
	if (splitted[1] != null) {
		var regexp_user = /^\"?[\w-_\.]*\"?$/;
		if (splitted[1].match(regexp_user) == null)
			return false;
	}
	if (splitted[2] != null) {
		var regexp_domain = /^[\w-\.]*\.[A-Za-z]{2,4}$/;
		if (splitted[2].match(regexp_domain) == null) {
			var regexp_ip = /^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
			if (splitted[2].match(regexp_ip) == null)
				return false;
		}
		return true;
	}
	return false;
}


function removeLink(obj){
	var con = confirm("Are you sure to remove this link?");
	//alert($(obj).attr('accesskey'));
	if(con){
		alert("Sorry! Remove option still not completed.");
	}
}
