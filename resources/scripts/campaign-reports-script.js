$(document).ready(function(){
	if(BASE_URL.length > 0 == false){
		alert("Base URL is undefined in javascript. Please assign a javascript variable name 'BASE_URL' and set the base url ");
	}	
	/*******GET Campaign Included Links***********/
	
	$('.all-campaigns').change(function (){
		var cmpID = $(this).val();
		var selectOption = "<option value=''>Select</option>";
		if(parseInt(cmpID) > 0){
			 $.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/campaign-included-links.jsp",
				 type:"POST",dataType:"html",data:({cmpID:cmpID}),success:function(jsonResponse){
			  	var json = $.parseJSON(jsonResponse);
			  	if(json.status == "success"){
			  		for(k in json){
			  			if(k != "msg" && k != "status"){
			  				selectOption +="<option value='"+k+"'>"+json[k]+"</option>";
			  			}
			  		}
			  	}
			  	$('.cmp-included-links').html("").html(selectOption);
				},error:function(xhr,textStatus,error){
					alert(textStatus);
				}
			});
		}
	});
	
	/*Delete Campaign*/
	$('.delete-cmp').click(function(evt){
		evt.preventDefault();
		var cmpID = $(this).attr('accesskey');
		if(parseInt(cmpID) > 0){
			var con = confirm("Are you sure?");
			if(con){
				$.ajax({url:BASE_URL+"resources/ajax-files/campaign-ajaxfiles/campaign-delete.jsp",
					 type:"POST",dataType:"html",data:({cmpID:cmpID}),success:function(jsonResponse){
				  	var json = $.parseJSON(jsonResponse);
				  	alert(json.msg);
				  	if(json.status == "success"){
				  		$('.row-'+cmpID).slideUp();
				  	}
					},error:function(xhr,textStatus,error){
						alert(textStatus);
					}
				});
			}else{
				return false;
			}
		}
	});
	
	$('.datetimepicker').datetimepicker({
		step:15,
		format:'d/m/Y H:i',
	});
	
});



