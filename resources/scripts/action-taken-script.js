$(document).ready(function(){
	$('#leadType').change(function(){
		var leadType = $(this).val() * 1;
		$('.lead-quality').val("");
		$('.lead-source').val("");
		$('.action-taken').val("");
		$('.follow-up-action').val("");
		$('.at-desc').html("");
		switch(leadType) 
		{
		case 1:
			$('.lead-quality').val("0");
			$('.lead-source').val("");
			$('.action-taken').val("");
			$('.follow-up-action').val("");
			$('.at-desc').html("");
			break;
		case 2:
			break;
		case 3:
			$('.lead-quality').val("4");
			break;
		case 4:
			$('.lead-quality').val("2");
			$('.lead-source').val("Other");
			$('.action-taken').val("27");
			$('.follow-up-action').val("47");
			$('.at-desc').html("Lead Type Selected Not Relevant");
			break;
		case 5:
			$('.lead-quality').val("4");
			$('.lead-source').val("Other");
			$('.action-taken').val("12");
			$('.follow-up-action').val("44");
			$('.at-desc').html("Lead Type Selected Not Yet Determined");
			 break;
		case 6:
			$('.lead-quality').val("2");
			$('.lead-source').val("Other");
			$('.action-taken').val("11");
			$('.follow-up-action').val("47");
			$('.at-desc').html("Lead Type Selected End User");
			 break;
		 }
	});
});


