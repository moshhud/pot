$(document).ready(function(){
	$('.emailField').blur(function(){
		if(this.value.length > 0 )
		{
			if(validateEmail(this.value) ==  false){
				alert('Invalid Email ID! Please check...');
				this.value = '';
				if(this.name.indexOf('alt') < 0){this.focus();}
				
			} else {
				$.post('../ajax-files/check-invalid-email.jsp', {email: this.value}, function(data){
					data = $.trim(data);
					if(data == 'true')
					{
						alert('System has detected this email to be invalid');
						$('.emailField').val('');
					}
				});
			}
		}
	});
	$('.nameField').keydown(function(){
		return isChar(event);
	});
	
	$('.ipField').keydown(function(){
		return isIPNumber(event);
	});
	
	$('.ipField').blur(function(){
		if(this.value.length > 0)
		{
			if(isValidIP(this.value) == false)
			{
				alert('This IP is invalid! Please check again.');
				this.value = "";
			}
		}
	});
	
	$('.numberField').keydown(function(){
		return isNumber(event);
	});
	
	$('.decimalField').keydown(function(){
		return isDecimalNumber(event, this.id);
	});
	function isNumber(e)
	{
		// allow ONLY backspace, delete, arrows, numbers and keypad numbers
		var key = e.charCode || e.keyCode || e.which || 0;
		return (key == 8 || key == 46 
				|| (key >= 37 && key <= 40) 
				|| (key >= 48 && key <= 57) 
				|| (key >= 96 && key <= 105));
	}
	
	function isDecimalNumber(e, id)
	{
		// allow ONLY backspace, tab, delete, arrows, numbers, keypad numbers and only one decimal point
		var key = e.charCode || e.keyCode || e.which || 0;
		if(key == 8 ||	key == 9 ||
	       key == 46 || (key >= 37 && key <= 40) ||
	       (key >= 48 && key <= 57) ||
	       (key >= 96 && key <= 105) ||
	       key  == 110 || key == 190)
		{
			if(key  == 110 || key == 190)
			{
				var input = document.getElementById(id).value + '.';
				if((input.indexOf("\.") == input.lastIndexOf("\.")) == false)
			    {
					alert('Sorry Only one decimal point is allowed');
					document.getElementById(id).value = input.substr(0, input.length - 1);
			    }
			}
			return true;
		}
		return false;
	}
	
	function isChar(e)
	{
		// allow ONLY backspace, delete, arrows and alphabets
		var key = e.charCode || e.keyCode || e.which || 0;
		return !((key >= 48 && key <= 57) 
				|| (key >= 96 && key <= 105));
	}

	function validateEmail(text)
	{
		var email = text;
	    var splitted = email.match("^(.+)@(.+)$");
	    if(splitted == null) return false;
	    if(splitted[1] != null )
	    {
	      var regexp_user=/^\"?[\w-_\.]*\"?$/;
	      if(splitted[1].match(regexp_user) == null) return false;
	    }
	    if(splitted[2] != null)
	    {
	      var regexp_domain=/^[\w-\.]*\.[A-Za-z]{2,4}$/;
	      if(splitted[2].match(regexp_domain) == null) 
	      {
		    var regexp_ip =/^\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\]$/;
		    if(splitted[2].match(regexp_ip) == null) return false;
	      }// if
	      return true;
	    }
		return false;
	}

	function isValidIP (IPvalue) {
	IPvalue = $.trim(IPvalue);
	var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
	var ipArray = IPvalue.match(ipPattern);

	if (ipArray == null){
	return false;
	}
	else{
		for(i = 1; i < 5; i++){
			segment = parseInt(ipArray[i],10);
			if(segment > 255){
			return false;
			}
		  }
	}
	return true;
	}

	function isIPNumber(e)
	{
		// allow ONLY backspace, tab, delete, arrows, numbers, keypad numbers and decimal points
		var key = e.charCode || e.keyCode || e.which || 0;
		return (key == 8 ||	key == 9 ||
	       key == 46 || (key >= 37 && key <= 40) ||
	       (key >= 48 && key <= 57) ||
	       (key >= 96 && key <= 105)||
	       key  == 110 || key == 190);
	}// end of isIPNumber
	
	function validateDate(date)
	{
		var validformat= '/^\d{2}\/\d{2}\/\d{4}$/';
		return date.match(validformat);
	}
});