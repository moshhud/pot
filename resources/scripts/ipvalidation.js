
function isValidIP (IPvalue) {
IPvalue = $.trim(IPvalue);
var ipPattern = /^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/;
var ipArray = IPvalue.match(ipPattern);

if (ipArray == null){
return false;
}
else{
	for(i = 1; i < 5; i++){
		segment = parseInt(ipArray[i],10);
		if(segment > 255){
		return false;
		}
	  }
}
return true;
}

function isIPNumber(e)
{
	// allow ONLY backspace, tab, delete, arrows, numbers, keypad numbers and decimal points
	var key = e.charCode || e.keyCode || 0;
	return (key == 8 ||	key == 9 ||
       key == 46 || (key >= 37 && key <= 40) ||
       (key >= 48 && key <= 57) ||
       (key >= 96 && key <= 105)||
       key  == 110 || key == 190);
}// end of isIPNumber