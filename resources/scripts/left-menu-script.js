ddaccordion.init( {
	headerclass : "submenuheader", //Shared CSS class name of headers group
	contentclass : "submenu", //Shared CSS class name of contents group
	revealtype : "click", //Reveal content when user clicks or onmouseover the header? Valid value: "click", "clickgo", or "mouseover"
	mouseoverdelay : 200, //if revealtype="mouseover", set delay in milliseconds before header expands onMouseover
	collapseprev : true, //Collapse previous content (so only one open at any time)? true/false 
	defaultexpanded : [], //index of content(s) open by default [index1, index2, etc] [] denotes no content
	onemustopen : false, //Specify whether at least one header should be open always (so never all headers closed)
	animatedefault : false, //Should contents open by default be animated into view?
	persiststate : true, //persist state of opened contents within browser session?
	toggleclass : [ "", "" ], //Two CSS classes to be applied to the header when it's collapsed and expanded, respectively ["class1", "class2"]
	togglehtml : [ "suffix",
			"",
			"" ], //Additional HTML added to the header when it's collapsed and expanded, respectively  ["position", "html1", "html2"] (see docs)
	animatespeed : "fast", //speed of animation: integer in milliseconds (ie: 200), or keywords "fast", "normal", or "slow"
	oninit : function(headers, expandedindices) { //custom code to run when headers have initalized
		// do nothing
	},
	onopenclose : function(header, index, state, isuseractivated) { //custom code to run whenever a header is opened or closed
		// do nothing
	}
});
//numeric check//
$('.numeric').keydown(function(event) {
	// Allow: backspace, delete, tab, escape, and enter//
    if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || 
         // Allow: Ctrl+A//
        (event.keyCode == 65 && event.ctrlKey === true) || 
         // Allow: home, end, left, right//
        (event.keyCode >= 35 && event.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    else {
        // Ensure that it is a number and stop the keypress//
        if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
            event.preventDefault(); 
            //alert("Numeric Only!");
            //$('.numeric').val("");
        }   
    }
});
function confirmFormAction(){
	var result=false;
		var con=confirm("Are you sure?");
		if(con){
			result=true;
		}
		return result;
}

$(document).ready(function(){
	/***************Show hide more option*******/
	$('.more-info-check').click(function(){
		var className=$(this).attr('accesskey');
		if($(this).is(':checked')){
			$('.'+className).each(function(){
				$(this).removeClass('hidden').addClass('show');
			});
		}else{
			$('.'+className).each(function(){
				$('.'+className+' input').val("");
				$(this).removeClass('show').addClass('hidden');
			});
		}
	});
	$('#checkAll').click(function(){
		if($(this).is(':checked')){
			$('.idCheckBox').each(function(){
				$(this).attr('checked',true);
				$(this).prop( "checked", true );
			});
		}else{
			$('.idCheckBox').each(function(){
				$(this).attr('checked',false);
			});
		}
	});
	$('.idCheckBox').click(function(){
		var totalBox=0;
		var checked=0;
		$('.idCheckBox').each(function(){
			totalBox++;
			if($(this).is(':checked')){
				checked++;
			}
		});
		if(totalBox==checked){
			$('#checkAll').prop( "checked", true );
			$('#checkAll').attr('checked',true);
		}else{
			$('#checkAll').attr('checked',false);
		}
	});
	/*end */
});
$(window).load(function(){
	$('.more-info-check,.advance-info-check').attr("checked",false);
});

