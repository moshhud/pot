function getProductList(){
		var plCurrentStatus="active";
		var plIsDeleted="0";
		try{
		    
			$.post( BASE_URL +"resources/ajax-files/get-product-list.jsp",
	    			{"plCurrentStatus":plCurrentStatus,
				     "plIsDeleted":plIsDeleted
	    			},
	    			function(options)
	    	        {
		    			options = $.trim(options);
						options = $.parseJSON(options);    					
						if(options.status == 'success') {					
							$("select#plProductName").html(options.msg);
						}
						else {
							alert(options.msg);
						}
	    	        });
		} catch(err){
			alert(err);
		}
}



$(document).ready(function(){
	
	$('input[type=radio][name=formType]').change(function() {
	    if (this.value == 'fullFormButton') {
	    	showFullForm();
	    }
	    else if (this.value == 'shortFormButton') {
	    	hideFullForm();
	    }
	});
	
	function hideFullForm(){		
		$('div#fullForm').css('display', 'none');
		$('.required').prop('required',false);
	}
	
	function showFullForm(){		
		$('div#fullForm').css('display', 'block');
		$('.required').prop('required',true);
	}
	
	$('#plProductName').change(function(){		
			getPrice();			
	});
	
	$('#plPriceUSD').change(setPrice);
	$('#pdQuantity').change(setPrice);
	$('#shippingCharge').change(setPrice);
	$('#tax').change(setPrice);
	
	function setPrice(){
		var productID = $('#plProductName option:selected').val();
		
		if(productID>0){
			var price = $('#plPriceUSD').val();		
			var quantity = $('#pdQuantity').val();
			var ship = $('#shippingCharge').val();
			var tax = $('#tax').val();
			var total=0;
			var gtotal=0;
			total = quantity*price+ship*1.0;
			$('#totalPriceUSD').val(total.toFixed(4));
			gtotal = total*(1+tax*1.0/100);
			$('#totalPriceUSD2').val(gtotal.toFixed(4));
		}
		else{
			$('#plPriceUSD').val("0.0");
			$('#pdQuantity').val("1");
			$('#shippingCharge').val("0.0");
			$('#tax').val("0.0");
			$('#totalPriceUSD').val("0.0");
			$('#totalPriceUSD2').val("0.0");
		}
		
	}
	
	function getPrice(){
		var productID = $('#plProductName option:selected').val();
		
		if(productID>0){
			try{			    
				$.post( BASE_URL +"resources/ajax-files/get-product-price.jsp",
		    			{"productID":productID
		    			},
		    			function(options)
		    	        {
			    			options = $.trim(options);
							options = $.parseJSON(options);    					
							if(options.status == 'success') {
								$('#plPriceUSD').val(options.msg.toFixed(4));
								setPrice();
							}
							else {
								
							}
		    	        });
				
				
			} catch(err){
				alert(err);
			}
			
			
		}	
		else{
			setPrice();
		}
	}
	
	
	
	$('.addProducts').click(function(event) {
		var form = document.forms[0];
		
		var pname = $('#plProductName  option:selected').text();
		var id = $('#plProductName  option:selected').val();
	    var price = $('#plPriceUSD').val();
	    var quantity = $('#pdQuantity').val();
	    var totalprice = $('#totalPriceUSD').val();
	    var ship = $('#shippingCharge').val();
	    var tax = $('#tax').val();
	    var gtotalprice = $('#totalPriceUSD2').val();
	    var data = "";
	    data = id+":"+price+":"+quantity+":"+ship+":"+tax;
	    if(id==0){
	    	alert("Please select a Product");
	    	return;
	    }
	    for ( var i = 0; i < form.productDetailsInfo.length; i++) {
	    	index = form.productDetailsInfo[i].text.indexOf(":");
	    	str = form.productDetailsInfo[i].text.substring(0, index);
			if (str == id) {
				alert("Product " + pname + " already added");
				return;
			}
		}
	    
	    var table = document.getElementById("ProductList");
		var row = table.insertRow(table.rows.length);
		var cell1 = row.insertCell(0);
		var cell2 = row.insertCell(1);	
	    var cell3 = row.insertCell(2);	
	    var cell4 = row.insertCell(3);
	    var cell5 = row.insertCell(4);
	    var cell6 = row.insertCell(5);
	    var cell7 = row.insertCell(6);
	    
	    
		cell1.innerHTML = pname;
		cell2.innerHTML = price;
		cell3.innerHTML = quantity;
		cell4.innerHTML = ship;
		cell5.innerHTML = totalprice;
		cell6.innerHTML = tax;
		cell7.innerHTML = gtotalprice;
		
		if (data.length != 0) {
			form.productDetailsInfo[form.productDetailsInfo.options.length] = new Option(
					data, data, false, false);
		}
		
		
		
    });
	
	$('.removeProducts').click(function(event) {
	    try {
	    	var form = document.forms[0];
			var table = document.getElementById("ProductList");
			var rowCount = table.rows.length;
			if (rowCount <= 1)
				return;	
			form.productDetailsInfo.remove(rowCount - 2);
			table.deleteRow(rowCount - 1);

		} catch (e) {
	     alert(e);
		}
	});
	
	
});


