$(document).ready(function(){
	$('.showHideAdvanceFields').click(function(e){
		e.preventDefault();
		var className = '.' + $(this).attr('title');
		var action = $(this).attr('id');
		$(className).css('display', action);
		if(action == 'table-row' || action == 'block') {
			$(this).attr('id', 'none');
			$(this).html('Hide Advance Options');
		} else {
			$(this).attr('id', $(this).attr('accesskey'));
			$(this).html('Show Advance Options');
		}
	});
	$(".datepicker").datepicker({
		changeMonth: true,
	    changeYear: true,
		dateFormat: 'dd/mm/yy',
		showAnim: 'blind'
	});
	$('#checkAll').click(function(){
		if($(this).is(':checked')){
			$('.idCheckBox').each(function(){
				$(this).attr('checked',true);
			});
		}else{
			$('.idCheckBox').each(function(){
				$(this).attr('checked',false);
			});
		}
	});
	$('.idCheckBox').click(function(){
		var totalBox=0;
		var checked=0;
		$('.idCheckBox').each(function(){
			totalBox++;
			if($(this).is(':checked')){
				checked++;
			}
		});
		if(totalBox!=checked){
			$('#checkAll').attr('checked',false);
		}else{
			$('#checkAll').attr('checked',true);
		}
	});
	$('.atn-btn-srvripblck').click(function(e){
		e.preventDefault();
		var actionType = $(this).val();
		var con = confirm("Are you sure you want to take this action?");
		if(con){
			var checkedIDs = "";
			$('.select-box').each(function(){
				if($(this).is(':checked')){
					checkedIDs +=$(this).val()+",";
				}
			});
			if(checkedIDs != ""){
				$(this).html("").html("Plz Wait..");
				checkedIDs = checkedIDs.substring(0,checkedIDs.length-1);
				$.ajax({url: BASE_URL+"resources/ajax-files/request-for-server-status-change.jsp",data:({IDs: checkedIDs, type: actionType}),type:"POST",dataType:"HTML",success:function(value)
					{
						alert(value.trim());
						location.reload();
					},error:function(xhr,textStatus,error){alert(xhr.status)}
				});
			}else{
				alert("Please mark the checkbox first!");
			}
		}else{
			return;
		}
	});
	
	$(".btnExcelExport").click(function (e) {
        try{
            e.preventDefault();
            table = $(this).attr('accesskey');
            content = $('.excelData').html();
            $.ajax({url: BASE_URL + 'resources/ajax-files/excel-report-download.jsp', type: 'POST', data: ({table: table, content: content})});
        $('.excelData').tableExport({type:'excel',escape:'false'});
        }catch(err) {alert(err)}
//        window.open('data:application/vnd.ms-excel,' + $('.excelData').html());
    });
});