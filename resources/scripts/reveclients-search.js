$(document).ready(function() {
	$("#search").click( function(event) {
		event.preventDefault();
		doSearch();
	});
});
//ready function//
function doSearch() {
	try {
		var search_type = $('[name~=searchType]:checked').val();
		var search_value = $('[name~=searchValue]').val();
		if (search_type.length > 0 && search_value.length > 0) {
			document.forms[0].reset();
			$('input[name~=searchType][value=' + search_type + ']').prop("checked",true);
			$('[name~=searchValue]').val(search_value);
			$.blockUI();
			$.ajax({url: BASE_URL +"resources/ajax-files/reveclients-search.jsp",data:({type: search_type, value: search_value}), dataType:"JSON",success:function(data)
				{
					if(data.status == 'success') {
						$('#rcClientID').html(data.rcClientID);
						$('#invcRcClientID').val(data.rcClientID);
						$('#hsRcClientID').val(data.rcClientID);
						$('#rcClientName').html(data.rcClientName);
						$('#rcEmail').html(data.rcEmail);
						$('#rcCellPhone').html(data.rcCellPhone);
						$('#rcCompanyName').html(data.rcCompanyName);
						$('#rcAccountManagerName').html(data.rcAccountManagerName.toUpperCase());
						$('#rcAccountManagerID').val(data.rcAccountManagerID);
						$('#hsAccountManagerID').val(data.rcAccountManagerID);
						$('.notification').html("");
						try{
							$('#invoice-details').removeClass('hidden');
						} catch (err){}
					}else{
						$('#rcClientID').html('');
						$('#invcRcClientID').data('');
						$('#hsRcClientID').val('');
						$('#rcClientName').html('');
						$('#rcEmail').html('');
						$('#rcCellPhone').html('');
						$('#rcCompanyName').html('');
						$('#rcAccountManagerName').html('');
						$('#rcAccountManagerID').val('');
						$('#hsAccountManagerID').val('');
						alert("No data found using your input!");
						try{
							$('#invoice-details').addClass('hidden');
						} catch (err){}
					}
					$.unblockUI();
				},error:function(xhr,textStatus,error){
					alert(error);
					$('.notification').html("");
					$.unblockUI();
				}
			});
		} else {
			alert('Enter a search value');
		}
	} catch (err) {
		alert(err);
	}
}
//end do search//

