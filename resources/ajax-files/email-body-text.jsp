<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>

<%
	try {
		RimsUsersDTO empDTO=null;
		ReturnObject ro=null;
		String condition=null;
		String signature=null;
		String skype = "";
		String mobile = "";
		RimsUsersDTO rimsUsersDTO=(RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		String emailBody=request.getParameter("emailBody");
		if(rimsUsersDTO != null){
			empDTO = rimsUsersDTO;
			if(emailBody!=null && emailBody.length()>0){
				if(empDTO.getOfficePhone()!=null && empDTO.getOfficePhone().length()>0){
					mobile = " Mobile : "+empDTO.getOfficePhone();
				}
				
				//signature="<b>"+empDTO.getUsrName()+"</b><br/>"+empDTO.getDesignation()+", "+empDTO.getDepartment()+" <br/>REVE Systems (S) Pte Ltd<br/>"
				signature="<b>"+empDTO.getUsrName()+"</b><br/>REVE Systems (S) Pte Ltd<br/>"
				+mobile+skype+"<a href='www.revesoft.com'>www.revesoft.com</a><br/>.......................<br/>"+
				"Other Offices:  Bangladesh | India | USA | UK <br/><b>.......................</b>";
				if(emailBody.equalsIgnoreCase("email-default-body")){
	%>
						
						Greetings from <b>REVE Systems</b>. Thank you for your interest in us.<br/><br/>	
						<b>REVE Systems</b> is the leading VoIP software solution provider. The company is RED Herring's 
						2012 Top 100 Global Winner and has also received many awards for its soft switch iTel Switch Plus, 
						which includes 2012 NGN Leadership Award and 2011 Unified Communication Excellence Award. <b>REVE Systems</b> 
						currently serves more than 1900 service providers across 70+ countries.<br/><br/>
						For further discussion regarding your query, please contact me directly in this email address.<br/><br/>
						Thanks and regards,<br/>
						<%=signature %>
	<%
				}else if(emailBody.equalsIgnoreCase("email-dialer-body")){
	%>
					
					Trust you are doing great!<br/><br/>
					You have tested our iTel Mobile Dialer Express sometimes back.  After your demo, some more exciting features have been added to the iTel Mobile Dialer Express. 
					The list includes Instant Messaging (IM) platform along with other features such as Auto Provisioning, Peer to Peer Texting and Voice Chatting, Push Notifications, 
					communicating with social networks- such as Facebook & Twitter, Localization and Customizable Interface - to name a few!!!<br/><br/>
					If you want to test the mobile dialer along with these new features, I will be happy to organize that for you. Alternatively, we can also have a discussion regarding this over the phone. 
					Let me know your preferences.<br/><br/>
					Have a great day!<br/><br/>
					Thanks and regards,<br/>
					<%=signature %>
	<%
				} else if(emailBody.equalsIgnoreCase("email-switch-body")) {
	%>
					
					Trust you are doing great.<br/><br/>
					You have tested our iTel Switch Plus sometimes back. After your demo, some more exciting features have been added to the iTel Switch Plus. 
					The list includes Switch and Reseller Partitioning, Advance Routing Management, Run behind NAT or on private IP, International Mobile Top Up facility, 
					Advanced client and Rate Plan Management, PIN to PIN calling and Balance Transfer Facility and Multiple IVR Languages Support - to name a few!!!<br/><br/>
					If you want to test the iTel Switch Plus along with these new features, I will be happy to organize that for you. Alternatively, 
					we can also have a discussion regarding this over the phone. Let me know your preferences.<br/><br/>
					Have a great day!<br/><br/>
					Thanks and regards,<br/>
					<%=signature %>
	<%
				} else if(emailBody.equalsIgnoreCase("email-fresh-body")){
					out.println(signature);
				}
			}
		}
	} catch(RuntimeException e) {
		Logger.getLogger("email-body-text").fatal("RuntimeException", e);
	}
%>
