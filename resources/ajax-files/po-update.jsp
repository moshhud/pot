<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDAO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListService"%>
<%@page import="dev.mashfiq.util.ActionMessages"%>
<%@page import="dev.mashfiq.mail.Mail"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.StringHelper"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>

<%  
	Logger logger = Logger.getLogger("po-update");
	RimsUsersDTO rimsUserDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
	String status = "failed";
	if(rimsUserDTO != null){
	if(rimsUserDTO.getPermissionLevelByModuleId(Permissions.PURCHASE_ORDER_MODIFICATION) >= Permissions.LEVEL_FOUR){
		String ids = request.getParameter("ids");
		String type = request.getParameter("type");
		String mailContent = null;
		ArrayList<String> list = null;
		String[] arr = null;
		HashMap<String,ArrayList<String>> idMap = null;
		String failedID = "These Operator Code failed to be Updated: ";
				String condition = null;
		ReturnObject ro = null;
		String additionalUpdateFields = null,partialContent = "";
		CommonDAO commonDAO = null;
		StringHelper sh = new StringHelper();
		int totalRequested = 0, totalSucced = 0;
		LinkedHashMap<String, OrderListDTO> data = null;
		
  if (ids != null && ids.length() > 0 && type != null && type.length()> 0) {
	arr = ids.split(",");//sh.getArrayFromString(ids, ",");
	if (arr != null && arr.length > 0) {
		list = new ArrayList<String>(Arrays.asList(arr));
		idMap = new HashMap<String,ArrayList<String>>(1);
		idMap.put(OrderListDAO.DEFAULT_KEY_COLUMN, list);
		totalRequested = list.size();
		String stat="blocked";
		logger.debug("Key Column: "+OrderListDAO.DEFAULT_KEY_COLUMN);
		ro = new OrderListService(rimsUserDTO).getMap(idMap,null,OrderListDAO.DEFAULT_KEY_COLUMN);
		if(ro != null){
			if(ro.getIsSuccessful()){
				try{
					data = (LinkedHashMap<String, OrderListDTO>) ro.getData();
					if(data != null && data.size() > 0){
						if ("delete".equalsIgnoreCase(type)) {												
							ro = new OrderListService(rimsUserDTO).deleteInvoice(idMap,1);
							stat="deleted";
						} 						
						for(OrderListDTO d : data.values()){
							try{
								if(d != null){	
									if(ro != null && ro.getIsSuccessful()){
										totalSucced ++;												
										partialContent += "<tr><td>"+d.getPurchaseOrderNumber()+"</td>";
										partialContent += "<td>"+d.getCustomerName()+"</td>";
										partialContent += "<td>"+d.getCustomerEmail()+"</td>";
										partialContent += "</tr>";
										status = "successful";
									}else{
										failedID +=" ID: "+d.getId()+",Reason: " + ro == null ? "N/A" : ro.getActionMessage() == null ? "N/A" : ro.getActionMessage().getMsg();
									}
									Thread.sleep(100);
								}
							}catch(InterruptedException e){
								logger.fatal("InterruptedException",e);
							}
						}
					}
				}catch(ClassCastException e){
					logger.debug("ClassCastException",e);
				}
				mailContent = "Dear Concern, <br/> Hope you are well. Bellow Invoice has been " +
						stat+"  By "+rimsUserDTO.getUsrName()+" from PO Tracker.<br/><br/><br/>";
				mailContent +="<table border='1'><thead><tr><th>PO Number </th><th>Client Name</th><th>Client Email</th></tr></thead><tbody>"+partialContent+"</tbody></table>";
				
				if(status.equals("successful")){ 
					new Mail("moshhud@revesoft.com","moshhud@revesoft.com","",
							"PO order deleted "+stat,mailContent,
							"moshhud@revesoft.com", null);
				}
			}
		}
		
	}
		}
	}else{
		status = ActionMessages.PERMISSION_DENIED.getMsg();
	}
		out.println(status);
	}
%>