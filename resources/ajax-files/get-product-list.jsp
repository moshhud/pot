<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map.Entry"%>

<%
Logger logger = Logger.getLogger( "get-product-list.jsp" );
logger.debug("get-product-list.jsp");
JSONObject json = new JSONObject();
String plCurrentStatus = request.getParameter("plCurrentStatus");
logger.debug(plCurrentStatus);
String options = "<option value='0'>Select</option>";
HashMap<Integer, String> map = null;
MdProductListRepository mdProductListRepository;
mdProductListRepository = MdProductListRepository.getInstance(false);

 map = mdProductListRepository.getProductIDName(plCurrentStatus,0);
if(map!=null && map.size() > 0){
	for (Entry<Integer, String> entry: map.entrySet()) {
		if(entry != null) {
			options += "<option value =\"" + entry.getKey() + "\">"+ entry.getValue() + "</option>";
		}
	}
} 
json.put("status", "success");
json.put("msg", options);

out.println(json);
/* logger.debug(json); */
%>