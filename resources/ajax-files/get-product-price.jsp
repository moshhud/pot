<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListDTO"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map.Entry"%>

<%
Logger logger = Logger.getLogger( "get-product-list.jsp" );
logger.debug("get-product-list.jsp");
JSONObject json = new JSONObject();
String productID = request.getParameter("productID");
logger.debug(productID);
String options = "<option value='0'>Select</option>";

MdProductListDTO dto = null;
dto = MdProductListRepository.getInstance(false).getDTOByPlProductID(Integer.parseInt(productID));
float price = 0;

if (dto != null) {
	price=dto.getPlPriceUSD();
}
json.put("status", "success");
json.put("msg", price);

out.println(json);
/* logger.debug(json); */
%>