<%@page import="java.net.UnknownHostException"%>
<%@page import="java.net.InetAddress"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="org.json.JSONObject"%>
<%
	JSONObject json = new JSONObject();
	json.put("status", "failure");
	json.put("msg", "Invalid Request");
	String ip = request.getParameter("ip");
	if(ip != null) {
		ip = ip.replace("https://", "");
		ip = ip.replace("http://", "");
	}
	if (new Validations().checkIp(ip) || ip
			.matches(Validations.HOST_VALIDATION_REGEX) ) {
		try{
			if (InetAddress.getByName(ip).isReachable(5000)) {
				json.put("status", "success");
				json.put("msg", "Successfully pinged");
			} else {
				json.put("msg", "Not reachable");
			}
		} catch(UnknownHostException e) {
			json.put("msg", "Unknown Host: " + ip);
		}
	} else {
		json.put("msg", "Invalid IP: " + ip);
	}
	out.print(json);
%>