<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.RimsUpdateLogHelper"%>
<%@page import="dev.mashfiq.util.Validations"%>
<%@page import="org.apache.log4j.Logger"%>
<%
	try {
		RimsUsersDTO rimsUsersDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
		if(rimsUsersDTO != null) {
			String fileName = (String) request.getParameter("fileName");
			String report = (String) session.getAttribute(fileName);
			byte[] bytes;
			Validations v = new Validations();
			if(v.checkInput(report) && v.checkInput(fileName)) {
				new RimsUpdateLogHelper(rimsUsersDTO.getUsrID(), fileName, 0, "downloaded<br/>" + report, "excelDownload> ");
				bytes = report.getBytes();
				fileName += ".xls"; 
				response.setContentType("application/vnd.ms-excel");
				response.setContentLength(bytes.length);
				response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName +"\"");
				response.getOutputStream().write(bytes);
				session.removeAttribute(fileName);
			}
		}
	} catch (RuntimeException e) {
		Logger.getLogger("exportToExcel").fatal("RuntimeException", e);
	} finally {
		response.getOutputStream().flush();
		response.getOutputStream().close();
	}
%>