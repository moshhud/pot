<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesDTO"%>
<%@page import="com.revesoft.rims.revesoft.rimsModules.RimsModulesDAO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDTO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListDAO"%>
<%@page import="com.revesoft.po.revesoft.orderlist.OrderListService"%>
<%@page import="dev.mashfiq.util.ActionMessages"%>
<%@page import="dev.mashfiq.mail.Mail"%>
<%@page import="com.revesoft.rims.dialerRegistration.mdProductList.MdProductListRepository"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="dev.mashfiq.util.StringHelper"%>
<%@page import="dev.mashfiq.common.CommonDAO"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.ArrayList"%>
<%@page import="dev.mashfiq.util.ApplicationConstant"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="dev.mashfiq.util.Permissions"%>

<%  
	Logger logger = Logger.getLogger("po-update");
	RimsUsersDTO rimsUserDTO = (RimsUsersDTO) session.getAttribute(ApplicationConstant.LOGIN_INFO);
	String status = "failed";
	if(rimsUserDTO != null){
	if(rimsUserDTO.getPermissionLevelByModuleId(Permissions.RIMS_MODULES) >= Permissions.LEVEL_FOUR){
		String ids = request.getParameter("ids");
		String type = request.getParameter("type");
		String mailContent = null;
		ArrayList<String> list = null;
		String[] arr = null;
		HashMap<String,ArrayList<String>> idMap = null;
		String failedID = "failed to delete: ";
				String condition = null;
		ReturnObject ro = null;
		String additionalUpdateFields = null,partialContent = "";
		CommonDAO commonDAO = null;
		StringHelper sh = new StringHelper();
		int totalRequested = 0, totalSucced = 0;
		LinkedHashMap<String, RimsModulesDTO> data = null;
		
  if (ids != null && ids.length() > 0 && type != null && type.length()> 0) {
	arr = ids.split(",");//sh.getArrayFromString(ids, ",");
	if (arr != null && arr.length > 0) {
		list = new ArrayList<String>(Arrays.asList(arr));
		idMap = new HashMap<String,ArrayList<String>>(1);
		idMap.put(RimsModulesDAO.DEFAULT_KEY_COLUMN, list);
		totalRequested = list.size();
		String stat="blocked";
		logger.debug("Key Column: "+RimsModulesDAO.DEFAULT_KEY_COLUMN);
		ro = new RimsModulesDAO().getMap(idMap,null,RimsModulesDAO.DEFAULT_KEY_COLUMN);
		if(ro != null){
			if(ro.getIsSuccessful()){
				try{
					data = (LinkedHashMap<String, RimsModulesDTO>) ro.getData();
					if(data != null && data.size() > 0){
						if ("delete".equalsIgnoreCase(type)) {												
							ro = new RimsModulesDAO().deleteModule(rimsUserDTO, idMap);
							stat="deleted";
						} 						
						for(RimsModulesDTO d : data.values()){
							try{
								if(d != null){	
									if(ro != null && ro.getIsSuccessful()){
										totalSucced ++;												
										partialContent += "<tr><td>"+d.getId()+"</td>";
										partialContent += "<td>"+d.getModuleName()+"</td>";
										partialContent += "<td>"+d.getModuleDescription()+"</td>";
										partialContent += "</tr>";
										status = "successful";
									}else{
										failedID +=" ID: "+d.getId()+",Reason: " + ro == null ? "N/A" : ro.getActionMessage() == null ? "N/A" : ro.getActionMessage().getMsg();
									}
									Thread.sleep(100);
								}
							}catch(InterruptedException e){
								logger.fatal("InterruptedException",e);
							}
						}
					}
				}catch(ClassCastException e){
					logger.debug("ClassCastException",e);
				}
				mailContent = "Dear Concern, <br/> Hope you are well. Bellow web Module has been " +
						stat+"  By "+rimsUserDTO.getUsrName()+" from PO Tracker.<br/><br/><br/>";
				mailContent +="<table border='1'><thead><tr><th>ID</th><th>Module Name</th><th>Module Description</th></tr></thead><tbody>"+partialContent+"</tbody></table>";
				
				if(status.equals("successful")){ 
					new Mail("moshhud@revesoft.com","moshhud@revesoft.com","",
							"PO module deleted "+stat,mailContent,
							"moshhud@revesoft.com", null);
				}
			}
		}
		
	}
		}
	}else{
		status = ActionMessages.PERMISSION_DENIED.getMsg();
	}
		out.println(status);
	}
%>