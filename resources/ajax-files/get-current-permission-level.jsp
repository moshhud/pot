<%@page import="com.revesoft.rims.revesoft.rimsPermissions.RimsPermissionsDAO"%>
<%@page import="dev.mashfiq.util.ReturnObject"%>
<%@page import="com.revesoft.rims.revesoft.rimsPermissions.RimsPermissionsDTO"%>
<%@page import="java.util.LinkedHashMap"%>
<%@page import="org.json.JSONObject"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersRepository"%>
<%@page import="com.revesoft.po.revesoft.users.RimsUsersDTO"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="org.apache.log4j.Logger"%>
<%
	JSONObject json = new JSONObject();
	json.put("status", "failure");
	json.put("msg", "Invalid Request");
	try {
		RimsUsersDTO usrDTO = RimsUsersRepository.getInstance(false).getDTOByUsrID(NumericHelper.parseInt(request.getParameter("usrID")));
		int moduleId = NumericHelper.parseInt(request.getParameter("moduleId"));
		LinkedHashMap<String, RimsPermissionsDTO> data = null;
		ReturnObject ro = null;
		String res = null;
		if (usrDTO != null && moduleId > 0) {
			res = "";
			ro = new RimsPermissionsDAO().getMap(null, " AND ((permission_type='user' AND table_id="
					+ usrDTO.getUsrID() + ") OR (permission_type='role' AND table_id=" + usrDTO.getUsrRoleId() + ")) AND module_id=" + moduleId, null);
			if (ro != null && ro.getIsSuccessful()) {
				data = (LinkedHashMap<String, RimsPermissionsDTO>) ro.getData();
				if(data != null && data.size() > 0) {
					for(RimsPermissionsDTO dto: data.values()) {
						res += dto.getPermissionType() + " : " + dto.getPermissionLevel() + "<br/>";
					}
					if(res.endsWith(",")) {
						res = res.substring(0, res.length() -1);
					}
				}
			} else {
				res = "N/A";
			}
			json.put("status", "success");
			json.put("msg", res.toUpperCase());
		}
	} catch(RuntimeException e) {
		Logger.getLogger("get-current-permission-level").fatal("RuntimeException", e);
	}
	out.println(json);
%>