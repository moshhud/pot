<%@page import="java.util.GregorianCalendar"%>
<%@page import="java.util.Calendar"%>
<%@page import="dev.mashfiq.util.NumericHelper"%>
<%@page import="dev.mashfiq.util.DateAndTimeHelper"%>
<%@page import="dev.mashfiq.util.URLHelper"%>
<%
	
	String today = new DateAndTimeHelper().getDateFromLong(System.currentTimeMillis());
	int day = NumericHelper.parseInt(today.split("/")[0]);
	URLHelper urlHelper = new URLHelper();
	String urlString = "https://rims.revesoft.com/apis/service-expiration-reminder.jsp";
	urlHelper.readWriteToURl(urlString, null, false);
	urlString = "https://rims.revesoft.com/apis/update-dialer-concurrent-registration.jsp";
	urlHelper.readWriteToURl(urlString, null, false);
	switch(day) {
	case 1:
	case 11:
	case 21:
		urlString = "https://rims.revesoft.com/apis/licensed-dialer-plus-rent-reminder.jsp";
		urlHelper.readWriteToURl(urlString, null, false);
		break;
	case 14:
		urlString = "https://rims.revesoft.com/apis/generate-softlayer-linux-service-monthly-invoice.jsp";
		urlHelper.readWriteToURl(urlString, null, false);
		break;
	case 20:
		urlString = "https://rims.revesoft.com/apis/generate-hosted-services-monthly-invoice.jsp";
		urlHelper.readWriteToURl(urlString, null, false);
		break;
	case 28:
		urlString = "https://rims.revesoft.com/apis/new-generate-hosted-dialer-plus-monthly-invoice.jsp";
		urlHelper.readWriteToURl(urlString, null, false);
		break;
	}
%>

<%!
	public String getDate(String date, int day, int week) {
		String arr[];
		Calendar calendar = Calendar.getInstance();
		if(date != null) {
			arr = date.split("/");
			if(arr != null && arr.length == 3) {
				calendar = Calendar.getInstance();
				calendar.set(NumericHelper.parseInt(arr[2]), NumericHelper.parseInt(arr[1]), NumericHelper.parseInt(arr[0]));
				calendar.set(GregorianCalendar.DAY_OF_WEEK, day);
		    	calendar.set(GregorianCalendar.DAY_OF_WEEK_IN_MONTH, week);
		    	date = new DateAndTimeHelper().getDateFromLong(calendar.getTimeInMillis());
			}  else {
				date = "N/A";
			}
		}  else {
			date = "N/A";
		}
		return date;
}
%>