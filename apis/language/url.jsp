<%@ page contentType="text/html; charset=UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>url encoder</title>
		<meta http-equiv="Cache-control" content="public">
		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		
		<link href="../../resources/font-awesome-4.1.0/css/font-awesome.min.css" type="text/css" rel="stylesheet" />
		<link href="../../resources/css/metisMenu.css" type="text/css" rel="stylesheet" />
		<link href="stylesheet/style.css" rel="stylesheet"> 
		<script src="../../resources/scripts/metisMenu.min.js" type="text/javascript"></script>
		<script src="../../resources/scripts/sideMenu.js" type="text/javascript"></script>
		
		<script type="text/javascript">
		$(document).ready(function() {
			
			$('.update').click(function(event) {
					try {
						event.preventDefault();
						var txt = $("#txt").val();
						var type = $(this).attr('href');
						
						if (checkInput()) {
							$.post('get-unicode.jsp',
								{									
									type : type,
									txt : txt									
								},
								function(data) {
									data = $.trim(data);
									data = $.parseJSON(data);
									if(data.status == 'success') {
										$('#rText').val(data.msg);
										/* window.location.href = "http://localhost:8080/rims/apis/language/get-decoded-value.jsp"; */
									}
									else{
										alert('No data found');
									}									
							});
						} 
					} catch (e) {
						alert(e);
					}
			});
			
			function checkInput(){
				var txt = $("#txt").val();				
				try{
					
					if(isEmpty(txt)){
						alert("Please enter text");
						return false;
					}				
					
				}
				catch(err){
					alert("Error: "+err.message);
				}		
				return true;
			}
			function isEmpty(s)
			{
			  for(var i=0;i<s.length;i++)
				  if(s.charAt(i)!=' ')break;

			  if(i==s.length)
				  return true;
			  else
				  return false;      
			}
						
			$('.copyText').click(function(event) {
				try {
					event.preventDefault();
					var copyText = document.getElementById("rText");
					copyText.select();
					document.execCommand("copy");
					/* alert("Copied the text: " + copyText.value); */
					
				} catch (e) {
					alert(e);
				}
			});
		
	});
		</script>
		
	</head>
	<body>
		 <%@ include file="language-header.jsp"%>
		<div class="container-fluid">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
					<%@include file="left-menu.jsp" %>
					</div>
					<div class="col-md-4">
					  <div id="login" class="container">
				         <div id="loginbox">			    
							  <div class="form-group">
								<label for="txt">Language</label>
								<input type="text" name="txt" class="form-control" id="txt" placeholder="Put Language text here">
							  </div>
							  
							  <div  id="btncontainer">	
								   <a href="update" class="update btn btn-success">Submit</a>
							  </div>							  
							  <div class="form-group">
								<label for="txt">Encoded value</label>
								<input type="text" name="rText" class="form-control" id="rText" >
							  </div>
							  <div  id="btncontainer">	
								   <a href="copyText" class="copyText btn btn-success">Copy Encoded Text</a>
							  </div>
							  
					        </div>
						</div>
					</div>
					<div class="col-md-4">
					 </div>
			</div>
		</div>
		 
	  </div>
	  <%-- <div><%@ include file="get-decoded-value.jsp" %></div> --%>
	  
	</body>
</html>