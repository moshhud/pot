<div class="navbar-default sidebar" role="leftnavigation">
	<div class="sidebar-nav navbar-collapse leftnavbar-collapse">
	     <ul class="nav" id="side-menu">
	     
	         <li class="list-group-item"><a class="active menuitem submenuheader" href="#">Master Creation<span class="fa arrow"></span></a>
	             <ul class="nav nav-second-level">
	                <li><a href="#">Add User</a></li>
	                <li><a href="#">Search User</a></li>
	             </ul>
	         </li>
	         
	         <li class="list-group-item"><a class="active menuitem submenuheader" href="#">Products<span class="fa arrow"></span></a>
	             <ul class="nav nav-second-level">
	                <li><a href="#">Add Product</a></li>
	                <li><a href="#">Search Product</a></li>
	             </ul>
	         </li>
	         
	         
	     </ul>
	</div>
</div>

