<%@page import="java.sql.DriverManager"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLDecoder"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="org.json.JSONObject"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Statement"%>

<%
   Logger logger = Logger.getLogger("get-unicode");
   JSONObject json = new JSONObject();
   String type = request.getParameter("type");
   String txt = request.getParameter("txt");
   logger.debug("Text: "+txt);
   
   String str = URLEncoder.encode(txt, "utf-8");
   logger.debug("Encoded Text: "+str);
   
   Connection connection = null;
   PreparedStatement pstmt = null;
   Statement stmt = null;
   String sql = null;
	
   try {
	   Class.forName("com.mysql.jdbc.Driver"); 
	   connection = DriverManager.getConnection(  
			   "jdbc:mysql://localhost:3306/DialerRegistration","root","root");
	   sql = "UPDATE vbRegistration SET mandatory_update_reason=? where  operator_code='24950'";
	   pstmt = connection.prepareStatement(sql);
	   pstmt.setString(1, str);
	   pstmt.executeUpdate();
	   
	   pstmt.close();
	   connection.close();
   }
   catch (Exception e) {
		logger.fatal(e.toString());
	}  
   
   //str = URLDecoder.decode(str, "utf-8");    
   json.put("status", "success");
   json.put("msg", str);
   out.println(json);
		
%>